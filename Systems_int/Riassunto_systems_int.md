# Systems integration

[[_TOC_]]

## Teoria

### Parte 1 - Intro

- Applicativi monolitici e a micro-servizi
    - Monolitico
        - Singola applicazione costituita da al massimo un paio di componenti dedicati appositamente per tale applicazione

        ![](./res/monol.png)

    - A micro-servizi (tanti piccoli pezzi di un puzzle)
        - Molteplici componenti separati e customizzabili
        - In esecuzione su uno stesso host oppure su più host distribuiti in rete
        - Componenti che interagiscono tra loro scambiandosi messaggi
        - Vantaggi
            - Riusabilità
            - Scalabilità
            - Deployment: può essere realizzato sia su una stessa macchina che su macchine differenti
            - Cloud
        - Problematiche
            - Costruire micro-servizi che espongano solo interfacce software e protocolli di comunicazione diffusi e condivisi (standard)
            - Assicurare che la rete di comunicazione permetta il passaggio di questi messaggi
            - In generale la progettazione delle comunicazioni è un elemento fondamentale in questa materia

        ![](./res/micserv.png)

- Micro-servizi

    - Esempio infrastruttura

        ![](./res/exscheme.png)

        - Importante
            - OPC client fa da relay fra il back end e la macchina PLC
            - Il back end e il browser utente fanno comunicazione bilaterale (il back end può mandare comunicazioni al browser senza che queste vengano richieste)
            - Ci sono vari back end perché questi vengono tirati su in base a quanti utenti richiedono il servizio (su diverse macchine)
        - Bus di Messaggi

            ![](./res/exschemetot.png)

            - Questi protocolli
                - Definiscono il formato base dei messaggi trasportati, che include un body (le applicazioni costruiscono un loro strato software che definisce il formato interno dei messaggi, spesso JSON)
                - Permettono di scegliere le modalità di consegna dei messaggi
                - Mantengono i messaggi in code
                - Mettono a disposizione API per diversi linguaggi
    - Infrastruttura basata principalmente sul cloud (ultascalabile)

        ![](./res/cloudscheme.png)

        - IoT Hub è una generalizzazione di OPC client che si occupa più he altro della parte di autenticazione (mediante certificati)
        - I servizi tipo DB, archiviazione... sono "paghi per quanto consumi" (il provider ti vuole fottere se dice così, paghi in realtà per le "risorse che hai prenotato")
        - Ogni quadrato blu è una VM; per gestire in questa maniera il carico abbiamo bisogno quindi di un Load Balancer (Kubernetes) che tira su e giù le macchine in base al carico
- Sicurezza
    - Centralizzazione in un servizio unico delle funzionalità di autenticazione e autorizzazione all'uso di risorse
        - Pro: fattore di sicurezza poiché limita i punti di attacco e favorisce il controllo
        - Contro: se viene compromesso il server centrale verranno compromesse anche tutte le sotto-macchine
    - I Servizi di Directory si occupano anche di autenticazione e autorizzazione
    - Tipi
        - Sicurezza nei mezzi trasmissivi
            - Canali wirelesses
                - Radius Server

                    ![](./res/radius.png)

            - Comunicazioni applicative (es: https, ssl/tls)
            - VPN
        - Autenticazione delle entità (certificati X.509)
        - Autenticazione degli utenti (Autenticazione multi-fattore)
        - Autorizzazione all'accesso alle risorse (directory)
- Directory Service: sistema centralizzato di gestione/fruizione di informazioni per utenti, reti, servizi, applicazioni in un dominio
    - Permette di effettuare una configurazione centralizzata e automatica (inclusa l'assegnazione di nomi) delle macchine (e/o dei servizi) appartenenti ad un'organizzazione

    ![](./res/dirser.png)

    - Autenticazione
        - Tipi
            - Locale

                ![](./res/autloc.png)

                - La macchina stessa su cui si fa login verifica le credenziali di autenticazione (database locale)
            - Remota

                ![](./res/autrem.png)

                - La macchina su cui si fa login invia (tramite un canale criptato) una richiesta per verificare la credenziali di autenticazione verso un server
        - Note
            - Per entrare a far parte di un dominio la singola macchina deve fare un join al dominio, questo significa che il dominio e la macchina locale si devono scambiare delle chiavi crittografiche che serviranno per rendere sicura la comunicazione fra i due sistemi
            - Se c'è un problema di rete e le macchine locali non riescono a parlare con il sistema centrale, non è detto che le macchine locali non siano utilizzabili; fra queste e il sistema centrale potrebbe esserci un server "intermediario" che contiene riferimenti al server centrale (quindi in questo caso il server intermediario funge da "backup" del server centrale)
            - In realtà in generale non ci sono server di "backup", ma server che si dividono il carico
        - Single-sign-on (paradigma): autorizzazione all'uso di più applicazioni con una sola richiesta di credenziali utente
            - L'implementazione varia in base a:
                - Dominio/Remoto
                    - Dominio (Kerberos): utilizzabile solo all'interno dell'organizzazione stessa per cause di sicurezza

                        ![](./res/ssodom.png)

                - Sistema operativo
                - Web/Non web
                    - Web (Shibboleth): utilizzabile anche all'esterno del dominio

                        ![](./res/shibb.png)

                        - L'utente, dopo il login, riceve un ticket temporaneo firmato che lo autenticherà e sarà valido per tutti i servizi relativi al ticket; questo poiché ogni applicazione interrogherà Shibbolet per verificare l'autenticità e i livelli di privilegio del ticket
- Virtualizzazione (server): tecnologia mediante cui si esegue un sistema operativo ospitato isolandolo all'interno di una macchina che non è fisica, bensì ottenuta da uno strato software denominato Virtual Machine Monitor o Hypervisor (può girare su un SO o direttamente sulla macchina cruda)
    - Pregi
        - Isolare una applicazione all'interno del SO installato sulla VM
    - Difetti
        - Richiede l'esecuzione di tutto un SO nella sua VM per isolare ed eseguire una specifica applicazione
        - L'applicazione da eseguire comunque dipende dalla specifica versione, configurazione, presenza di software installato nel SO installato nella VM

    ![](./res/vm.png)

- Containerizzazione (applicazione): realizzano una specie di sottoinsieme delle risorse offerte da un SO e mettono a disposizione queste risorse alle applicazioni che vengono eseguite all'interno dei container
    - Ogni container ha il suo spazio utente e sfrutta i servizi del kernel sottostante (della macchina su cui sono in esecuzione)
    - La containerizzazione ci permette di eseguire più applicazioni (sulla stessa macchina) che necessitano di utilizzare versioni diverse della stessa libreria
    - Facilita la scalabilità
        - All'aumentare degli utenti, si possono lanciare più istanze dello stesso container

    ![](./res/contain.png)

- Cloud e virtualizzazione
    - I servizi cloud si distinguono per il livello di componibilità che offrono e per la capacità di scalare
        - SaaS (Software as a service): servizio applicativo che non vede né sistema operativo né host su cui lavora (google documents)
        - PaaS (Platform as a service): fornisce delle API di sviluppo per comporre servizi più complessi, ma non sai su che sistema operativo lavori e nemmeno su che host (gestione indiretta, scalabilità automatica con costo, alto livello)
        - IaaS (Infrastructure as a service): fornisce una infrastruttura su cui installare servizi (ad esempio una macchina virtuale); la scalabilità deve essere effettuata o aumentando la potenza o aumentando il numero delle istanze (gestione diretta, scalabilità manuale, basso livello)
    - Piattaforme di gestione di cloud privati
        - Tante macchine fisiche molto potenti (cluster)
        - Su queste girano software che permettono di tirare su varie VM, sono gli orchestratori (OpenStack, OpenNebula)
        - Queste VM possono essere collegate fra loro e con l'esterno
- VPN: rete privata tra host anche geograficamente molto distanti
    - Tipi
        - Site-to-site: tra due reti
        - Remote access: collega un singolo computer ad una rete privata
    - Problema di sicurezza: la compromissione di un computer collocato in una rete VPN rischia di compromettere tutta la rete del dominio
- SSH remote port forwarding (bypass firewall, metodo insicuro)
    - Uso un server che fa da relay fra un server protetto da firewall e un client della rete esterna (è come se io aprissi un buco nel firewall, non ottimo)
    - Comando: `ssh -R public_IPaddr:55555:localhost:44444 joe@public_IPaddr`
- Superamento firewall e NAT (metodi migliori)
    - Esempio: webcam in insediamento remoto protetto da firewall/NAT con cloud di supporto deve essere raggiunta da user protetto da firewall/NAT

        ![](./res/nocondir.png)

        - Se uno dei due firewall non fosse simmetrico la comunicazione diretta sarebbe possibile

        1. Collegamento mediante Intermediario in Cloud

            ![](./res/colintcloud.png)

            - Problema: se abbiamo tanti utenti che vogliono vedere lo stesso flusso video il cloud deve essere molto prestante (duplicazione di flusso)

        2. Collegamento mediante server TURN
            - Esistono dei protocolli (ICE, che si appoggiano su server esterni STUN e TURN) che cercano di capire che tipo di firewall/NAT c'è fra gli end system che vogliono comunicare
                - Non ci sono firewall -> Comunicazione diretta (tramite WebRTC & TURN: instaurazione comunicazione diretta fra browser mediante un server di segnalazione)
                - Ci sono -> Comunicazione indiretta mediante server TURN
                    - In questo caso è più conveniente utilizzare un collegamento mediato dal Cloud (anche se più costoso) in quanto il flusso di dati è minore lato Plant (il plant manda il video live al cloud che lo ridistribuisce a tutti gli utenti che vogliono vederlo)
                - Uno dei due non ha firewall simmetrico -> Tramite dei trucchi creiamo una comunicazione diretta

            ![](./res/turn.png)

            - Prima di usare i server STUN e TURN l'utente si deve autenticare mediante TURN-enabling Signalling Server che tira su un canale criptato mediante il cloud fra gli end point

### Parte 2 - Sincronizzazione degli orologi (protocollo NTP)

- Lo scopo di sincronizzare due computer è assicurare che diversi processi fisicamente distanti (cioè su computer diversi) abbiano la stessa nozione del tempo
    - È importante conoscere almeno uno fra:
        - Istante in cui un evento si è verificato
        - Intervallo di tempo fra due eventi
        - Ordine relativo fra vari eventi
    - A cosa serve?
        - I sistemi e protocolli deputati alla sicurezza fanno uso di marche temporali per concedere permessi di accesso con una validità limitata nel tempo (SSL/TLS e Kerberos)
            - Gli orologi devono essere sincronizzati entro un ragionevole intervallo
    - Tipi di Clock
        - Hardware (al quarzo)
            - È realizzato mediante un contatore digitale alimentato da una batteria
            - Conta il passare del tempo anche a computer spento
            - È sensibile a variazioni della tensione di alimentazione o della temperatura
            - È possibile leggere il clock hardware ed anche modificarlo (hwclock)
            - Quando è attivo un servizio di sincronizzazione del tempo via rete (ad es. NTP) il kernel, ad intervalli regolari, assegna al clock hardware il valore di orologio ottenuto via rete
        - Software
            - Avanza solo a computer acceso e SO in esecuzione
            - All'accensione del computer il valore del clock hardware viene usato per configurare il valore del clock software
            - Come avviene l'avanzamento?
                1. L'orologio hardware scatta ad intervalli abbastanza regolari e produce l'incremento unitario di un contatore (tick)
                2. Il valore del contatore (tick) è convertito in un tempo generando il valore corrente del clock software
                3. Durante la conversione, si può aggiungere/sottrarre un tempo di offset per ottenere la sincronizzazione
                4. Ogni volta che il contatore viene incrementato (ogni volta che scatta il tick) viene aggiornato il valore dell'orologio software aggiungendo il tempo trascorso dall'ultimo tick (può variare nel tempo); il valore di questa aggiunta è ciò che deve essere determinato dagli algoritmi di sincronizzazione
        - Sincronizzazione
            - Logica
                - Tipi
                    - Round-trip (BEST)
                        1. Lo slave spedisce il proprio clock T1 nel messaggio di richiesta
                        2. Il master risponde mettendo nel messaggio di risposta il valore T1 ricevuto, il valore T2 dell'istante di ricezione, il valore T3 dell'istante di spedizione della risposta che è anche il valore del clock dato allo slave
                        3. Lo slave calcola il tempo presunto di trasmissione dal master allo slave approssimandolo con la metà del Round Trip Time
                        4. Lo slave all'istante T4 cambia il proprio clock assegnandogli il valore T4'

                        ![](./res/ntprtt.png)

                        - La stima del RTT/2 approssima bene la realtà solo se i tempi di trasmissione si mantengono abbastanza costanti nel tempo e il tempo di andata e il tempo di ritorno sono simili (non va bene per percorsi asimmetrici)
                    - Tree-way (viene usato quando il master ha bisogno di sapere il suo scostamento rispetto al tempo dello slave)
                        1. Master spedisce il proprio clock T1 nel messaggio di richiesta
                        2. Slave risponde mettendo nel messaggio di risposta il valore T1 ricevuto, il valore T2 dell'istante di ricezione, il valore T3 dell'istante di spedizione della risposta
                        3. Master approssima il tempo di trasmissione dal master allo slave con la metà del round trip time
                        4. Master approssima l'offset dell'orologio dello slave rispetto al master (offset indica di quanto bisogna rallentare il clock dello slave)
                        5. Master invia l'ordine di modifica del clock dello slave con un messaggio contenente l'offset più il tempo di trasmissione
                        6. Lo slave toglie al suo clock l'offset ricevuto

                        ![](./res/treewayntp.png)

                - Problemi
                    - A causa dei diversi ritardi di trasmissione ogni processo non può avere una vista istantanea globale di tutti gli altri clock
                    - Anche se tutti i clock fossero inizializzati contemporaneamente, essi non rimarrebbero sincronizzati a causa del drift nei rispettivi periodi
                    - La presenza di elementi malfunzionanti distribuiti nel sistema complica ulteriormente il raggiungimento di una visione unica del tempo
            - Fisica
                - Universal Coordinated Time (UTC): definizione di un tempo "ufficiale"
                    - 50 laboratori sparsi nel mondo che hanno orologi atomici basati su cesio 133
                    - Periodicamente ogni laboratorio comunica al BIH (Bureau International de l’Heure di Parigi) quanti tick ha battuto il proprio orologio
                    - BIH esegue la media di questi valori e produce il TAI, Tempo Atomico Universale (giorno del TAI dura circa 3 microsecondi in meno del giorno solare, il BIH risolve il problema inserendo del tempo di compensazione)
                - WWV: sincronizzare gli orologi ("lato server") direttamente con i detentori del tempo ufficiale via radio
                    - Il WWV (stazione radio) emette un beep ad ogni secondo UTC, gli altri ascoltano
                    - Per poter compensare i ritardi introdotti dalla propagazione del segnale nell’atmosfera bisogna conoscere con accuratezza la distanza tra trasmettitore e ricevitore per calcolare il ritardo di propagazione
                - NTP: sincronizzare gli orologi di tutti gli utenti
                    - Interna (non approfondita): NON è disponibile il tempo reale fornito dall'esterno, ho più clock fisici paritetici e l'obiettivo è minimizzare la massima differenza  esistente tra ogni coppia di clock, coordinandoli tra di loro a formare un clock (quasi) comune
                    - Esterna: un clock sorgente fornisce il tempo reale e l'obiettivo degli altri clock è essere vicini a questo tempo sorgente il più possibile (NTP)
                        - I server NTP sono divisi in strati (stratum)
                        - I server NTP primari sono classificati come stratum 1; essi non devono essere usati per sincronizzare le macchine client perché sono in numero esiguo ma servono a sincronizzare i server NTP di stratum 2
                        - I server pubblici dello stratum 2 sono disponibili per la sincronizzazione dei client
                        - I server NTP di stratum 2 fanno una media del tempo di più server di stratum 1
                    - Formato dei dati
                        - NTP Date
                            - 128 bit
                              - 64 bit per istante espresso in secondi (interi) su un intervallo di 584 milioni di anni
                                  - 32 bit: Eranumber, stabilisce una Era di 136 anni
                                  - 32 bit: Eraoffset, stabilisce uno scostamento in secondi a partire dall'inizio dell'era specificata
                              - 64 bit per aggiunta di una frazione di secondo, con una precisione di 0.5e-18
                        - NTP Timestamp
                            - 64 bit
                                - 32 bit contengono i secondi
                                - 32 bit contengono una frazione di secondo
                            - Non molto preciso ma non è un grosso limite poiché trasmettendo in rete la precisione non la potremmo supportare poiché siamo soggetti alle fluttuazioni dei tempi di trasmissione
                            - Per poter aggiustare il valore del clock del client occorre che i due orologi (del client e del server) non siano troppo disallineati, in particolare devono avere due valori che non distano per più di una mezza Era (68 anni), altrimenti la correzione non è possibile; quindi si usa un file di config
                        - NTP Short
                            - 32 bit, il resto come NTP Timestamp
                    - Un client per sincronizzarsi fa n richieste ai server NTP che ha in memoria; di queste richieste (fatte mediante round-trip) scarta quelle che si discostano troppo e fa una media pesata delle rimanenti; poi aggiorna il suo orario e sistema di aggiustamento dell'orario
                    - Messaggio NTP (incapsulato in UDP)

                        ![](./res/packntp.png)

                    - Su Linux
                        - `ntpd`: demone NTP
                        - `ntpdate`: aggiorna brutalmente l'orologio
                        - `ntpq -p`: lista server NTP

### Parte 3 - Identity and Access Management

- Buone prassi di sicurezza
    - Stabilire permessi di accesso alle risorse dell'host da parte degli utenti
    - Identificare (autenticare) gli utenti, e controllare gli accessi alle risorse dell'host, sulla base dell'identità dell'utente
    - Comunicazioni siano il più possibile rese incomprensibili e non modificabili ad altri che non siano i legittimi mittenti e destinatari
    - Le password vanno protette e non devono passare in chiaro in rete (possibilmente non dovrebbero nemmeno essere salvate in chiaro su disco)
    - Le credenziali di autenticazione non dovrebbero passare in rete né in chiaro né criptate, dovrebbero passare in rete solo credenziali temporanee generate di volta in volta
- Concetti sulla sicurezza informatica
    - Confidenzialità
        - Si ottiene la confidenzialità nascondendo ai non autorizzati le informazioni o le risorse (crittografia)
    - Integrità: le informazioni o le risorse “sensibili” non devono subire alterazioni non autorizzate
        - Dati (e delle comunicazioni): le informazioni devono rimanere come il legittimo proprietario le ha costruite
        - Sorgente (o mittente)
            - In una comunicazione, il ricevitore del messaggio deve vedere non modificato il mittente del messaggio
            - Un messaggio mandato da A verso C non deve apparire a C come se fosse stato mandato da B, né per opera di A né per opera di B, né per opera di chicchessia
            - I meccanismi per garantire l’integrità si dividono in meccanismi di prevenzione e di scoperta
                - Prevenzione: impediscono di modificare le informazioni
                - Scoperta: permettono di sapere se le informazioni sono state modificate
    - Non repudiabilità (rintracciabilità): capacità di stabilire chi e quando ha compiuto una determinata azione o chi e quando ha costruito un determinato dato
        - Per assicurare questa proprietà occorre:
            - Autenticare gli utenti in modo da poter collegare le azioni pericolose a chi le ha compiute
            - Salvare in un log le azioni, i dati prodotti con le azioni e gli autori delle azioni
            - Assicurare la coerenza tra i dati salvati e i dati originali
            - Assicurare l'integrità sia dei dati che delle sorgenti, intese come entità che hanno svolto le azioni
    - Disponibilità: possibilità di usare una determinata risorsa o un’informazione entro un intervallo di tempo ragionevole (il DoS attacca questa proprietà)
- GDPR (General Data Protection Regulation): definisce responsabilità e procedure operative da attuare per chi deve usare o conservare informazioni personali di un utente
    - Tipi di dati
        - Sensibili: informazioni riguardanti gli aspetti intimi delle persone (preferenze sessuali, amicizie, dati sanitari...)
        - Personali: informazioni generiche sulle persone (indirizzi, numeri di telefono, email, immagine di una videocamera...)
        - Di macchina (non personali): dati prodotti dai sensori che operano su una macchina
    - Politiche e meccanismi
        - Politica: indicazione di cosa è permesso e cosa non è permesso fare
            - Politica di sicurezza
                - Definisce:
                  - Le operazioni che si possono effettuare su certi dati e l’utente che può usarle
                  - Gli utenti che possono accedere a certi dati
                  - Eventuali profili di utente con specifici diritti
                - Si focalizza su:
                    - Dati (proteggere)
                    - Operazioni (controllare)
                    - Utenti/profili/gruppi (controllare)
        - Meccanismo: metodo per garantire una politica
            - Meccanismo di sicurezza
                - Prevenzione: il meccanismo deve rendere impossibile l’attacco
                    - Aspetto psicologico: gli utenti hanno bisogno di sicurezza, ma la sicurezza viene difficilmente apprezzata (sicurezza != facilità d'uso)
                - Scoperta: il meccanismo è in grado di scoprire se un attacco è in corso o è stato effettuato nel passato
                - Recupero
                    - Interrompere il funzionamento del sistema e ripristinare un backup
                    - Far continuare il funzionamento del sistema anche mentre un attacco è in corso
- Auditing: processo sistematico, indipendente, documentato, svolto secondo regole precise prefissate, che controlla le azioni svolte per individuare le violazioni e suggerire rimedi
    - Richiede:
        - Selezione e salvataggio delle azioni pericolose
        - Protezione dei file di log
        - Autenticazione degli utenti in modo da poter collegare le azioni pericolose a chi le ha compiute
- Sicurezza a livelli: la sicurezza è presente in TUTTI i livelli della macchina/sistema
- Diritto di copia e di possesso
    - Copia: consente al possessore di un oggetto `o` di concedere ad altri soggetti un diritto `r` su `o`
        - Principio di attenuazione: il possessore di `o` può concedere ad altri solo diritti che lui possiede su `o`
    - Possesso: consente al possessore di `o` di auto-concedersi oppure sottrarsi diritti su `o`
    - In alcuni sistemi invece i possessori non hanno né diritto di possesso né diritto di copia, è l'amministratore di sistema che stabilisce i diritti
- Fasi di progettazione di un sistema sicuro (un sistema completamente sicuro non esiste)
    - Specificazione: descrizione del funzionamento desiderato del sistema
    - Progetto: traduzione delle specifiche in componenti che le implementeranno
        - Considerazioni progettuali
            - Analisi costi-benefici della sicurezza
            - Analisi dei rischi (valutare le probabilità di subire attacchi e i danni che possono causare)
            - Aspetti legali (ad esempio uso della crittografia negli USA) e morali
            - Problemi organizzativi (ad esempio la sicurezza non "produce" nuova ricchezza, riduce solo le perdite)
            - Aspetti comportamentali delle persone coinvolte
    - Implementazione: creazione del sistema che soddisfa le specifiche
- Identity & Access Management (IAM): sistema di procedure, politiche e tecnologie per gestire il ciclo di vita delle credenziali elettroniche e i diritti di accesso alle risorse digitali che quelle credenziali consentono
    - AAA
        - Authentication: verificare l'identità di un utente del sistema quando questo richiede un servizio
            - È necessario una operazione preliminare di identificazione (identification) dell'utente che consente di inserire l'utente nel sistema (provisioning) e consegna all'utente le credenziali per le successive operazioni di autenticazione
        - Authorization: il sistema controlla se l'utente ha i permessi di accesso al servizio richiesto
            - È necessaria una operazione preliminare di assegnamento dei diritti (granting), effettuata durante il provisioning, che stabilisce quali sono i servizi a cui quell'utente ha diritto di accesso e li salva come attributi in strutture dati di un apposito database
        - Accounting: tracciare e salvare le operazioni svolte dagli utenti
    - Importante: occorre avere una identità digitale diversa per ciascun dominio in cui volete fruire dei servizi; questo poichè ogni dominio ha diversi livelli di sicurezza
    - Centralizzare l'Identity Management: architettura hardware e software centralizzata che fornisca un servizio di Autenticazione e Autorizzazione
        - A tutti gli utenti di un determinato dominio (autenticazione di dominio)
        - Valida per tutti gli host del dominio usando le stesse proprie credenziali per tutti i PC (credenziali di dominio)
        - Permetta di autenticarsi una sola volta per sessione di lavoro e di poter poi fruire, nel corso di quella sessione di lavoro, delle risorse di tutti gli host del dominio senza dover ripetere le procedure di autenticazione (single sign on)
        - Pro e contro
            - Vantaggi
                - Una sola credenziale per ciascun utente
                - Una sola autenticazione per sessione di lavoro
                - Utilizzo delle risorse di tutto il dominio
                - Stesse risorse disponibili da PC diversi del dominio
            - Svantaggi
                - Mantenere struttura centralizzata di autenticazione
                - Il sistema centralizzato è un single point of failure (impossibile autenticazione in caso di crash del servizio, necessaria ridondanza dei server
                - Se ti rubano quell'unica credenziale, rischi danni in TUTTI i servizi che quelle credenziali permettono (devastation trigger)
                - La connettività è indispensabile (impossibile autenticazione in caso di crash della rete)
        - Entità operanti con gli IAM Centralizzati
            - Subject (User): chi vuole ottenere il servizio
            - IdentityProvider (IAM System): riconosce l'utente e autorizza l'uso del servizio
            - Service Provider: chi fornisce il servizio richiesto dall'utente
    - Strutturazione dei Domini degli IAM
        - Un dominio può essere strutturato in sottodomini
        - Un dominio può essere organizzato in gruppi che possono travalicare i confini dei sottodomini
        - Uno IAM system di un dominio si occupa degli utenti di un determinato dominio, organizzandoli in sottodomini e gruppi
        - Uno IAM system di un dominio si occupa delle risorse di un determinato dominio, organizzandole in sottodomini
    - Federazioni di Sistemi di IAM: consente che l'autenticazione di un utente effettuata da uno IAM `A` sia valida (con opportune regole e limitazioni) anche per i servizi forniti mediante un diverso IAM `B`
        - Realizzato configurando opportunamente alcune relazioni di fiducia digitale (trust) tra gli IAM systems `A` e `B` (solitamente asimmetriche)
    - Utilizzi di uno IAM system di dominio
        - Uno IAM system viene usato per autorizzare un utente ad utilizzare le risorse (file, device...) di un dominio
        - L'utente richiede l'utilizzo della risorsa specificandone il nome, ed il dominio deve anche identificare e distinguere la risorsa che si trova in rete, associando al nome della risorsa un identificatore di rete univoco
            - Per identificare e accedere a queste risorse è necessario che:
                - Il dominio possegga un servizio (servizio di naming) che mappa i nomi negli indirizzi di rete (non sempre indirizzi IP); nei sistemi moderni e più grandi il naming è il DNS
                - Il dominio disponga di un protocollo per trasmettere file o blocchi di file (file sharing)
            - Spesso lo IAM di un dominio integra questi servizi
    - Directory Service: insieme organizzato di tecnologie hardware, programmi, procedure e protocolli che nel loro complesso provvedono a:
        1. Organizzare, memorizzare e gestire le informazioni riguardanti:
            - Le risorse condivise all'interno di una rete di computer (il dominio)
            - Gli utenti del dominio che possono fruire di quelle risorse condivise
            - I permessi concessi agli utenti per accedere a quelle risorse
        2. Controllare l'accesso alle risorse da parte degli utenti secondo i permessi e le modalità stabilite
        - Tipi
            - Microsoft: Active Directory
            - Linux: SAMBA
    - IAM system vs Directory Service
        - IAM system e Servizio di Directory sono praticamente la stessa cosa in quanto uno ha bisogno dell'altro e vivono in coppia; hanno due compiti differenti:
            - IAM system: autenticazione e autorizzazione dell'utente
            - Directory Service: strutturare e mantenere le informazioni su utenti, risorse, gruppi e permessi
    - Scenari di Autenticazione e Controllo di Accesso
        1. Host (mobile o no) accede a mezzo trasmissivo condiviso, gestito da dispositivi di accesso (AP wifi o switch ethernet)
        2. Join di un Host (tipicamente fisso) ad un dominio
           - Un controller di un dominio ed un host vengono entrambi configurati affinché l'host venga visto come appartenente al dominio
           - L'host viene fornito di credenziali di autenticazione salvate nell'host stesso
           - Mediante quelle credenziali l'host crea un canale criptato con il controller del dominio mediante il quale si scambiano informazioni
        3. Utenti accedono ad un host isolato o in un dominio
        4. Utenti accedono ad applicazioni distribuite mediante un host
    - Importante
        - In generale i sistemi di Active Directory/IAM sono costosi in quanto, anche per piccole organizzazioni, servono server robusti; per questo ci sono delle alternative (NIS...) più leggere ma meno affidabili
        - Solitamente gli accessi fuori dominio sono a base web
    - Autenticazione per accesso ad AP WiFi
        - Modalità
            - Direttamente col server Radius

                ![](./res/accretiam.png)

                - L'AP fa solo da intermediario fra l'host e l'IAM tramite il protocollo RADIUS
                - Il WT si può fidare dell’AP come se si fidasse direttamente del server RADIUS perché AP e server RADIUS condividono una chiave segreta utilizzata nelle loro comunicazioni
                - Tipi di sicurezza
                    - EAP-TLS, certificati X.509
                    - PEAP e MS-CHAPv2 con username e password
            - Tramite un Server di Accesso alla Rete (NAS, quello che usano gli ISP per l'autenticazione, più o meno lo stesso funzionamento di RADIUS)
    - Join al dominio
        - LOGON: configurare un PC affinché conceda l'accesso (logon) ad utenti di un dominio
        - L'autenticazione centralizzata degli utenti al logon viene effettuata solo su PC che hanno effettuato il join al dominio (certificazione interna) e che devono essere amministrati dagli amministratori del dominio
        - APPLICAZIONI: diverso è il discorso se consideriamo una autenticazione a livello applicativo degli utenti il cui fine è che un utente, tramite una applicazione, possa usufruire di un servizio remoto
    - NIS (Network Information Service): utile in sistemi piccoli
        - File sensibili Linux (NIS consente che più host condividano le informazioni contenute in questi files)
            - `/etc/passwd`: informazioni sugli utenti
            - `/etc/group`: gruppi di utenti
            - `/etc/shadow`: password criptate
            - `/etc/aliases`: alias di posta elettronica
            - `/etc/hosts`: traduzione degli indirizzi IP degli host della rete locali
            - `/etc/networks`: traduzione degli indirizzi IP delle sottoreti (locali)
            - `/etc/protocols`: nomi e numeri dei protocolli di rete
            - `/etc/rpc`: numeri delle chiamate RPC
            - `/etc/services`: abbinamento dei servizi di rete ai numeri di porta corrispondenti
        - Come sono usati i file di configurazione locali
            - La lettura/scrittura del contenuto di questi file può essere fatta
                1. Dall'utente, a scopo informativo, con le usuali funzioni di lettura/scrittura
                2. Dal sistema operativo per uso effettivo di configurazione, mediante opportune syscall specializzate, chiamate in opportune funzioni di libreria di sistema, oppure mediante comandi che usano quelle stesse funzioni
                3. Dall'utente, ad uso effettivo, con i comandi che usano quelle stesse funzioni
        - E con NIS?
            - NIS permette, ad un gruppo di macchine di uno stesso NIS domain, di condividere un insieme di files di configurazione la cui copia principale è collocata in un master server; in realtà per ciascun file del Master viene condiviso un Database (mappa) che contiene le stesse informazioni contenute in quel file
            - Funzionamento
                - Il server master possiede la copia autoritativa dei DB, risponde alle richieste di lettura e modifica provenienti da server slave e da client (con una certa latenza) e, se configurato, avvisa i server slave che lui ha modificato il DB e quindi i server slave devono chiedergli la copia aggiornata del DB
                - I client non hanno una copia dei DB e ogni volta leggono e modificano dati sul server master; possono chiedere dati anche ad uno slave, se il master non risponde
                - I server slave sono dei server master che hanno una copia non autoritativa del DB; gli slave aggiornano la loro copia dal server master, agendo nei suoi confronti come client (se il master va in crash viene sostituito da uno slave)

                ![](./res/nis.png)

            - Comandi: classici ma prepongo `yp` (es: `yppasswd`)
            - Protocollo: RPC (remote procedure call, non sicuro)
            - Servizio: `rpcbind`
            - File di configurazione
                - Tipi
                    - Locali: possono esserci informazioni (sempre `/etc/passwd` ecc) locali, che non vengono sincronizzate con il master e sono storate all'interno dei file di configurazione stessi; inoltre, dentro a questi file, sarà presente un riferimento alla mappa NIS locale di configurazione
                    - Remoti: le informazioni di configurazione sono storate sul server master e quando il client ne ha bisogno le "scarica" nella mappa NIS locale
                - Syscall: le syscall sono differenti nel caso in cui si stia parlando di file di configurazione "classici" o "NIS oriented"
    - NFS (Network File System): directory personali in server remoto, utile in sistemi piccoli

        ![](./res/nfs.png)

        - Non scarica tutto il filesystem, ci si scambia file mano a mano che servono
    - PAM (Pluggable Authentication Modules): sistema di autenticazione degli utenti per l'uso di applicazioni che operano su un host, e che delega l'autenticazione all'host
        - Come? -> Mette a disposizione delle semplici API che le applicazioni invocano per chiedere l'autenticazione di un utente proprio a livello di linguaggio di programmazione
            - Funzionamento
                1. Se un programma ha bisogno di far autenticare un utente, chiama la routine `pam_authenticate`
                2. Questo programma accede ad un file di configurazione nella cartella `/etc/pam_d` avente lo stesso nome del programma
                3. Il file di configurazione, preparato dal sysadmin, contiene i moduli di autenticazione che devono essere chiamati e indica come usare i risultati che restituiscono i moduli; in tal modo le modalità di autenticazione sono stabilite dall'amministratore di sistema

                ![](./res/pam.png)

    - Single-Sign-On per applicazioni web, anche all'esterno di un dominio

        - SAML 2.0: standard per realizzare il Single Sign On in ambito web
        - Shibboleth: implementa SAML 2.0

        ![](./res/webssoo.png)

        - Funzionamento
            - Quando l’utente, tramite browser, accede alla risorsa, la libreria SP manda al client un redirect http verso l’IdP con i dati (in POST) della richiesta di autenticazione
            - L’IdP richiede l’autenticazione all’utente attraverso una form web o simile
            - Se l’utente è correttamente autenticato l’IdP raccoglie gli attributi relativi e manda al client un redirect verso il SP con in POST l’assertion XML contenente gli attributi richiesti
            - Il SP checka le informazioni dell'utente comunicando con l'IdP
            - Il login vale su tutti gli SP fino a che il token non scade/si fa logout
                - In questo caso l'IdP informa tutti gli SP che il token generato non è più valido

### Parte 4 - Introduzione alla Autenticazione come Servizio di Terza Parte: Kerberos

- Crittografia
    - Crittografia a chiave segreta (simmetrica)
        - Medesima chiave per codifica e decodifica
        - Le funzioni di codifica e decodifica sono note,la chiave è nota solo ai 2 che comunicano
        - Non repudiabilità non garantita: non posso garantire "chi" ha mandato il messaggio
        - Algoritmi veloci ma le parti che comunicano devono prima scambiarsi la chiave in modo sicuro
            - Esempio: `AES a 64, 128, 168, 256 bit`

        ![](./res/seckey.png)

    - Crittografia a chiave pubblica (asimmetrica)
        - Le funzioni di codifica/decodifica sono note,la chiave pubblica è nota, la chiave privata è segreta
        - Una chiave per codifica, un’altra per decodifica
        - Mittente e destinatario NON condividono una chiave segreta
        - Ogni utente ha una coppia di chiavi
            - Privata: segreto personale da custodire
            - Pubblica: informazione da diffondere a tutti
        - Il testo codificato con una chiave può essere decodificato solo con l'altra chiave
        - Algoritmi pesanti ma almeno non dobbiamo scambiarci chiavi private (però come facciamo ad essere certi che la chiave pubblica di Alice è veramente di Alice? Problema)
            - Esempio: `RSA a 2048 bit`
        - Nomenclatura
            - Sintassi
                - `K+`: cifratura mediante chiave pubblica
                - `K-`: cifratura mediante chiave privata
            - Esempi
                - `K+ (K- (testo)) == testo`
                - `K- (K+ (testo)) == testo`
        - Tipi
            - Segretezza

                ![](./res/pubkey.png)

            - Autenticazione, integrità e Non Repudiabilità

                ![](./res/pubkey1.png)

            - Segretezza, Autenticazione, integrità e Non Repudiabilità

                ![](./res/pubkey2.png)

    - Funzioni Hash Crittografiche (one-way hash o digest)
        - Definizione: una `MD()`, message digest, è una funzione che prende in input una stringa (messaggio) `m`, a lunghezza variabile, e produce una stringa (digest) a lunghezza fissa `MD(m)` per cui dato `y = MD(x)`, con `x` sconosciuta, allora è impossibile determinare `x`
        - Algoritmo: `SHA-512`
        - MAC (Message Authentication Code): **autenticare** (parzialmente)l'origine di un messaggio (riconoscere il mittente) e **garantire l'integrità** del messaggio stesso facendo uso combinato della crittografia (a chiave pubblica o a chiave segreta) e delle funzioni hash crittografiche (**NON RENDE MESSAGGIO PRIVATO**)
            - Tipi
                - Chiave segreta: MAC

                    ![](./res/mac.png)

                - Chiave pubblica: Firma digitale (autenticazione totale)

                    ![](./res/macpub.png)

- Kerberos
    - Funzioni
        - Autenticazione: verificare l'identità di un client o di un servizio
        - Autorizzazione: autorizzare un client autenticato ad utilizzare un particolare servizio
        - Cifratura: capacità del sistema di prevenire che terze parti ascoltino i contenuti di qualunque comunicazione
    - Funzionamento
        - Permette agli utenti di accedere a servizi distribuiti nella rete senza che l'utente debba fidarsi dei server fornitori di servizio; ipoteticamente (salvo keylogger e simili), non richiede agli utenti di fidarsi delle workstation nella rete in cui opera, invece deve fidarsi del server di autenticazione centralizzato
        - Le due parti vengono a trovarsi in una relazione di fiducia solo verso una terza parte avente funzione di garante dell'identità dell'uno verso l'altro

            ![](./res/kerbmode.png)

            - Il key distribution center (parte di Kerberos) identifica un dominio in cui gli host si fidano fra di loro (mediante Kerberos) chiamato Realm
    - Assunzioni
        - Password non facili
        - Workstation sicure (no keylogger)
        - La rete può essere insicura ma sarebbe meglio usare kerberos solo all'interno di un dominio controllato
    - Strategie
        - Single sign-on
        - Password sempre trasmessa cifrata
        - Si usano solo chiavi private fra i due che devono parlare
        - Ogni dominio di sicurezza di kerberos (realm) deve far eseguire in un ambiente fisico sicuro il servizio di distribuzione delle chiavi
        - Needham-Schroeder per la distribuzione di chiavi: il centro di distribuzione delle chiavi mantiene una chiave segreta per ciascun principal (utente, servizio, host) che deve comunicare
    - Tempo
        - La sicurezza è basata anche sui time stamp dei ticket perciò è d’importanza critica che gli orologi dei server siano regolati con accuratezza; permettendo agli orologi di subire scostamenti si rende la rete vulnerabile ad attacchi
        - Se gli orologi non sono sincronizzati entro un ragionevole intervallo Kerberos presenta errori fatali e smette di funzionare
    - Kerberos + LDAP
        - Kerberos = Autenticazione
        - LDAP = Autorizzazione e name services
    - Environment (Realm)
        - Attori e processo di autenticazione
            - Kerberos server (KDC)
            - Clients, registrati con il server con il quale hanno scambiato le chiavi
            - Application servers, chiavi scambiate con il server

                ![](./res/kerbenv.png)

        - Comunicazione e chiavi

            ![](./res/kerbkeys.png)

            - Chiavi
                - `Kc`: statica ed è ricavata dalla password dell'utente per il realm dell'AS applicandovi una funzione Hash
                - `Kc,tgs e Kc,s`: chiavi di sessione condivise create a run-time su richiesta dell'utente C che richiede il servizio
            - Scambio di chiavi e messaggi
                1. Ottenere il ticket-granting ticket
                    1. C -> AS
                       - `(Options; IDc; Realmc; IDtgs; EKc[Times1]; Nonce1)`
                           - `Times1 = Time1; Lifetime1`
                    2. AS -> C
                        - `(Realmc; IDc; Ticket tgs; EKc[Kc,tgs; IDtgs; Times2; Nonce1; Realm tgs])`
                            - `Ticket tgs = EKtgs[Flags; Kc,tgs; Realm c; IDc; ADc; Times2]`
                                - `ADc = network address machine C`
                            - `Times2 = Time2; Lifetime2`
                2. Ottenere service-granting ticket
                    1. C -> TGS
                        - `Options; IDs; Times2; Nonce2; Ticket tgs; Authenticator c`
                            - `Ticket tgs = EKtgs[Flags; Kc,tgs; Realm c; IDc; ADc; Times2]`
                                - `Times2 = Time2; Lifetime2`
                            - `Authenticator c = EKc,tgs[IDc; Realmc; Time3]`
                                - `ADc = network address machine C`
                    2. TGS -> C
                        - `Realmc; IDc; Ticket s; EKc,tgs[Kc,s; Times2; Nonce2; IDs; Realm s]`
                            - `Ticket s = EKs[Kc,s; Realmc; IDc; ADc; Times3]`
                                - `Times3 = Time3; Lifetime3`
                3. Ottenere il servizio
                    1. C -> S
                        - `Options; Ticket s; Authenticator c`
                            - `Ticket s = EKs[Flags; Kc,s; Realmc; IDc; ADc; Times3]`
                            - `Authenticator c = EKc,s[IDc; Realmc; Time4; Subkey; Seq#]`
                            - `Times3 = Time3; Lifetime3`
                            - `ADc = network address machine C`
                    2. S -> C
                        - `EKc,s[Time4; Subkey; Seq#]`
                - Note
                    - Times ha 2 campi ma potrebbe averne anche 3:
                        - From: tempo di inizio
                        - Till: tempo di scadenza
                        - Rtime: richiesta di aggiornamento di tempo "fino a"
                    - Informazioni di connessione
                        - Protocollo: UDP
                        - Porta 88 per KDC
                        - Porta 1024 per Client
        - [Video spiegazione](https://www.youtube.com/watch?v=qW361k3-BtU)

### Parte 5 - Active Directory

- Active Directory: insieme di sistemi hardware e software che l'organizzazione dispiega per erogare i servizi informatici agli utenti interni, riconoscendo l'identità digitale degli utenti, controllando i loro permessi di accesso ai servizi e infine erogando i servizi richiesti
    - Elementi base
        - Base di dati distribuita e replicata che conservi caratteristiche, preferenze, diritti e permessi delle persone e dei gruppi
        - Identificare i computer dell'organizzazione a cui fornire alcuni servizi di base
        - Riconoscere le identità digitali degli utenti che richiedono il servizio informatico, mediante modalità standard, e permettere loro di accedere ai servizi
        - Individuare i server a cui rivolgere le richieste provenienti dagli utenti
    - Join del computer al dominio
        - Utente interno: utente che si trova su macchine interne alla rete dell'organizzazione
        - Una macchina all'interno del dominio vede più risorse e ha più diritti rispetto ad una macchina esterna; inoltre l'accesso ai servizi da parte degli utenti esterni è realizzato con procedure diverse ed esposto su una rete separata da quella interna
        - La procedura con cui un computer viene accettato come appartenente alla rete interna viene detta Join; questa procedura richiede una operazione manuale di un amministratore del sistema che accetta e registra il computer su un server del dominio
    - Progettazione: un servizio di directory deve essere progettato per potersi adattare a diversi aspetti riguardanti l'organizzazione, deve essere perciò dinamico sotto questi punti di vista
        - Strutturazione in sedi e collocazione geografica delle sedi dell'organizzazione
            - Concetti base
                - Suddividere: poter decidere in quali sedi collocare i server, e le repliche dei server, in modo che ogni sede abbastanza grande abbia propri server a cui attingere servizi in modo da minimizzare i tempi di accesso e non dover dipendere dalle reti esterne alla sede
                - Scalare: poter decidere di creare dei cluster di server in una sede per poter soddisfare richieste di servizi delle sedi con grandi numeri di utenti
                - Replicare: poter decidere se e dove collocare dei server che agiscano come repliche dei server principali
                    - Modalità
                        - Strutture gerarchiche in cui un server agisce come principale e gli altri come backup
                        - Strutture paritetiche in cui i server agiscono come peer
                - Ridondare per ottimizzare: poter decidere di replicare localmente, parzialmente e automaticamente i contenuti di server di altre sedi allo scopo di migliorare i tempi di accesso
            - Requisiti
                - Separare le reti interne da quelle esterne per mezzo di firewall
                - Creare delle comunicazioni affidabili e riservate tra le diverse sedi, in modo che le diverse reti interne delle diverse sedi appaiano come una stessa rete, ma siano logicamente separate dalle reti esterne, anche se le attraversano (VPN)
        - Struttura organizzativa del personale
            - Concetti base
                - Mantenere un database frazionato, distribuito, replicato e coordinato tra le diverse sedi, con le informazioni su tutte le persone, le risorse e i servizi dell'organizzazione
                - Strutturare il database per mantenere l'organizzazione gerarchica del personale, distinguendo ruoli e responsabilità
                - Strutturare il database per mantenere le informazioni su gruppi di persone e sui permessi di accesso ai servizi concessi ai diversi gruppi di persone
                - Strutturare il database per mantenere le informazioni sui computer e sui servizi forniti dai e ai singoli computer appartenenti al dominio, oltre che ai server
            - Il punto di vista organizzativo nel servizio di directory viene realizzato definendo opportunamente lo SCHEMA del database cioè definendo nomi e attributi delle classi di oggetti che devono comparire nel database
        - Organizzazione delle reti e dei nomi dei computer dell'organizzazione
            - Concetti base
                - Individuare l'indirizzo di un computer partendo dal suo nome; l'assegnamento degli indirizzi viene fatto in base alla rete e sottorete in cui si trova lo specifico computer, mentre l'assegnamento dei nomi viene fatto in base alla sede dell'organizzazione in cui si trova quella sottorete e ad altri criteri
                - Il servizio DNS suddivide la rete in zone, per ciascuna zona esiste un server DNS che ha l'autorità di mappare i nomi in indirizzi IP; a questo server viene chiesto di risolvere i nomi in indirizzi per quella zona
                - Una zona può essere frazionata in sottozone affidate alla responsabilità di un altro server DNS
                    - Esempio

                        ![](./res/dns.png)

                        - La gestione dei nomi per tutta la zona di `unibo.it` comprese le due sottozone `cs.unibo.it` e `campus-fc.unibo.it`, può essere effettuata dal DNS collocato in `unibo.it`
                        - L'amministratore di `unibo.it` però può decidere di delegare la gestione dei nomi delle due sottozone ai DNS server collocati nelle due sottozone
                - La risoluzione dei nomi dei computer interni, viene utilizzata solo dagli utenti interni; agli esterni non è concesso conoscere nomi e indirizzi dei computer interni
                - Per rendere accessibile alcuni computer dall'esterno, si dovrà collocare i computer in una sottorete separata e configurare un servizio DNS separato
                - Ciascuna "zona dei nomi" coincide con una sede e viene chiamata dominio
    - Lo standard dei Servizi di Directory: LDAP
        - Specifiche
            - Molte letture poche scritture
            - Né transazioni né rollback (non gestisce accessi concorrenti)
            - Replica dei dati
            - Consistenza fra le copie lasca (può volerci un po a sincronizzare le copie)
            - Architettura master-slave
            - Possibilità di avere amministratori locali
            - Gli oggetti (i dati) sono relativamente piccoli
            - Esiste uno schema che definisce il formato degli oggetti memorizzati
            - L'informazione è basata su attributi degli oggetti
            - La ricerca è l'operazione effettuata più spesso
        - Rappresentazione Fisica e Logica (Gerarchia) della Base di Dati
            - Il servizio di directory è composto da oggetti detti entry
                - Appartenenti a classi la cui definizione è contenuta nello schema
                - Organizzazione
                    - Logica: livelli gerarchici di entry a formare una struttura ad albero (DIT ,Directory Information Tree)
                        - Tutti gli oggetti di uno stesso livello sono distinguibili dagli altri dello stesso livello per mezzo di un identificatore Relative Distinguished Name (RDN)
                        - Non possono esistere due oggetti di stesso livello con stesso RDN
                        - Ciascun oggetto contiene degli attributi, anch'essi oggetti descritti nello schema
                            - Possiamo vedere le classi come delle tabelle in un DB relazionale
                            - Gli oggetti sono le singole righe di una tabella
                            - Gli attributi sono i campi delle tabelle (ma possono avere più di un valore)
                        - La descrizione della classe a cui una entry appartiene, indica un elenco ordinato di nomi di attributi detti naming attributes; questi formano il Relative Distinguished Name (RDN)
                        - Standard dei nomi per i primi livelli della DIT
                            - Nei primi due livelli del DIT si usa come RDN un attributo che è adatto per distinguere tra le organizzazioni mediante il loro dominio di rete (domain component, dc)
                            - I successivi due livelli tipicamente usano come RDN un attributo che è adatto per descrivere l'organizzazione del personale o dei servizi o delle sedi (organization unit, ou)
                            - Un ulteriore livello identifica le singole persone (user id, uid)

                            ![](./res/stdname.png)

                        - Esempio

                            ![](./res/esdit.png)

                            - Nelle stringhe dei DN, RDN e BDN
                                - I valori possono essere racchiusi tra doppi apici (`"`) permettendo ai caratteri speciali di non essere interpretati
                                - I caratteri speciali possono essere preceduti dal backslash (`\`) in maniera da non essere interpretati
                                - Gli spazi che non fanno parte (centrale) di un valore sono ignorati
                        - Schema: insieme di regole che descrivono le entry (oggetti) contenute nel DIT mediante OID
                            - Indica, per ciascun oggetto:
                                - Quali attributi, obbligatori o facoltativi, contiene
                                - Come confrontare gli attributi
                                - Il tipo di dato memorizzabile in un attributo

                            ![](./res/ditschema.png)

                            - OID (Object Identifier): stringa che viene utilizzata per identificare in modo univoco vari elementi nel protocollo LDAP
                                - Possono essere utilizzati per rappresentare facilmente identificatori univoci senza il rischio di introdurre conflitti
                                - Hanno una gerarchia ad albero intrinseca e ogni organizzazione può chiedere che gli venga assegnata una propria base, `Private Enterprise Number` (PEN) che è una foglia in un albero
                                - [Esistono OID fissi che indicano elementi definiti](https://www.ldap.com/ldap-oid-reference)
                                - Esempio: il nodo con percorso `iso.identified-organization.dod.internet.mgmt.mib-2` ha OID `1.3.6.1.2.1`

                                    ![](./res/esoid.png)

                        - Fisica: entry collocate in una struttura sequenziale (DIB) salvata in un file
                - Definizione di attributi e oggetti
                    - Esistono classi predefinite (`person`, `organizationalPerson`, `inetOrgPerson`...)
                    - Attributi
                        - Ogni tipo di attributo
                            - È associato ad un OID (codice numerico) univoco
                            - Ha un nome che permette di riferirsi ad attributi di quel tipo
                            - Ha regole che dicono quali tipo di valori può contenere
                            - Ha regole che dicono come confrontare il proprio valore con un altro valore
                            - Può possedere alcune opzioni
                    - Classi
                        - Ciascuna classe
                            - È associata ad un OID univoco
                            - Ha un nome che permette di riferirsi ad attributi di quel tipo
                            - Può avere attributi
                        - Ereditarietà delle classi di oggetti: le classi possono essere estese costruendo delle classi derivate che ereditano le caratteristiche delle classi "super"
                        - ObjectClass: servono ad istanziare le entry in una directory LDAP e ne caratterizzano il tipo
                            - `Abstract`: da cui si possono solo derivare altre classi, ma da cui non è possibile istanziare direttamente una entry
                            - `Structural`: da cui si possono istanziare direttamente le entry
                            - `Auxiliary`: da cui non si possono derivare direttamente delle entry se non affiancandole alle classi `structural`; una sorta di ereditarietà da più classi
                        - Esempio

                            ![](./res/class.png)

                    - Esempio di oggetto `inetOrgPerson`
                        ```
                        version: 1
                        dn: cn=BarbaraJensen,ou=Product Development,dc=siroe,dc=com
                        objectClass: top
                        objectClass: person
                        objectClass: organizationalPerson
                        objectClass: inetOrgPerson
                        cn: Barbara Jensen
                        cn: Babs Jensen
                        displayName: Babs Jensen
                        sn: Jensen
                        givenName: Barbara
                        initials: BJJ
                        title: manager, product development
                        uid: bjensen
                        mail: bjensen@siroe.com
                        telephoneNumber: +14085551862
                        facsimileTelephoneNumber: +14085551992
                        mobile: +14085551941
                        roomNumber: 0209
                        carLicense: 6ABC246
                        o: Siroe
                        ou: Product Development
                        departmentNumber: 2604
                        employeeNumber: 42
                        employeeType: full time
                        preferredLanguage: fr, en-gb;q=0.8,en;q=0.7
                        labeledURI: http://www.siroe.com/users/bjensen MyHomePage
                        ```
                    - Esempi di definizione di Attributi e ObjectClass
                        ```
                        attributetype(1.3.6.1.1.1.1.0 NAME 'uidNumber'
                            DESC 'An integer uniquely identifying a user'
                            EQUALITY integerMatch
                            SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE)

                        attributetype(1.3.6.1.1.1.1.4 NAME 'loginShell'
                            DESC 'The path to the login shell'
                            EQUALITY caseExact IA5Match
                            SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE)
                        ```
        - Domini, Alberi e Foreste
            - Foreste: uno o più domain trees, ognuno avente il suo name space
                - Tutti i domini di una foresta condividono lo stesso schema
            - Domain trees: uno o più domini con namespaces contigui
            - Domains: insieme logico di computer e reti

### Parte 6 - Virtualization e container

- Tipi di virtualizzazione
    - Virtualizzazione: il codice della macchina virtuale deve essere codice macchina eseguibile sulla macchina hardware reale sottostante; non posso virtualizzare un sistema operativo che dovrebbe girare su un x86 a 64 bit su un processore ARM
        - Sistema operativo: offre contenitori per applicazioni isolati ed esegue un solo kernel, quello del SO Host
            - Container (Docker, sfrutta il supporto kernel LXC di Linux): all'utente viene presentata una partizione del sistema operativo corrente, su cui installare ed eseguire applicazioni che rimangono isolate nella partizione, pur accedendo ai servizi di uno stesso OS
                - Pro
                    - Pacchettizzo una applicazione, rendendola pronta per il deployment
                    - Replicazione di un container più volte
                    - Riutilizzare librerie fra vari container
                    - Condividere file fra vari container
        - Hardware: i sistemi operativi vengono eseguiti in modo concorrente sullo stesso hardware e possono solitamente essere eterogenei
            - Hypervisor: si occupa di multiplexare l'accesso alle risorse hardware e garantire protezione e isolamento tra le macchine
                - Si appoggia direttamente sull'hardware (bare-metal): il SO è assente, il suo posto è preso dall'hypervisor
                - Si appoggia sul sistema operativo (hosted): l'hypervisor è un processo utente (Virtual Box)
            - Sistema operativo guest modificato
                - Para-virtualization: la macchina virtuale presenta un'interfaccia differente confrontata con una macchina fisica; questo comporta dover modificare il sistema operativo guest (da parte del produttore) per chiamare istruzioni privilegiate relative all'hypervisor e non "all'hardware" (in alcuni casi queste istruzioni sono già implementate in delle librerie)

                    ![](./res/parvirt.png)

                - Full-virtualization: macchine virtuali che hanno la medesima interfaccia di una macchina fisica

                    ![](./res/fulvirt.png)

                    - Esecuzione di istruzioni privilegiate
                        - HW assisted
                            - Viene aggiunto un ring "-1"; il sistema operativo host e hypervisor vengono eseguiti nel nuovo ring, mentre il sistema operativo guest è eseguito nel ring 0. Così facendo il sistema operativo guest può eseguire le istruzioni privilegiate, sotto però il controllo dell'hypervisor che decide quando mettere la CPU in modo -1 oppure in modo 0
                            - Vengono poi aggiunte delle funzionalità che aggiungono un ulteriore livello di mappatura delle pagine in memoria: Intel Extended Page Tables (EPT)
                                - Una volta individuata la pagina in memoria virtuale nel modo classico, una ulteriore tabella stabilisce dove viene collocata la pagina dall'Hypervisor
                            - Possibilità di annidare VM (VMCS Shadowing)
                        - Traduzione binaria: riscrivere dinamicamente a run-time, man mano che vengono eseguite, le parti del codice del sistema operativo guest che dovrebbero essere eseguite in modo privilegiato
    - Emulazione di CPU (QEMU): l'hardware viene completamente emulato dal programma di controllo, cioè ogni istruzione che il sistema guest esegue viene tradotta in una sequenza di istruzioni della macchina host (prestazioni più basse)

### Parte 7 - NAT, firewall e NAT traversal

- Spazi di indirizzi privati
    - 10.0.0.0/8
    - 192.168.0.0/16
    - 172.16.0.0 through 172.31.255.255 (172.16/12 prefix)
- Range delle porte
    - Well-known: 1-1023
    - Registered: 1024-49151
    - Dynamic/Private: 49152-65535
- NAT
    - Il NAT runna sui routers che connettono le reti private a Internet, questo rimpiazza la coppia indirizzo IP-porta (pubblica) di un pacchetto IP con un'altra coppia (privata)
    - Tipi indirizzi
        - Inside
            - Local: usati dagli host lato privato
            - Global: indirizzo del router
        - Outside
            - Local: contattare un host pubblico tramite un "alias" locale
            - Global: indirizzo pubblico di un host raggiungibile

        ![](./res/olog.png)

    - IP-masquerading: NAT + PAT
        - NAT: riscrittura dell'ip sorgente e/o destinazione di un pacchetto IP
        - PAT: riscrittura dell'indirizzo IP e della porta del campo sorgente e/o destinazione di un pacchetto IP
        - Nota: nel NAT vengono salvati i mapping temporanei (indirizzo interno inizia conversazione con uno esterno) e ogni due minuti (o quando la conversazione termina) il mapping viene eliminato
    - Complessità
        - Semplice

            ![](./res/easynat.png)

        - Multiplo

            ![](./res/neasynat.png)

    - Esempio (con un po di errori nell'immagine)

        ![](./res/esnat.png)

    - Perché viene usato il NAT
        - Nasconde gli IP privati, più sicuro
        - È possibile usare vari indirizzi IP privati con uno pubblico
        - Gli IPv4 stanno finendo
        - Aumenta il livello di sicurezza con il Firewall
            - Firewall: sistema che filtra pacchetti TCP o UDP secondo delle regole (può trovarsi sia sul router o sulla macchina lato utente)
        - Gli indirizzi privati non sono raggiungibili dall'esterno (se non viene fatto port forwarding o se la comunicazione non è iniziata dall'interno)
    - Tipi (in base al mapping)
        - Statico: mappo un indirizzo IP privato con uno pubblico (utile quando ho vari IP pubblici disponibili)
        - Dinamico: mappo un indirizzo IP privato (in uscita) con uno pubblico disponibile
        - Overloading (PAT): funzionamento classico
    - Note
        - Il NAT può anche essere utilizzato per il bilanciamento del carico; un indirizzo pubblico che manda le richieste ricevute ad uno degli host privati
        - Svantaggi
            - Amenta la latenza
            - Sono necessari due DNS, uno interno e uno esterno
            - Alcune applicazioni non funzionano perché hanno bisogno di sapere gli host end to end
    - Tipi (in base ai livelli di restrizione)
        - Full-cone: tutte le richieste fatte da un IP privato vengono mappate su un certo IP pubblico e porta; quando il mapping è creato qualsiasi cosa arriva all'ip pubblico e porta assegnato viene inoltrato all'ip privato mappato

            ![](./res/fcc.png)

        - Restricted-cone: tutte le richieste fatte da un IP privato vengono mappate su un certo IP pubblico e porta; in questo caso un host remoto può parlare con l'host privato solo se è stato contattato almeno una volta (anche se il pacchetto non è stato ricevuto)

            ![](./res/rcc.png)

        - Port-restricted-cone: come restricted-cone ma un host remoto può parlare con l'host privato solo se è stato contattato a sua volta da questo su una porta e un indirizzo ben definiti usando questi ultimi

            ![](./res/prcc.png)

        - Symmetric: per la comunicazione verso l'esterno viene scelta una porta random, per le comunicazioni in ingresso l'host remoto può parlare con un host privato solo se l'host privato ha iniziato la comunicazione
            - L'unico modo per bypassare sto NAT è usare un relay
    - Connettersi ad un host nattato dall'esterno
        - Scenario
            - Due host nattati devono comunicare mediante un terzo host tramite autenticazione con credenziali condivise che si sono scambiati tramite il protocollo SIP (session initiation protocol) utilizzando il terzo host
            - La comunicazione verrà, una volta iniziata, delegata completamente agli host in modo che questi parlino tra loro senza un terzo
            - SIP
                - Registrazione

                    ![](./res/sipreg.png)

                - Connessione base

                    ![](./res/sipcon.png)

                - Ma se i due host sono nattati non riescono lo stesso a parlare

                    ![](./res/voip.png)

                - Bisogna usare un terzo server, il rendezvous

                    ![](./res/rendezvous.png)

        - Metodi
            - STUN (connection reversal): scopre le porte e gli IP da usare per la comunicazione che verranno poi usate direttamente dagli host creando una connessione diretta
            - TURN (relay): il server TURN fa da intermediario per la comunicazione fra i due host
            - ICE: STUN + TURN
                - WebRTC: connessione fra due browser

                    ![](./res/wrtc.png)

## Laboratorio

### Parte 1 - Active Directory

- Dominio Windows: gruppo LOGICO di computer che condividono un database di directory centralizzato (Domain Controller); questo database contiene gli account utente e delle informazioni sulle risorse appartenenti al dominio come ad esempio le stampanti e le condivisioni di file. All’interno di un dominio ad un utente può essere garantito o negato l’accesso a risorse utilizzando un unico username e password
    - Domain Controller: computer server che risponde alle richieste di autenticazione all’interno del dominio; sono paritari, cioè ognuno di essi ha una copia del database in lettura/scrittura. Ognuna di queste copie viene poi sincronizzata attraverso appositi protocolli
    - Directory Service: programma o insieme di programmi che provvedono ad organizzare, gestire e memorizzare informazioni e risorse centralizzate e condivise all'interno di reti di computer, rese disponibili agli utenti tramite la rete stessa, fornendo anche un controllo degli accessi sull'utilizzo delle stesse utile al lavoro dell'amministratore di sistema
        - Struttura dei dati (simile a database)
            - Organizzato in struttura gerarchica ad albero
            - Ottimizzato in lettura
            - Non gestisce le transazioni
            ```
            dn: cn=John Doe, dc=example, dc=com
            cn: John Doe
            givenName: John
            sn: Doe
            telephoneNumber: +1 888 555 6789
            telephoneNumber: +1 888 555 1232
            mail: john@example.com
            manager: cn=Barbara Doe, dc=example, dc=com
            objectClass: inetOrgPerson
            objectClass: organizationalPerson
            objectClass: personobject
            Class: top
            ```

            ![](./res/stadat.png)

        - Schema: insieme di definizioni e vincoli riguardanti la struttura del Directory Information Tree (DIT). Contiene la lista delle classi e degli attributi delle classi di oggetti definiti all’interno della directory. Ricorda lo schema ER di un database
- Active Directory: insieme di protocolli di rete in cui il principale di essi è un servizio di directory che lavora in simbiosi con protocolli di autenticazione e naming
    - Protocolli
        - LDAP: Lightweight Directory Access Protocol (protocollo di accesso a un servizio di directory)
            - Definisce il protocollo per lo scambio di informazioni della directory fra client e server
            - È possibile "navigare" all’intento dei dati della directory attraverso appositi strumenti chiamatiLDAP Browser
            - È possibile interrogare le directory attraverso delle query LDAP
        - Kerberos: protocollo di rete che permette l’autenticazione degli utenti e non l’autorizzazione che invece viene gestita o dalla directory o dal file system
        - NTP: Network Time Protocol
            - Protocollo che serve a sincronizzare gli orologi dei vari computer attraverso una rete a commutazione di pacchetto, quindi con latenze non predicibili
        - DNS: Domain Name System
            - Serve a "risolvere" i nomi di domini in indirizzi IP e viceversa
            - Attraverso questo sistema è possibile collegarsi ad un qualsiasi computer in rete senza doversi ricordare il suo indirizzo IP, ma un piu semplice nome logico, ad esempio www.unibo.it -> 137.204.24.35
    - Struttura
        - Domini, alberi, foreste
            - Dominio: gruppo LOGICO di computer che condividono un database di directory centralizzato
            - Albero: struttura di domini che condividono uno spazio dei nomi contiguo
            - Foresta: insieme di Alberi che non condividono uno spazio dei nomi contiguo ma condividono il global catalog, lo schema e la configurazione della directory. Ogni albero ha il suo dominio Radice ed il primo domino Radice creato è anche il Dominio "Radice della Foresta" ("ForestRoot Domain"): il suo nome identifica tutta la Foresta
        - Organizational Units: contenitore che ci permette di organizzare gli oggetti per meglio rappresentare la struttura della nostra organizzazione
            - Non hanno uno spazio dei nomi separato, quindi ad esempio due utenti in diverse OU NON possono avere lo stesso username
            - All’interno delle OU vengono inseriti i computer ai quali vogliamo vengano applicati particolari settaggi a seconda della OU a cui appartengono. Questo viene realizzato attraverso le Group Policy
        - Siti: raggruppamenti fisici e non logici di computer, spesso legati da una stessa sottorete IP
            - La definizione dei siti è indipendente dalla struttura dei domini e delle OU (un sito può apparire in diversi domini e diversi domini possono apparire in un sito)
        - Partizioni: le informazioni all’interno di AD sono logicamente suddivise in partizioni
            - Schema Partition: c’è una sola Schema Partition nella foresta ed è archiviata in ogni DC della foresta stessa. Contiene la definizione degli oggetti e le regole per la loro creazione e gestione. Viene replicata in ogni DC della foresta
            - Configuration Partition: esiste una sola Configuration che viene replicata su tutti i DC della foresta. Contiene la topologia della foresta, compresi i DC e i siti
            - Domain Partition: esistono molte Domain Partition all’interno della foresta, una per ogni dominio. Queste vengono memorizzate in tutti i DC del dominio. Contengo le informazioni sugli utenti, sui gruppi, i computers e le OU del dominio
            - Application Partition: qui vengono memorizzate le informazioni delle applicazioni di Active Directory. Ad esempio qui sono memorizzate le informazioni del DNS nel caso venga utilizzato il DNS di Microsoft
        - Global Catalog
            - Un Domain Controller configurato come Global Catalog conserva tutte le informazioni relative al suo dominio più le informazioni relative agli altri domini della foresta
            - Per ogni Sito della Foresta è preferibile avere almeno un Global Catalog. Le richieste in questo modo sono fatte al Global Catalog senza la necessità di interrogare Domain Controller al di fuori del proprio sito
            - Funzioni
                - Ricerche
                - Logon dell’utente e appartenenza ai Gruppi Universali (accedo a vari servizi mediante il Global Catalog)
                - UPN: quando un utente si autentica usando l’UPN (User PrincipalName) il Global Catalog si occupa di capire a quale dominio l’account appartiene. Se ad esempio l’account mario.rossi@unibo.it appartiene al dominio personale.dir.unibo.it allora il Global Catalog comunicherà al client di contattare un domain controller di quel dominio
        - Ruoli FSMO (FlexibleSingle Master Operation)
            - In Active Directroy, i DC sono paritetici, quindi le modifiche agli oggetti possono essere fatte su uno qualunque dei DC, questa funzionalità viene detta multimaster update.
            - Gli eventuali conflitti vengono gestiti attraverso un processo che viene chiamato conflict resolution, che in pratica fa "vincere" l’ultima modifica fatta all'oggetto
            - Ci sono dei casi invece in cui è meglio prevenire eventuali conflitti, e qui entra in gioco l’operation master, cioè l’unico DC del dominio o della foresta che può effettuare determinate modifiche (tale ruolo è possibile trasferirlo da un DC all’altro)
            - Suddivisione
                - Per-domains
                    - PDC Emulator: è il master per la sincronizzazione degli orologi, gestisce i cambi di password degli utenti del dominio
                    - RID Master: gestisce i Relative ID degli oggetti creati ed è responsabile dello spostamento di un oggetto da un dominio ad un altro
                    - Infrastructure Master: si preoccupa che il riferimento di oggetti fra domini sia consistente
                - Per-forest
                    - Schema Master: gestisce i cambiamento allo schema della foresta e la propagazione delle modifiche agli altri DC
                    - Domain Naming Master: si occupa dell’aggiunta o rimozione di domini da una foresta
        - Relazioni di Trust: è una relazione stabilita fra 2 domini che permette agli utenti di un dominio, di essere riconosciuti da un altro dominio
            - Permette agli utenti di poter accedere alle risorse di altri domini ed agli amministratori di potere gestire i permessi di accesso alle risorse da parte di utenti di altri domini
            - Tipi
                - Unidirezionali
                - Bidirezionali (default in stesso albero)
                - Transitivi (default in stesso albero)
                - Intransitivi
            - Attraverso una relazione di trust che unisce due alberi di AD, si forma una foresta; questo tipo di relazione deve essere esplicita
        - Replicazione all'interno del dominio: quando viene fatta una modifica, assieme ai dati della modifica viene registrato un timestamp; la modifica con timestamp più alto (quindi la più recente) vince sull’altra
        - Deleghe: l’amministratore di dominio può concedere la delega di amministrazione dei rami di Active Directory agli utenti cui ritiene più opportuno con diritti diversi per ognuno
        - Group Policy: configurazioni che vengono applicate ai computer e/o agli utenti appartenenti ad un dominio
            - Contenute in un Group Policy Object
            - Possono essere applicate all’intero dominio, ai siti o alle singole OU
            - Vengono propagate per tutto i rami della OU o del dominio
            - Ordine applicazione
                1. Locale
                2. Sito
                3. Dominio
                4. OU
            - In caso di conflitto "vince" la policy della OU; l’amministratore di dominio può però far prevalere una policy di dominio
- Tutorials (pdfs)
    - [Windows](./pdfs/win/)
    - [Linux](./pdfs/ActiveDirectoryLinux%202019.pdf)

### Parte 2 - Docker

- Comandi base
    - Pulla ed esegui un immagine: `docker run $nomeimmagine`
        - `--rm`: quando il container viene stoppato si cancella automaticamente
    - Cerca un immagine: `docker search $nomeimmagine`
    - Pulla un immagine: `docker pull $nomeimmagine`
    - Lista immagini: `docker images`
    - Elimina immagine: `docker rmi $nomeimmagine`
    - Esegui immagine e ottieni shell: `docker run -it $nomeimmagine`
        - Specifica il nome del container: `--name $name`
    - Elimina container: `docker rm $nomecontainer` oppure `docker rm $id`
    - Lista container (default attivi): `docker ps`
        - `-a`: attivi e non attivi
        - `-l`: ultimo creato
        - `-f status=$stato`: quelli che hanno stato `$stato`
        - `-q`: mostra solo gli id
    - Stop container: `docker stop $id` oppure `docker stop $nomecontainer`
    - Start container: `docker start $id` oppure `docker start $nomecontainer`
        - `-ia`: riagganciati alla bash
    - I cambiamenti apportati ad un container possono essere committati in un'immagine docker: `docker commit -m "$commitmessage" -a "$author" $containerid $username/$destinationdockerimage`
    - Login dockerhub: `docker login -u $username`
    - Push docker image: `docker push $username/$destinationdockerimage`
    - Eseguire uno specifico comando in un container: `docker run -it --name $containername $imagename $commandtoexecute $commandargs`
    - Definire variabile d'ambiente in un container: `-e "VARNAME=VARVALUE"`
    - Container in background: `-d -it` o `-d -i`
    - Esegui un comando interattivamente in un container in esecuzione in background: `docker exec -it -u $username:$group -w $workdir $container $commandtoexecute $commandargs`
    - Esegui un comando in background in un container in esecuzione in background: `docker exec -d -u $username:$group -w $workdir $container $commandtoexecute $commandargs`
    - Attachati ad una shell in esecuzione su un container  in background: `docker attach $containername`
    - Detachati da un container: `CTRL+p CTRL+q`
- Networking
    - Driver: `bridge, host, overlay, macvlan, none, custom network plugins`
        - `bridge` (default) -> può comunicare all'esterno e con gli altri container che fanno parte della stessa rete bridge
        - `host`: il container si trova sulla stessa rete della macchina host
        - `overlay`: crea una rete che connette vari container (usato in swarm, dove creo dei cluster di container)
        - `macvlan`: assegno un mac address al container che quindi risulta un dispositivo fisico
        - `none`: no rete
    - `--network`: definisci il network driver
        - `--ip`: definisic ip
        - `--hostname`: definisci hostname (default è il nome)
    - `docker network connect`: connetti un container ad una rete
        - `--alias`: network alias addizionale per il container
        - `--ip`: definisici ip
    - DNS
        - Viene adottata di default la configurazione del Docker deamon, includendo (`/etc/hosts` e `/etc/resolv.conf`)
        - `--dns`: indirizzo ip del server dns (possono essercene vari)
- Condivisione file con host
    - I container vivono solo fino a quando non li stoppo e rimuovo, per qesto per salvare i file conviene montare un volume (folder condivisa con la macchina host)
        - `-v $HostDirectory:$ContainerDirectory`
        - `chmod 666 $file`: permetti che la docker possa modificare il contenuto dei file montati
    - Pubblicare porte su un container
        - `-p $hostport:$containerport/$protocol`: la porta `$hostport` sull'host punterà alla porta `$containerport` sul container con protocollo `$protocol`
        - `-p` può essere specificato più volte
- Dockerfile
    - "Script" che vengono eseguiti per costruire l'immagine di un container
    - Comandi
        - `FROM`: nome dell'immagine di base da cui si parte per costruire il container (deve essere in cima al dockerfile)
        - `MAINTAINER`: creatore dell'immagine (opzionale)
        - `RUN`: usato per eseguire un comando all'interno del container durante l'esecuzione della build
        - `COPY` e `ADD`: copia file da host a container (`ADD` anche da rete)
        - `ENV`: definisci una variabile d'ambiente nel container
        - `CMD` e `ENTRYPOINT`: comando da eseguire quando il container parte
        - `WORKDIR`:
        - `USER`: user/uid del container creato mediante l'immagine
        - `VOLUME`: abilita l'utilizzo di volumi
    - Esempio
        ```dockerfile
        # Dockerfile
        FROM ubuntu:latest
        MAINTAINER chi lo sa
        ENV MYHOME=/home/

        RUN apt-get  update
        RUN apt-get -y install net-tools
        RUN apt-get -y install netcat
        ADD hello.sh /home/hello.sh
        ADD hello1.sh /home/hello1.sh
        RUN chmod 777 ${MYHOME}/hello.sh
        RUN chmod 777 ${MYHOME}/hello1.sh

        WORKDIR /home
        CMD ./hello.sh
        BUILD
            dockerbuild -t "simple_netcat:dockerfile" .
        VERIFY
            docker images "simple_netcat:dockerfile"
        RUN
            docker run simple_netcat:dockerfile
        ```
- User-defined networks
    - `docker inspect $container`: stampa informazioni su elementi docker a basso livello (anche relative alla rete)
        - `--format`: utile per estrapolare informazioni mirate
        - `docker inspect $container`: identico ma solo per la rete
    - Netfilter e iptables
        - Transito dei pacchetti su linux
            1. Pacchetto entra (anche da un processo locale)
            2. Viene deciso se deve essere forwardato o mantenuto (all'interno) in base a che pacchetto è
            3. Se il pacchetto deve essere forwardato poi verrà decisa l'interfaccia su cui forwardare il pacchetto, altrimenti viene mandato alla coda di input

            ![](./res/networktab.png)

        - Netfilter: parte del kernel che definisce i percorsi dei pacchetti
        - Iptables: cli tool che comanda Netfilter
            - `iptables --list`: visualizzazione delle regole presenti nella tabelle du default
            - `iptables -t mangle --list -n`: visualizzazione delle regole presenti nella tabella mangle
            - `-F`: eliminazione di tutte le regole presenti nella tabella specificata con `-t`
            - `-t mangle -AOUTPUT -p icmp -j DROP`: aggiungi regola (`-A`)
            - `-t mangle -D OUTPUT -p icmp -j DROP`: rimuovi regola (`-D`)
            - ` --to-source`: definisci la sorgente dei pacchetti
            - `iptables -t nat -A POSTROUTING -p tcp -o eth0 -j SNAT --to-source 194.236.50.155-194.236.50.160:1024-32000`: intercettazione di pacchetti contenenti TCP che escono dall'interfaccia eth0 a cui vengono modificati l'indirizzo IP e la porta mittenti mettendo quelli indicati nell'intervallo `--to-source`; anche i pacchetti della stesse connessioni TCP che seguono il percorso al contrario (cioè entrano da eth0)vengono nattate al contrario
    - Gestisci le network dei container (sotto usa delle regole di iptables)
        - `docker network create $networkname`: crea la network `networkname`
            - Di default è bridge
            - Ogni container in una network può pingare gli altri mediante il container name a causa del DNS embeddato in docker
            - `-d`: definisci il driver (bridge, nat...)
            - `-o`: il prossimo argomento è una opzione
                - Esempio: `dockernetwork create -o"com.docker.network.bridge.host_binding_ipv4"="172.23.0.1"my-network`
            - `--subnet=`: definsci subnet
            - `--gateway=`: definisci indirizzo gateway
            - `--aux-address="my-router=192.168.1.3"`: aggiungi un indirizzo ausiliario
            - `--ip-masq`: l'indirizzo sorgente del pacchetto che esce da quella rete viene sostituito con l'indirizzo del gateway
                - Opzione: `com.docker.network.bridge.enable_ip_masquerade`
            - `--icc`: abilita/disabilita le comunicazioni inter-container
                - Opzione: `com.docker.network.bridge.enable_icc`
            - `--ip`: binda un container su un interfaccia fisica
                - Opzione: `com.docker.network.bridge.host_binding_ipv4`
            - `--internal`: network non raggiungibile dall'esterno
        - `docker network connect $networkname $containername`: connetti `containername` a `networkname`
        - `docker network ls`: lista tutte le network
        - `docker network rm $networkname`: cancella la rete `networkname`
        - `docker network disconnect`: disconnetti un container da una network
        - `docker network inspect $networkname`: vedi informazioni sulla rete `networkname`
- Docker compose
    - Viene utilizzato per automatizzare il deploy delle docker
    - Esempio
        - Creiamo una sorta di contatore di accessi web utilizzando Flask e Redis su due docker diverse
        - `app.py`
            ```python
            from flask import Flask
            from redis import Redis

            app = Flask(__name__)
            redis = Redis(host = "redis") # IP della macchina redis

            @app.route("/")
            def hello():
                visits = redis.incr('counter')
                html = "<h3>HelloWorld!</h3>" \
                    "<b>Visits:</b> {visits}" \
                    "<br/>"
                returnhtml.format(visits=visits)

            if__name__ == "__main__":
                app.run(host="0.0.0.0", port=80)
            ```
        - `requirements.txt`
            ```
            Flask
            Redis
            ```
        - Dockerfile macchina lato web
            ```dockerfile
            FROM python:3.7
            WORKDIR /app
            ADD requirements.txt /app/requirements.txt
            RUN pip install -r requirements.txt
            ADD app.py /app/app.py
            EXPOSE 80
            CMD ["python", "app.py"]
            ```
        - `docker-compose.yml`
            ```yaml
            web:
                build: .
                dockerfile: Dockerfile
                links:
                    -redis
                ports:
                    -"8888:80"
            redis:
                image: redis
            ```
        - `docker-compose -f docker-compose.yml build`: crea i container (fai il build dell'environment)
        - `docker-compose -f docker-compose.yml up -d`: esegui i container buildati in daemon mode `-d`
        - `docker-compose -f docker-compose.yml stop`: ferma i container
    - Struttura docker compose
        - `version`: versione del compose file
        - `services`: servizi dell'applicazione, i container
            - `build`: dettagli della build
                - `context`
                - `args`: argomenti di build
                - `dockerfile`: dockerfile location
                - `target`
            - `runtime`
                - `image`: nome dell'immagine
                - `entrypoint`
                - `container_name`: nome del container (non è possibile avviare più volte in questo caso)
                - `environment`: passa variabili ambientali
                - `env_file`: file contenente le variabili d'ambiente
            - `deploy`: dedicato a docker swarm
        - `networks`: setup delle reti
        - `volumes`: volumi dati utilizzati
        - Esempio
            ```yaml
            version: '3'
            services:
                app:
                    build:
                        context: .
                        args:
                            - IMAGE_VERSION=3.7.0-alpine3.8
                        image: takacsmark/flask-redis:1.0
                        environment:
                            - FLASK_ENV=development
                        ports:
                            - 80:5000
                        networks:
                            - mynet
                redis:
                    image: redis:4.0.11-alpine
                    env_file: env.txt
                    networks:
                        - mynet
                    volumes:
                        - mydata:/data
            networks:
                mynet
            volumes:
                mydata
            ```
    - Comandi
        - `docker-compose --help`: help
        - `docker-compose build`: build
        - `docker-compose up`: se non sono state buildate builda i container e tirali su
        - `docker-compose up --build`: ribuilda i container e tirali su
        - `docker-compose ps`: lista i container dell'applicazione
        - `docker-compose logs`: printa tutti i log
            - `$servicename`: printa i log di `servicename`
        - `docker-compose top`: vedi i processi di un applicativo
        - `docker-compose down`: stoppa i container dell'applicativo
            - `--rmi all`: cancella anche tutti i container e le immagini
        - `docker-compose run $service $commandname $arguments`: esegui un comando con degli argomenti su un servizio; se questo servizio non esiste buildalo e tiralo su
            - `run` non mappa le porte al contrario di `up`, se le vuoi mappare usa `run -p`
- Docker swarm
    - Uno swarm è composto da uno o più nodi (macchine fisiche o virtuali); un nodo specifico è il manager del cluster, le altre sono dei worker
    - Lo stack è un gruppo di servizi che realizzano un'applicazione
    - Un servizio può essere deployato varie volte sui worker, creando così più copie dello stesso servizio; ogni istanza di container è detta task
    - In sostanza uno stack è un set di task (containers) raggruppati in servizi che runnano su un cluster di nodi worker coordinati da un nodo manager che distribuisce il carico fra i worker

    ![](./res/swarm.png)

    - Esempio di struttura

        ![](./res/esswarm.png)

    - [Esempio di utilizzo di swarm](./pdfs/swarm/)
    - Comandi base
        - `docker swarm init --advertise-addr $addr`: la macchina su cui eseguo questo comando sarà il master; `addr` è l'indirizzo del manager sull'interfaccia di rete condivisa con i worker
        - `docker service create --name $registername --publish published=$port,target=$port registry:2`: creo sul manager il registry (quella parte del manager che contiene le immagini da mettere a disposizione dei worker)
        - `docker service ls`: vedi tutti i servizi in esecuzione
        - `docker service rm $servicename`: cancella servizio
        - `docker swarm join --token $token $ipmanager:$portmanager`: la macchina su cui eseguo questo comando sarà un worker
        - `docker-compose build`: builda l'app
        - `docker-compose push`: pusha l'app sul registry
        - `docker stack deploy --compose-file $composefile $stackname`: crea lo stack
        - `docker stack ls`: lista gli stack
        - `docker stack services $stackname`: visualizza i servizi dello stack E LE LORO REPLICHE
        - `docker swarm leave`: fa uscire il nodo su cui viene eseguito dallo swarm
            - `--force`: esegui su manager
        - `docker stack rm $stackname`: elimina lo stack
        - `docker service rm $servicename`: elimina il servizio da tutti i worker

### Parte 3 - Kubernetes

- Concetto
    - Kubernetes è una architettura software con una struttura client-server che esegue su un cluster in cui dispiega la sua applicazione. L'interfacciamento dell'amministratore di kubernetes avviene tipicamente mediante un client (`kubectl`)
    - Componenti
        - NODES: host del cluster
        - PODS: gruppi di container che devono lavorare cooperando (si trovano sullo stesso host, anche più contemporaneamente sullo stesso nodo)
            - Condividere uno stesso indirizzo IP
            - Avere lo stesso spazio delle porte di protocollo (due container in uno stesso pod non possono attestarsi sulla stessa porta di protocollo)
            - Comunicare tra di loro mediante il localhost (invece due container contenuti in pods diversi per comunicare tra di loro devono specificare l'indirizzo IP del pod a cui appartiene l'altro container)
            - Interagire  tra  di  loro  mediante  le  classiche Inter  Process Communications (IPC)
        - LABELS: stringa che identifica uno o più pods; tutti i pods a  cui è stata assegnata una stringa con stesso nome e stesso valore possono essere considerati equivalenti per fornire una funzionalità. Questi pods equivalenti funzioneranno come delle repliche per fornire quella funzionalità (es: `role=production`)
        - SELECTORS: filtro tramite il quale possono essere cercati i pods mediante la loro label
        - DEPLOYMENTS: gruppo di pods replicati a cui può essere richiesto uno stesso servizio (bilanciamento di carico)
        - SERVICES: interfaccia verso l'esterno di un gruppo di deployments, si occupa lui del bilanciamento di carico

        ![](./res/kubestruc.png)

        - Note
            - È buona norma tirare su prima i pods del service
            - Un pod X può dipendere da un altro Y, in questo caso quando a X viene mandata una richiesta questa viene inoltrata anche a Y mediante un secondo service; il bilanciamento di carico può quindi essere "stratificato" e "ricorsivo"
- Architettura
    - L'architettura di kubernetes prevede che i nodi del cluster siano connessi da una rete di tipo overlay e che tra i nodi del cluster ci sia un nodo che svolga il ruolo di master

        ![](./res/kubar.png)

- Come si usa
    - Modalità di dispiegamento
        - Cloud: vari nodi in cloud
            - Dashboard integrata in cloud
        - On premise: su un cluster di nodi locale
            - FOSS: `RancherOS`
        - Singolo nodo: per il testing
            - `Minikube`
    - [Tutorial installazione e quick start (da 4 in poi)](./pdfs/kubernetes/Kubernetes_Introduction.pdf)
    - [Esempio di utilizzo](./pdfs/kubernetes/Kubernetes_Esempio1.pdf)
        - [File relativi all'esempio](./pdfs/kubernetes/guestbook.tgz)
