# Riassunto IOT

[[_TOC_]]

### Parte 1

- Sistemi embedded: sistemi di elaborazione special-purpose - ovvero che svolgono una specifica funzione o compito - incorporati (embedded) in sistemi o dispositivi elettronici di diverse dimensioni; il compito tipicamente richiede di  interagire con il mondo fisico mediante opportuni sensori e attuatori
- Funzionalità specifica: un sistema embedded tipicamente esegue uno specifico programma ripetutamente
- Caratteristiche
    - Risorse limitate: per questo la progettazione è orientata all'efficienza (energy efficient, code-size efficient, run-time efficient, weight efficient, cost efficient)
    - Affidabilità
        - Dependability, reliability, availability: alta probabilità di corretto e continuo funzionamento
        - Safety and security: alta probabilità che non rechi danni agli utenti o all’ambiente in cui è immerso e obtemperi norme di sicurezza e privacy 
    - Reattività: spesso i sistemi embedded sono utilizzati in contesti dove devono prontamente reagire a stimoli che provengono dall’ambiente (fisico) e quindi eseguire elaborazioni ed eventualmente azioni in real-time, senza ritardi
- Cyber-physical Systems: sistemi che integrano computazione con processi fisici
    - Gestione del tempo: non solo performance, ma correttezza
    - Concorrenza: processi fisici sono tipicamente in parallelo 
    - Reattività, eventi asincroni
    - Parti
        - Fisica
        - Computazionale/embedded
        - Rete
- Architettura
  
    ![alt text](./res/arch-hl.png "Architettura alto livello")

    ![alt text](./res/arch-hw.png "Architettura hardware")

    - Processori
        - General-purpose (=> software): processori che hanno una architettura e un insieme di istruzioni (instruction set architecture, ISA) predefinito  e lo specifico comportamento è definito dal programma (software) in esecuzione
        - Single-purpose: circuiti digitali progettati per implementare la specifica funzionalità o programma (ASIC = Application-Specific Integrated Circuit)
            - Full-Custom/VLSI: l’implementazione avviene progettando l’intero processore in modo custom, ottimizzando tutti i livelli
            - Semi-custom: ASIC, in questo caso si parte da un certo insieme di livelli parzialmente o totalmente costruiti, implementandovi sopra il processore 
            - PLD (programmable logic device): tutti i livelli sono già presenti, quindi è possibile acquistare un IC per poi “programmarlo” opportunamente; i livelli implementano circuiti programmabili, dove la programmability è data dalla possibilità di creare/distruggere connessioni...
            - PLA (Programmable Logic Array): array programmabili di porte AND e array programmabili a porte OR
            - PAL (Programmable Array Logic): solo un tipo di array
            - FPGA (Field Programmable Gate Array): circuiti integrati con funzionalità programmabili via software, implementazione di funzioni logiche anche molto complesse (milioni di porte logiche), elevata scalabilità
        - Application-Specific Processors (ASIP): una via intermedia, processori programmabili, ottimizzati per una specifica classe di applicazioni avente caratteristiche comuni (anche in questo caso la logica viene specificata a livello software)
    - Sensori e attuatori
        - Sensori: dispositivi trasduttori che permettono di misurare una certo fenomeno fisico o rilevare e quantificare una concentrazione  chimica
            - Analogici: la grandezza elettrica prodotta in uscita - tensione o corrente - varia con continuità in risposta alla variazione della grandezza misurata (nel micro-controllore è tipicamente incluso un convertitore analogico digitale, ADC)
            - Digitali: due soli valori o insieme discreto di valori
        - Attuatori: dispositivi che producono un qualche effetto misurabile sull’ambiente, a partire da una specifica condizione o evento chiamato trigger
    - BUS e protocolli di comunicazione
        - Trasmissione seriale: parole multi-bit vengono inviate sequenzialmente come serie di bit 
        - Tipi: vari ma il più supportato e datato dei protocolli è UART/USART e altre che approfondiremo poi
            - UART (Universal Asynchronous Receiver-Transmitter): consente di convertire flussi di bit di dati da un formato parallelo a un formato seriale asincrono o viceversa
            - USART (Universal Synchronous/Asynchronous Receiver-Transmitter): estende il protocollo con la trasmissione di un segnale di clock per la sincronizzazione
    - Interfacce e tecnologie di comunicazione, wireless (Bluetooth and Bluetooth Low-Energy (BLE), ZigBee, Z-Wave, LoRoWan, Wifi...)
- Tecnologie
    - Micro-controllori (MCU): dispositivi elettronici, nati come evoluzione alternativa ai microprocessori integrando su singolo chip un sistema di componenti che permette di avere la massima autosufficienza funzionale per applicazioni embedded (8, 16, 32 bit); in generale i MCU sono meno performanti dei microprocessori
        - Single-board Micro-controller: soluzioni che incorporano in unica scheda il micro-controllore e tutta la circuiteria necessaria per eseguire dei compiti di controllo (Arduino)
    - SOC (System-on-a-chip): chip stesso che incorpora un sistema completo, che include CPU, memoria, controllori... tipicamente usati per creare delle single-board CPU (es: BROADCOM BCM2837 64bit ARMv8 Cortex A53  Quad Core (1.2Ghz), usato in Raspberry Pi 3)
    - Quale tecnologia usare (tendenza: MCU, Arduino e SOC, RPI)

        ![alt text](./res/quale-usare.png "Quale tecnologia usare")

- Progettazione sistemi embedded
    - Processo iterativo
        - Modeling: processo finalizzato ad ottenere un’approfondita comprensione o conoscenza relativamente al sistema da costruire; tale conoscenza è rappresentata da modelli, che sono il risultato di questo processo e rappresentano cosa il sistema deve fare
            - Modello: descrizione degli aspetti rilevanti del sistema, utili per la comprensione di proprietà del sistema stesso e della sua dinamica in particolare; un modello di un sistema embedded include tutte le tre parti (physical, computing, networking)
        - Design: processo finalizzato alla creazione degli artefatti tecnologici che rappresentano come il sistema fa quello che deve fare
            - Scegliere la tecnologia e architettura HW + scegliere l’architettura software più appropriata
            - Top-down o Bottom-up
                - Requisiti
                    - Funzionali: funzioni che deve svolgere il sistema
                    - Non-funzionali: performance, costo, dimensioni fisiche, peso, consumo energia e potenza...
                - Specifiche: descrizione più precisa e rigorosa dei requisiti, serve come contratto fra cliente e progettisti (UML)
                - Progettazione: descrive come il sistema implementa le funzioni descritte nella specifiche
                    - Architettura: piano della struttura complessiva del sistema, che verrà usata poi per guidare lo sviluppo dei componenti, descrive quindi quali componenti servono e come essi interagiscono
                        - Diagrammi a blocchi hardware e software
                    - Componenti: "costruzione" dei componenti (che possono anche essere ready-made)
                - Integrazione del sistema: integrazione dei componenti seguendo l’architettura (parte critica del design)
                - Sviluppo e programmazione
                - Deployment, debugging e testing
        - Analysis: processo finalizzato ad ottenere un’approfondita comprensione e conoscenza del comportamento del sistema; specifica perché un sistema fa quello che fa

### Parte 2

- Micro-controllore
    - Elementi
        - CPU
            - Architettura
                - Von Neumann: fetch, decode, execute
                - Harvard: istruzioni e dati sono memorizzate in memorie fisicamente separate (codice e dati possono essere letti/scritti parallelamente)
                    - Codice in FLASH memory
                    - Dati in SRAM
            - Funzionamento
                - Ha un suo Instruction Set Architecture (ISA)
                - Registri
                    - Programmabili (GPR)
                    - Dedicati (SFR)
                        - Program counter (PC): indirizzo della prossima istruzione da eseguire
                        - Program Status Word (PS): informazioni sullo stato del processore
        - Unità di memoria
            - Tipi
                - Flash (volatile)
                - SRAM (volatile)
                - EEPROM (non volatile)
            - I registri general-purpose e di I/O sono mappati in memoria
        - Porte di Input/Output (GPIO)
            - General purpose: possono fungere sai da input che da output
            - Tipi di pin
                - Digitali: può assumere solo due valori, HIGH o LOW (1 o 0) 
                - Analogici: può assumere un qualsiasi valore reale all’interno di un certo range
            - Parametri di funzionamento
                - Tensione (Volt): da applicare in input, prodotta in output
                - Corrente (Ampere): che può ricevere (input) o che può fornire (output)
                - Presenza o meno di circuiti/resistori di pull-up: per fissare il valore di tensione anche quando il circuito attaccato al pin è aperto ed evitare malfunzionamenti dovuti al floating del segnale
            - Costituite da uno o più registri special purpose (SRF) che storano le info, di cui uno mantiene lo stato del pin a cui la porta è associata
            - Manipolare i pin via software
                - `pinMode()`: setta la direzione INPUT/OUTPUT di un pin
                - `digitalWrite()`: imposta il valore di un pin di OUTPUT
                - `digitalRead()`: legge il valore di un pin di INPUT
        - Convertitori analogico/digitali
            - Un segnale analogico su un pin può assumere un valore continuo all'interno di un certo range; per poter elaborare un segnale analogico da un sistema di elaborazione (che lavora solo con valori digitali), questo deve essere convertito. La conversione avviene mediante un componente detto convertitore ADC (Analog-to-Digital), che mappa il valore continuo in un valore discreto
        - Timers 
            - Contatore che viene incrementato a livello HW ad una certa frequenza (configurabile), di cui si può leggere il valore e al quale è possibile agganciare interruzioni per implementare computazioni event-driven (time-triggered)
            - Watch dog: contiene un timer in grado di eseguire un conteggio fino ad un certo valore, dopo il quale genera un segnale di output con cui resetta il circuito
                - Funzionamento: riceve (periodicamente) un segnale prima che arrivi alla soglia, con cui resetta il conteggio; se non riceve il segnale in tempo, allora significa che il microprocessore è entrato in una situazione critica (es: blocco) e resetta il circuito
            - Power management: modalità che comportano livelli diversi di risparmio energetico
        - Bus e protocolli
            - Comunicazione seriale (bit a bit)
                - Tipi
                    - Sincronismo
                        - Asincroni
                            - Non viene utilizzato alcun clock per la sincronizzazione 
                            - Due linee: di trasmissione e ricezione
                            - Aspetti da stabilire
                                - Baud rate: quanto velocemente i dati sono inviati sulla linea seriale (bits-per-second, bps)
                                - Data frame: ogni blocco di dati - tipicamente un byte - è inviato in forma di pacchetto (packet o frame) di bit. Questi frame sono creati aggiungendo un synchronization e parity bit ai dati inviati

                                    ![alt text](./res/dataframe.png "Schema data frame")

                                    - Il data chunk rappresenta le vere e proprie informazioni trasmesse; in ogni pacchetto si riescono ad inviare dai 5 ai 9 bit (8 solitamente)
                                    - Occorre specificare e accordarsi sull'endianness dei dati (default little endian)
                                    - I bit di sincronizzazione includono bit di start e stop, per segnare inizio e fine pacchetto
                                    - I bit di parità (opzionali) sono usati per avere una forma di low-level error checking
                        - Sincroni
                            - Per la sincronizzazione fra le parti che comunicano viene utilizzata una linea con un segnale di clock, che si affianca alla linea dove vengono trasmessi i dati
                            - Protocollo $`I^{2}C`$ (I2C)
                                - Architettura master-slave
                                    - Un I2C bus è controllato da un master device 
                                    - Contiene uno o più slave device che ricevono informazioni dal master
                                    - Più device condividono le stesse due linee di comunicazione 
                                        - Un clock signal (Serial Clock Line, SCL), che serve per sincronizzare la comunicazione
                                        - Una linea bidirezionale (Serial Data Line, SDA) per inviare e ricevere dati dal master agli slave
                                    - Funzionamento
                                        - Master invia start bit (broadcast a tutti gli slave)
                                        - Master invia l'id a 7 bit del dispositivo con cui vuole comunicare
                                        - Master invia read (0) o write (1) bit, a seconda che voglia scrivere dati nei registri del dispositivo o leggerli
                                        - Slave risponde con un ack (un ACK bit)
                                        - In write mode, il master invia un byte di informazioni alla volta e lo slave risponde con degli ACK; in read mode il master riceve 1 byte alla volta e invia un ACK dopo ogni byte 
                                        - Quando la comunicazione è finita, il master invia uno STOP bit
                            - Protocollo SPI
                                - Seriale full-duplex
                                - Master-slave
                                - Usa linee diverse per trasmettere e ricevere dati, e utilizza una linea per selezionare lo slave
                                - Funzionamento
                                    - Per comunicare ci sono 3 linee
                                        - Uno shared  clock signal (Shared Serial Clock, SCK) per sincronizzare la comunicazione (in Arduino pin 13)
                                        - Una linea Master Out Slave In (MOSI) per inviare i dati dal master allo slave (in Arduino pin 11)
                                        - Una linea Master In Slave Out (MISO) per inviare i dati dallo slave al master (in Arduino pin 12)
                                        - Una linea SS (Slave Select) per selezionare lo slave (in Arduino pin 10)
                                    - Multi-slave
                                        - Utilizzare più linee SS 
                                        - Utilizzare un collegamento daisy chain, in cui il master comunica con tutti gli slave simultaneamente
                    - "Duplexity"
                        - Un'interfaccia seriale dove entrambi i dispositivi possono inviare e ricevere dati può essere 
                            - Full-duplex: entrambi i dispositivi possono inviare e ricevere simultaneamente
                            - Half-duplex: i dispositivi a turno inviano e ricevono 
                        - Single-wire: alcuni bus seriali possono avere la necessità di un solo collegamento tra un dispositivo che invia e quello che riceve
                - Hardware
                    - 2 fili, uno invia e uno riceve
                    - 2 pin, un ricevitore (RX) e un trasmettitore (TX); quindi il pin RX di un dispositivo dovrebbe essere collegato a quello TX dell'altro dispositivo, e vice-versa
                    - UART (universal asynchronous receiver/transmitter): blocco circuitale che ha il compito di convertire i dati verso e dall'interfaccia seriale (agisce da intermediario fra l'interfaccia parallela e seriale)
            - Comunicazione parallela (più bit allo stesso tempo)
        - Oscillatore/clock 
        - Circuito di alimentazione
    - Programmazione
        - Mediante un sistema esterno (PC), scrivo e compilo il codice (in C/C++ con avr-gcc) e mediante un IDE (Arduino IDE) basato su Wire
        - Il binario viene mandato alla scheda 
        - Il software viene eseguito direttamente dal processore, non esiste il SO
    - Struttura eseguibile: super loop
        - setup(): prima esecuzione
        - loop(): per sempre
    - I/O e interruzioni
        - Il meccanismo delle interruzioni permette al micro-controllore (o meglio: al programma in esecuzione) di reagire ad eventi, evitando di fare polling
        - Le CPU mettono a disposizione uno o più pin, chiamati IRQ (Interrupt Request) e connessi ai GPIO, ove ricevere i segnali di interruzione; quando la CPU riceve una richiesta di interruzione, sospende l'esecuzione della sequenza di istruzioni, salva sullo stack  l'indirizzo della prossima istruzione che avrebbe eseguito  e quindi trasferisce il controllo alla interrupt routine corrispondente chiamata interrupt handler o interrupt service routine (ISR)
        - Programmare interrupt routine
            - `attachInterrupt(intNum, ISR, mode)`
                - Parametri
                    - intNum: numero interruzione (int)
                    - ISR: puntatore all’interrupt handler (tipo: void (*func)(void))
                    - mode: quando deve essere generata l’interruzione
                        - CHANGE/FALLING/RISING: ad ogni cambiamento logico (1 -> 0, 0 -> 1)
                        - LOW/HIGH: quando è 0 o 1 (logici)
            - Disabilitare interruzioni: mediante un bit nel registro di stato del processore
                - `noInterrupts()`
                - `interrupts()`
            - Design interrupt handler
                - Devono essere eseguiti in tempi brevi
                - Non possono mai bloccarsi o eseguire loop infiniti
                - Bassa interrupt latency (quantità di tempo che impiega il sistema per reagire ad una interruzione) 
                    - Nel caso in cui sia necessario eseguire compiti computazionali onerosi l'interrupt handler produce un task - in un buffer - che verrà successivamente eseguito ad esempio dal main loop
    - Esempio: Arduino UNO
        - MCU ATMega 328P (architettura Harvard)
            - AVR8: RISC, 8 bit codice, 16 bit dati, 16 MhZ
                - Registri
                    - 32 general-purpose ad 8 bit (R0–R31)
                    - Dedicati
                        - PC: 16 o 22 bit program counter
                        - SP: 8 o 16 bit stack pointer
                        - SREG: 8 bit status register
                            - C Carry flag: riporto, con istruzioni di sub
                            - Z Zero flag: settato quando un risultato aritmetico è 0 
                            - I Interrupt flag: settato quando le interruzioni sono abilitate
            - Flash memory: 32 KB (qui le istruzioni)
            - SRAM memory: 2 KB (qui lo stack), EEPROM: 1 KB (qui dati persistenti)
        - 14 pin digitali input/output (6 possono essere usati come uscite PWM)
            - Energia
                - Operano ad una tensione di 5V
                - Possono fornire o assorbire fino a 40mA di corrente
            - 3 porte da 8 bit l'una (B: pin 8..13, C: pin analogici, D: pin 0..7)
                - Registri
                    - Ogni porta è gestita da 3 registri (x è il nome della porta)
                        - DDRx: lettura/scrittura e contiene la direzione dei pin ad esso collegati (un bit a 0 => pin impostato come INPUT; un bit ad 1 => un pin impostato come OUTPUT)
                        - PORTx: questo registro è di lettura/scrittura e contiene lo stato dei pin, che cambia a seconda della direzione del pin
                            - Se il pin è impostato come INPUT, un bit ad 1 attiva la resistenza di PULL-UP, mentre un bit a 0 la disattiva
                            - Se il pin è impostato come OUTPUT, un bit ad 1 indica uno stato HIGH sul relativo pin, mentre un bit a 0 indica la presenza dello stato LOW sullo stesso pin 
                            - PINx: questo registro è di sola lettura e contiene, nel caso di un pin im4postato come INPUT, la lettura del segnale collegato al pin; 1 per un segnale alto (HIGH) e 0 per un segnale basso (LOW)
            - Funzioni specifiche
                - 0, 1: interfaccia seriale TTL
                - 2, 3: interruzioni
                    - Di default le interruzioni sono disabilitate quando l’handler è in esecuzione
                    - Non tutte le primitive di libreria funzionano se chiamate all’interno di un interrupt handler (non funzionano quelle funzioni il cui funzionamento si basa direttamente o indirettamente sull’uso di interruzioni)
                - 3, 5, 6, 9, 10, 11: PWM
                    - PWM (Pulse with modulation): tecnica di pilotaggio di dispositivi di output che permette di emulare in uscita un segnale analogico a partire dalla generazione di segnali digitali detti PWM
                        - Il segnale analogico di un certo valore V su un pin è “emulato” mediante un segnale periodico che si ottiene modulando  il duty cycle (percentuale di tempo che il segnale è ad 1 rispetto a quella in cui è a 0) di un’onda quadra, ovvero un segnale che passa ripetutamente da 0 ad 1
                - 10, 11, 12, 13: comunicazione SPI
                    - Pin
                        - 10: SS
                        - 11: MOSI
                        - 12: MISO
                        - 13: SCK
                    - SPI e Wiring
                        - Inizio sessione: `SPI.beginTransaction(settings)`
                            - Settings
                                - Massima velocità che il dispositivo può usare  
                                - Se i dati sono in formato MSB (Most Significant Bit) o LSB (Least Significant Bit) 
                                - Modalità relativa al sampling dei dati 
                                    - 4 modi di trasmissione, che specificando il fronte del clock sul quale i dati sono campionati o inviati (clock phase) e se il clock è idle quando vale HIGH e LOW (clock polarity)
                                        - `SPI_MODE0`: Pol = 0, Phase = 0
                                        - `SPI_MODE1`: Pol = 0, Phase = 1
                                        - `SPI_MODE2`: Pol = 1, Phase = 0
                                        - `SPI_MODE3`: Pol = 1, Phase = 1
                                    - Esempio: `SPI.beginTransaction(SPISettings(14000000, MSBFIRST, SPI_MODE0));`
                        - Trasferimento (invio e ricezione "simultanei")
                            - `receivedVal = SPI.transfer(val)`: val = byte da inviare sul bus
                            - `receivedVal16 = SPI.transfer16(val16)`: val16 = word da inviare nel bus
                            - `SPI.transfer(buffer, size)`: buffer = array di byte da inviare/ricevere
                        - Fine sessione: `SPI.endTransaction()`
                        - Interazione con uno slave
                            1. Si setta lo slave select pin a LOW (mediante digitalWrite sul pin SS)
                            2. Si chiama `SPI.transfer()` per trasferire i dati 
                            3. Si setta il pin SS a HIGH
                - 13: builtin led
        - 6 input analogici (solo input, porta C, A0-A5)
            - Energia
                - Operano ad una tensione di 5V
            - Ogni segnale analogico viene convertito in un valore a 10 bit, quindi da 0 a 1023 (funzione: `int analogRead(int PIN)`)
            - Funzioni specifiche
                - A4 e A5: I2C
        - Timer
            - 3 timer, ognuno dei quali ha un contatore che è incrementato ad ogni tick del clock del timer
                - timer0 e timer2 hanno i contatori a 8 bit
                - timer1 ha il contatore a 16 bit
            - Interruzioni
                - CTC (Clear Timer on Compare Match): le interruzioni del timer sono generate quando il contatore raggiunge un certo specifico valore (memorizzato nel compare match register); quando il contatore del timer raggiunge questo valore, resetta il conteggio al clock successivo e quindi riparte a contare
                    - I contatori sono incrementati a 16 MHz (per andare in overflow e ripartire ci mettono pochissimo)
                    - Questa frequenza può essere modulata specificando un valore detto prescaler (1, 8, 64, 256, 1024), che sostanzialmente funge da divisore della frequenza originaria
                    - `desired interrupt frequency (Hz) = 16,000,000 Hz / (prescaler * (compare match register + 1))`
                    - `compare match register = (16,000,000 Hz / (prescaler * desired interrupt freq)) - 1 (1 Hz = 1 s, capiscimi)`
                        - `timer 0 e 2: compare match register < 256`
                        - `timer 1: compare match register < 65526`
            - Registri
                - I registri dei timer che memorizzano la configurazione sono due e a 8 bit (x = numero del timer 1..3)
                    - TCCRxA
                    - TCCRxB (la modalità CTC si abilita attivando uno specifico bit di questo registro)
                        - 3 bit meno significativi (determinano la configurazione del clock del timer)
                            - CSx2 
                            - CSx1
                            - CSx0 
                - Il compare and match register è etichettato con OCRxA
            - Libreria ad alto livello: TimerOne
            - PWM: i timer vengono utilizzati anche per realizzare le uscite PWM
            - Siccome usiamo i timer per tante cose ci possono essere dei prolemi (guarda la documentazione per maggior informazioni)
            - Power management
                - 5 modalità di sleep (con risparmio crescente)
                    - Idle Mode
                    - ADC Noise Reduction Mode
                    - Power-save Mode
                    - Standby Mode
                    - Power-down Mode
                - Esempio
                    ```c
                    void sleepNow(){
                        set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here
                        sleep_enable();    // enables the sleep bit in the mcucr register
                                        // so sleep is possible. just a safety pin
                        
                        attachInterrupt(0,wakeUpNow, LOW); // use interrupt 0 (pin 2) 
                                                        // and run function wakeUpNow 
                                                        // when pin 2 gets LOW
                                                        
                        sleep_mode();      // here the device is actually put to sleep!!
                                        // THE PROGRAM CONTINUES FROM HERE AFTER WAKING
                                        // UP
                        
                        sleep_disable();   // first thing after waking from sleep:   
                                        // disable sleep...
                        detachInterrupt(0);// disables interrupt 0 on pin 2 so the
                                        // wakeUpNow code will not be executed
                                        // during normal running time.
                    }
                    ```
        - Porta seriale 
            - 1 porta seriale hardware TTL, multiplexed sui GPIO 0 e 1 (connessi ai pin corrispondenti USB-to-TTL-serial chip che converte seriale TTL in USB e viceversa)
                - Pin 0 (RX): usato per ricevere
                - Pin 1 (TX): per trasmettere
            - Usando la libreria Serial la porta seriale può essere usata per mandare l'output al PC
        - Connettore USB (da cui si programma, trasferimento grazie ad un bootloader)
        - Power jack
        - ICSP header
        - Pulsante di reset
        - Operating voltage: 5V 
        - Input voltage (recom.): 7-12 V 
        - DC current per I/O pin: 40 mA 
        - DC current per 3.3V: 50mA
        - Durata alimentazione esterna
            - Esempio
                - Consumo stimato di 150mA e durata almeno 8 ore => capacità batteria >= 150mA*8 = 1200mAh

        ![alt text](./res/arduino-uno.png "Arduino UNO")

    - [Sistemi ESP8266 / NodeMCU](https://docs.google.com/document/d/1F2Xr2NeXIJ7Ug6Bru6pBFdCLF8JfVXMGOWoZY2czPUw/edit)

### Parte 3

- Un sistema embedded interagisce con l’ambiente in cui è situato mediante sensori e attuatori
    - Sensori
        - Dispositivi trasduttori che permettono di misurare una certo fenomeno fisico (come la temperatura, radiazioni, umidità, etc) o rilevare e quantificare una concentrazione  chimica (es: il fumo)
            - Le grandezze fisiche possono essere
                - Continue: valori continui all’interno di un certo intervallo (es: temperatura di un ambiente, velocità di rotazione di un motore, etc)
                    - Descritte da segnali analogici
                - Discrete: assumono un insieme discreto di valori (es. verso di rotazione di un motore, numero di pezzi lavorati al minuto, ecc...)
                    - Descritte da
                        - Segnali logici (due valori ammissibili)
                        - Segnali codificati (più di due valori ammissibili)
        - Fornisce una rappresentazione misurabile di un fenomeno su una certa specifica scala o intervallo (tipo una tensione in volt)
        - Possono essere analogici (se la grandezza elettrica prodotta in uscita - tensione o corrente - varia con continuità in risposta alla variazione della grandezza misurata) o digitali (insieme discreto di valori)
        - Funzionamento: basato su una legge fisica nota che regola la relazione grandezza fisica da misurare e grandezza elettrica d’uscita
        - Classificazione
            - Analogici: forniscono un segnale elettrico continuo a risoluzione infinita
            - Digitali: forniscono una informazione di tipo numerico con risoluzione finita
                - Logici: booleani
                - Codificati: uscita numerica codificata in una stringa di bits
        - Campionamento del segnale: acquisizione di campioni del segnale analogico ad istanti discreti di tempo e successiva conversione di questi in segnali digitali
            - Quantizzazione: operazione di approssimazione del valore campionato al più vicino valore digitale
            - Incertezza: può essere interpretata come il dubbio riguardante il risultato, ovvero la dispersione dei valori attribuibili al misurando (valore ± incertezza)
                - Errori
                    - Sistematici:  in grandezza e segno ha la stessa influenza sul risultato della misura
                    - Accidentali: errori la cui influenza sulla misura può cambiare in grandezza e segno se si ripete la procedura di misurazione (dati solitamente da n variabili ambientali)
                    - Grossolani: errori che riguardano l’operatore o guasti dello strumento
        - Caratteristiche metrologiche
            - Statiche: variata molto lentamente la variabile di ingresso del sensore, registrando la corrispondente variabile di uscita
                - Definite da Y = f(X) dove X rappresenta il segnale di ingresso e Y il segnale di uscita
                - La caratteristica è definita su di un intervallo finito chiamato campo di ingresso avente estremi Xm e XM ed ha valori sul campo di uscita (output range o span) con estremi Ym e YM
                    - Range d’ingresso: Xs = XM - Xm
                    - Range d’uscita: Ys = YM - Ym
                - La costante di proporzionalità fra valori di ingresso e di uscita viene chiamata guadagno (K) del trasduttore (idealmente è lineare) 
                    - I trasduttori commerciali hanno una caratteristica statica reale che si differenzia da quella ideale a causa di inevitabili imperfezioni costruttive quindi la qualità di un sensore si misura in base a quanto la caratteristica reale si scosta da quella ideale
                    - Per un trasduttore ideale la relazione tra la grandezza fisica misurata e il segnale in uscita è: Y = K X
            - Dinamiche: variabile di ingresso varia velocemente l’uscita può evidenziare un’attenuazione rispetto alla caratteristica statica ed un ritardo
        - Errori
            - Errore di linearità: massima deviazione dell’uscita del trasduttore rispetto alla caratteristica lineare che approssima al meglio la caratteristica reale
            - Errore di offset: valore d che assume l’uscita del trasduttore quando la grandezza da misurare è nulla (per rendere lineare la caratteristica occorre eliminare il termine di “offset” dalla caratteristica del trasduttore)
            - Errore di soglia: più basso livello di segnale rilevabile dal sensore (per rendere lineare la caratteristica occorre eliminare il termine di “soglia” dalla caratteristica del trasduttore)
            - Errore di guadagno: differenza tra il guadagno della caratteristica ideale del trasduttore (K) e il guadagno della retta (K1) che approssima al meglio la caratteristica reale del trasduttore
            - Errore di quantizzazione: l'operazione di quantizzazione comporta inevitabilmente l’introduzione di un errore sul segnale acquisito
        - Precisione vs Accuratezza
            - Accuratezza: misura di quanto il valore letto dal sensore si discosti dal valore corretto (spesso si fa riferimento invece all'incertezza)
                - Come si calcola: massimo scostamento tra la misura fornita dal sensore  e il  “valore  vero” del segnale misurato
                    - Esempio: un sensore di pressione che misura valori del range 0-10 bar con inaccuratezza pari a ± 1.0%; allora il massimo errore di misura è 10*1.0% = 10*0.01 = 0.1 bar
            - Precisione: descrive quanto un sensore sia soggetto o meno ad errori accidentali; se eseguiamo un numero elevato di letture con un sensore che ha precisione elevata, il range di tali valori sarà piccolo (alta precisione non implica elevata accuratezza)
            - Esempio
                - La larghezza di una stanza è stata misurata con un sensore ultrasuoni 10 volte riportando le seguenti misure: 5.381, 5.379, 5.378, 5.382, 5.380, 5.383, 5.379,  5.377, 5.380, 5.381
                - Misura corretta: 5.374m
                - Precisione
                    - Valor medio delle misure: 5.380
                    - Massima deviazione sotto il valore: 5.377-5.380 = -0.003
                    - Massima deviazione sopra il valore:  5.383-5.380 = +0.003
                    - Precisione: ± 0.003 m 
                - Massima inaccuratezza della misura = differenza fra il valore corretto e misura dal valore più distante = 5.383 - 5.374 = +0.009 m

            ![alt text](./res/accprec.png "Accuratezza vs Precisione")

        - Calibrazione e taratura
            - Calibrazione: aggiustamento dei parametri del sensore per farne corrispondere l’uscita a valori rilevati accuratamente con un altro strumento
            - Taratura: misurazione della grandezza di uscita per valori noti della grandezza di ingresso
        - Sensibilità: rapporto fra variazione della grandezza (segnale) di uscita e corrispondente variazione (segnale) d’ingresso
        - Risoluzione: capacità dello strumento (sensore) di risolvere stati (livelli) diversi del misurando
        - Ripetibilità: attitudine dello strumento a fornire per uno stesso misurando valori di lettura vicini tra loro, in letture consecutive eseguite in breve intervallo di tempo
        - Riproducibilità: vicinanza di risultati ottenuti sullo stesso misurando in diverse specifiche condizioni di misura
        - Stabilità: attitudine di uno strumento a fornire valori di lettura poco differenti tra loro in letture eseguite indipendentemente sullo stesso misurando in un intervallo di tempo definito
        - Comportamento dinamico: quando in ingresso al trasduttore applichiamo una sollecitazione a gradino (cioè un gradino della grandezza da misurare) l’uscita (risposta) varierà fino a raggiungere, dopo un certo tempo, un nuovo valore
            - Tempo di salita: tempo impiegato per passare dal 10% al 90% del valore finale
            - Tempo di risposta: tempo impiegato per raggiungere una percentuale prefissata del valore finale
        - Tipologie
            - Prossimità: rilevare la presenza di oggetti nelle immediate vicinanze del "lato sensibile" del sensore stesso, senza che vi sia un effettivo contatto fisico
                - Utilizzo
                    - Si invia un impulso sul pin Trig e misura quanto tempo impiega ad arrivare l'echo sul pin Echo; dato tale tempo è possibile ricavare la distanza

                ![alt text](./res/prossi.png "Sensore di prossimità")

            - Movimento (es PIR): sensore elettronico che misura i raggi infrarossi (IR) irradiati dagli oggetti nel suo campo di vista che possono essere usati come rilevatori di movimento (variazioni di temperatura)

                ![alt text](./res/pir.png "Sensore di movimento")

            - Accelerazione

                ![alt text](./res/accgir.png "Esempio di accelerometro e giroscopio")

                - Tipi
                    - Accelerometro: sensore che misura l'accelerazione a cui è soggetto l'oggetto a cui è connesso il sensore
                    - Giroscopio: dispositivo fisico rotante mantiene il suo asse di rotazione orientato in una direzione fissa (es d'uso: bussola)
                - Gli accelerometri e giroscopi elettronici citati si basano su tecnologia  MEMS: coniugano le proprietà elettriche degli integrati a semiconduttore con proprietà opto-meccaniche
            - Contatto
                - Pulsanti
                - Potenziometri
                - Capacitivi: il loro lato sensibile ne costituisce un’armatura, l’eventuale presenza nelle immediate vicinanze di un oggetto conduttore, realizza l’altra armatura del condensatore

                    ![alt text](./res/sencap.png "Sensore capacitivo")

            - Pressione
            - Ottici: converte una immagine ottica in un segnale elettrico
                - Foto-rivelatori: rivelare la radiazione elettromagnetica (luce)
                    
                    ![alt text](./res/foto.png "Sensore luminoso")

            - Elettricità e magnetismo
            - Tempo atmosferico e clima
                - Temperatura
                - Umidità 
                - Pressione
            - Sostanze chimiche
                - Fumo
            - Suono
            - GPS

                ![alt text](./res/gps.png "Rilevatore GPS")

            - Identificazione
                - RFID
                - NFC
            - Input dati
                - Keypad            
    - Attuatori (trasduttori di output): dispositivi che producono un qualche effetto misurabile sull’ambiente (analogici o digitali)
        - Permettono di realizzare sistemi embedded che eseguono e controllano azioni sull'ambiente fisico in cui sono immersi (azione più semplice: accendere o spegnere un dispositivo) 
        - Interfacciamento
            - Con alimentazione: mediante un GPIO si apre/chiude l’altro circuito mediante dispositivi come transistor e relè che fungono da “interruttori”
            - Senza alimentazione: è sufficiente la corrente/tensione in uscita ai GPIO per alimentare il trasduttore
        - Categorie
            - Carichi resistivi: possono essere assimilati ad un componente che al passaggio della corrente oppone una certa resistenza che implica una variazione della tensione
            - Carichi induttivi: dispositivi che operano inducendo corrente su un filo mediante la corrente su un altro filo o immergendo il filo in un campo magnetico 
        - Esempi
            - LED
            - Display LCD
                - Collegamento
                    - 4/8 pin: 6 pin sono da collegare ai pin digitali + 2 sono da collegare VCC e GND; dei 6 pin, 4 servono per trasmettere i dati, gli altri due per funzioni di controllo

                        ![alt text](./res/eslcd.png "Esempio di collegamento di uno schermo LCD all'arduino")

                    - I2C
                - Libreria LiquidCrystal fornita con Arduino IDE
                    ```c
                    #include <LiquidCrystal.h>
                    
                    int time = 0;
                    
                    /* sequenza dei pin: RS EN D4 D5 D6 D7 */
                    LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
                    
                    void setup(){
                        lcd.begin(16, 2); // display 16 caratteri per 2 righe
                        lcd.print(“Cesena”);
                    }
                    
                    void loop(){
                        lcd.setCursor(0,1);
                        lcd.print(time);
                        delay(1000);
                        time++;
                    }
                    ```
            - Motori elettrici
                - Corrente continua (utili per fare per esempio le ruote di un robot)
                    - La corrente elettrica continua passa in un avvolgimento di spire che si trova nel rotore, composto da fili di rame, creando un campo elettromagnetico al passaggio di corrente
                    - Questo campo elettromagnetico è immerso in un altro campo magnetico creato dallo statore, il quale è caratterizzato dalla presenza di una o più coppie polari (calamite, elettrocalamite, ecc.)
                    - Il rotore per induzione elettromagnetica inizia a girare, in quanto il campo magnetico del rotore tende ad allinearsi a quello dello statore analogamente a quanto avviene per l'ago della bussola che si allinea col campo magnetico terrestre
                    - Durante la rotazione il sistema costituito dalle spazzole e dal collettore commuta l'alimentazione elettrica degli avvolgimenti del rotore in modo che il campo magnetico dello statore e quello del rotore non raggiungano mai l'allineamento perfetto, in tal modo si ottiene la continuità della rotazione

                    ![alt text](./res/motcon.png "Motore in corrente continua")

                    - Alimentazione separata
                        - Perchè?
                            - Richiesta più corrente
                            - Picchi di tensione generati dal motore possono danneggiare l'arduino
                        - Schema

                            ![alt text](./res/motalsep.png "Schema di un motore con alimentazione separata")

                            - Q1 è un transistor usato come interruttore sull'alimentazione di 9V
                                - Modulando il pin base con un segnale PWM, si può controllare la velocità del motore, accendendo e spegnendo il transistor rapidamente
                                - Il duty cycle del segnale PWM determina la velocità del motore: duty cycle = 100% velocità massima (sempre acceso), duty cycle = 0% velocità nulla (motore non alimentato)

                                ![alt text](./res/trans.png "Transistor")

                            - R1 è una resistenza da 1 KOhm usato per separare il morsetto base del transistore dal pin di controllo di Arduino
                            - U1 è il motore 
                            - C1 è una condensatore usato per filtrare il rumore generato dal motore 
                            - D1 è un diodo che protegge l'alimentazione dall'inversione di tensione causata dal motore
                        - Funzionamento
                            - Quando una tensione sufficientemente elevata viene applicata alla base, il circuito si chiude, la corrente fluisce e il motore gira 
                            - Se l'alimentazione viene rimossa in modo istantaneo (non graduale), allora l'energia contenuta nel motore viene dissipata in termini di picco di tensione inversa, che può danneggiare i componenti del circuito, per questo motivo si piazza un diodo di protezione (diodo di ricircolo) in parallelo al motore, che assicura che la corrente inversa generata dal motore fluisca nel diodo e che la tensione inversa non ecceda la tensione soglia di forward del diodo
                        - Sketch

                            ![alt text](./res/ardumotalim.png "Sketch di arduino ed un motore con alimentazione esterna")

                        - Code
                            ```c
                            const int MOTOR = 9;
                            const int POT = 0;
                            int val = 0;
                            
                            void setup(){  
                                pinMode(MOTOR, OUTPUT);
                            }
                            
                            void loop(){  
                                val = analogRead(POT);  
                                val = map(val,0,1023,0,255);  
                                analogWrite(MOTOR,val);
                            }
                            ```
                    - Controllo della direzione del movimento
                        - E' possibile invertire il senso di marcia nel motore invertendo la polarità della tensione applicata ai morsetti; allo scopo nei circuiti si usa tipicamente un componente chiamato ponte H
                        - Il ponte integra 4 switch (realizzati da transistor) e tutta la circuiteria di protezione
                        - 4 stati operazionali
                            - Open: tutti gli switch aperti e il motore non gira

                                ![alt text](./res/ponteopen.png "Ponte in stato open")

                            - Forward: sono aperti due switch su diagonale opposta, la corrente fluisce e il motore si muove in una direzione

                                ![alt text](./res/ponteforw.png "Ponte in stato forward")

                            - Braking: tutto il movimento residuo causato dal momento angolare viene cessato e il motore si ferma

                                ![alt text](./res/pontebre.png "Ponte in stato breaking")

                            - Backward: sono aperti gli altri due switch su diagonale opposta, la corrente fluisce nel motore nella direzione opposta e il motore si muove nella direzione opposta

                                ![alt text](./res/ponteback.png "Ponte in stato backward")

                        - WARNING: se entrambi gli switch a sinistra o a destra vengono chiusi, si crea un corto fra la batteria e terra, la batteria si riscalda velocemente e potrebbe in alcuni casi prender fuoco o esplodere
                        - Sketch

                            ![alt text](./res/pontehmot.png "Sketch arduino + motore + ponte h + potenziometro")

                        - Code (agli estremi porti il motore a velocità massima in una direzione o nell’altra, mentre a metà lo fermi)
                            ```c
                            const int EN = 9;  // Half bridge1 Enable
                            const int MC1 = 3; // Motor Control 1 
                            const int MC2 = 2; // Motor Control 2 
                            const int POT = 9; // POT on Analog pin 0
                            int val = 0; // memorizza valore potenziometro
                            int velocity = 0; // velocità desiderata (0-255)
                            
                            void forward(int rate){  
                                digitalWrite(EN, LOW);  
                                digitalWrite(MC1, HIGH);  
                                digitalWrite(MC2, LOW);  
                                analogWrite(EN, rate);
                            }
                            
                            void backward(int rate){  
                                digitalWrite(EN, LOW);  
                                digitalWrite(MC1, LOW);  
                                digitalWrite(MC2, HIGH);  
                                analogWrite(EN, rate);
                            }
                            
                            void brake(){  
                                digitalWrite(EN, LOW);  
                                digitalWrite(MC1, LOW);  
                                digitalWrite(MC2, LOW);  
                                digitalWrite(EN, HIGH);
                            }
                            
                            void loop(){  
                                val = analogRead(POT);  

                                if (val > 562){    
                                    velocity = map(val, 563, 1023, 0, 255);
                                    forward(velocity);  
                                } else if (val < 462) {    
                                    velocity = map(val, 0, 461, 0, 255);    
                                    forward(velocity);  
                                } else {    
                                    brake();  
                                }
                            }
                            ```

                - Passo-passo (la posizione del motore può essere controllata accuratamente, usati nelle stampanti 3D)

                    ![alt text](./res/motpas.png "Motore passo-passo")

                - Servo

                    ![alt text](./res/servo.png "Servo motore")                    
                    - Si controllano specificando precisamente l’angolo al quale devono posizionare l’albero (-90 a +90 gradi)
                        - Libreria: Servo
                            - Esempi
                                - [Knob](https://www.arduino.cc/en/Tutorial/Knob): controllo mediante un potenziometro analogico
                                - [Sweep](https://www.arduino.cc/en/Tutorial/Sweep97): continua rotazione avanti indietro 
                    - Tre fili
                        - 2 per l’alimentazione (nero => GND, rosso => +5V)
                        - 1 come segnale di controllo digitale (bianco/giallo)
                    - Controllo: inviando uno stream di impulsi al segnale di controllo ad una specifica frequenza (50 Hz); la lunghezza dell’impulso determina l’angolo al quale vogliamo portare il servo (da 1 ms, -90, a 2  ms, +90)
                    - Collegamento

                        ![alt text](./res/servoardu.png "Esempio di collegamento di un servo ad arduino")

                    - Esempio di programma (i porta il servo ripetutamente dalla posizione -90 a +90)
                        ```c
                        int servoPin = 2;
                        
                        /* Invia un impulso della durata specificata (in microsecondi) e quindi attende 15 ms.
                        In realtà, per avere un segnale a 50 Hz, l’attesa dovrebbe essere pari a 1000/50 = 20ms tuttavia le istruzioni che vengono eseguite ciclicamente hanno una durata non nulla, per cui è stato scelto empiricamente un periodo inferiore.
                        In questo caso la lunghezza dell’impulso che corrisponde a -90 è circa 250ms, mentre +90 è circa 2250 ms */
                        void pulseServo(int servoPin, int pulseLen){
                            digitalWrite(servoPin, HIGH);
                            delayMicroseconds(pulseLen);
                            digitalWrite(servoPin,LOW);
                            delay(15);
                        }
                        
                        int c;
                        enum { MINUS_90, PLUS_90 } state;
                        
                        void setup(){
                            pinMode(servoPin, OUTPUT);
                            state = MINUS_90;
                            Serial.begin(9600);
                            c = 0;
                        }
                        
                        void loop(){
                            c++;
                            switch (state){
                                case MINUS_90:
                                    pulseServo(servoPin, 250);
                                    if (c > 100){
                                        Serial.println("--> +90");
                                        state = PLUS_90;
                                        c = 0;
                                    }
                                    break;
                                case PLUS_90:
                                    pulseServo(servoPin, 2250);
                                    if (c > 100){
                                        Serial.println("--> -90");
                                        state = MINUS_90;        
                                        c = 0;      
                                    }  
                            }
                        }
                        ```
            - Pilotaggio di circuiti esterni mediante relè e fotoaccoppiatori
                - Relè: permette di aprire e chiudere il secondo circuito mediante l’azione di un elettro-magnete, pilotato dal primo circuito
                    - Come nei motori è necessario usare opportuni diodi per evitare danni ai componenti
                - Fotoaccoppiatori: basati su un led interno, pilotato dal circuito a bassa tensione; quando il led viene illuminato, attiva una fotocellula e quindi l’interruttore del secondo circuito si chiude
            - Shift register: convertire input sequenziale/parallelo in output parallelo/sequenziale
                - La conversione (seriale => parallelo) è fatta mediante registri SIPO (Serial In Parallel Out), dispositivi che accettano in ingresso uno stream sequenziale di bit e fornisce in uscita l'output di tali bit come porta di I/O parallela
                - Segnali in input
                    - Segnale CLOCK: sincronizza l'acquisizione dei dati in input
                    - Segnale DATA: pin dove insiste lo stream di bit in input
                    - Segnale LATCH: campiona l'output e lo rende disponibile
                - Funzionamento (caricamento valore 10101010)
                    - Device sincrono, che acquisisce il valore dei bit in input al fronte di salita del segnale del clock
                        - Ad ogni fronte, tutti i valori al momenti presenti nel registro vengono fatti scorrere a sinistra di una posizione e viene introdotto il nuovo bit
                        - Il pin LATCH è attivato alla fine dell'acquisizione

                        ![alt text](./res/shiftreg.png "Esempio di funzionamento degli shift registers")
                - Esempio (pilotaggio 8 led in Arduino, mediante 3 segnali)
                    - Sketch

                        ![alt text](./res/rsardu.png "Sketch di esempio di utillizzo di shift registers")

                    - Code 
                        ```c
                        const int SER = 8;
                        const int LATCH = 9;
                        const int CLK = 10;
                        
                        void setup(){  
                            pinMode(SER,OUTPUT);  
                            pinMode(LATCH,OUTPUT);  
                            pinMode(CLK,OUTPUT);  
                            digitalWrite(LATCH,LOW);  
                            shiftOut(SER,CLK, MSBFIRST, B10101010);  
                            digitalWrite(LATCH,HIGH);
                        }
                        void loop(){}
                        ```
                    - Daisy chain
                        - Pilotare un numero maggiore di uscite usando più shift register è possibile mediante collegamenti daisy-chain
                            - Collegando il pin QH’ al pin DATA di un altro shift register il pin QH’ contiene il valore del bit più significativo che ad ogni shift “esce”, facendo posto a quello meno significativo  
                            - Facendo in modo che tutti gli shift register coinvolti condividano i segnali LATCH e CLOCK
                            - Nel caso di un numero elevato di led è necessario disporre di alimentazione supplementare, oltre a quella fornita da Arduino

- Livelli di tensione differenti
    - I sensori/attuatori per sistemi embedded possono funzionare con livelli di tensione diversa, ad esempio 5 Volt (Arduino) e 3.3 Volt (Raspberry Pi)
    - Un modo per adattare livelli di tensione è utilizzare circuiti chiamati partitori di tensione

### Parte 4

- Approcci sviluppo software
    - Top-down: partiamo dal dominio e dalla sua modellazione con paradigmi di alto livello
        - Modellazione: in che modo concettualizzo, rappresento, progetto un sistema software e in che modo analizzo e formulo la soluzione di un problema in certo dominio applicativo
            - Modello: rappresentazione degli aspetti salienti del sistema, astraendo da quelli che non sono significativi
            - Concetti fondamentali
                - Struttura: come sono organizzate le varie parti
                - Comportamento: comportamento computazionale di ogni singola parte
                - Interazione: come interagiscono le varie parti
            - Paradigmi: definiscono un insieme coerente di concetti e i principi con cui definire i modelli
                - Object-oriented
                    - Modularità 
                    - Incapsulamento 
                    - Meccanismi per riuso ed estendibilità
            - Linguaggio di modellazione: forniscono un modo rigoroso non ambiguo per rappresentare i modelli (es: UML)
            - Modellazione su micro-controllore
                - Elementi
                    - Controllore: incapsula la logica di controllo per lo svolgimento del compito o dei compiti del sistema
                    - Elementi controllati: modellano le risorse gestite/utilizzate dal controllore per svolgere il compito
                - Esempio (button-led)

                    ![alt text](./res/butled.png "Esempio di modellazione")

                    - Led

                        ![alt text](./res/ledob.png "Modellazione del led")

                        ```c
                        // Light.h
                        #ifndef __LIGHT__
                        #define __LIGHT__
                        
                        class Light {
                            public:  
                                virtual void switchOn() = 0;  
                                virtual void switchOff() = 0;    
                        };
                        #endif
                        
                        // Led.h
                        #ifndef __LED__
                        #define __LED__
                        #include "Light.h"
                        
                        class Led: public Light { 
                            public:  
                                Led(int pin);  
                                void switchOn();  
                                void switchOff();    
                            protected:  
                                int pin;  
                        };
                        #endif
                        
                        // Led.cpp 
                        #include "Led.h"
                        #include "Arduino.h"
                        
                        Led::Led(int pin){  
                            this->pin = pin;  
                            pinMode(pin,OUTPUT);
                        }
                        
                        void Led::switchOn(){  
                            digitalWrite(pin,HIGH);
                        }
                        void Led::switchOff(){  
                            digitalWrite(pin,LOW);
                        }
                        ```
                    - Button

                        ![alt text](./res/butob.png "Modellazione del bottone")

                        ```c
                        // Button.h
                        #ifndef __BUTTON__
                        #define __BUTTON__
                        
                        class Button {
                            public:   
                                virtual bool isPressed() = 0;
                        };
                        #endif
                        
                        // ButtonImpl.h
                        #ifndef __BUTTONIMPL__
                        #define __BUTTONIMPL__
                        #include "Button.h"
                        
                        class ButtonImpl: public Button { 
                            public:   
                                ButtonImpl(int pin);  
                                bool isPressed();
                            protected:  
                                int pin;
                        };
                        #endif
                        
                        // ButtonImpl.cpp
                        #include "ButtonImpl.h"
                        #include "Arduino.h"
                        
                        ButtonImpl::ButtonImpl(int pin){  
                            this->pin = pin;  
                            pinMode(pin, INPUT);     
                        } 
                        
                        bool ButtonImpl::isPressed(){  
                            return digitalRead(pin) == HIGH;
                        }
                        ```
                    - Main
                        ```c
                        #include "Led.h"
                        #include "ButtonImpl.h"
                        #define LED_PIN 13
                        #define BUTTON_PIN 2
                        
                        Light* light;
                        Button* button;
                        boolean lightOn;
                        
                        // Creazione oggetti
                        void setup(){  
                            light = new Led(LED_PIN);    
                            button = new ButtonImpl(BUTTON_PIN);  
                            lightOn = false;
                        }
                        
                        // Main
                        void loop(){  
                            if (!lightOn && button->isPressed()){    
                                light->switchOn();    
                                lightOn = true;  
                            } else if (lightOn && !button->isPressed()){    
                                light->switchOff();    
                                lightOn = false; 
                            }
                        };
                        ```
            - Modello "totale" = loop + agenti
                - Modello a loop: ad ogni ciclo il controller legge gli input di cui necessita e sceglie le azioni da compiere, la scelta dipende dallo stato in cui si trova; l'azione può cambiare anche lo stato interno
                    - Note
                        - Efficienza: quando il controllore è in idle in realtà sta ciclando a vuoto
                        - Reattività: dipende da quanto rapidamente viene completato un ciclo, se durante l’esecuzione del ciclo avvengono ripetute variazioni di stato nei sensori, tali variazioni non vengono rilevate
                    - Basato sulla decomposizione per task
                - Modello ad agenti: entità attive (es: controllore), dotate di un flusso di controllo logico autonomo, progettate per svolgere uno o  più  compiti (task) che richiedono di elaborare informazioni che provengono in input dall’ambiente da sensori e di agire su attuatori in output,  eventualmente comunicando con altri agenti
                    - Basato sulla decomposizione per task (agente = esecutore di uno o più task)
    - Bottom-up: partiamo dalle caratteristiche a livello hardware e dal comportamento che il sistema deve avere a questo livello

### Parte 5

- Macchine a stati finiti di Mealy (automi a stati finiti): modello discreto cioè che opera in una sequenza di passi discreti e la sua dinamica è caratterizzata da sequenze di eventi discreti

    ![alt text](./res/esmac.png "Esempio di FSM generica, Parking garage")

    - Segnali input/output
        - I segnali u e d di input (up e down) che rappresentano il succedersi di eventi discreti possono essere rappresentati come funzioni
            - u: ℝ ⟶ { absent, present }
                - Segnale puro: che trasporta la sola assenza/presenza di un evento
        - Il segnale di uscita può essere rappresentato invece con una funzione
            - c: ℝ ⟶ { absent } U ℕ
                - Non è un segnale puro poichè ha contenuto informativo
        - Modellazione input/output: mediante variabili a cui la macchina può accedere
            - Variabili di input: modificate dall’ambiente in cui opera la macchina
            - Variabili di output: modificate dalla macchina stessa mediante azioni, come controllo dell’ambiente
    - Reazioni: step (passi), ognuno dei quali si presuppone sia istantaneo, ovvero abbia durata nulla
        - Scatenate (triggered) dall'ambiente in cui opera il sistema
            - Scatenate da eventi di input: event-triggered
    - Valutazione input/output: l’esecuzione di una reazione comporta la valutazione degli input e degli output, salvati nelle rispettive variabili
    - Stato: rappresenta tutto ciò che è successo nel passato che ha un effetto nel determinare la reazione del sistema agli input correnti e futuri
    - Transizioni: coinvolge sempre due stati e può essere rappresentata da due elementi
        - Guardia: è una funzione booleana sulle variabili di input e specifica le condizioni per cui la transizione può avvenire
        - Azione: specifica il valore che devono assumere le variabili di output come risultato della transizione; se una variabile di output non viene specificata, si assume implicitamente che assuma il valore absent
    - Tipi
        - Asincrone (event-triggered): la reazione avviene a fronte di un evento di input, quindi è l’ambiente in cui opera la macchina che stabilisce quando la valutazione e quindi le reazioni devono avvenire
        - Sincrone (time-triggered): le reazioni avvengono ad intervalli regolari di tempo; è definito un periodo e quindi una frequenza di funzionamento
            - Esempio in pseudocodice
                ```cpp
                volatile int timerFlag = 0;
                
                void timerISR(){  
                    timerFlag = 1;
                }
                
                /* procedure implementing the step of the state machine */
                void step(){...} 
                
                loop(){  
                    while (!timerFlag){}; /* wait for a tick for doing the next step */
                    timerFlag = 0;  
                    step();
                }
                ```
            - Esempio reale (Blink)
                ```cpp
                #include "Led.h"
                #include "Timer.h"
                
                #define LED_PIN 13
                
                Light* led;
                Timer timer;

                enum { ON, OFF} state;
                
                void step(){  
                    switch (state){    
                        case OFF:      
                            led->switchOn();      
                            state = ON;      
                            break;    
                        case ON:      
                            led->switchOff();      
                            state = OFF;      
                            break;  
                    }
                }

                void setup(){  
                    led = new Led(LED_PIN);   
                    state = OFF;  
                    timer.setupPeriod(500);
                }
                
                void loop(){  
                    timer.waitForNextTick();  
                    step();
                }
                ```
            - Più intervalli temporali di lunghezza diversa
                - Come periodo si sceglie il massimo comun divisore dei vari intervalli
                - Esempio (Blink)
                    - LED acceso per 500 ms e spento per 750 ms => si sceglie periodo di 250 ms
            - Campionamento input: lettura periodica di un sensore ad una data frequenza
                - Caratterizzato da un certo il periodo di campionamento (sampling rate)
                    - Scelta periodo di campionamento: deve essere il più grande (per risparmio energetico) fra i valori sufficientemente piccoli (per perdita input) da garantire il corretto funzionamento del sistema
                - Minimum event separation time (MEST): intervallo più piccolo che - dato l’ambiente in cui opera il nostro sistema - può esserci fra due eventi di input (scegliendo un periodo inferiore al MEST si garantisce che tutti gli eventi saranno rilevati)
            - Latenza: intervallo che trascorre tra l'evento di input e la generazione dell'output
                - Obiettivo: minimizzare la latenza     
            - Condizionamento dell'input
                - Button bouncing: il segnale corrispondente passa da un valore LOW ad un valore HIGH e viceversa più volte, prima di assestarsi su valore HIGH (sembra che il pulsante sia stato premuto più volte)
                    - Fix: button de-bouncing
                        - Va considerata una frequenza di campionamento inferiore alla frequenza del bouncing (basta modificare il periodo del timer)
                - Glitch/spike: segnali spuri dovuti alle condizioni ambientali
                    - Fix
                        - Riduzione del periodo di campionamento (non elimina il problema)
                        - Richiedere che il segnale rilevato che rappresenta uno specifico evento, per essere considerato tale, abbia una certa durata minima (non elimina il problema)
                            - Esempio: per un periodo di campionamento di 50 ms di un pulsante e glitch durano al più 10 ms, possiamo richiedere che una serie di 2 campionamenti siano rilevati ad HIGH per poter considerare il pulsante premuto
        - Esempio (può essere implementato in tutti e due i modi)
            - HVAC (heating, ventilation, air conditioning)

                ![alt text](./res/esfsm.png "Esempio di FSM")

     - Importante: per corretto funzionamento
        - Ogni azione deve sempre terminare, non ci devono essere loop infiniti
        - La valutazione di una condizione non deve cambiare lo stato delle variabili
    - Mapping Macchina a stati -> Codice
        - Rappresentazione esplicita stati con costanti enumerative
        - Variabile che tiene traccia dello stato corrente
        - step() della macchina nel main loop caratterizzato da un costrutto di selezione sullo stato corrente, con un ramo per ogni stato; a seconda dello stato si eseguono le azioni associate alla transizione e si cambia stato, riassegnando la variabile relativa allo stato corrente
        - Esempio
            - FSM

                ![alt text](./res/fsmexbl.png "Esempio di trasformazione da fsm a codice")

            - Code
                ```c
                enum States { LightOn, LightOff } 
                
                statesetup(){  
                    state = LightOff
                }
                
                step() {  
                    switch (state){    
                        case LightOff:      
                            if (button.isPressed()){        
                                light.switchOn()        
                                state = LightOn      
                            }     
                        case LightOn:      
                            if (!button.isPressed()){        
                                light.switchOff()        
                                state = LightOff      
                            }  
                    }
                }
                
                loop {  
                    step()
                }
                ```
    - State pattern
        - Diagram

            ![alt text](./res/statpatdiag.png "Diagramma di state pattern di esempio")

        - Code
            ```cpp
            // State
            interface State {  
                do()  
                nextState(): State
            }
            class MyState1 implements State {...}
            class MyState2 implements State {...}

            // Loop
            state = initialState
            loop { 
                state.do() 
                state = state.nextState()
            }
            ```
    - Extended FSM (per macchine a stati articolate)
        - Esempio generico

            ![alt text](./res/esextfsm.png "Esempio di Extended FSM")

        - Esempio di time triggered EFSM (Semaforo pedonale, periodo = 1s)

            ![alt text](./res/timetrigefsm.png "Esempio di time triggered EFSM")

        - Numero di stati: include tutti quelli che derivano da ogni possibile configurazione ammissibile delle variabili di stato
            - Con
                - n = numero stati discreti
                - m = numero variabili che descrivono lo stato
                - p = possibili valori che può assumere ogni variabile
            - Si ha: $`|States| = n \times p^{m}`$

### Parte 6

- Task concorrenti
    - Decomposizione del comportamento complessivo in più task
        - Ogni task rappresenta un compito ben definito, una unità di lavoro da svolgere
        - Il comportamento di ogni task può essere descritto da una opportuna macchina a stati
        - Il comportamento complessivo è dall’insieme delle macchine a stati
        - Esempio: Led Show con 4 LED
            - Blinking di un led ogni 500 ms
            - Contemporaneamente: gestione di 3 led di cui se ne accenda uno alla volta, in sequenza, ogni 500ms

            ![alt text](./res/ledshowdiag.png "Esempio di FSM con i task")
    - Pro
        - Modularità
        - Chiarezza, manutenibilità, estendibilità, riusabilità
    - Contro
        - L'esecuzione dei task si sovrappone nel tempo
        - Va gestita la sincronizzazione (mutua esclusione, comunicazione)
    - Sistemi multi-tasking
        - Modelleremo ogni task come classe separata che estende da una classe base task, in cui è definito il metodo step, specializzato in ogni classe concreta; quindi nel programma avremo una sola istanza di questa classe (pattern singleton)
            - Esempio

                ![alt text](./res/estaskint.png "Esempio di utilizzo di task")

                ```cpp
                // Classe astratta Task
                #ifndef __TASK__
                #define __TASK__
                
                class Task {
                    public:  virtual void init() = 0;  
                    virtual void tick() = 0;
                };
                
                #endif

                // BlinkTask
                // BlinkTask.h
                #ifndef __BLINKTASK__
                #define __BLINKTASK__
                
                #include "Task.h"
                #include "Led.h"
                
                class BlinkTask: public Task {  
                    int pin;  
                    Light* led;  
                    enum { ON, OFF} state;
                    
                    public:  
                        BlinkTask(int pin);    
                        void init();    
                        void tick();
                };
                    
                #endif

                // BlinkTask.cpp
                #include "BlinkTask.h"

                BlinkTask::BlinkTask(int pin){  
                    this->pin = pin;    
                }
                
                void BlinkTask::init(){  
                    led = new Led(pin);   
                    state = OFF;    
                }
                
                void BlinkTask::tick(){  
                    switch (state){    
                        case OFF:      
                            led->switchOn();      
                            state = ON;       
                            break;    
                        case ON:      
                            led->switchOff();      
                            state = OFF;      
                            break;  
                    }
                }

                // Three Leds Task
                // ThreeLedsTask.h
                #ifndef __THREELEDSTASK__
                #define __THREELEDSTASK__
                
                #include "Task.h"
                #include "Led.h"
                
                class ThreeLedsTask: public Task {  
                    int pin[3];  
                    Light* led[3];  
                    int state;
                    
                    public:  
                        ThreeLedsTask(int pin0, int pin1, int pin2);    
                        void init();    
                        void tick();
                    };
                #endif

                // ThreeLedsTask.cpp
                #include "ThreeLedsTask.h"
                
                ThreeLedsTask::ThreeLedsTask(int pin0, int pin1, int pin2){
                    this->pin[0] = pin0;      
                    this->pin[1] = pin1;      
                    this->pin[2] = pin2;    
                }
                
                void ThreeLedsTask::init(){  
                    for (int i = 0; i < 3; i++){    
                        led[i] = new Led(pin[i]);   
                    }  
                    state = 0;    
                }
                
                void ThreeLedsTask::tick(){  
                    led[state]->switchOff();  
                    state = (state + 1) % 3;  
                    led[state]->switchOn();
                }

                // Main
                #include "Timer.h"
                #include "BlinkTask.h"
                #include "ThreeLedsTask.h"
                
                Timer timer;
                
                BlinkTask blinkTask(2);
                ThreeLedsTask threeLedsTask(3,4,5);
                
                void setup(){  
                    blinkTask.init();  
                    threeLedsTask.init();  
                    timer.setupPeriod(500);
                }
                
                void loop(){  
                    timer.waitForNextTick();  
                    blinkTask.tick();  
                    threeLedsTask.tick();
                }
                ```
        - Gestione periodi diversi
            - Tenere traccia per ogni task anche del periodo specifico
            - Periodo = massimo comun divisore dei periodi
            - Tener traccia per ogni task del tempo trascorso e nel caso si sia raggiunto il valore del periodo, richiamare il tick
            - Esempio 
                ```cpp
                // Task
                #ifndef __TASK__
                #define __TASK__
                
                class Task {  
                    int myPeriod;  
                    int timeElapsed;
                    public:  
                        virtual void init(int period){    
                            myPeriod = period;      
                            timeElapsed = 0;  
                        }  
                        
                        virtual void tick() = 0;  
                        
                        bool updateAndCheckTime(int basePeriod){    
                            timeElapsed += basePeriod;    
                            if (timeElapsed >= myPeriod){      
                                timeElapsed = 0;      
                                return true;    
                            } else {      
                                return false;     
                            }  
                        }
                };
                    
                #endif

                // Blink Task
                // BlinkTask.h
                #ifndef __BLINKTASK__
                #define __BLINKTASK__

                #include "Task.h"
                #include "Led.h"
    
                class BlinkTask: public Task {  
                    int pin;  
                    Light* led;  
                    enum { ON, OFF} state;
                        
                    public:  
                        BlinkTask(int pin);    
                        void init(int period);    
                        void tick();
                };
                    
                #endif

                // BlinkTask.cpp
                #include "BlinkTask.h"
                    
                BlinkTask::BlinkTask(int pin){  
                    this->pin = pin;    
                }
                    
                void BlinkTask::init(int period){  
                    Task::init(period);  
                    led = new Led(pin);   
                    state = OFF;    
                }
                    
                void BlinkTask::tick(){  
                    switch (state){    
                        case OFF:      
                            led->switchOn();      
                            state = ON;       
                            break;    
                        case ON:      
                            led->switchOff();      
                            state = OFF;      
                            break;  
                    }
                }
                
                // Three Leds Task
                // ThreeLedsTask.h
                #ifndef __THREELEDSTASK__
                #define __THREELEDSTASK__
                
                #include "Task.h"
                #include "Led.h"
                
                class ThreeLedsTask: public Task {  
                    int pin[3];  
                    Light* led[3];  
                    int state;
                    
                    public:  
                        ThreeLedsTask(int pin0, int pin1, int pin2);
                        void init(int period);    
                        void tick();
                    };
                
                #endif

                // ThreeLedsTask.cpp
                #include "ThreeLedsTask.h"
                
                ThreeLedsTask::ThreeLedsTask(int pin0, int pin1, int pin2){
                    this->pin[0] = pin0;      
                    this->pin[1] = pin1;      
                    this->pin[2] = pin2;    
                }
                
                void ThreeLedsTask::init(int period){  
                    Task::init(period);  
                    for (int i = 0; i < 3; i++){    
                        led[i] = new Led(pin[i]);   
                    }  
                    state = 0;    
                }
                
                void ThreeLedsTask::tick(){  
                    led[state]->switchOff();  
                    state = (state + 1) % 3;  
                    led[state]->switchOn();
                }

                // Main
                #include "Timer.h"
                #include "BlinkTask.h"
                #include "ThreeLedsTask.h"
                
                const int basePeriod = 50;
                
                Timer timer;
                
                BlinkTask blinkTask(2);
                ThreeLedsTask threeLedsTask(3,4,5);
                
                void setup(){  
                    timer.setupPeriod(basePeriod);  
                    blinkTask.init(500);  
                    threeLedsTask.init(150);
                }
                
                void loop(){  
                    timer.waitForNextTick();  
                    if (blinkTask.updateAndCheckTime(basePeriod)){    
                        blinkTask.tick();  
                    }  
                    if (threeLedsTask.updateAndCheckTime(basePeriod)){   
                        threeLedsTask.tick();  
                    }
                };
                ```
        - Scheduler cooperativo
            - Nel main loop si visita ogni elemento in coda, richiedendone il tick nel caso in cui il tempo trascorso sia pari al periodo previsto dal task
            - Esempio
                ```cpp
                // Scheduler.h
                #ifndef __SCHEDULER__
                #define __SCHEDULER__
                
                #include "Timer.h"
                #include "Task.h"
                
                #define MAX_TASKS 10
                
                class Scheduler {  
                    int basePeriod;  
                    int nTasks;  
                    Task* taskList[MAX_TASKS];    
                    Timer timer;
                    public:  
                        void init(int basePeriod);  
                        virtual bool addTask(Task* task);  
                        virtual void schedule();
                };
                
                #endif

                // Scheduler.cpp
                #include "Scheduler.h"
                
                void Scheduler::init(int basePeriod){  
                    this->basePeriod = basePeriod;  
                    timer.setupPeriod(basePeriod);    
                    nTasks = 0;
                }
                
                bool Scheduler::addTask(Task* task){  
                    if (nTasks < MAX_TASKS-1){    
                        taskList[nTasks] = task;    
                        nTasks++;    
                        return true;  
                    } else {    
                        return false;   
                    }
                }
                
                void Scheduler::schedule(){  
                    timer.waitForNextTick();  
                    for (int i = 0; i < nTasks; i++){    
                        if (taskList[i]->updateAndCheckTime(basePeriod)){   
                            taskList[i]->tick();    
                        }  
                    }
                }

                // Main
                #include "Scheduler.h"
                #include "BlinkTask.h"
                #include "ThreeLedsTask.h"
                
                Scheduler sched;

                void setup(){  
                    sched.init(50); 

                    Task* t0 = new BlinkTask(2);  
                    t0->init(500);  
                    sched.addTask(t0);  
                    
                    Task* t1 = new ThreeLedsTask(3,4,5);  
                    t1->init(150);  
                    sched.addTask(t1);  
                }
                
                void loop(){  
                    sched.schedule();
                }
                ```
        - Dipendenze fra task
            - Tipi
                - Temporale: un task T3 può essere eseguito solo dopo che sono stati eseguiti T1 e T2
                - Produttore/consumatore: un task T1 ha bisogno di una informazione prodotta da un task T2
                - Relative a dati: task T1 e T2 necessitano di condividere dati
                - ...
            - Soluzione: variabili condivise
                - Potrebbero verificarsi corse critiche, per evitarlo si definisce che le transizioni di ogni macchina a stati sono eseguite atomicamente
                - "Tipi"
                    - Variabili globali condivise
                    - Messaggi: ogni task ha la propria coda/buffer di messaggi e la comunicazione avviene inviandoli in modo asincrono
        - Interrupt driven cooperative scheduler
            - Scheduler invocato direttamente dall'interrupt handler del timer (nel loop non eseguo quindi niente)
            - Utile per sfruttare le modalità a basso consumo del controller (nel loop sono in sleep e mi sveglio tramite gli interrupt)
            - Attenzione però al tempo impiegato per entrare/uscire dalla modalità a basso consumo (magari la latenza che genero non è compatibile con la latenza delle operazioni del mio programma)
            - [Esempi](https://iol.unibo.it/mod/page/view.php?id=263405)
        - Nelle macchine sincrone abbiamo fatto l'assunzione che le azioni eseguite avessero un tempo di esecuzione nullo; per sistemi "multitasking" tale assunzione può non essere sempre plausibile, in tal caso lo sviluppatore deve porre attenzione alla reale durata delle azioni in relazione al periodo per fare in modo che non ci siano malfunzionamenti
            - Eccezione di overrun: il tempo di esecuzione delle azioni oltrepassa il periodo 
                - Esempio: consideriamo uno scheduler interrupt-driven in cui le interruzioni possono essere annidate, per cui una nuova interruzione può essere generata prima che l'interrupt handler di un triggering precedente abbia concluso la propria esecuzione

                    ![alt text](./res/esoverr.png "Esempio di overrun")

        - Parametro di utilizzo del processore e worst-case execution time
            - Parametro di utilizzo: percentuale di tempo in cui il microprocessore è utilizzato per eseguire dei task = `U = (tempo utilizzo per task / tempo totale) * 100%` 
                - Se `U > 100%` allora ho un'eccezione di overrun
                    - Soluzioni
                        - Utilizzare macchine a stati con periodi più lunghi
                        - Ottimizzare la sequenza di condizioni/azioni più lunga 
                        - Spezzare sequenze lunghe fra più stati
                        - Usare un microcontrollore più veloce
                        - Eliminare funzionalità dal sistema
            - Worst-Case-Execution-Time (WCET): tempo di esecuzione nel caso peggiore in termini di numero di istruzioni eseguite ad ogni periodo della macchina sincrona
                - Più stati: si considera lo stato/transizione con la sequenza di condizioni/azioni più lunga, va considerato nel calcolo anche il tempo per valutare le condizioni
                - Più task: se i task hanno lo stesso periodo, allora il WCET si calcola come somma dei singoli WCET dei task
                - Più periodi: considero degli iper-periodi (periodi che siano il minimo comune multiplo fra i periodi considerati)
            - Esempio di analisi (LedShow)
                - No problemi
                    - BlinkLed (BL) e ThreeLeds (TL), ognuno con periodo 500ms
                    - Supponiamo che il microcontrollore esegua 100 istruzioni/sec, ovvero 0.01 sec/istruzione 
                    - WCET per BL = 3 istruzioni => 3*0.01 = 0.03 sec 
                    - WCET per TL = 9 istruzioni => 9*0.01 = 0.09 sec 
                    - U = (0.03 + 0.09)/0.5 (essendo 0.5 il periodo) = 24 %
                - Con overrun
                    - WCET per BL = 0.2 sec 
                    - WCET per TL = 0.35 sec
                    - U = (0.2 + 0.35)/0.5 = 1.1 => overrun
                - Con periodi diversi
                    - BL con periodo = 0.3 sec
                    - TL con periodo = 0.2 sec
                    - MCM = 0.6 sec
                    - In 0.6 sec
                        - BL esegue 0.6/0.3 = 2 volte
                        - TL esegue 0.6/0.2 = 3 volte
                        - WCET BL: 0.02 sec
                        - WCET TL: 0.09 sec
                        - U = (2 * 0.02 ms + 3 * 0.09 ms)/0.6 ms = 55%
        - Jitter: ritardo  che intercorre dal momento che un task è pronto per essere eseguito e il momento in cui viene effettivamente eseguito
            - Dare priorità ai task con periodo più piccolo porta a minimizzare il jitter medio
        - Deadline: intervallo di tempo entro il quale un task deve essere eseguito dopo essere diventato ready
            - Se un task non viene eseguito entro la sua deadline, si ha una eccezione di missed-deadline che può portare - a seconda dei casi - a malfunzionamenti del sistema
            - Se dato un task non viene specificata la sua deadline, allora per default la deadline è data dal periodo, ovvero il tempo entro il quale il task torna ad essere ready
            - Esempio
                - BL
                    - Periodo = 0.3 sec
                    - WCET = 0.03 ms
                    - Deadline = 0.3 sec
                - TL
                    - Periodo = 0.2 sec
                    - WCET = 0.09 sec 
                    - Deadline = 0.1 sec
                - Se priorità a BL: deadline per TL non è rispettata ad ogni prima occorrenza dell’iper-periodo

                    ![alt text](./res/essched1.png "Esempio di scheduling")

                - Se priorità a TL: jitter è maggiore rispetto al primo caso, però non ci sono missed deadline

                    ![alt text](./res/essched2.png "Esempio di scheduling")
        
        - Priorità: ordinamento con cui devono essere eseguiti i task (quando ci sono più task ready, allora lo scheduler sceglie ed esegue quello prioritario)
            - Statica: assegnata ad ogni task prima che i task siano eseguiti e queste non cambiano durante l'esecuzione
                - Tipi
                    - Shortest deadline first: assegnare la priorità maggiore ai task con la deadline più piccola 
                    - Shortest-period-first: se tutti i task hanno le deadline pari ai periodi dei task, allora questo approccio risulta nell'assegnare la priorità maggiore ai task con il periodo più piccolo
                    - Shortest WCET first: assegna la priorità al task con WCET più piccolo (riduce il jitter ma richiede per essere applicato la conoscenza del WCET)
                - Assegnamento: si determina la priorità inserendo i task in un certo ordine
            - Dinamica: determina le priorità dei task man mano che il programma viene eseguito, per cui le priorità assegnate possono cambiare nel tempo (riduce jitter e missed deadline)
                - Earliest deadline first (EDF): si applica ciò poichè i task ready che hanno la deadline più vicina siano quelli che possono violarla se non vengono eseguiti subito
        - Scheduler preemptive vs cooperativi
            - Cooperativo: una volta selezionato un task esegue fino al completamento
            - Preemptive: è possibile togliere il processore ad un task in esecuzione prima che abbia completato
        - Macchine a stati sincrone event-triggered: uso gli interrupt per evitare di fare polling; mi fermo e mi metto in ascolto su un interrupt, quando questo viene rilevato eseguo ciò che devo eseguire
            - Macchina sincrona vs sincrona event-triggered (che sfrutta task detti aperiodici)

                ![alt text](./res/aperiodica.png "FSM sincrona vs sincrona event-triggered")

        - Macchine a stati event-driven
            - Asincrone: il modello sincrono è modellato come macchina che reagisce ad eventi periodici “tick” a frequenza prefissata
            - Transizioni di stato sono determinate da guardie eventi/condizioni
            - Non è necessario inserire lo stato “inattivo” esplicitamente nel diagramma degli stati

            ![alt text](./res/eventdriv.png "Macchina a stati sincrona event driven")

### Parte 7

- Dalle interruzioni ad architetture ad alto livello
    - Possiamo sfruttare le interruzioni per realizzare architetture/pattern ad eventi di alto livello
        - Pattern-observer (NOT GOOD)
            - Elementi
                - Sorgente o generatore di eventi: mette a disposizione una interfaccia per registrare gli osservatori interessati a ricevere la notifica degli eventi
                - Eventi generati
                - Osservatori (o listener) di una sorgente: mettono a disposizione un'interfaccia per la notifica dell’evento
            - Implementazione mediante interruzioni: utilizzate all’interno dei componenti generatori per fare in modo che all’occorrenza dell’interruzione associata ad un evento vengano chiamati gli ascoltatori registrati sul componente
                - Esempio: button generatore di eventi
                    - Schema

                        ![alt text](./res/btnevent.png "Esempio di pattern observer in Arduino")

                    - Codice
                        ```cpp
                        // ButtonImpl.h
                        #ifndef __BUTTONIMPL__
                        #define __BUTTONIMPL__
                        
                        #include "Button.h"
                        
                        #define MAX_BUTTON_LISTENERS 5
                        
                        class ButtonImpl: public Button {
                            public:   
                                ButtonImpl(int pin);  
                                bool isPressed();  
                                bool registerListener(ButtonListener* listener);  
                                
                                void notifyListeners();
                            
                            private:  
                                int pin;  
                                int nListeners;  
                                ButtonListener* listeners [MAX_BUTTON_LISTENERS];
                        };
                        #endif

                        // ButtonImpl.cpp
                        #include "ButtonImpl.h"
                        #include "Arduino.h"
                        
                        ButtonImpl* buttons[2]; 
                        
                        void notifyButtonListeners_0(){  
                            buttons[0]->notifyListeners();
                        }
                        
                        void notifyButtonListeners_1(){  
                            buttons[1]->notifyListeners();
                        }
                        
                        void registerNewButton(int pin, ButtonImpl* button){  
                            switch (pin){    
                                case 2:       
                                    buttons[0] = button;
                                    attachInterrupt(0, notifyButtonListeners_0, RISING);
                                    break;
                                case 3:       
                                    buttons[1] = button;
                                    attachInterrupt(1, notifyButtonListeners_1, RISING);
                                    break;  
                            }
                        }

                        void ButtonImpl::notifyListeners(){  
                            for (int i = 0; i < nListeners; i++){
                                listeners[i]->notifyButtonPressed();  
                            }  
                        }
                        
                        ButtonImpl::ButtonImpl(int pin){  
                            this->pin = pin;  
                            pinMode(pin, INPUT);    
                            nListeners = 0;    
                            registerNewButton(pin,this);
                        } 
                        
                        bool ButtonImpl::isPressed(){  
                            return digitalRead(pin) == HIGH;
                        }
                        
                        bool ButtonImpl::registerListener(ButtonListener* l){
                            if (nListeners < MAX_BUTTON_LISTENERS){
                                listeners[nListeners++] = l;     
                                return true;     
                            } else {    
                                return false;   
                            }
                        }

                        // Esempio di utilizzo: Listener (sarebbe un main)
                        #include "ButtonImpl.h"
                        
                        class MyListener : public ButtonListener {
                            public:  
                                MyListener(){    
                                    count = 0;    
                                }  
                                
                                void notifyButtonPressed(){    
                                    count++;  
                                }  
                                
                                int getCount(){    
                                    cli();    
                                    int c = count;    
                                    sei();    
                                    return c;    
                                }  
                            
                            private:  
                                volatile int count;
                        };
                            
                        Button* buttonA, *buttonB;
                        MyListener* listener;
                            
                        void setup(){  
                            Serial.begin(9600);  
                            buttonA = new ButtonImpl(2);  
                            buttonB = new ButtonImpl(3);  
                            listener = new MyListener();  
                            buttonA->registerListener(listener);
                            buttonB->registerListener(listener);
                        }
                            
                        void loop(){  
                            Serial.println(listener->getCount());
                        };
                        ```
                - Note
                    - Solo 2 pin possono generare interruzioni “external” quindi possiamo gestire simultaneamente al più due sorgenti
                    - La routine registrata come interrupt handler deve avere 0 parametri, e nessun valore di ritorno (non è possibile agganciare un metodo di un oggetto)
                    - Nel pattern observer il flusso di controllo che esegue i listener non è quello dell’osservatore, in questo caso invece lo è, ovvero il main loop stesso che viene interrotto
                    - Si possono effettuare delle corse critiche nella scrittura delle variabili, per evitarle bisogna rendere atomica l’esecuzione dei blocchi di codice eseguiti dal flusso principale in cui si accede ad informazioni modificate dai listener disabilitando e riabilitando le interruzioni
                    - Il listener deve essere corto poichè viene mandato in esecuzione direttamente dall’interrupt handler, ad interruzioni disabilitate
        - Reazioni (NOT GOOD)
            - Elementi
                - Eventi: rappresentati da un tipo, sorgente e un contenuto informativo
                - Reazioni: come computazioni che l’agente controllore esegue a fronte dell’occorrenza di eventi con un certo tipo da una certa sorgente
            - Implementazione: ad ogni occorrenza di interruzioni, il gestore delle reazioni prenderà il controllo e verificherà quali reazioni sono applicabili e possono essere quindi immediatamente eseguite
                - Esempio
                    - Codice
                        ```cpp
                        // react.h
                        #ifndef __REACT__
                        #define __REACT__
                        
                        class EventSource { };
                        
                        class Event {
                            public: 
                                Event(int type, EventSource* pSource); 
                                int getType(); 
                                EventSource* getSource();
                            private:  
                                int type;  
                                EventSource* pSource;
                        };
                        
                        bool addReaction(int event, EventSource* pSource, void (*proc)(Event* ev));  
                        void checkReactions(Event* ev);
                        
                        #endif

                        // ReactionManager.h
                        #include "react.h"
                        
                        #define MAX_REACTIONS 5
                        
                        class Reaction {
                            public:  
                                Reaction(int type, EventSource* pSource, void (*proc)(Event* ev));  
                                bool isTriggered(Event* ev);  
                                bool fire(Event* ev);
                            private:  
                                int type;  
                                EventSource* pSource;   
                                void (*proc)(Event* ev);
                        };
                        
                        class ReactionManager {
                            public:  
                                static ReactionManager* getInstance();
                                bool addReaction(int event, EventSource* pSource, void (*proc)(Event* ev));    
                                void checkReactions(Event* ev);  
                            private:  
                                ReactionManager();      
                                int nReactions;  
                                Reaction* pReactions[MAX_REACTIONS]; //singleton
                                static ReactionManager* instance; 
                        };

                        // Implementazione (in parte) di ReactionManager
                        //...
                        ReactionManager* ReactionManager::instance = new ReactionManager();

                        ReactionManager* ReactionManager::getInstance(){ return instance; }
                        
                        ReactionManager::ReactionManager(){  
                            nReactions = 0;  
                        }
                        
                        bool ReactionManager::addReaction(int event, EventSource* pSource, void (*proc)(Event* ev)){  
                            if (nReactions < MAX_REACTIONS){    
                                pReactions[nReactions] = new Reaction(event, pSource, proc);    
                                nReactions++;    
                                return true;  
                            } else {    
                                return false;    
                            }
                        }
                        
                        void ReactionManager::checkReactions(Event* ev){  
                            for (int i = 0; i < nReactions; i++){    
                                if (pReactions[i]->isTriggered(ev)){ 
                                    pReactions[i]->fire(ev);    
                                }    
                            }
                        }

                        // API pubbliche
                        // Registra una nuova reazione
                        bool addReaction(int event, EventSource* pSource, void (*proc)(Event* ev)){
                            ReactionManager::getInstance()->addReaction(event, pSource, proc);
                        }
                        
                        // Chiamata internamente dai dispositivi all’occorrenza di interruzioni
                        void checkReactions(Event* ev){
                            ReactionManager::getInstance()->checkReactions(ev);
                        }
                        ```
                    - Pulsanti come sorgenti
                        ```cpp
                        // Button.h
                        #ifndef __BUTTON__
                        #define __BUTTON__
                        
                        #include "react.h"
                        
                        #define BUTTON_PRESSED 1
                        
                        class Button: public EventSource {
                            public:   
                                virtual bool isPressed() = 0;
                        };
                        
                        #endif

                        // ButtonImpl.h
                        #ifndef __BUTTONIMPL__
                        #define __BUTTONIMPL__
                        
                        #include "Button.h"
                        
                        class ButtonImpl: public Button {
                            public:   
                                ButtonImpl(int pin);  
                                bool isPressed();
                            private:  
                                int pin;
                        };
                        
                        #endif

                        // ButtonImpl.cpp
                        #include "ButtonImpl.h"
                        #include "Arduino.h"
                        #include "react.h"
                        
                        ButtonImpl* buttons[2]; 
                        
                        void notifyButtonListeners_0(){  
                            if (buttons[0] != NULL){    
                                Event* ev = new Event(BUTTON_PRESSED, buttons[0]);    
                                checkReactions(ev);    
                                delete ev;  
                            }
                        }
                        
                        void notifyButtonListeners_1(){  
                            if (buttons[1] != NULL){    
                                Event* ev = new Event(BUTTON_PRESSED, buttons[1]);    
                                checkReactions(ev);    
                                delete ev;  
                            }
                        }
                        
                        void registerNewButton(int pin, ButtonImpl* button){  
                            switch (pin){    
                                case 2:       
                                    buttons[0] = button;
                                    attachInterrupt(0, notifyButtonListeners_0, RISING);
                                    break;    
                                case 3:       
                                    buttons[1] = button;
                                    attachInterrupt(1, notifyButtonListeners_1, RISING);
                                    break;  
                            } 
                        }
                        
                        ButtonImpl::ButtonImpl(int pin){  
                            this->pin = pin;  
                            pinMode(pin, INPUT);    
                            registerNewButton(pin,this);
                        } 
                        
                        bool ButtonImpl::isPressed(){  
                            return digitalRead(pin) == HIGH;
                        }

                        // Main
                        volatile int count = 0;
                        
                        void buttonPressedReactionA(Event* ev){ count++; }
                        void buttonPressedReactionB(Event* ev){ count+=10; }
                        
                        int getCurrentCountValue(){  
                            cli();  
                            int c = count;  
                            sti();  
                            return c;
                        }
                        
                        Button* buttonA, *buttonB;
                        
                        void setup(){  
                            Serial.begin(9600);  
                            buttonA = new ButtonImpl(2);   
                            buttonB = new ButtonImpl(3);
                            addReaction(BUTTON_PRESSED,buttonA,buttonPressedReactionA);
                            addReaction(BUTTON_PRESSED,buttonB,buttonPressedReactionB);
                        }
                        
                        void loop(){  
                            Serial.println(getCurrentCountValue());
                        };
                        ```
                - Note: valgono le precedenti
        - Event-loop (BEST)
            - Funzionamento 
                - Ogni controllore ha una coda (buffering di eventi) ove vengono aggiunti eventi di interesse (via interruzioni o altro)
                - Il ciclo di controllo è dato da un loop infinito in cui si attende che ci sia nella coda almeno un evento da elaborare, lo si elabora e lo si rimuove
            - Implementazione: sul [repo](https://iol.unibo.it/mod/page/view.php?id=263405) del prof, lab 2.4

### Parte 8

- Microcontrollori vs SOC
    - Microcontrollori = no sistema operativo
    - SOC = si sistema operativo
        - Contiene
            - Uno o più processori/core
            - Memoria: RAM, ROM, EEPROM, Flash...
            - Generatore di clock
            - Interfacce standard: USB, Ethernet...
            - Alimentatori DAC e ADC e circuiti di gestione dell'alimentazione
- SOC (System on a chip)
    - Gerarchia

        ![alt text](./res/gersoc.png "Gerarchia di un SOC")

    - Interfacce di comunicazione dei livelli
        - Instruction Set Architecture (ISA): interfaccia che separa livelli HW e SW
            - User ISA: user reserved
            - System ISA: system reserved

            ![alt text](./res/isa.png "Livello ISA")

        - Application Binary Interface (ABI): interfaccia fornita ai programmi per accedere  alle risorse HW e ai servizi del sistema (user ISA + System Call Interface)
            - System Call interface: insieme di operazioni che l’OS mette a disposizione ai programmi come servizi fondamentali

            ![alt text](./res/abi.png "Livello ABI")

    - Sistema operativo (SO): controlla l’esecuzione dei programmi applicativi e funge da intermediario fra questi e la macchina fisica
        - Obiettivi
            - Eseguire programmi degli utenti e controllare la loro esecuzione, in particolare l’accesso alla macchina fisica
                - S.O. come extended / virtual machine (astrazione, controllo e protezione)
                    - Portabilità
                    - Semplicità di programmazione
            - Rendere agevole ed efficace l’utilizzo delle risorse del computer 
                - S.O. come resource manager (ottimizzazione e protezione)
            - Abilitare e coordinare le interazioni fra applicazioni, utenti, risorse
                - Più applicazioni in esecuzione concorrente
                - Più utenti che condividono e usano simultaneamente il sistema
        - Soluzioni
            - Virtualizzazione delle risorse
                - Memoria virtuale: fare in modo che un programma in esecuzione veda la memoria a disposizione come uno spazio lineare virtualmente illimitato
                - File system virtuale: accedere e modificare file nel file system in modo uniforme a prescindere dal tipo specifico di file system e dalla effettiva locazione dei file stessi
            - Esecuzione di programmi
                - Multi-programmazione (multi-programming): capacità di caricare in memoria centrale più programmi che vengono eseguiti in modo da  ottimizzare l'utilizzo della CPU
                - Time-sharing (o multi-tasking): capacità di eseguire più programmi concorrentemente con condivisione della CPU fra i programmi in esecuzione secondo determinate strategie di schedulazione
        - Funzionamento
            - Interrupt-driven
                - Interruzioni hardware (asincrone): segnali inviati da dispositivi (es: tastiera, timer, dischi,...)
                - Trap/interruzioni sw (sincrone): richieste da parte dei programmi di eseguire servizi (chiamate di sistema) oppure eccezioni generate da errori nei programmi
            - Modalità operative della CPU
                - User-mode: esecuzione del programmi utente
                - Kernel-mode: esecuzione delle parti di programma del sistema operativo
        - Sistemi embedded real-time (RTOS)
            - Real-time: devono essere in grado di reagire/rispondere ad eventi e input entro limiti di tempo prestabiliti (deadline)
                - Hard: le deadline devono essere rispettate sempre 
                    - Proprietà che DEVONO essere predicibili
                        - Tempo impiegato per svolgere un certo task
                        - Tempo massimo richiesto per eseguire una certa azione o acquisire un certo valore in input o da un sensore o a rispondere ad una certa interruzione
                        - Il numero di cicli richiesti per eseguire una certa operazione deve essere sempre lo stesso
                - Soft: le deadline devono essere rispettate in condizioni normali, sono ammessi casi in cui non vengono rispettate
            - Organizzazione: come un normale OS (un processo contiene vari thread) + gestione di interrupt e sensori come Arduino
            - Benefici
                - Migliorare la responsiveness e diminuire overhead 
                    - Evitare polling/looping sfruttando la possibilità di eseguire "context switch" al processore, da un task ad un altro in modo trasparente all'applicazione 
                        - Context switch: viene eseguito lo switch fra più thread contexts (i classici dati in memoria che vanno salvati) così da rendere il sistema multi-tasking
                    - Realizzare in modo trasparente alle applicazioni multi-tasking 
                - Semplificare la condivisione delle risorse
                    - Allocazione/deallocazione memoria a runtime
                    - Semafori/mutex per controllare accesso a risorse HW o a sezioni critiche
                - Semplificare lo sviluppo, debugging e maintenance 
                    - Lo sviluppatore non deve gestire aspetti di basso livello come interruzioni, timer...
                    - Supporti per il debugging
                    - Sviluppo modulare delle applicazioni
                - Aumentare la portabilità
                    - Un'applicazione usa le RTOS API, non accede direttamente all'HW specifico; questo permette di poter cambiare HW trasparentemente
                - Abilitare l'uso di servizi e middleware stratificati 
                    - File system
                    - TCP/IP network stack
                    - USB stack
                    - Graphics 
                    - ...
                - Rendere più veloce il time-to-market delle applicazione
            - Scheduling
                - Big loop
                    - Ogni task è polled per verificare se richiede di essere eseguito
                    - Polling procede sequenzialmente o in ordine di priorità
                    - Inefficiente, mancanza di responsiveness
                - Round-robin 
                    - Processore dato a turno ai task ready
                    - Imposto un time-slice ad ogni thread
                - Priority-based (usato in RTOS, molto responsivo ma rischio starvation)
                    - Processore esegue sempre il thread con priorità maggiore 
                    - Per thread con la stessa priorità => round-robin 
            - Comunicazione e sincronizzazione
                - Semafori
                    - Regolare la competizione
                        - Mutua esclusione
                        - Accesso regolato
                    - Sincronizzazione
                        - Semafori evento
                - Scambio messaggi
                    - Code di messaggi (FIFO)
                    - Primitive: send e receive
                    - Architettura produttore-consumatore dove le code di messaggi sono bounded buffer
            - Parametri temporali task
                - Tempo di rilascio (release time): istante in cui il task entra nella coda di ready, essendo pronto per essere eseguito
                - Tempo di esecuzione (execution time) = WCET: durata massima esecuzione del task
                - Tempo di risposta del task (response time): intervallo di tempo che intercorre da quando il task è rilasciato a quando ha completato la sua esecuzione
                - Scadenza (deadline): massimo intervallo di tempo permesso  per l’esecuzione di un task

                ![alt text](./res/temppar.png "Parametri temporali di esecuzione di un task")

            - Tipi di task
                - Periodici: c’è un intervallo di tempo prefissato (periodo p) che intercorrere fra release time dei vari task
                - Aperiodici: task rilasciati a tempi arbitrari
                - Sporadici: task rilasciati a tempi arbitrari, con hard deadline
                - NB: Il progettista di un sistema real-time deve decidere cosa fare se una deadline non è soddisfatta
            - Tipi di scheduler: in base alle "decisioni" prese e al "tempo"
                - Decisioni
                    - Assegnamento: quale processore usare per eseguire un certo task 
                    - Ordine: in quale ordine ogni processore deve eseguire i suoi task
                    - Timing: quando un determinato task deve o può essere eseguito
                - Tempo: ognuno di questi tre tipi di decisione può essere preso o a design time o a run time, durante l'esecuzione del programma
                - Tipi di scheduler
                    - Offline schedulers
                        - Fully-static: tutte e 3 le decisioni sono prese a design time; è usato quando si conoscono perfettamente tutte le tempistiche dell'esecuzione dei task, tipicamente indipendenti 
                        - Static-order: task assignment e ordering a design time, tuttavia la decisione di quando eseguire i task a livello temporale viene fatta a runtime
                    - Online schedulers
                        - Static assignment: assegnamento a design time e tutto il resto a runtime (presenza di un run-time scheduler)
                        - Fully-dynamic scheduler: come Static ma anche l'assegnamento viene deciso a runtime, quando un processore si rende disponibile 
            - Tipi di sistemi real-time
                - Sincroni
                    - Un clock hardware è utilizzato per suddividere il tempo del processore in intervalli chiamati frame
                    - Il programma deve essere suddiviso in task in modo che ogni task possa essere completamente eseguito nel caso peggiore in un singolo frame
                        - Mediante una tabella di scheduling si assegnano i task ai frame, in modo che tutti i task all’interno di un frame siano completamente eseguiti prima della fine del frame 
                        - Quando il clock segnala l’inizio del nuovo frame, lo scheduler richiama l’esecuzione dei task specificata in tabella 
                        - Se un task è ha una durata che eccede un frame, lo si deve esplicitamente suddividere in sotto-task più piccoli che possano essere individualmente schedulati in più frame successivi
                    - Svantaggi
                        - Sistemi molto fragili dal punto di vista della progettazione (una modifica/estensione del sistema può portare a task troppo lunghi quindi deve essere ricomputata la tabella)
                        - Il tempo del processore può essere sprecato (il task che costituisce il caso-peggiore condiziona l’intero sistema)
                    - Esempio (pseudocodice)
                        ```
                        taskAddressType array[0..NFrames-1] tasks = [ taskAddr0, taskAddr1, ...]
                        integer currentFrame <- 0
                        
                        loop 
                            await beginning of the frame 
                            invoke tasks[currentFrame]
                            currentFrame = (currentFrame + 1 ) % NFrames
                        ```
                - Asincroni
                    - Non si richiede che i task completino l’esecuzione entro frame temporali prefissati; ogni task prosegue la propria esecuzione e lo scheduler è invocato per selezionare il prossimo task pronto per l’esecuzione
                    - Pro: efficienza
                    - Contro: il soddisfacimento delle deadline è un problema più complesso
                    - Esempio (pseudocodice)
                        ```
                        queue of taskAddressType readyQueue = ...
                        taskAddressType currentTask
                        loop 
                            await readyQueue not empty 
                            currentTask = readyQueue.removeHead 
                            invoke currentTask
                        ```
                    - Priorità: task più importanti eseguiti quando necessario, a discapito di task meno importanti
                        - Funzionamento: priorità descritta da un numero naturale assegnato al task; un task non viene eseguito nel caso in cui in un sistema ci sia un task nella coda di ready con priorità maggiore
                        - Scheduler preemptive
                            ```
                            queue of taskAddressType readyQueue <- ...
                            taskAddressType currentTask
                            
                            loop 
                                await a scheduling event 
                                if (currentTask.priority < highest priority of  a task on readyQueue)
                                    save context of currentTask and place on readyQueue
                                    currentTask <- take task with highest priority from readyQueue
                                else if (currentTask's timeslice is past and it exists some other tasks in readyQueue with the same priority)
                                    save context of currentTask and place on readyQueue
                                    currentTask <- take task with of the same priority from readyQueue
                            resume currentTask
                            ```
                            - Tecnica time-slicing  e scheduling round-robin: insieme di ready queue, una per ogni priorità e ognuna con strategia di scheduling round robin
                        - Watch-dog task
                            - Periodico, con priorità massima
                            - Verificare che determinati task siano sempre eseguiti e segnalare eventuali problemi
                        - Sistemi interrupt-driven: gli interrupt handler sono modellati come task la cui priorità è superiore a qualsiasi altro (normale) software task, questo garantisce che gli interrupt handler possono essere eseguiti come sezioni critiche, non interrompibili da altri task
                            - Interrupt overflow: la priorità assoluta data alle interruzioni rispetto ai task software può creare situazioni problematiche, specialmente quando gli interrupt sono generati da componenti HW sui quali il designer ha poco controllo
                        - Inversione priorità: un task a bassa priorità può ritardare l’esecuzione di un task a priorità più elevata
                            - Esempio
                                - Supponiamo di avere un task T con una certa priorità p che entra in sezione critica
                                - Supponiamo quindi che un task T1 con priorità p1 > p sia quindi schedulato per essere eseguito e che tale task necessità anch’esso di entrare in sezione critica 
                                - Il task a priorità più alta T1 deve aspettare che il task a priorità più bassa completi la propria sezione critica 
                                - Il problema dell’inversione di priorità succede se un terzo task T2 con priorità p < p2 < p1 che non richiede di entrare in sezione critica viene a questo punto schedulato
                                - Il processore è tolto a T e dato a T2, dal momento che la priorità di T2 è superiore a quella di T; così facendo, il task con priorità media T2 può ritardare l’esecuzione del task prioritario T1 per un tempo arbitrariamente lungo
                            - Soluzioni
                                - Priority inheritance: il problema può essere risolto aumentando temporaneamente la priorità del task in sezione critica ad essere pari a quella massima fra tutti i task che devono/possono essere schedulati in futuro e che vogliono entrare in sezione critica 
                                - Priority ceiling locking: l’idea è di assegnare al monitor una priorità di base che sia maggiore o uguale alla più alta priorità posseduta dai task che richiedono di interagire con il monitor chiamando le sue operazioni; quando un task chiama una di queste operazioni, eredità la ceiling priority del monitor
                        - Assegnazione feasible delle priorità: assegnare le priorità ai task in modo che tutti i task rispettino le scadenze designate
                            - Algoritmi di scheduling per RTOS
                                - Rate-monotonic
                                    - Fixed-priority: le priorità sono definite staticamente e non cambiano, e il loro valore è dato dall’inverso del periodo; per cui più frequentemente il task deve essere eseguito maggiore è la sua priorità, a prescindere dalla sua durata prevista
                                - Earliest deadline first
                                    - Assegna la priorità maggiore dinamicamente (quando interviene lo scheduler) al task con deadline più vicina 
                                    - Non è sempre applicabile
                                        - HW task come gli interrupt handler possono richiedere di avere priorità pre-fissate
                                        - Comporta un certo overhead sullo scheduler che in alcuni casi può non essere accettabile (ricalcolo priorità)
            - Multi-threading (si ricollega all'assignment 2, esempi in [modulo lab 3.2](https://iol.unibo.it/mod/page/view.php?id=263405))
                - Thread vs Task
                    - Thread: meccanismo abilitante fornito dal sistema operativo 
                    - Task: astrazione con cui modularizzare/decomporre il comportamento di un sistema
                - Mapping
                    - One to one: ogni task ha un proprio thread e un proprio loop
                    - Master worker: più task eseguiti dal medesimo pool di thread
                - Interazione e coordinazione fra task: i task di un sistema possono essere totalmente indipendenti, oppure avere esibire dipendenze che richiedono opportune forme/meccanismi di gestione e coordinazione
                    - Meccanismi
                        - Semafori e monitor
                        - Scambio messaggi e eventi
                            - Receive esplicita: fornita primitiva receive che blocca il flusso di controllo in attesa che sia disponibile un messaggio (usare solo se è consentito bloccare il flusso di controllo)
                            - Receive implicita: non c’è la primitiva receive, la ricezione del messaggio viene gestita come evento

### Parte 9

- Identificazione dei sistemi embedded
    - RFID (Radio-Frequency Identification): tecnologia per l'identificazione e/o memorizzazione automatica di informazioni inerenti oggetti basata sulla capacità di memorizzazione di dati da parte di particolari etichette elettroniche, chiamate tag, e sulla capacità di queste di rispondere all'interrogazione a distanza da parte di appositi apparati fissi o portatili
        - Tipi tag
            - Passivi
                - Non alimentati, o meglio, alimentato indirettamente, a distanza, dalle onde radio inviate da un RFID reader che ne vuole leggere il contenuto
                - Può essere scritto da un RFID reader
                - Distanza fino a 2m
                - Capacità di memorizzazione dell’ordine del kilobyte
            - Attivi: hanno batterie proprie e trasmettono in broadcast il proprio contenuto informativo continuamente 
            - Semi-passivi o battery assisted passive (BAP): come il caso attivo ma invia le informazioni solo se sollecitato da un RFID reader, supporta distanze maggiori rispetto al passivo
    - NFC (Near-Field Communication)
        - Piccole distanze (<10cm)
        - Bidirezionale
        - Maggiore velocità di trasmissione (424 kBit/s)
        - Interazione direttamente fra 2 lettori
- Stadi IOT
    1. Product stage: condizionatore
    2. Smart product: condizionatore programmabile
    3. Smart connect products: condizionatore accessibile tramite Internet
    4. Product systems: il termostato intelligente parla con le finestre automatiche
        - Punti fondamentali
            - Interoperability & communication standards
            - Command and control platforms: Apple HomeKit, Amazon Echo, Google Home
    5. Systems of systems: gestore della casa parla con sistema di sicurezza che parla con la macchina...
        - Punti chiave
            - Interoperability
            - Scalability
            - Openness 
            - Governance
- IOT e Enterprise
    - Nel contesto industriale, IoT può essere inquadrato come evoluzione dei sistemi Machine-To-Machine (M2M): insieme di tecnologie che supportano la comunicazione wired e wireless fra dispositivi/macchine al fine di permettere scambio di informazioni locali
        - Tipi M2M
            - Sistema di controllo industriale (ICS): sistemi computerizzati che monitorano e controllano i processi industriali in un certo ambiente fisico 
                - SCADA: sistemi di supervisione che operano con codifiche di segnali trasmesse per canali di comunicazione al fine di realizzare un controllo remoto di sistemi/equipaggiamenti/impianti (è un ICS a larga scala)
                    - Elementi principali
                        - Sensori
                        - Remote terminal units (RTUs): raccogliere i dati rilevati dai sensori e trasformarli in segnali digitali da inviare alla postazione di controllo remota
                        - Programmable logic controller (PLCs): supervisionare la raccolta di informazioni, fornendo istruzioni sia alle RTU sia direttamente ai sensori (indica alla rete sensoriale quali sono gli intervalli di tempo nei quali effettuare le misurazioni e controllare i valori dei macchinari)
                    - Linguaggio: IEC 61131-3
                    - Elementi secondari
                        - Sistema di telemetria: connette PLC e RTU con centri di controllo (banalmente è una rete)
                        - Computer supervisore: server di controllo che raccoglie periodicamente i dati dai PLC, li elabora per ottenerne informazioni utili, memorizza le informazioni, invia comandi di controllo
                        - Interfaccia uomo-macchina: permette ad operatori umani di accedere ai dati processati, monitorarli e interagirvi
                        - Historian: servizio software che memorizza time-stamped data, eventi e allarmi, in un data base che viene poi interrogato al fine di creare report grafici nel HMI
- IOT e Cloud
    - Cloud
        - Paradigma di erogazione di risorse informatiche, come l'archiviazione, l'elaborazione o la trasmissione di dati, caratterizzato dalla disponibilità on demand attraverso Internet a partire da un insieme di risorse preesistenti e configurabili
        - Tre livelli
            - IAAS (infrastructure as a service): macchine virtuali, server, storage, network
            - PAAS (platform as a service): execution runtime, database, web servers, IDEs, ...
            - SAAS (software as a service): giochi, office apps, mail apps, ...
    - Interazione IOT e Cloud
        - Il cloud viene utilizzato per:
            - Raccolta dati inviati dai dispositivi
            - Accesso ai dati inviati dai dispositivi
            - Gestione "aperta" dei dispositivi 
- Carenze IOT
    - Sicurezza
    - Privacy
- IoT + WEB = WEB-OF-THINGS: sistemi che incorporano gli oggetti fisici di uso quotidiano nel World Wide Web

### Parte 10

- IOT e comunicazione
    - La comunicazione può essere sia wired, sia wireless; in entrambi i casi i dispositivi vengono dotati di opportuni moduli di comunicazione (la comunicazione fra micro-controllore e moduli di comunicazione avviene usualmente mediante la porta seriale)
        - Wireless: canale radio, diffuse nell'etere al fine di trasportare a distanza l'informazione tra utenti attraverso segnali elettromagnetici appartenenti alle frequenze radio o microonde dello spettro elettromagnetico detta anche banda radio; i segnali inviati a tali frequenze vengono perciò detti segnali a radiofrequenza (RF)
            - Un sistema di radiocomunicazione è composto da
                - Canale trasmissivo: comprende la tratta radio, l'emettitore e il ricevitore
                - Modem: modulatore in trasmissione e un demodulatore in ricezione
                - Codec: codificatore del segnale in ingresso e un decodificatore del segnale in uscita al canale radio
            - Tecnologie e protocolli
                - Short Range (decine di metri)
                    - Bluetooth
                        - Consumi bassi

                            ![alt text](./res/bt.png "Consumi e range bluetooth")

                        - Funzionamento
                            - La rete di base BT è chiamata piconet, basata su architettura master-slave
                                - Ogni dispositivo Bluetooth è in grado di gestire simultaneamente la comunicazione con altri 7 dispositivi slave
                                - Un dispositivo per volta può comunicare con il master
                                - La comunicazione è sincronizzata con il clock del master
                            - E' possibile connettere tra loro piconet a formare una scatternet: gli slave possono appartenere a più piconet contemporaneamente mentre il master di una piconet può al massimo essere lo slave di un’altra
                            - Ogni dispositivo Bluetooth è configurabile per cercare costantemente altri dispositivi e per collegarsi a questi, può essere impostata una password per motivi di sicurezza
                            - Due tipi di collegamenti
                                - Asincrono senza connessione (ACL, Asynchronous ConnectionLess), per traffico di tipo dati, servizio di tipo best-effort
                                - Sincrono orientato alla connessione (SCO, Synchronous Connection Oriented), traffico di tipo real-time e multimediale
                            - RFCOMM: sfruttare Bluetooth come una classica linea di comunicazione seriale
                            - Sicurezza: algoritmo SAFER+ (Secure And Fast Encryption Routine) per autenticare i dispositivi e per generare la chiave utilizzata per cifrare i dati
                        - Tutorial Arduino: [Configurazione e uso modulo Bluetooth HC-06 (HC-05)](https://docs.google.com/document/d/1K6ssX5rR63T4AFL4FYP9a0367Mc0Y3u_N-j-37fliik/edit)
                        - Evoluzioni
                            - BT 4.0: implementa Bluetooth Low Energy (BLE), protocollo alternativo a quello BT classico pensato appositamente per IoT
                            - BT 5.0
                                - 4x area di trasmissione ma stesso livello di consumo
                                - 2x velocità (fino a 2Mbps in modalità a basso consumo)
                                - 8x capacità di trasmissione
                    - Standard IEEE 802.15.4
                        - Caratteristiche protocollo
                            - Comunicazione a bassi consumi e agile fra dispositivi vicini 
                            - Supporti per la  comunicazione real-time e comunicazioni sicure
                        - Modello di rete
                            - Nodi
                                - FFD (full-function device): coordinatore, parla con un qualsiasi altro nodo
                                - RFD (reduced-function devices): possono comunicare solo con un FFD
                            - Topologie
                                - Star e peer-to-peer: ogni rete deve avere almeno un FFD che funge da coordinatore
                                - Cluster tree: un RFD può avere un solo FFD

                            ![alt text](./res/modrete.png "Modello di rete")

                        - Caratteristiche
                            - Frequenza: 2.4 GHz, 900 MHz e 868 MHz
                            - Data rate: 250 kbps
                            - Range: da 100 m a 1 Km in base alla potenza in uscita utilizzata
                            - Consumo: 50mA, una batteria ricaricabile di 850 mAH può fornire circa 17 ore di uso continuato
                        - Implementazioni: ZigBee
                            - Dispositivi
                                - ZigBee Coordinator (ZC)
                                    - Radice di una rete ZigBee, può operare da ponte tra più reti
                                    - Ci può essere un solo Coordinator in ogni rete
                                    - Memorizzare informazioni riguardo alla sua rete e può agire come deposito per le chiavi di sicurezza
                                - ZigBee Router (ZR): router intermedi che passano i dati da e verso altri dispositivi
                                - ZigBee End Device (ZED)
                                    - Includono solo le funzionalità minime per dialogare con il suo nodo parente (Coordinator o Router)
                                    - Non possono trasmettere dati provenienti da altri dispositivi
                            - Topologie

                                ![alt text](./res/zbtopology.png "Topologie rete ZigBee")
                            
                    - Radio Frequency Identification (RFID)
                    - Near Field Communication (NFC) 
                    - Beacon: piccolo trasmettitore a bassissimo consumo che emette continuamente un segnale contenente il suo ID che può essere rilevato da un qualsiasi dispositivo BLE
                - Medium Range (centinaia di metri): Wi-Fi, consumi rilevanti
                - Long Range (distanze superiori al miglio): reti cellulari (2G, 3G, 4G, 5G), consumi alti
    - Connessione a Internet
        - Protocollo: quelli utilizzati nella rete Internet
        - Modalità
            - Indirettamente: mediante la comunicazione di un nodo intermediario che funge da gateway
            - Direttamente: montando un modulo di comunicazione opportuno

### Parte 11

- Modello di comunicazione a scambio messaggi
    - Primitive
        - `send`
        - `receive`
    - Tipologie di comunicazione
        - Diretta/indiretta
            - Diretta: avviene direttamente fra i processi (thread, ...) specificando il loro identificatore
                - Primitive
                    - `send(ProcId,Msg)`
                    - `receive(): Msg`
            - Indiretta: si utilizzano dei canali che fungono da mezzi di comunicazione, l'invio e la ricezione avvengono specificando un canale
                - Primitive
                    - `send(ChannelId,Msg)`
                    - `receive(ChannelId):Msg`
        - Sincrona/asincrona
            - Sincrona: la send ha successo (e completa) quando il messaggio specificato è ricevuto dal destinatario mediante una receive
            - Asincrona: la send ha successo (e completa) quando il messaggio è stato inviato ma non necessariamente ricevuto dal destinatario 
                - Importante qua la bufferizzazione, le code dei messaggi devono riuscire a contenerli tutti
    - Rappresentazione dei messaggi
        - Medesimo linguaggio utilizzato da tutti i partecipanti alla comunicazione (Es: JSON, XML) 
        - Formato universale
    - Modello Publish-Subscribe
        - Channels/topics: canali/argomenti sui quali è possibile inviare messaggi e che è possibile "osservare" mediante sottoscrizione 
        - Publishers: inviano messaggi su canali
        - Subscribers: si registrano su canale/topic per ricevere tutti messaggi pubblicati
    - Problemi relativi alle chiamate bloccanti in alcuni tipi di sistemi
        - Loop-oriented
            - Problema
                - Receive bloccante: comporta il blocco del loop, per questo non è usabile nell'implementazione di macchine a stati 
            - Soluzione
                - Receive non bloccante: nel caso in cui non ci siano messaggi la receive non si blocca  
                    - `receiveMsg(): { Msg, errore/null }` 
                - Aggiunta di una primitiva per testare presenza messaggi
                    - `isMsgAvailable(): boolean`
        - Generale
            - Problema
                - Selezione messaggi: supponiamo che nel medesimo stato possano essere ricevuti più messaggi, di tipo diverso, e che a seconda del messaggio ricevuto la macchina a stati debba transitare in uno stato diverso; cosa succede?
            - Soluzione
                - Message pattern
                    - `isMsgAvailable(Pattern pattern): boolean`: è true se è presente un messaggio che corrisponde al pattern specificato
                    - `receiveMsg(Pattern pattern): Msg`: recupera un qualsiasi messaggio che corrisponde al pattern specificato

### Parte 12 (NON IMPORTANTE)

- Device e servizi/server-side
    - Dispositivi
        - Piccoli
            - Micro-controllori/System-On-Chip (SOC) a 8-bit senza Sistema Operativo (es: Arduino Uno)
        - Medi
            - Low-power gateway: risiedono fra device e Internet, funzioni di aggregazione dei dati, event-processing, bridging
                - Piccoli router e derivati, basati su architetture a 32 bit (ARM, Atheros)
            - Server-side
                - Sistemi operativi embedded/dedicati (es: embedded Linux, TinyOS, Arduino Yun, ...)
        - Computing platform a 32/64 bit complete
            - Low-power gateway (più o meno)
                - Sistema Operativo completo (es: Raspberry Pi, Smartphone, ...), spesso fungono da gateways o bridges per dispositivi più piccoli
- Middleware: livello software che risiede tra il sistema operativo e le applicazioni; fattorizza servizi e funzionalità di alto livello e le rende disponibili ad ogni applicazione che ne ha bisogno
    - Funzioni
        - Connettività e Comunicazione
            - Interoperabilità: gestione/comunicazione/integrazione di dispositivi e applicazioni di tipo diverso
        - Device Management
            - Discovery dinamico di dispositivi
            - Disconnessione dispositivi con problemi o che creano problemi
            - Aggiornamento software su un dispositivo dinamicamente, da remoto 
            - Cambiamento delle credenziali di sicurezza
            - Abilitazione o disabilitazione funzionalità HW
            - Rintracciamento di un dispositivo smarrito
            - Cancellazione dati sensibili da un dispositivo rubato
        - Data collection, analysis, e actuation
        - Scalabilità (cloud computing)
        - Sicurezza (punto critico IoT)
    
    ![alt text](./res/middleware.png "Modello middleware di riferimento")

    - Architettura a livelli (da basso ad alto)
        - Device layer
            - Ogni dispositivo deve avere una propria identità
                - UUID
                - Definita a livello HW
                - Fornita dal sistema di rete (Bluetooth id, WiFi MAC Address, ...)
                - OAuth2 Refresh/Bearer Token
                - Identificatore memorizzato nella memoria non-volatile
            - Protocollo: UPnP
                - Usa più protocolli per reti ad hoc di dispositivi per servizi di base vari
                    - Discovery dispositivi
                    - Detect servizi forniti da dispositivi
                    - Esecuzione azioni su dispositivi e generazione eventi
        - Connection layer
            - Protocolli
                - Generali
                    - HTTP/HTTPS (e approcci RESTful)
                - Ottimizzati per IoT
                    - MQTT: scambio di messaggi, publish-subscribe, basato su modello a broker
                        - Bassissimo overhead (2 byte per messaggio) 
                        - Supporta comunicazioni in reti con connessione intermittente e non affidabile
                        - Il modello a broker permette di funzionare anche in caso di firewall

                        ![alt text](./res/mqttbroker.png "MQTT a broker")

                    - Constrained Application Protocol (CoAP)
                        - RESTful come HTTP ma binario invece di text-based
                        - Client/Server
                        - Protocollo: UDP
                    - XMPP (Extensible Messaging and Presence Protocol)
                        - Scambio di messaggi, real-time messaging
                        - Message brokers
                        - Publish-subscribe, point-to-point, request-response, asynchronous messaging, ... 
                        - Federazioni di server/gateway XMPP per avere massima scalabilità

                        ![alt text](./res/xmpp.png "Architettura XMPP")

        - Aggregation/bus layer: aggrega e gestisce le comunicazioni
            - Funzioni
                - Abilitare la comunicazione fra HTTP server e/o MQTT broker con i dispositivi
                - Aggregare e comunicare la comunicazione da dispositivi diversi verso uno specifico dispositivo 
                - Bridging tra protocolli (es: API HTTP-based supportate da messaggi MQTT)
                - OAuth2 Resource Server: validazione Bearer Token e il relativo access scope
                - Policy Enforcement Point (PEP) per accessi policy-based
                - Protocol bridging: integrare sottosistemi che usano protocolli diversi
        - Event processing/analytics layer: rileva eventi dal bus e fornisce funzionalità di elaborazione e attuazione
    - Architettura a servizi multilivello (cloud computing, PAAS platform as a service)
        - Sensing: sistemi/dispositivi che rilevano flussi di dati/eventi relativamente all'ambiente in cui le "Thing" si trovano e operano
        - Networking: funzionalità e meccanismi che permettono ai vari oggetti di comunicare e condividere le informazioni, nonché meccanismi per aggregare informazioni provenienti da altre infrastrutture 
        - Service (livello del middleware)
            - Componenti
                - Service discovery: trovare oggetti che possono offrire un certo servizio o informazione in maniera efficiente
                - Service composition: interazione e comunicazione tra oggetti connessi
                    - Il componente precedente sfrutta le relazioni tra i differenti oggetti per trovare il servizio desiderato, mentre questo componente schedula o ricrea servizi più adatti, in modo da ottenere il servizio che soddisfi la richiesta in modo migliore
                - Trustworthiness management: utilizzare le informazioni fornite dai servizi in modo sicuro
                - API per i servizi: interazione tra i servizi
        - Interface: semplificare la gestione, l’interconnessione e l'interoperabilità dei servizi distribuiti nella rete

## Android

### Parte 13

- Panoramica Android
    - Basato su Linux (ma non è una distro Linux)
    - Sviluppato in Java
    - Ogni applicazione è un utente del sistema a cui è associato un ID univoco
    - Ogni processo del sistema è eseguito mediante una propria virtual machine
    - Least Privilege: ogni applicazione ha accesso ai soli componenti esplicitamente richiesti per eseguire i propri compiti
    - Software stack
        - Hardware Abstraction Layer (HAL): fornisce l’interfaccia standard per sfruttare tutte le funzionalità dei dispositivi hardware (sensori, connettività, ...); delegazione al sistema operativo di operazioni varie
        - Java API Framework: fornisce l’intero feature-set di Android OS
            - Dalvik VM
                - Diversa dalla JVM (JVM = stack machine, Dalvik = registri)
                - Compilazione Just-in-time (JIT): ogni app è compilata solo in parte dallo sviluppatore, a run-time sarà l’interprete di Dalvik a compilare definitivamente il codice e ad eseguirlo
            - Android Run Time (ART): da Android 5 sostituisce la Dalvik VM
                - Compilazione AOT (ahead-of-time): compilazione avviene durante l’installazione dell’app su un device e non durante l’esecuzione della stessa
                - Garbage Collection (GC) e meccanismi di debugging ottimizzati
- Applicazioni
    - Linguaggio: Kotlin o Java 7
    - Installer: Android Package (APK), in pratica è un JAR
    - Certificato
    - Possibilità di gestire tutte le dimensioni di schermo
    - Building Blocks
        - Il comportamento di ciascuna applicazione Android può essere progettato mediante istanze di 4 componenti principali
            - Activity
                - Rappresenta una singola schermata di un’applicazione con associata una propria interfaccia utente (la finestra che si apre quando apri un'app)
                - L’interfaccia utente di ciascun applicazione è definita mediante uno o più file di risorse XML
                - Ciascuna applicazione può definire ed eseguire più activity, una sola delle quali può trovarsi in stato di foreground in un preciso momento
            - Service
                - Componente che esegue la propria computazione in background rispetto alle activity (no interazione con utente)
                - Non è associato ad alcuna interfaccia grafica
                - Può prevedere meccanismi per la richiesta di esecuzione di compiti da componenti esterni
                - Esempio: un'app che tiene traccia della mia posizione avrà un'activity per la visualizzazione della mappa e un service per il tracking GPS
            - Content Provider (non approfondito)
                - Consente di accedere, gestire e modificare i dati persistenti di un'applicazione
                - I dati salvati in un Content Provider possono essere privati per una specifica applicazione o condivisi con le altre
            - Broadcast Receiver
                - Componente che permette di catturare e/o rispondere agli annunci (messaggi) propagati all’interno del sistema
                - Un annuncio può riguardare un qualsiasi elemento del sistema e può essere propagato sia dal sistema operativo stesso, sia da una qualunque applicazione (solitamente annunci mandati in broadcast)
                - Esempi
                    - Il display del dispositivo è stato spento
                    - Un determinato file è stato scaricato ed è ora disponibile
                    - E' stata identificata una nuova rete WiFi disponibile
                - Non è associato ad alcuna interfaccia utente ma può attivare una notifica sulla barra di stato
        - Note importanti
            - Principio di Isolamento delle App: **le app devono chiedere al SO ogni volta che hanno bisogno di accedere a delle risorse relative ad un'altra app o al SO**
            - Non è necessario reimplementare gli elementi base
                - Esempio: per scattare una foto da una propria applicazione non è necessario implementare un componente specifico ma è sufficiente richiamare il componente (activity) dell’applicazione dedicata che potrà eseguire il compito associato ed informare l’applicazione chiamante quando la foto risulta disponibile
    - Manifest: descrive l'app tramite sintassi XML
        - Deve essere obbligatoriamente presente nella root directory dell’applicazione in un file denominato `AndroidManifest.xml`
        - Dichiara l’API Level di riferimento (minVersion e targetVersion)
        - Elenca tutti i componenti dell’applicazione che il sistema può eseguire
        - Elenca i permessi che l’applicazione richiede per la propria esecuzione
        - Dichiara i componenti HW e SW che l’applicazione intende utilizzare
        - ...
    - Attivazione dei Componenti
        - Ciascun componente può essere attivato inviando un messaggio asincrono al sistema chiamato Intent
        - Tipi Intent
            - Espliciti: conosco il nome del componente che devo chiamare

                ![alt text](./res/intentesp.png "Esempio di Intent esplicito")

                - Creazione e utilizzo
                    ```java
                    Intent i = new Intent(getApplicationContext(), MyActivity.class);
                    i.putExtra("USERNAME", "mario.rossi");
                    i.putExtra("PASSWORD", "abc123");
                    ```

            - Impliciti: NON conosco il nome del componente che devo chiamare quindi descrivono un’azione generica da eseguire che può essere intercettata da un componente che sia in grado di eseguirla
                - Se viene identificato un unico componente candidato, questo viene eseguito automaticamente
                - Se vengono identificati più componenti candidati la scelta sul quale eseguire è demandata all’utente

                ![alt text](./res/intentimp.png "Esempio di Intent implicito")

                - Creazione e utilizzo
                    - Client
                        ```java
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, textMessage);
                        sendIntent.setType("text/plain");
                        // Check if at least a component that handles the Intent is present
                        if(sendIntent.resolveActivity(getPackageManager()) != null){
                            startActivity(sendIntent);
                        }
                        ```
                    - Provider (definisco se un componente può intercettare un particolare Intent)
                        ```xml
                        <activity android:name="ShareActivity">
                        
                            <intent-filter>
                                <action android:name="android.intent.action.SEND"/>
                                <category android:name="android.intent.category.DEFAULT"/>
                                <data android:mimeType="text/plain"/>
                            </intent-filter>
                            
                        </activity >
                        ```

### Parte 14

- Activity
    - Overview
        - Un’applicazione Android consiste in una collezione di Activity tra loro interconnesse
        - In un’applicazione Android deve sempre esistere un’activity principale (Main Activity), avviata dal sistema contestualmente alla richiesta diavvio dell’applicazione
    - Lifecycle
        - Stato
            - RESUMED (RUNNING): l’activity è in foreground e ha il focus
            - PAUSED: un’altra activity è in stato running, ma questa è ancora visibile (CAN BE DELETED)
                - Esempio: tastiera in foreground e l'app su cui sto scrivendo è in pausa
            - STOPPED: l’activity è in background, ovvero completamente oscurata da un’altra activity (CAN BE DELETED)
            - INSTALLED BUT NOT RUNNING
        - Callbacks: le transizioni da uno stato all’altro di un'app provocano l’invocazione automatica di uno o più metodi (callbacks)
            - Tipi
                - `onCreate()`
                    - Invocata quando l’activity è creata per la prima volta
                    - Provvede al setup dell’activity
                    - Riceve dal sistema l’eventuale stato precedentemente salvato dell’activity
                - `onStart()`
                    - Invocata subito prima che l’activity sia resa visibile all’utente la prepara affinchè possa essere posta nello stato di foreground
                - `onResume()`
                    - Invocata dopo che l’activity è stata resa visibile all’utente ma prima che quest’ultimo possa cominciare ad interagire con essa
                    - Al termine della sua esecuzione l’activity si trova nello stato RESUMED (RUNNING)
                - `onPause()`
                    - Invocata dal sistema sull’activity quando un’altra activity deve essere portata allo stato di RUNNING (NB: multi-window mode)
                    - Al termine della sua esecuzione l’activity si trova nello stato PAUSED
                - `onStop()`
                    - Invocata quando l’activity non è più visibile all’utente, al termine della sua esecuzione l’activity si trova nello stato STOPPED
                - `onRestart()`
                    - Invocata quanto l’activity, precedentemente interrotta (ovvero che si trova nello stato STOPPED), deve essere riportata allo stato RUNNING
                - `onDestroy()`
                    - Invocata immediatamente prima della fase di distruzione dell’activity (sia lato utente che lato sistema), ovvero prima che sia rimossa dalla memoria centrale
            - Diagramma di flusso (IMPORTANTE, pensa alla scrittura del codice in base a questo)

                ![alt text](./res/diaglifecallbacks.png "Diagramma del lifecycle e delle callbacks")

            - IMPORTANTE: la ridefinizione di ciascuna callback deve obbligatoriamente richiamare l’implementazione originale corrispondente
                - Esempio: la ridefinizione del metodo `onStart()` in una propria activity deve richiamare nel suo corpo lo stesso metodo definito nella super-class Activity con l’istruzione `super.onStart()`
    - Gestione activity
        - Creazione di un'activity
            - Ogni nuova activity deve essere definita come sottoclasse di `android.app.Activity`
            - Metodi obbligatori
                - `onCreate()`
                    - Nel corpo del metodo deve essere associata all’activity la risorsa che definisce la relativa User Interface (`setContentView(int resID)`)
                - `onPause()`: per il rilascio di eventuali risorse occupate
            - How-to
                ```java
                import android.app.Activity;
                
                public class MyActivity extends Activity{
                    
                    @Override
                    public void onCreate(Bundle savedInstanceState){
                        super.onCreate(savedInstanceState);
                        setContentView(R.layout.myactivity_layout);
                    }
                    
                    @Override
                    public void onPause(){
                        super.onPause();
                    }
                    
                }
                ```
            - Ciascuna activity DEVE essere dichiarata nel File Manifest
                ```xml
                <manifest> <!-- Manifest tag properties omitted -->
                    <application>
                        <activity
                            android:name="com.example.MyActivity"
                            android:label="@string/app_name"/>
                    </application>
                </manifest>
                ```
            - Almeno un'activity del manifest deve essere etichettata come Main Activity specificando anche un Intent Filter
                ```xml
                <activity
                    android:name="com.example.MainActivity"
                    android:label="@string/app_name">
                    
                    <intent-filter>
                        <action android:name="android.intent.action.MAIN"/>
                        <category android:name="android.intent.category.LAUNCHER"/>
                    </intent-filter>

                </activity>
                ```
        - Avvio di un'activity da un'altra
            ```java
            Intent intent = new Intent(this, NewActivity.class);
            intent.putExtra("MESSAGE_FROM_CREATOR","I’m your  creator!");
            startActivity(intent);
            
            // Within the NewActivity instance onCreate() callback
            String msg = getIntent().getStringExtra("MESSAGE_FROM_CREATOR");
            ```
        - Avvio di un'activity da un'altra con risultato
            ```java
            // Caller

            public class MyActivity extends Activity{
                
                private static final int REQUEST_CODE = 12345;
                
                public void startButtonClick(View view){
                    Intent i = new Intent(this, CheckLoginActivity.class);
                    startActivityForResult(i, REQUEST_CODE);
                }
                
                @Override
                protected void onActivityResult(int reqID, int res, Intent data){
                    if(reqID == REQUEST_CODE && res == Activity.RESULT_OK){
                        // Do something
                        data.getStringExtra("username")
                    }
                }

            }


            // Responder

            public void closeButtonClick(View view){
                Intent returnIntent = new Intent();
                returnIntent.putExtra("username","mario.rossi");
                
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
            ```
        - Terminazione di un'activity
            - In genere la terminazione delle activity è lasciata al sistema
            - `finish()` (non molto utilizzato)
    - Back Stack: stack LIFO con app in background
        - Le activity sono inserite nello stack secondo l’ordine di attivazione/avvio
        - Alla base dello stack è sempre presente l’activity che rappresenta la Home Screen del sistema
        - Alla pressione del tasto back, l’activity in foreground viene rimossa dallo stack e quella precedente viene portata nello stato di foreground

### Parte 15

- Tutti i componenti di un’applicazione Android sono eseguiti nell’ambito dello stesso Processo (Main Thread) creato dal sistema all’avvio del primo componente dell’applicazione
- "As long as possible” policy
    - Android assicura che ciascun processo sia mantenuto attivo per il maggior tempo possibile ma il sistema può decidere in qualunque momento di terminare qualunque processo attivo, ad eccezione di quello attualmente in foreground
    - A tutti i processi attivi è associato un ranking che consente di stabilire quale/i processo/i interrompere per rilasciare risorse necessarie o per questioni di performance
- Main Thread
    - Se resta bloccato per più di 5 secondi il sistema interrompe l’intera applicazione (ANR Message Dialog)
    - E' l’unico thread dell’applicazione che può eseguire operazioni sull’interfaccia utente (l’UI toolkit di android non è thread-safe)
    - Non può eseguire compiti "long-running" (fai thread specifico che gestisce questi compiti)
        - Worker Thread
            - Esempio: alla pressione di un bottone, si vuole scaricare dalla rete un’immagine e associarla ad un componente grafico dedicato presente sulla UI
                ```java
                public void onButtonClick(View v){
                    
                    Thread worker = new Thread(new Runnable(){
                        public void run(){
                            final Bitmap b = downloadImage("http://site.com/img.png");
                            
                            runOnUiThread(new Runnable(){
                                public void run(){
                                    imageView.setImageBitmap(b);
                                }
                            });
                        }
                    });
                    
                    worker.start();
                }
                
                private Bitmap downloadImage(String url){/* Long-Running Task */}
                ```
                - Thread-safe
                - IMPORTANTE: l’uso di più thread innestati rende molto difficile la comprensione del codice scritto (soluzioni: Handler, Async Task)
        - Handler: consente di accodare un messaggio alla coda dei messaggi di un Thread definito secondo il pattern MessageLoop (EventLoop); il Main Thread di Android è di fatto un MessageLoop
            - Approccio generale per la comunicazione tra Worker Thread e Main Thread
            - Quando si instanzia un Handler quest’ultimo è automaticamente associato al flusso di controllo del thread che lo crea
            ```java
            // Handler per ImageDownloaderTask
            public class MyHandler extends Handler{
                
                public static final int IMAGE_DOWNLOADED_MSG = 1;
                public static final String NEW_IMG = "new-img";
                private ImageView imageView;
                
                public MyHandler(Looper looper, ImageView iv){
                    super(looper); // Looper = event loop
                    this.imageView = iv;
                }

                @Override
                public void handleMessage(Message msg){
                    switch(msg.what){
                        case IMAGE_DOWNLOADED_MSG:
                            byte[] blob = msg.getData().getByteArray(NEW_IMG);
                            Bitmap img = BitmapFactory.decodeByteArray(blob, 0, blob.length);
                            imageView.setImageBitmap(img);break;
                    }
                }
            }
            
            // Creazione dell’Handler
            Handler h = new MyHandler(Looper.getMainLooper(),imageView);

            // WorkerThread che utilizza l’Handler
            public void onButtonClick(View v){
                new Thread(new Runnable(){
                    public void run(){
                        final Bitmap img = downloadImage("http://site.com/img.png");
                        final byte[] blob = convertImage(img);

                        final Bundle b = new Bundle();
                        b.putByteArray(MyHandler.NEW_IMG, blob);
                        
                        final Message msg = new Message();
                        msg.what = MyHandler.IMAGE_DOWNLOADED_MSG;
                        msg.setData(b);
                        
                        h.sendMessage(msg);
                    }
                }).start();
            }
            ```
            - Message e Bundle
                - Su un istanza di un handler è possible invocare il metodo `sendMessage()` per trasmettere all’handler un messaggio
                - Il messaggio trasmesso deve essere di tipo `Message`; il campo `what` consente di specificare un valore intero per discriminare il messaggio ricevuto
                - Per definire il contenuto di un messaggio conviene utilizzare un oggetto di tipo `Bundle` il quale consente di definire il contenuto secondo il pattern chiave-valore
            - Looper
                - E’ sempre possibile ottenere l’istanza del `MessageLoop` associato al Main Thread mediante il metodo `Looper.getMainLooper()`
                - Esempio
                    ```java
                    class LooperThread extends Thread {
                        public Handler handler;
                        
                        public void run(){
                            Looper.prepare();
                            handler = new Handler(){
                                public void handleMessage(Message msg){ /*  manage  msgs */ }
                            };
                            
                            Looper.loop();
                        }
                    }
                    ```
        - AsyncTask: semplifica l’esecuzione dei worker thread quando devono essere eseguiti compiti asincroni che hanno come effetto azioni sull’interfaccia utente
            - Permette di specificare quale porzione di codice deve essere eseguita in background da un worker thread e quale invece deve essere demandata al Main Thread
            - Non richiede di creare esplicitamente il worker thread
            - Esempio 1
                ```java
                // ImageDownloader con Async Task
                class DownloadImageTask extends AsyncTask <String, Void, Bitmap>{

                    // AsyncTask<Params,Progress,Result>:
                    //  - Params: tipo parametri passati a doInBackground
                    //  - Progress: tipo parametri passati a onProgressUpdate
                    //  - Result: tipo parametro di ritorno doInBackground() e parametro in ingresso onPostExecute()
                    
                    protected Bitmap doInBackground(String... urls){
                        Bitmap b = downloadImage(urls[0]);
                        return b;
                    }
                    
                    protected void onPostExecute(Bitmap result){
                        imageView.setImageBitmap(result);
                    }
                }
                
                // Caller
                public void onButtonClick(View v){
                    DownloadImageTask task = new DownloadImageTask();
                    task.execute("http :// site.com/img.png");
                }
                ```
            - Metodi ridefinibili
                - `void onPreExecute()`
                    - Primo metodo che viene invocato successivamente alla richiesta di esecuzione del task stesso
                    - Eecuzione gestita dal Main Thread
                - `Result doInBackground(Params... params)`
                    - Invocato subito dopo la terminazione di `onPreExecute()`
                    - Viene eseguito in un worker thread opportunamente creato
                    - Riceve in ingresso i parametri passati al metodo `execute()` richiamato sull’istanza del task
                    - Il risultato ritornato dal metodo è passato come parametro di input al metodo `onPostExecute()`
                    - Può richiamare il metodo `publishProgress()` per eseguire computazione minimale sul Main Thread
                - `void onProgressUpdate(Progress... params)`
                    - Eseguito sul Main Thread ogni volta che nel metodo `doInBackground()` viene richiamato il metodo `publishProgress()`
                    - Riceve in ingresso i parametri passati al metodo `publishProgress()`
                - `void onPostExecute(Result res)`
                    - Eseguito sul Main Thread successivamente alla terminazione del metodo `doInBackground()`
                    - Il risultato ritornato dalla `doInBackground()` è passato come parametro di input al metodo
            - Esempio completo
                ```java
                // Download N File
                class DownloadFilesTask extends AsyncTask<URL, Integer, Long>{
                    private int nFileDownloaded;
                    
                    protected void onPreExecute(){
                        nFileDownloaded = 0;
                        setupDownloadManager();
                    }
                    
                    protected Long doInBackground(URL... urls){
                        long bytesDownloaded = 0;
                        
                        for(int i = 0; i < urls.length; i++){
                            bytesDownloaded += downloadFile(urls[i]);
                            nFileDownloaded++;
                            publishProgress(nFileDownloaded);
                        }

                        return bytesDownloaded;
                    }
                    
                    protected void onProgressUpdate(Integer n){
                        updateDownloadCounter(n);
                    }
                    
                    protected void onPostExecute(Long bytes){
                        updateTotalBytesDownloadedCounter(bytes);
                    }
                }
                
                // Caller
                public void onButtonClick(View v){
                    DownloadFilesTask task = new DownloadFilesTask();
                    task.execute(url1, url2, url3);
                }
                ```
            - Importante
                - Tutti gli Async Task devono essere creati nel Main Thread, solo il Main Thread può richiamare il metodo `execute()` sull’istanza di un task
                - Su ciascuna istanza di Async Task il metodo `execute()` può essere richiamato una sola volta
                - Non devono essere chiamati manualmente i metodi `onPreExecute()`, `doInBackground()`, `onProgressUpdate()` e `onPostExecute()`
                - Tutti gli Async Task creati ed eseguiti all’interno di una stessa applicazione fanno riferimento allo stesso worker thread
                    - Esempio: se due diversi Async Task sono eseguiti all’interno della stessa applicazione il metodo `doInBackground()` del secondo task potrà essere eseguito solo successivamente alla terminazione dell’esecuzione dello stesso metodo del primo task
                    - Qualora si abbia la necessità di eseguire compiti in parallelo devono essere sfruttati altri meccanismi

### Parte 16

- Android SDK
    - Android Device Bridge (adb): comunicare con l’istanza dell’emulatore o con un device fisico connesso all’ambiente di sviluppo
    - Android DDMS: monitoraggio e interazione con le applicazioni in esecuzione su un device Android (importante per il debugging)
        - Logging: LogCat
            - Analizzare i flussi di logging di un qualunque device collegato all’IDE e quelli di una specifica applicazione in esecuzione
            - `android.util.Log` per aggiungere messaggi ai log
                - Esempio:
                    ```java
                    Log.d(C.LOG_TAG, "My message content");
                    
                    /* ... */
                    
                    public class C{
                        public static final String LOG_TAG = "MyAppTag";
                    }
                    ```
                - Importante: se uso `System.out.println(String msg)` il messaggio viene rediretto sul flusso di logging, senza tag, con livello verbose
- Android Studio: IDE
    - Usa Gradle
        - Importante
            - Alcune dichiarazioni che dovrebbero essere inserite nel file Manifest sono state spostate nel file `build.gradle`
            - Abilitare supporto a Java 8
                ```
                // build.gradle
                android {
                    ...
                    
                    compileOptions {
                        sourceCompatibility JavaVersion.VERSION_1_8
                        targetCompatibility JavaVersion.VERSION_1_8
                    }
                }
                ```
    - Supporta l’Instant Run
    - Gestione integrata dei repository GitHub
    - Android Virtual Device (AVD, in IDE AVD Manager): emulatore per dispositivi (smartphone/tablet) Android-based
        - Creazione di un AVD
            1. Tools > Android > AVD Manager > Create Virtual Device
            2. Phone > Nexus 5 (o quello che preferisci)
            3. Selezionare l’immagine di sistema da associare all’emulatore (meglio se Raccommended, il prof sceglie Marshmallow)
            4. Settare nome
            5. Opzioni avanzate
                - Emulated Performance: utilizzare o meno il supporto della GPU (Automatic)
                - Camera: webcam
                - Memory and Storage: RAM (768Mb - 1Gb), heap (64Mb) storage interno (1Gb), SDCard
                - Device Frame: skin emulatore
                - Keyboard: tastiera hardware
        - Simulare alcuni sensori (GPS, Batteria, Rete)
        - Simulare ricezione di chiamate/SMS
            - "More", l’ultimo tra quelli proposti dalla barra laterale dell’emulatore
    - Device fisico (how to)
        1. Abilitare le opzioni per lo sviluppatore sul device
        2. "Opzioni Sviluppatore", abilitare il Debug via USB
        3. Accettare la fingerprint della sorgente (PC) alla prima esecuzione dell’applicazione sul device fisico

### Parte 17

- GUI Android
    - Risorse (file aggiuntivi, un po come Scene Builder)
        - XML
        - Contenuto statico
        - E' possibile farne riferimento dal codice sorgente
        - Possono essere scelte (mediante identificatori) e "attivate" a runtime
            - Esempi
                - Se la GUI di una Activity è definita sotto forma di risorsa potranno essere definite più risorse per la stessa GUI e potrà quindi essere scelta quella più opportuna in funzione delle dimensioni dello schermo del device sul quale l’applicazione sarà seguita
                - Definire tutti i messaggi e i riferimenti testuali di un’applicazione in modo separato consente di agevolare la localizzazione dell’applicazione (passaggio da una lingua a un’altra)
        - L’Android Development Kit genera automaticamente un file `R.java` con tutti i riferimenti alle risorse definite nel progetto, in questo modo sarà possibile accedere a queste con un nome logico
        - Nella home directory del progetto è presente una directory `res` per le risorse organizzata in sub-directory
- UI Android
    - Rappresenta tutto ciò che l’utente può vedere e con cui può interagire direttamente
    - Esistono diversi componenti predefiniti per creare ed organizzare il layout di un’applicazione; a ciascuno di essi sono associati uno o più eventi, gestibili attraverso opportuni listener
    - Esempio
        ```xml
        <!-- myactivity_layout.xml -->
        <?xml version="1.0" encoding="utf-8"?>
        <!-- Il componente più esterno di qualunque layout deve avere "xmlns:android..." -->
        <LinearLayout
            xmlns:android="http://schemas.android.com/apk/res/android"
            android:layout_width="fill_parent"
            android:layout_height="fill_parent"
            android:orientation="vertical">

            <!-- "android:id": nome logico che sarà nel file R -->
            <TextView
                android:id="@+id/mytextview"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="I am a TextView"/>

            <Button
                android:id="@+id/mybutton"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="I am a Button"/>
        </LinearLayout>
        ```
    - Associazione di una GUI ad una Activity
        ```java
        @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            /* ... */
        }
        ```
    - Accesso ad elementi della GUI
        ```java
        Button btn = (Button)findViewById(R.id.mybutton);
        TextView txt = (TextView)findViewById(R.id.mytextview);
        ```
    - Associazione di un Listener al click del bottone
        ```java
        Button btn = (Button)findViewById(R.id.mybutton);
        btn.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v){/* do something */}
        });
        ```
    - Modifica del testo della TextView
        ```java
        TextView txt = (TextView)findViewById(R.id.mytextview);
        txt.setText("nuovo testo");
        ```
    - Dialogs: popup utilizzato generalmente per comunicare messaggi all’utente, per proporre una scelta e/o richiedere una conferma
        - `AlertDialog`
            ```java
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle("File unsaved...")
                .setMessage("Do you want to save this file?")
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        // save  file
                    }
                })
                .setNeutralButton("Undo", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){/*...*/}
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){/*...*/}
                })
                .create ();
            ```
    - Notifiche: messaggio che può essere proposto all’utente da un'applicazione al di fuori del layout dell’applicazione stessa
        - Standard
            ```java
            // Creazione e visualizzazione di Notifiche
            final int NOTIFICATION_ID = 1234;
            
            PendingIntent tapPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MyActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
            
            Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("File Downloaded")
                .setContentText("The requested file has been downloaded")
                .setAutoCancel(true)
                .setContentIntent(tapPendingIntent)
                .build();
                
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            
            notificationManager.notify(NOTIFICATION_ID, notification);
            ```
        - Toast: pop-up che appare in foreground sul display per un tempo predefinito occupando solo lo spazio richiesto per il testo della notifica (richiamabili anche da processi in background)
            ```java
            // Creazione e visualizzazione di Toast
            CharSequence text = "Hello toast!";
            
            Toast toast = Toast.makeText(getApplicationContext(), text,Toast.LENGTH_SHORT);
            toast.show();
            ```
    - Stringhe come risorse
        ```xml
        <!-- /res/values-{LANG}/strings.xml -->
        <?xml version="1.0" encoding="utf-8"?>
        <resources>
            <string name="appName">MyFirstApp</string>
            <string name="btn01text">Click me!</string>
            <string name="settingsMenuLabel">Settings</string>
            <string name="myActivityTitle">MyActivity</string>
        </resources>

        <!-- Uso di risorse testuali in altre risorse -->
        <Button android:id="@+id/mybutton"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/btn01text"/>
        ```
        ```java
        // Uso di risorse testuali nel codice sorgente
        String s = getString(R.string.myActivityTitle);
        ```
    - Shared Preferences: salvare informazioni (dati) persistenti in un’applicazione Android
        - Livelli di accesso
            - `Context.MODE_PRIVATE`: solo l'app può accedere ai dati
            - `Context.MODE_WORLD_READABLE`: tutte le app possono accedere ai dati in lettura
            - `Context.MODE_WORLD_WRITEABLE`: tutte le app possono accedere ai dati in scrittura
        - Esempio
            ```java
            // Salvataggio di elementi nelle Shared Preferences
            final String PREFERENCES_ID = "my-app-preferences";
            
            SharedPreferences preferences = getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
            
            preferences.edit()
                .putString("USERNAME", "mario.rossi")
                .putString("PASSWORD","abc123")
                .putBoolean("LOGGED",true)
                .commit ();

            // Verifica e Recupero di elementi precedentemente salvati
            final String PREFERENCES_ID = "my-app-preferences";
            
            SharedPreferences preferences = getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
            
            String username = preferences.getString("USERNAME", "unavailable");
            boolean logged = preferences.getBoolean("LOGGED", false);

            // Shared Preferences listener
            SharedPreferences.OnSharedPreferenceChangeListener spcl = new SharedPreferences.OnSharedPreferenceChangeListener(){
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sp, String s){
                    //do  something
                }
            };
            
            preferences.registerOnSharedPreferenceChangeListener(spcl);
            ```

### Parte 18

- Comunicazione
    - HTTP
        - Richieste
            - Permessi
                ```xml
                <uses-permission android:name="android.permission.INTERNET"/>
                <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
                ```
                - Da API Level 28 per effettuare richieste via rete non criptate va specificato l’attributo `android:usesCleartextTraffic="true"` in `<application>` nel file manifest; diversamente sono consentite solo richieste criptate con SSL o certificate
            - Esempi
                ```java
                // GET
                String doGet(URL url){
                    HttpURLConnection c = (HttpURLConnection)url.openConnection();
                    c.setRequestMethod("GET");
                    
                    if(c.getResponseCode() == HttpURLConnection.HTTP_OK)
                        return readStream(c.getInputStream());
                }

                // POST
                String doPost(URL url, byte[] payload){
                    HttpURLConnection c = (HttpURLConnection)url.openConnection();
                    c.setRequestMethod("POST");
                    c.setDoOutput(true);
                    c.getOutputStream().write(payload);
                    
                    if(c.getResponseCode() == HttpURLConnection.HTTP_OK)
                        return readStream(c.getInputStream());
                }
                ```
            - Note
                - Non è consentito effettuare richieste HTTP sul Main Thread
                - Le richieste HTTP sono intrinsecamente asincrone
    - Verifica della connettività
        ```java
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
       
        if(activeNetwork.isConnectedOrConnecting()){
            // Network is reachable
        }
        ```
    - Bluetooth
        - Permessi
            ```xml
            <uses-permission android:name="android.permission.BLUETOOTH"/>
            <uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
            <!-- Necessario per device con Android 6.0 o superiore -->
            <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
            ```
        - Inizializzazione
            ```java
            public class MyActivity extends Activity{
                
                private BluetoothAdapter btAdapter;
                private final int ENABLE_BT_REQ = 1;
                
                @Override
                protected void onCreate(Bundle savedInstanceState){
                    super.onCreate(savedInstanceState);
                    
                    /* ... */
                    
                    btAdapter = BluetoothAdapter.getDefaultAdapter();
                    
                    if(btAdapter == null){
                        Log.e("MyApp", "BT is not available on this device");
                        finish();
                    }

                    if(!btAdapter.isEnabled()){
                        startActivityForResult(
                            new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                            ENABLE_BT_REQ);
                    }
                }
                    
                @Override
                public void onActivityResult(int reqID, int res, Intent data){
                    if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_OK){
                        // BT enabled
                    }
                    
                    if(reqID == ENABLE_BT_REQ && res == Activity.RESULT_CANCELED){
                        // BT enabling process aborted
                    }
                }
            }
            ```
        - Ricerca dispositivi
            - Device Discovery
                - Procedura di scanning delle frequenze BT per identificare gli eventuali device presenti nel raggio di visibilità
                    ```java
                    public class MyActivity extends Activity{

                        private BluetoothAdapter btAdapter;
                        private Set <BluetoothDevice> nbDevices = null;
                        
                        private final BroadcastReceiver br = new BroadcastReceiver(){
                            @Override
                            public void onReceive(Context context, Intent intent){
                                BluetoothDevice device = null;
                                
                                if(BluetoothDevice.ACTION_FOUND.equals(intent.getAction())){
                                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                                    nbDevices.add(device);
                                }
                            }
                        };

                        @Override
                        protected void onCreate(Bundle savedInstanceState){
                            super.onCreate(savedInstanceState);
                            setContentView(/* ... */);
                            
                            btAdapter = BluetoothAdapter.getDefaultAdapter();
                            
                            /* Initialize BT... */
                            
                            registerReceiver(br, new IntentFilter(BluetoothDevice.ACTION_FOUND));
                        }
                        
                        @Override
                        public void onStart(){
                            super.onStart();
                            btAdapter.startDiscovery();
                        }

                        @Override
                        public void onStop(){
                            super.onStop();
                            btAdapter.cancelDiscovery();
                        }
                    }
                    ```
                - Per poter attivare la connessione con uno dei device identificati deve essere eseguita l’operazione di pairing (procedura di accoppiamento di due dispositivi BT a carico dell’utente)
                    ```java
                    // Lista dei paired devices
                    String BT_TARGET_NAME = "mario-Phone";
                    
                    BluetoothDevice targetDevice = null;
                    
                    Set <BluetoothDevice> pairedList = btAdapter.getBondedDevices();
                    
                    if(pairedList.size() > 0){
                        for(BluetoothDevice device: pairedList){
                            if(device.getName() == BT_TARGET_NAME)
                                targetDevice = device;
                        }
                    }
                    ```
        - Creazione del canale di comunicazione
            - Tipi
                - Server side
                    1. Si attiva un Thread (MasterThread) che crea una serverSocket che attende richieste di connessioni (chiamata bloccante del metodo `accept()`); la serverSocket può essere ottenuta dal BluetoothAdaper mediante la funzionalità `listenUsingRfcommWithServiceRecord()` a cui deve essere passato un generico nome per il canale e l’UUID
                    2. Quando la serverSocket riceve una richiesta di connessione, questa restituisce la socket specifica su cui è strato attivato il canale
                    3. Tale socket è quindi passata all’istanza di un gestore della connessione bluetooth (ConnectionManager) il quale può essere usato sia per attendere i messaggi (dati raw) in ingresso, sia per inviare messaggi al client
                - Client side
                    1. Si esegue un task deputato ad eseguire il tentativo di connessione al server; l’istanza della socket su cui tentare la connessione al server può essere ottenuta mediante la funzionalità `createRfcommSocketToServiceRecord()` a cui si passa l’UUID condiviso con il server
                    2. Se la connessione va a buon fine viene eseguita un’istanza dello stesso gestore di connessione presente sul server (ConnectionManager) per gestire la connessione lato client
            - UUID (Universal Unique Identifier): utilizzato per identificare univocamente il canale di comunicazione
                - Client e Server devono condividere il medesimo UUID per poter comunicare
                - UUID per dispositivi embedded (no pairing esplicito): `00001101-0000-1000-8000-00805F9B34FB`
            - Esempio: `AndroidBluetoothEx`

### Parte 19

- Android Sensor Framework (ASF, `android.hardware.*`): costituisce la porzione di Framework Android per l’accesso e la gestione dei sensori
    - API
        - `SensorManager`: creare l’istanza di un oggetto che rappresenta il servizio associato ad un sensore specifico; fornisce i metodi per l’accesso ai sensori e per la registrazione di listener
        - `Sensor`: creare istanze specifiche per ciascun sensore
        - `SensorEvent`: istanza per ciascun evento propagato da ciascun sensore
        - `SensorEventListener`: interfaccia che deve essere implementata da qualunque oggetto che debba essere progettato per ricevere informazioni dai sensori d’interesse
    - Identificazione dei sensori disponibili
        ```java
        // Lista dei Sensori disponibili
        private SensorManager sm;
        
        @Override
        protected void onCreate(Bundle savedInstanceState){
            /* ... */
            
            sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            
            List <Sensor> sensors = sm.getSensorList(Sensor.TYPE_ALL);
        }

        // Disponibilità di uno specifico sensore
        private SensorManager sm;
        private Sensor accelerometer;
        
        @Override
        protected void onCreate(Bundle savedInstanceState){
            /* ... */
            accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            
            if(accelerometer != null){
                // The Accelerometer sensor is available
            }
        }
        ```
    - Monitoring dei dati prodotti dai sensori
        ```java
        // Activity che usa il sensore di luminosità ambientale
        public class MainActivity extends Activity {
            
            private SensorManager sm;
            private Sensor lightSensor;
            private LightSensorListener lsListener;
            
            @Override
            protected void onCreate(Bundle savedInstanceState){
                super.onCreate(savedInstanceState);
                setContentView(/* ... */);
                
                sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
                lightSensor = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
                
                if(lightSensor != null)
                    lsListener = new LightSensorListener();
            }

            @Override
            protected void onResume(){
                super.onResume();
                
                if(lightSensor != null)
                    sm.registerListener(lsListener, lightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
            }
            
            @Override
            protected void onPause(){
                super.onPause();
                
                if(lightSensor != null)
                    sm.unregisterListener(lsListener);
            }
        }

        // Definizione del listener
        public class LightSensorListener implements SensorEventListener{
            private static final String LOG_TAG = "app -tag";
            
            @Override
            public void onSensorChanged(SensorEvent event){
                float actualvalue = event.values[0];
                
                Log.d(LOG_TAG, "Actual Value: " + actualvalue);
            }
            
            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy){
                //do something
            }
        }
        ```
    - Best-practices
        - Deregistrare sempre i listener per i sensori nella callback `onPause()` dell’activity che utilizza il sensore
        - Non inserire meccanismi bloccanti nella funzione `onSensorChanged()`, usare task asincroni
        - Verificare sempre la presenza dei sensori prima di utilizzarli
- Sensori
    - Movimento: misurano le forze di accelerazione e le forze di rotazione relative agli assi

        ![alt text](./res/sisref.png "Sistema di riferimento per i Sensori")

        - Accelerometro: misura l’accelerazione ($`\frac{m}{s^{2}}`$) applicata al dispositivo lungo i tre assi del SdR, includendo anche la forza di gravità
            - A device fermo, appoggiato su una superficie piana con lo schermo rivolto verso l’alto, i valori di accelerazione per gli assi X e Y sono prossimi allo zero mentre il valore dell’accelerazione lungo l’asse Z è circa 9,81 (accelerazione di gravità)
            ```java
            SensorManager sm = ...
            // Hardware
            Sensor accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            // Software (no accelerazione di gravità)
            Sensor linear_accelerometer = sm.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELEROMETER);
            ```
        - Giroscopio: misura la rotazione ($`\frac{rad}{s}`$) rispetto ai tre assi del dispositivo (positiva = direzione oraria)
            - Generalmente, i valori ottenuti dal giroscopio sono combinati con i dati temporali per calcolare la rotazione del dispositivo con l’evolvere del tempo
            ```java
            SensorManager sm = ...
            Sensor gyroscope = sm.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            ```
    - Ambientali: misurano parametri ambientali come temperatura, pressione e grado di illuminazione
    - Posizione: determinano la posizione fisica del dispositivo (geolocalizzazione, vedi seguito)
- Near Field Communication (NFC, `android.nfc`)
    - La lettura di un generico tag NFC può essere attuata interpretando i valori di uno o più record NDEF (NFC Data Exchange Format) memorizzati nel tag
        - Struttura di un record NDEF

            ![alt text](./res/nfcstruc.png "Struttura di un record NDEF")

            - TNF (Type Name Format): specifica come interpretare il successivo campo Type
            - Type: tipo
            - ID (opzionale): id
            - Payload: contenuto
    - Permessi
        ```xml
        <!-- Min API level: 10 -->
        <uses-permission
            android:name="android.permission.NFC"/>
            
        <uses-feature
            android:name="android.hardware.nfc" android:required="true"/>
        ```
    - How-to
        - Setup
            ```java
            // Inizializzazione
            private static final String MIME_TEXT_PLAIN = "text/plain";
            
            private NfcAdapter nfcAdapter;
            
            @Override
            protected void onCreate(Bundle savedInstanceState){
                super.onCreate(savedInstanceState);
                setContentView(/* ... */);
                
                nfcAdapter = NfcAdapter.getDefaultAdapter(this);
                
                if(nfcAdapter == null){
                    //NFC not supported
                    finish();
                }
            }

            // Attivazione
            @Override
            public void onResume(){
                super.onResume();
                startNFCDispatch(this, nfcAdapter);
            }

            private void startNFCDispatch(Activity a, NfcAdapter adapter){
                Context ctx = a.getApplicationContext();
                
                final Intent i = new Intent(ctx, a.getClass());
                i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                
                final PendingIntent pi = PendingIntent.getActivity(ctx, 0, i, 0);
                
                IntentFilter[] filters = new IntentFilter[1];
                filters[0] = new IntentFilter();
                filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
                filters[0].addCategory(Intent.CATEGORY_DEFAULT);
                
                try {
                    filters[0].addDataType(MIME_TEXT_PLAIN);
                } catch(MalformedMimeTypeException e) { /* ... */ }
                
                String[][] techList = new String[][]{};
                adapter.enableForegroundDispatch(a, pi, filters, techList);
            }

            // Rilascio
            @Override
            protected void onPause(){
                stopNFCDispatch(this, nfcAdapter);
                super.onPause();
            }
            
            private void stopNFCDispatch(Activity a, NfcAdapter adapter){
                adapter.disableForegroundDispatch(a);
            }
            ```
        - Utilizzo
            ```java
            // Connessione ad un Tag NFC
            // Tag resta valido finch è il tag NFC resta in prossimità del sensore
            // E' possibile leggere o scrivere il tag NFC
            @Override
            protected void onNewIntent(Intent intent){
                Tag tag = (Tag)intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                
                if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()))
                    if(MIME_TEXT_PLAIN.equals(intent.getType())){
                        //do something (I/O on tag)
                    }
            }

            // Lettura
            public String readNFCTag(Tag tag) throws Exception{
                Ndef ndef = Ndef.get(tag);
                
                if(ndef == null)
                    return null;
                    
                NdefRecord[] records = ndef.getCachedNdefMessage().getRecords();
                
                for(NdefRecord r: records)
                    if(r.getTnf() == NdefRecord.TNF_WELL_KNOWN
                            && Arrays.equals(r.getType(), NdefRecord.RTD_TEXT))
                        return analyzePayload(r.getPayload());
                return null;
            }

            private String analyzePayload(byte[] payload) throws Exception{
                String encoding = ((payload[0] & 0200) == 0) ? "UTF-8":"UTF-16";
                int langCodeLen = payload[0] & 0077;
                
                String tagContent = new String(payload, langCodeLen + 1,
                    payload.length - langCodeLen - 1, encoding);
                
                return tagContent;
            }

            // Scrittura
            public void writeTag(String content, Tag tag) throws Exception{
                NdefRecord[] records = new NdefRecord[1];
                records[0] = createRecord(content);
                
                NdefMessage message = new NdefMessage(records);
                
                Ndef ndef = Ndef.get(tag);
                ndef.connect();
                ndef.writeNdefMessage(message);
                ndef.close();
            }

            private NdefRecord createRecord(String text) throws Exception{
                String lang = "en";
                
                byte[] textBytes = text.getBytes();
                byte[] langBytes = lang.getBytes("US-ASCII");
                
                int textLength = textBytes.length;
                int langLength = langBytes.length;
                
                byte[] payload = new byte[1 + langLength + textLength];
                payload[0] = (byte)langLength;
                
                System.arraycopy(langBytes, 0, payload, 1, langLength);
                System.arraycopy(textBytes, 0, payload, 1 + langLength,
                    textLength);
                
                NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                    NdefRecord.RTD_TEXT, new byte[0], payload);
                
                return recordNFC;
            }
            ```
- Geolocalizzazione (`android.location.*`)
    - Tipi
        - GPS-based
            - Molto precisa, entro qualche metro
            - Grande uso batteria
        - Network-based
            - Imprecisa
            - Piccolo uso batteria
    - Permessi
        ```xml
        <!-- Network -->
        <uses-permission
            android:name="android.permission.ACCESS_COARSE_LOCATION"/>
        <!-- GPS, include anche Network -->
        <uses-permission
            android:name="android.permission.ACCESS_FINE_LOCATION"/>
        ```
    - How-to
        ```java
        // Richiedere gli aggiornamenti sulla posizione
        LocationListener listener = new LocationListener(){
            public void onLocationChanged(Location location){
                makeUseOfNewLocation(location);
            }
            
            public void onStatusChanged(String provider, int status,
                Bundleextras) {/* ... */}
            public void onProviderEnabled(String provider) {/* ... */}
            public void onProviderDisabled(String provider) {/* ... */}
        };
        
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);

        // Richiedere l’ultima posizione rilevata
        String lp = LocationManager.NETWORK_PROVIDER; // or GPS_PROVIDER
        
        Location lastKnownLocation = lm.getLastKnownLocation(lp);

        // Interrompere gli aggiornamenti
        LocationListener listener = ...
        lm.removeUpdates(listener);
        ```
- Permessi
    - Da API level 23 i permessi sono stati suddivisi in due categorie (a seconda che si richieda l’accesso a feature che coinvolgano o meno la privacy dell’utente)
        - NORMAL: in manifest
        - DANGEROUS: in manifest e l’utente deve esplicitamente accettare la richiesta
            ```java
            // Richiesta relativa al permesso ACCESS_FINE_LOCATION
            final int ACCESS_FINE_LOCATION_REQUEST = 1234;
            
            int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
                
            if(permission == PackageManager.PERMISSION_GRANTED){
                // OK, user has confirmed the permission
            } else {
                ActivityCompat.requestPermissions(this,
                    new String[] {
                        Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    ACCESS_FINE_LOCATION_REQUEST);
            }

            // Callback di notifica
            @Override
            public void onRequestPermissionsResult(int reqCode,
                Stringpermissions[], int[] res){
                
                switch(reqCode){
                    case ACCESS_FINE_LOCATION_REQUEST:
                        // If request is cancelled, the result arrays are empty
                        if(res.length > 0 && res[0] ==
                            PackageManager.PERMISSION_GRANTED){
                            // permission was granted!
                        } else {
                            // permission denied!
                        }
                        break;
                }
            }
            ```
