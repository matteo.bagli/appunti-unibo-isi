# IOT

### Indice

- [Teoria](./Riassunto_IOT.md)
- [Laboratorio](./Lab_IOT.md)
- [Informazioni e suggerimenti](https://gitlab.com/p2f/seiot/course)

#### Progetti

- [Progetto 1: Led2Bag](https://gitlab.com/p2f/seiot/project/01-led2bag)
- Progetto 2: Smart Radar
    - [Controller](https://gitlab.com/p2f/seiot/project/smart-radar/controller)
    - [Console](https://gitlab.com/p2f/seiot/project/smart-radar/console)
    - [Report](https://gitlab.com/p2f/seiot/project/smart-radar/report)
- Progetto 3: Smart Dumpster
    - [Controller](https://gitlab.com/p2f/seiot/project/smart-dumpster/controller)
    - [Edge](https://gitlab.com/p2f/seiot/project/smart-dumpster/edge)
    - [Service](https://gitlab.com/p2f/seiot/project/smart-dumpster/service)
    - [App](https://gitlab.com/p2f/seiot/project/smart-dumpster/app)
    - [Report](https://gitlab.com/p2f/seiot/project/smart-dumpster/report)
