# DATABASE

## Indice

- [Parte 0 - Syllabus](./Parte0.md)
- [Parte 1 - Introduzione](./Parte1.md)
- [Parte 2 - Progettazione DB](./Parte2.md)
- [Parte 3 - Modello Entity Relationship](./Parte3.md)
- [Parte 4 - Progettazione concettuale schema E/R](./Parte4.md)
- [Parte 5 e 6 - Algebra relazionale](./Parte5-6.md)
- [Parte 7 - Progettazione logica](./Parte7.md)
- [Parte 8 - SQL](./Parte8.md)
- [Parte 9 - Normalizzazione](./Parte9.md)
- [Parte 10 - Funzionalità e architetture del DBMS](./Parte10.md)
- [Parte 11 - Livello fisico](./Parte11.md)
- [Parte 12 - Organizzazioni primarie](./Parte12.md)
- [Parte 13 - External sorting](./Parte13.md)
- [Parte 14 - Indici](./Parte14.md)
- [Parte 15 - B-tree e B+tree](./Parte15.md)
- [Parte 16 - Hashing](./Parte16.md)
- [Parte 17 a - Query plan cardinality and histogram selectivity](./Parte17a.md)
- [Parte 17 b - Query plan cardinality and histogram selectivity](./Parte17b.md)
- [Parte 18 - Join methods](./Parte18.md)
- [Costi - How to](./costi_howto.md)

## Progetto

[MediBase](https://gitlab.com/myasnik/medibase)
