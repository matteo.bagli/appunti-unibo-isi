### Parte 12 - Organizzazioni primarie

- **Classificazioni di organizzazioni di file**
    * Primaria vs secondaria
        + **Primaria**: impone un criterio di allocazione dei dati.
            - **Heap file**: strutture sequenziali non ordinate.
                * **Inserimento**: append alla fine del file, pertanto è sufficiente mantenere un riferimento all'ultimo blocco del file.
                * **Eliminazione**: marcare come "non valido" il record interessato.
                * **Ricerca**
                    + Ogni blocco ha probabilità 1/NP di ospitare il record cercato.
                        - Caso esistenza: 
                            * Migliore: in media si accede a NP/2 blocchi (NP >> 1)
                            * Peggiore: si accede a NP blocchi
                        - Caso inesistenza: si visitano NP blocchi
                * La struttura permette di gestire record:
                    - A lunghezza fissa
                    - A lunghezza variabile
                    - Spanning su più pagine
                * Per scandire il file in sequenza il DBMS deve tenere traccia dei blocchi (pagine) che contengono i record e di quelli marcati come non validi.
                * I DBMS implementano opportuni accorgimenti per gestire le eliminazioni e gli inserimenti di record in posizioni marcate non più valide, cercando così di non degradare l'efficienza delle operazioni.
                * **Utilità**: 
                    + Piccoli volumi di dati (se vengono usati grandi volumi di dati la struttura non è efficiente).
                    + Operazioni che interessano tutti o gran parte dei record.
                    + Aggiornamenti poco frequenti (se vengono effettuati vari aggiornamenti va eseguita periodicamente una riorganizzazione).
            - **Sorted sequential file**: strutture sequenziali ordinate in base ad un campo chiave.
                * Mantenere l'ordinamento puntuale a fronte di inserimenti, cancellazioni e modifiche può essere molto dispendioso. Per rendere più efficiente l'inserimento si prevedono spazi liberi nei blocchi e l'allocazione di ulteriori blocchi in un overflow file non ordinato.
                * Periodicamente i due file (overflow e master) vengono fusi.
                * **Ricerca**
                    + **Sequenziale**: (NP + 1)/2 blocchi.
                    + **Dicotomica** (NP = 2^M - 1 con M > 0)
                        - Caso esistenza (NP >> 1)
                            * Media: intinf(log2(NP)) blocchi
                            * Peggiore: intinf(log2(NP)) + 1 blocchi
                        - Caso inesistenza: intinf(log2(NP)) + 1 blocchi
            - **Hash file**: ad accesso calcolato.
                * Ogni indirizzo generato dalla funzione hash H individua una pagina logica (bucket).
                * Esistono le collisioni.
                * Overflow se chiave in bucket pieno.
                    + Area costituita dai bucket indirizzabili: area primaria.
                    + Area overflow.
                * Tipi
                    + Statiche: spazio indirizzi fissato.
                    + Dinamiche: spazio indirizzi varia in base al numero dei record.
            - **Indexed organization**: ad albero -> modalità migliore.
        + **Secondaria**: ulteriore metodo di accesso all'organizzazione primaria, utilizza le medesime strutture dati.
    * Statica vs dinamica
        + **Dinamica**: si adatta alla mole dei dati.
        + **Statica**: prevede fasi di riorganizzazione a fronte di variazioni, del volume di dati.
    * Per chiave primaria vs chiave secondaria
        + **Chiave primaria**: identifica un unico record.
        + **Chiave secondaria**: identifica più record.
- **Operazioni**
    * **Ricerca** (esatta) per:
        + Chiave primaria: restituisce al più un solo record.
        + Chiave secondaria: restituisce 0 o più record.
        + Intervallo: restituisce 0 o più record.
            - Es: I contribuenti con reddito inferiore a 40.000€
        + Varie combinazioni: restituisce 0 o più record.
    * **Inserimento**
    * **Cancellazione**
    * **Modifica**
- **Clustering**: presenza di "addensamenti" di dati nei blocchi del file.

    ![alt text](./res/clustering.png "Schema clustering.")

- **Indexing**: risolvere efficacemente operazioni di ricerca e aggiornamento tramite opportune strutture dati e metodi di accesso.

    ![alt text](./res/indexing.png "Schema indexing.")

- **File**
    * **Non strutturato**: sequenza di byte su cui è possibile operare attraverso primitive orientate alla manipolazione dei byte.
    * **Strutturato**: collezione di record a lunghezza fissa o variabile che appartengono a un certo tipo di dato astratto.