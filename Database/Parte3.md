### Parte 3 - Modello Entity Relationship

- **Inventore**: Peter Pin-Shan Chen
- **Data**: 1976
- **Elementi fondamentali**
    * **Entità**
        + **Denominazione**: sostantivo al singolare (es: persona, corso...) scritto in maiuscolo e unico.
        + Rappresenta una classe (insieme) di oggetti che possiedono caratteristiche comuni (es. persone, automobili...) e che sono indipendenti dalle proprietà ad esse associate.
        + Graficamente si rappresenta con un rettangolo al cui interno è evidenziata la denominazione dell'entità.
        + **Istanza**: specifico oggetto appartenente all'insieme che quella entità rappresenta, non possono esserci due istanze uguali.
            | Definizione adottata | Definizione Chen    | 
            | -------------------- |:-------------------:|
            | Entità               | Entity set          |
            | Istanza di entità    | Entity              |
        + **Tipi**
            - **Deboli**
                * Contengono istanze la cui presenza nella base dati è accettata solo se sono presenti determinate istanze di altre entità da cui queste dipendono.
                * Nel caso di eliminazione dell'istanza di riferimento le istanze deboli collegate devono essere eliminate o ridefinite.
                * L'identificatore dell'entità debole deve contenere l'identificatore dell'entità da cui dipende.
                * L'entità debole deve partecipare a una associazione con l'entità dominante con vincolo di identificazione e di partecipazione totale.
                * Esempio:

                ![alt text](./res/ent-debol.png "Esempio entità debole.")
            - **Dominanti**: altrimenti.
    * **Associazione**
        + **Denominazione**: sostantivo al singolare (es: residenza, appartenenza...) scritto in maiuscolo e unico.
        + Rappresenta un legame logico tra entità, rilevante nella realtà che si sta considerando.
        + Graficamente si rappresenta con un rombo al cui interno ne viene evidenziata la denominazione.
        + È possibile stabilire più associazioni, con diverso significato, tra le stesse entità.
        + **Ad anello**
            - Un'associazione ad anello lega un'entità con sé stessa, e quindi mette in relazione tra loro le istanze di una stessa entità.
            - Tipi
                * Simmetrica: (a,b) ∈ A ⇒ (b,a) ∈ A
                * Riflessiva: (a,a) ∈ A 
                * Transitiva: (a,b) ∈ A, (b,c) ∈ A ⇒ (a,c) ∈ A
            - Nelle non simmetriche è necessario specificare per ogni ramo dell'associazione il relativo ruolo.
            ![alt text](./res/non-sim.png "Associazione ad anello non simmetrica.")
            - N-arie

            ![alt text](./res/n-aria.png "Associazione ad anello n-aria.")
            
            Il dipendente d1 dirige il dipendente d2 all'interno del progetto p, o il contrario.
        + **XOR/AND**

            ![alt text](./res/xor-and.png "Associazione xor e and.")
        + **Grado**: un'associazione n-aria coinvolge n entità. Il grado di un'associazione è il numero di istanze di entità che sono coinvolte in un'istanza dell'associazione.
        + **Istanza**
            - Combinazione di istanze delle entità che prendono parte all'associazione; dunque una ennupla costituita da occorrenze di entità, una per ogni entità coinvolta nell'associazione.
            - L'insieme delle istanze di un'associazione è un sottoinsieme del prodotto cartesiano degli insiemi delle istanze di entità che partecipano all'associazione.
            - Non possono esserci istanze ripetute in un'associazione.

            | Definizione adottata    | Definizione Chen    | 
            | ----------------------- |:-------------------:|
            | Associazione            | Relationship set    |
            | Istanza di associazione | Relationship        |
    * **Attributo**
        + E' una proprietà elementare di un'entità o di un'associazione. È denotato con un nome che deve essere univoco all'interno dell'entità o associazione a cui si riferisce.
        + Ogni attributo è definito su un dominio di valori:
            - Per l'entità PERSONA gli attributi e i relativi domini sono:
                * Nome: stringa(20)
                * Cognome: stringa(20)
                * CodiceFiscale: stringa(16)
                * DataNascita: Giorno × Mese × Anno
                    + Giorno = 1...31
                    + Mese = {Gen,Feb,Mar,Apr,Mag,Giu,Lug,Ago,Set,Ott,Nov,Dic}
                    + Anno = 1900...2100
                * TitoloStudio: stringa(50)
        + Graficamente:

        ![alt text](./res/attributi.png "Esempio attributi.")
        + **Composto**
            - Si ottengono aggregando altri (sotto-) attributi, i quali presentano una forte affinità nel loro uso e significato.
            ![alt text](./res/at-comp.png "Esempio attributi composti.")
    * **Vincolo di cardinalità**
        + **Tipi**
            - **Implicito**: dipende dalla semantica stessa dei costrutti del modello.
            - **Esplicito**: definiti da chi progetta lo schema E/R sulla base della conoscenza della realtà che sta modellando.
        + **Attributi**
            - E' possibile specificare il numero minimo e il numero massimo di valori che possono essere associati a un'istanza della corrispondente associazione o entità a cui l'attributo appartiene.
            - Graficamente:

            ![alt text](./res/vincoli-att.png "Esempio vincoli attributi.")
            - **Tipi**
                * Opzionale: cardinalità minima = 0
                * Monovalore: cardinalità massima = 1
                * Multivalore: cardinalità massima = N
        + **Associazioni**
            - Sono coppie di valori (min-card, max-card) associati a ogni entità che partecipa a un'associazione; specificano il numero minimo e massimo di istanze dell'associazione a cui un'istanza dell'entità può partecipare.
            - Graficamente:

            ![alt text](./res/vincoli-ass.png "Esempio vincoli associazione.")
            - Esempio relativo all'immagine:
                * Ogni istanza di E partecipa almeno a una istanza di A
                * Ogni istanza di E può partecipare a più istanze di A
            - **Terminologia**
                * Si dice che la partecipazione di E1 in A e:
                    + **Opzionale** se min-card(E1, A) = 0
                    + **Obbligatoria (o totale)** se min-card(E1, A) > 0
                    + **A valore singolo** se max-card(E1, A) = 1
                    + **A valore multiplo** se max-card(E1, A) > 1

    * **Identificatore**
        + Ha lo scopo di permettere l'individuazione univoca delle istanze di un'entità.
        + Ogni entità deve avere almeno un identificatore (interno o esterno).
        + Nel caso di più identificatori è ammesso che gli attributi o le entità coinvolti in alcune identificazioni, tranne una, possano essere opzionali.
        + Deve valere anche la proprietà di minimalità: nessun sottoinsieme dell'identificatore deve essere a sua volta un identificatore.
        + Gli attributi delle associazioni non possono essere identificatori.
        + **Tipi**
            - **In base a cosa è composto**
                * **Interno**: si usano uno o più attributi dell'entità
                * **Esterno**: si usano altre (una o più) entità, collegate a E da associazioni, più eventuali attributi propri di E
                * **Misto** 
            - **In base a "quanto" è composto**
                * **Semplice**: il numero di elementi che costituiscono l'identificatore è pari a 1
                * **Composto**: altrimenti

            ![alt text](./res/tipi-id.png "Esempio tipi identificatori.")
        + **Esempi**

            ![alt text](./res/es-id-comp.png "Esempio id composti.")
            ![alt text](./res/es-id-ex.png "Esempio id esterni.")
        + **Reificazione**
            - Se un'associazione ha un attributo composto e ripetuto, e uno degli attributi componenti è necessario per identificare le istanze dell'associazione, si trasforma l'associazione in entità e si crea un identificatore misto.
            ![alt text](./res/reificazione1.png "Esempio reificazione parte 1.")
            ![alt text](./res/reificazione2.png "Esempio reificazione parte 2.")
    * **Gerarchia di generalizzazione**

        ![alt text](./res/ger-gen.png "Esempio gerarchia generalizzazione.")
        + Ciascuna sottoclasse può avere una sola superclasse (ereditarietà singola). Alcune estensioni del modello prevedono la modellazione di ereditarietà multipla.
        + **Subset**: caso particolare di gerarchia (is a) in cui si evidenzia una sola classe specializzata.

        ![alt text](./res/subset.png "Esempio subset.")
- **Associazioni ternarie to binarie**
    * Quando in un'associazione ternaria esistono dipendenze funzionali tra le entità in gioco è preferibile sostituire la ternaria con associazioni binarie (che modellano esplicitamente i vincoli del problema), reificando.

    ![alt text](./res/t2b.png "Esempio ternaria to binaria.")
    * **False ternarie**: se una (o più) entità partecipano con cardinalità massima 1 a un'associazione ternaria siamo in presenza di una "falsa ternaria" che può essere sempre modellata, in modo equivalente, attraverso associazioni binarie.

    ![alt text](./res/false-ternarie.png "Esempio false ternarie.")
- **Soluzioni a problemi comuni**
    * Si vuole memorizzare l'orario dei treni, e i ritardi che essi hanno. Inoltre si vogliono gestire le prenotazioni dei clienti nell'ipotesi che:
        + L'orario sia sempre lo stesso indipendentemente dai giorni
        + Un treno non percorra la stessa tratta più di una volta al giorno
        ![alt text](./res/prob-treno.png "Problema comune del treno.")
    * In una biblioteca si vogliono mantenere informazioni sui libri (titolo, autore, anno, codice ISBN, stato conservazione) e sui prestiti relativi (data prestito, eventuale data restituzione, utente), segnalando eventuali danni apportati al volume.

    ![alt text](./res/prob-lib.png "Problema comune del libro.")
    * Docente e anno accademico dei corsi

    ![alt text](./res/prob-docente.png "Problema comune del docente.")
- **Limite fondamentale E/R**: non tutti i vincoli di integrità sono esprimibili in uno schema E/R, per esempio:
    * Per sostenere un esame è necessario avere sostenuto tutti gli esami propedeutici
    * Un laureando deve aver sostenuto almeno tutti gli esami dei primi anni