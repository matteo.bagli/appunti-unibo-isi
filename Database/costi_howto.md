### Costi - How to

- Indicizzazione
    - Primary: no ripetizioni
    - Secondary: ripetizioni
    - Dense: nell'albero ci sono tanti nodi quante foglie
    - Sparse: nodi < foglie
- Selettività
    - =: $`\frac{1}{NK}`$
    - range: $`\frac{v2 - v1}{vmax - vmin}`$
    - Nel caso in cui ci siano più selettività
        - AND: moltiplico le selettività
        - OR: $`f1 + f2 - (f1 \times f2)`$
        - NOT: $`1 - f`$

### Pattern esercizi

1. Selettività dei vincoli: ($`\frac{selezionati}{totali}`$)
2. Expected tuples (o records): $`\lceil NT \times (selettività) \rceil`$ (fare riferimento alla sezione soprastante "selettività")
3. Costo di sort (sempre Z-merge):
    1. Numero pagine relative alla query ($`NP_{R}`$): $`\lceil \frac{len(selected) \times ET}{D} \rceil`$
    2. Costo di sort: $`2 \times NP_{R} \times \lceil\log_{z}NP_{R}\rceil`$
4. Costo sequenziale (opzionale): $`NP + Csort`$
5. Costo clustered (opzionale): $`(h - 1) + EL + EP_{clustered} + Csort`$
    - $`(h - 1)`$ è opzionale (dipende dal testo)
    - Expected leafs: $`\lceil selettività_{indice} \times NL_{indice}\rceil`$
        - Chiave primaria: $`\lceil \frac{(len(chiave) + len(TID))  \times ER}{D} \rceil`$
        - Chiave secondaria: $`\lceil \frac{(len(indice) \times NK_{indice}) + (len(TID) \times NT)}{D} \rceil`$
    - Expected pages: $`\lceil selettività_{indice} \times NP\rceil`$
6. Costo unclustered (opzionale): $`(h - 1) + EL + EP_{unclustered} + Csort`$
    - $`(h - 1)`$ è opzionale (dipende dal testo)
    - Expected leafs
        - $`EK_{indice} \times \lceil\frac{1}{NK_{indice}} \times NL_{indice}\rceil`$
        - $`\lceil selettività_{indice} \times NL_{indice}\rceil`$
        - Chiave primaria: $`\lceil \frac{(len(chiave) + len(TID))  \times ET}{D} \rceil`$
        - Chiave secondaria: $`\lceil \frac{(len(indice) \times NK_{indice}) + (len(TID) \times NT)}{D} \rceil`$
    - Expected pages: $`EK_{indice} \times \Phi(\frac{NT}{NK_{indice}}, NP)`$
        - $`\Phi(\frac{NT}{NK_{indice}}, NP) = NP \times (1 - (1 - \frac{1}{NP}^{\frac{NT}{NK_{indice}}}))`$ 
        - Usata maggiormente: $`\Phi(\frac{NT}{NK_{indice}}, NP) = NP \times (1 - e^{- \frac{\frac{NT}{NK_{indice}}}{NP}})`$ 
7. Tuple attese dovute al group by: $`min(ET, NK_{gruppo})`$
- NB: D può essere anche D* (cioè $`D \times u`$) un po a piacere, negli esercizi non è definito bene