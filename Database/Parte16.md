### Parte 16 - Hashing

- Utilizza una funzione hash, H, che trasforma ogni valore di chiave in un indirizzo; H non è iniettiva per questo si possono verificare collisioni.
- Ogni indirizzo generato dalla funzione hash individua una pagina logica, o bucket. Si definisce capacità C del generico bucket come il numero di elementi che esso può ospitare.
    * Se una chiave viene assegnata a un bucket pieno, si verifica un overflow.
    * La presenza di overflow può richiedere l'uso di un'area di memoria separata, detta appunto area di overflow.
    * L'area di memoria costituita dai bucket è detta area primaria.
- Un elemento di un bucket è:
    * una coppia (key,RID) se l'organizzazione è un indice
    * un record dati se l'organizzazione è primaria
- Esempio
    * Hash file (Organizzazione primaria)

        ![alt text](./res/hash-or-pri.png "Esempio di Hash file (Organizzazione primaria).")

    * Secondary hash index on hash file

        ![alt text](./res/sec-has-in.png "Esempio di Secondary hash index on hash file.")

    * Hash index on heap file
        + La funzione hash non preserva l'ordinamento, dunque l'indice non è idoneo per ricerche di range.

        ![alt text](./res/hash-in-heap.png "Esempio di Hash index on heap file.")

- **Organizzazioni** (possono essere mischiate a coppie)
    * **Statica**: il valore di NP è costante (se ci sono troppi overflow bisogna riorganizzare l'intera struttura).
    * **Dinamica**: l'area primaria può espandersi e contrarsi, si rendono necessarie più funzioni di hash.
    * **Primaria**: hash file.
    * **Secondaria**: hash index, sconsigliati se sono possibili interrogazioni di range.
- **Organizzazione primaria statica**
    * **Ricerca** (nell'ipotesi di perfetta ripartizione dei record)
        + Un hash file che memorizza NR record in bucket di capacità C=C(ov), e la cui area primaria consiste di NP bucket, comporta che:
            - ogni indirizzo è generato $`\frac{NR}{NP}`$ volte
            - ogni catena consiste di $`\frac{NR}{(NP×C)}`$ bucket.
        + **Costo**: proporzionale a $`\frac{NR}{(NP×C)}`$
    * **Inserimento/cancellazione**: Ricerca + Scrittura di un bucket
- **Static hash index**
    * **Ricerca** (nell'ipotesi di perfetta ripartizione dei record)
        + Un hash index su chiave primaria memorizza NRI = NR coppie (ki,RIDi), una per ogni record del file dati. Sia NPI il numero di pagine bucket dell'indice nell'area primaria e sia CI la capacità di un bucket dell'indice.
        + **Costo**: $`\frac{NR_{I}}{2\times (NP_{I}\times C_{I})}`$ bucket + 1 data page.
            - Caso peggiore/insuccesso si accede a tutti i blocchi della catena se i bucket non mantengono le chiavi in ordine.
    * **Inserimento/cancellazione**: Ricerca + Scrittura di un bucket indice + Scrittura pagina dati
- **Hashing per chiavi multiple**
    * Esempio
        + Attributi chiave
            - A1: int(5)
            - A2: int(9)
            - A3:string(10)
        + Area primaria di $`2^{9}`$ = 512 bucket (indirizzabile con h=9 bit)
            - b1=4 per A1
            - b2=3 per A2
            - b3=2 per A3 
        + Funzioni hash
            - H1=A1 mod 16
            - H2=A2 mod 8
            - H3=(n. caratteri # blank di A3) mod 4
        + Esempio: (58651, 130326734, "mamma") -> 11|6|1 in binario:1011|110|01 bucket #377
        + Esempio: 
            - Se mancano condizioni su A3: (58651, 130326734, ?) -> 1011|110|{00,01,10,11}
- **Distribuzioni dei valori di chiave note a priori**: si possono adottare funzioni hash ad hoc.
- **Funzioni hash**
    * Funzioni suriettive.
    * Il caso ideale rispetto al quale è ragionevole confrontare una specifica funzione hash H è quello di distribuzione uniforme sullo spazio degli indirizzi, in cui, ognuno degli NP indirizzi ha la stessa probabilità 1/NP di essere generato.
    * **Qualità**: le prestazioni variano al variare dello specifico set di chiavi.
        + Esempio: La funzione H(ki)=ki mod NP è una "buona" funzione, ma nel caso, ad esempio, del set di chiavi: {NP, 2×NP, 3×NP, ...  ,NR×NP} alloca tutte le chiavi nel bucket 0.
    * **Tipi**
        + **Mid square**: la chiave è elevata al quadrato, si estrae poi un numero di cifre centrali pari a quelle di NP - 1.
        + **Shiftig**: la chiave è suddivisa in un certo numero di parti, ognuna costituita da un numero di cifre pari a quelle di NP - 1, si sommano poi le parti.
        + **Folding**: come nello shifting, in più le parti vengono reversate.
        + **Divisione** (più affidabile): modulo P (se P<NP, si deve porre NP=P per non perdere la suriettività).
            - P è il più grande numero primo minore o uguale a NP.
            - P è non primo, minore o uguale a NP, con nessun fattore primo minore di 20.
- **Fattore di caricamento** (best [0.7,0.8])
    * Rapporto fra il numero di chiavi allocate e la capacità dell'area primaria: $`d=\frac{NR}{NP\times C}`$ 
    * All'aumentare del fattore di caricamento aumenta la percentuale di record in overflow.
    * d può essere maggiore di 1 nel caso di area separata per l'overflow.
    * **Ricerca d e NP adeguati**

        ![alt text](./res/ricerca-p-np.png "Schema di ricerca dei valori d e NP.")

- **Trasformazioni di chiavi alfanumeriche**
    * Stabilisco:
        + Un alfabeto A, a cui appartengono i caratteri delle stringhe
        + Una funzione biiettiva ord(), che associa a ogni elemento dell'alfabeto un intero nel range [1,|A|]
        + Una base di conversione b
        + **Formula**: $`k(S)=\sum_{i=0}^{n-1} ord(s(i))\times b^{i}`$ 
        + Esempio
            - A = {a,b,...,z}
            - ord() in [1,26]
            - b=32
            - $`k("indice")=9\times 32^5 +14\times 32^4 +4\times 32^3 +9\times 32^2 +3\times 32^1 +5\times 32^0 = 316810341`$ 
    * Problemi metodo divisione -> b e NP fattori primi in comune
        + A = {a,b,...,z}
        + ord() in [1,26]
        + b=32
        + NP=512
        + Valore di chiave determinato solo da ultimi due caratteri
            - "folder"
                * I valori ordinali sono 6, 15, 12, 4, 5, 18 
                * k("folder")=217452722
                * H(217452722)=178
            - "primer"
                * I valori ordinali sono 16, 18, 9, 13, 5, 18
                * k("primer")=556053682
                * H(556053682)=178 
- **Capacità dei bucket**
    * All'aumentare di C, e a parità di fattore di caricamento d, la percentuale di record in overflow diminuisce.
    * E' consigliabile scegliere C massima purché la lettura di un bucket comporti una singola operazione di I/O e il trasferimento di un bucket di capacità C avvenga in un tempo minore del trasferimento di due bucket di capacità minore di C.
- **Gestione dell'overflow**
    * **Open hashing**
        + **Metodi di concatenamento (chaining)**: fanno uso di puntatori.
            - I record in overflow possono essere allocati 
                * In un'area separata (area di overflow)
                    + Per evitare eccessivo spreco di spazio:
                        - la capacità Cov dei bucket di overflow può essere minore di C
                        - è possibile che più bucket di overflow siano mappati in un singolo blocco
                * Nella stessa area primaria
                    + **Liste separate**: se un bucket j è saturo, i record in overflow aventi j come home bucket sono allocati nel primo bucket non pieno, eseguendo una ricerca a partire dal bucket j+1. Tutti i record che collidono sono collegati a lista, inclusi quelli non in overflow. Ogni record deve pertanto includere un campo puntatore al record successivo della catena.
                    + **Liste confluenti** (coalesced chaining): si fa uso di un puntatore per bucket, se il bucket j va in overflow:
                        - il record viene inserito nel primo bucket non pieno, j+h
                        - si attiva un collegamento da j a j+h
                        - se anche j+h va in overflow, allora i nuovi record vengono inseriti nel bucket j+r
    * **Closed hashing**
        + **Metodi di indirizzamento aperto (open addressing)**: non fanno uso di puntatori.
            - I record in overflow sono allocati in area primaria tramite una legge di scansione (la funzione STEP).
            - Le collisioni sono risolte cercando nei vari slot dell'array finchè il record o uno slot libero non sono trovati (probing).
            - L'utilizzazione della memoria allocata, u, coincide con il fattore di caricamento (u=d), il quale deve pertanto sempre essere minore o uguale a 1.
            - Nel caso in cui si renda necessario gestire un numero di record NR>NP×C, si deve provvedere a una riorganizzazione completa.
            - **Inserimento**

                ![alt text](./res/ins-op-ad.png "Esempio di inserimento con open addressing.")
            
            - **Cancellazione**
                - Esempio: la cancellazione della chiave 34 (in figura precedente) non permetterebbe più di reperire 89 e 50; dunque si deve gestire lo stato di ogni posizione all'interno dei bucket dopo la cancellazione (magari spostando).
            - **Well-known probe sequences**
                + **Linear probing**
                    - A ogni passo l'indirizzo è incrementato di una quantità costante s: $`STEP(H_{l-1}(k_{i}))=(H_{l-1}(k_{i})+s) mod NP`$
                    - Per garantire che, per ogni home bucket, siano generati tutti gli NP indirizzi è necessario che s non abbia divisori in comune con NP. In caso contrario, sono generati solo NP/MCD(NP,s) indirizzi distinti. Può pertanto accadere che non si riesca a inserire una chiave anche se il fattore di caricamento è minore di 1.
                    - **Clustering primario**: i record tendono ad addensarsi in alcuni bucket, a causa della linearità della funzione.
                        * Per evitarlo si ricorre al quadratic probing (scansione quadratica).
                + **Scansione quadratica** (quadratic probing)
                    - Le sequenze di indirizzi si diversificano in funzione dell'home bucket: $`STEP(H_{l-1}(k_{i}))=(H_{l-1}(k_{i})+a+b\times (2\times l-1)) mod NP`$
                    - Problema -> **Clustering secondario**: chiavi che hanno lo stesso home bucket produrranno sempre la stessa sequenza. Per evitarlo si ricorre al double hashing.
                + **Double hashing**
                    - Due funzioni hash, le sequenze di indirizzi sono date da:
                        * $`H_{0}(k_{i}) = H'(k_{i})`$
                        * $`H_{l}(k_{i}) = (H_{l-1}(k_{i}) + H''(k_{i})) mod NP`$ (l > 0)
                    - **Pro**
                        * Due chiavi generano la stessa sequenza di indirizzi se e solo se collidono sia con H' sia con H''. La tecnica di double hashing approssima abbastanza bene il caso ideale di "hash uniforme" (random probing), in cui ogni indirizzo ha la stessa probabilità di essere generato al passo l-esimo.
                    - **Contro**
                        * Forte variabilità degli indirizzi generati, dipendentemente dal valore di H''(ki) = appesantimento operazioni I/O.
    * **Prestazioni**
        + **Ricerca con successo**
            - **Overflow in area primaria**: u = d
            - **Overflow in area dedicata**: $`u=\frac{NR}{NP\times C+NP_{ov} \times C_{ov}`$ ($`NP_{ov}`$) = n. bucket overflow allocati.
        + **Ricerca con insuccesso**: open addressing non risulta ottimo.
    * **Prestazioni chaining**
        + **Coalesced chaining**
            - **Ricerca con successo**: $`1+\frac{1}{8\times d} (e^{2\times d} -1-2\times d)+\frac{d}{4}`$
            - **Ricerca con insuccesso**: $`1+\frac{1}{4} (e^{2\times d} -1-2\times d)`$
        + **Separate chaining**
            - **Ricerca con successo**: $`1+\frac{1}{2\times d}`$
            - **Ricerca con insuccesso**: $`e^{-d} +d`$
- **Organizzazione dei record nei bucket**
    * Inserimento in prima posizione libera.
    * Inserimento in ordine di chiave.
    * Inserimento in prima posizione libera + mantenimento di indice interno.
- **Organizzazioni hash dinamiche**
    * Adattano l'allocazione dell'area primaria alla dimensione corrente del file.
    * **Famiglie**
        + **Con direttorio** (direttorio = struttura ausiliaria)
            - **Virtual hashing**
                * Raddoppiare l'area primaria quando si verifica un overflow in un bucket, e ridistribuire i record tra il bucket saturo e il suo "buddy", facendo uso di una nuova funzione hash. In  pratica si esegue lo "split" del bucket saturo.

                    ![alt text](./res/virt-hash.png "Esempio concettuale di virtual hashing.")

                * Se, successivamente, qualche altro bucket nell'area primaria originale va in overflow, e il suo buddy non è ancora in uso, si ridistribuiscono i suoi record tra il bucket stesso e il buddy.

                    ![alt text](./res/virt-hash-1.png "Esempio concettuale di overflow in virtual hashing.")
                
                * Poiché, a un certo istante, solo alcuni buddy sono effettivamente in uso, è necessario fare ricorso a una struttura ausiliaria che permetta di determinare se occorre utilizzare la vecchia funzione hash o la nuova.
                * **Gestione area primaria**
                    + **Inizializzazione**
                        1. Allocati $`NP_{0}`$ bucket di capacità C, e si fa uso di una funzione hash $`H_{0}`$ a valori in [0,$`NP_{0}`$−1].
                        2. Si pone L=0; L rappresenta il numero di raddoppi eseguiti.
                        3. Si predispone un vettore binario V, di dimensione pari al numero di bucket, inizializzando tutti gli elementi al valore 1 (V[j]=1  indica che il j-esimo bucket è in uso).
                    + **Split di un bucket**
                        - Se bucket j in overflow e L=0 oppure il buddy di livello L è già in uso/non esiste:
                            1. Si incrementa L, si raddoppia l'area primaria e il vettore V; i nuovi elementi di V sono posti a 0, eccetto lo slot corrispondente al buddy appena usato per lo split che è posto a 1.
                            2. Si introduce la nuova funzione hash $`H_{L}`$ in $`[0, 2^{L}×NP_{0}−1]`$.
                            3. Si ridistribuiscono le chiavi del bucket j facendo uso della funzione $`H_{L}`$ 
                        - Altrimenti:
                            1. Si determina il buddy di livello minimo r (1 ≤ r ≤ L) non ancora in uso.
                            2. Si pone lo slot di V corrispondente al buddy sopracitato a 1.
                            3. Si ridistribuiscono le chiavi del bucket j facendo uso della funzione $`H_{r}`$ 
                        - Esempio

                            ![alt text](./res/virt-hash-ex.png "Esempio virtual hashing split.")

                            ![alt text](./res/virt-hash-ex-1.png "Esempio virtual hashing split.")

                            ![alt text](./res/virt-hash-ex-2.png "Esempio virtual hashing split.")

                            ![alt text](./res/virt-hash-ex-3.png "Esempio virtual hashing split.")

                        - **Importante**
                            * Inserimento della chiave k
                                + Se $`H_{2}(k)`$ =2 che è un bucket saturo e il buddy di livello 1 non è in uso allora si pone in uso V[9]=1 e si ridistribuisce con $`H_{1}`$.
                                + Se la ridistribuzione fallisce il prossimo buddy è quello di livello 2, si pone in uso V[16]=1 e si ridistribuisce con $`H_{2}`$.
                                + Se fallisce ancora si raddoppia.

                                ![alt text](./res/virt-hash-ex-4.png "Esempio virtual hashing split.")
                * **Funzioni hash**
                    + Condizioni da soddisfare
                        - **Range condition**: $`H_{L}`$ in $`[0, 2^{L}×NP_{0}−1]`$
                        - **Split condition**: lo split di un bucket deve lasciare una chiave nel bucket stesso, o allocarla nel buddy.
                * **Ricerca e inserimento di una chiave**
                    ```
                    Returns: address of searched key
                    Parameters: l = level, k = key

                        public int Search_key(int l, key k){
                            if l<0 then return l; //key does not exist
                            else if V[Hr(k)]==1 return Hr(k);
                            else return Search_key(l−1,k);
                        }
                    ```
            - **Dynamic hashing**
            - **Extendible hashing**
        + **Senza direttorio**
            - **Linear hashing**
                * Non si esegue lo split del bucket in cui si è verificato un overflow, ma si suddivide un altro bucket, scelto secondo un criterio prefissato.
                * **Gestione dell'area primaria**
                    + Si allocano $`NP_{0}`$ bucket e si usa la funzione hash: $`H_{0}(k)=k mod NP_{0}`$
                    + Si mantiene un puntatore (split pointer, SP) al prossimo bucket che deve essere suddiviso. Inizialmente SP=0.
                    + Se si verifica un overflow si aggiunge in coda un bucket di indirizzo $`NP_{0}+SP`$, si riallocano i record del bucket SP (inclusi quelli eventualmente presenti in area di overflow) facendo uso della nuova funzione hash: $`H_{1}(k)=k mod (2 \times NP_{0})`$ e si incrementa SP di 1.
                    + Dopo un'espansione completa,ci si predispone per una nuova espansione ponendo:
                        - SP=0
                        - $`H_{0}(k) = H_{1}(k)`$ 
                        - $`H_{1}(k) = k mod(2^{2} \times NP_{0})`$ 
                    + Durante la L-esima espansione:
                        - Funzioni hash
                            * $`H_{0}(k) = k mod (2^{L-1} \times NP_{0})`$ 
                            * $`H_{1}(k) = k mod (2^{L} \times NP_{0})`$ 
                        - Indirizzo home bucket di una chiave:
                            ```
                                if H0(k)<SP then Address←H1(k)
                                else Address←H0(k)
                            ```
                * Esempio

                    ![alt text](./res/es-lin-hash.png "Esempio linear hashing.")
                
                * **Pro**
                    + L'assenza di un direttorio e la politica di gestione degli split rendono semplice la realizzazione della struttura.
                    + La gestione dell'area primaria è immediata, in quanto i bucket vengono sempre aggiunti (e rimossi) in coda.
                * **Contro**
                    + 0.5 < $`u= \frac{NR}{C \times NP + C_{ov} \times NP_{ov}}`$ < 0.7
                    + La gestione dell'area di overflow presenta problemi simili a quelli di un'area primaria statica.
                    + Le catene di overflow relative ai bucket di indirizzo maggiore, non ancora suddivisi, possono diventare molto lunghe.
                * **Varianti**
                    + **Controllo del carico**: gli split avvengono ogni L inserimenti.
                    + **Split controllato**: si esegue lo split solo quando il fattore di caricamento, d, raggiunge un valore di soglia d_min.
            - **Spiral hashing**