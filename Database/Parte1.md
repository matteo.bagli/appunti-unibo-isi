### Parte 1 - Introduzione

- **Definizioni fondamentali**
    * **Dato** = ciò che è immediatamente presente alla conoscenza prima di ogni elaborazione.
    * **Informazione** = notizia, dato o elemento che consente di avere conoscenza più o meno esatta di fatti, situazioni, modi di essere.
- **Sistema Informativo** (SI): Un SI deve provvedere alla raccolta e alla classificazione delle informazioni, da attuarsi con procedure integrate e idonee, al fine di produrre in tempo utile e ai giusti livelli le sintesi necessarie per i processi decisionali, nonché per gestire e controllare le attività dell’ente nel suo complesso.
    * **SI != Sistema Informatico**
    * **SI** = Database + Sistema software.
- **Sistema complesso** = Macchina che riceve qualcosa in input, lo elabora, e restituisce in output (statico).
- **Sistema complesso adattivo** = Macchina che riceve qualcosa in input, lo elabora, e restituisce in output adattandosi al problema che deve risolvere (dinamico).
- **Classificazione dei SI**
    * **Executive Support Systems** (ESS)
    * **Decision Support System** (DSS) = a supporto dei processi direzionali e quindi delle decisioni non strutturate.
    * **Transaction Processing System** (TPS) = transazionali a supporto dei processi operativi e quindi delle decisioni strutturate.
    * **Management Information System** (MIS) = tattici a supporto dei processi gestionali e quindi delle decisioni semi strutturate.
    * **Enterprise Resource Planning** (ERP) = sistema di gestione che integra i processi di business rilevanti di un'azienda.
- **Che cosa rende un’informazione utile nel processo decisionale?**
    * **Soggettività**: il valore associato a un'informazione differisce da individuo a individuo e dipende dal tipo di decisione (un dato invece è oggettivo).
    * **Rilevanza**: l'informazione deve essere pertinente alla decisione da prendere.
    * **Tempestività**: l'informazione è utile alla decisione solo se è disponibile nel momento decisionale.
    * **Accuratezza**: le informazioni devono essere corrette e precise.
    * **Presentazione**: l'informazione deve essere utilizzabile direttamente per la decisione senza ulteriori elaborazioni.
    * **Accessibilità**: le informazioni devono essere disponibili appena necessarie a chi le richiede (tenendo in debito conto le necessarie problematiche di sicurezza e privatezza).
    * **Completezza**: il decisore deve avere a disposizione le informazioni necessarie per prendere una decisione corretta.
- **Sistemi informatici settoriali**
    * Il flusso di informazioni intra-settoriale è più intenso rispetto al flusso inter-settoriale.
        + Nei sistemi settoriali del passato il flusso di dati da un settore all'altro era di norma gestito tramite la creazione e trasmissione di copie in formato elettronico e/o cartaceo.
    * **Problemi**
        + La progettazione degli archivi di settore è effettuata sulla base di considerazioni locali. La mancanza di standard a livello globale complica la gestione dei flussi inter-settoriali e può creare problemi di incompatibilità nelle rappresentazioni adottate.
        + I dati sono soggetti a diversi vincoli di integrità, che riflettono la conoscenza della realtà specifica rappresentata. Se i vincoli emersi in fase di analisi dipendono solo da specifiche considerazioni settoriali, è evidente che si possono generare inconsistenze.
        + La presenza di copie dello stesso dato dà luogo a ridondanze. Di conseguenza si ha un inutile spreco di memoria. Inoltre,la ridondanza può dar luogo a problemi d'inconsistenza delle copie.
        + L'uso di file non condivisi e il mero ricorso ai servizi del filesystem presenta molteplici problemi in termini di facilità d'accesso ai dati, di sviluppo delle applicazioni, di flessibilità, di controllo degli accessi concorrenti, di sicurezza...
- **Perché non adottare un file system?**
    * Per gestire grandi quantità di dati, potenzialmente scalabili in dimensione, in modo persistente e condiviso, e consentire di soddisfare esigenze applicative che mutano nel tempo, il ricorso a un filesystem presenta molteplici inconvenienti:
        + Povertà dell'astrazione offerta per modellare i dati. È richiesta al programmatore l'esplicitazione, nel codice, dei percorsi per accedere ai dati.
        + Difficoltà per l'accesso alle informazioni: ogni nuova esigenza implica la scrittura di un modulo software ad hoc.
        + I meccanismi di condivisione sono in genere limitati.
        + Non sempre sufficienti i meccanismi forniti per la protezione a fronte di guasti.
        + Vincoli d'integrità sui dati hardcodati (difficoltà notevoli per la modifica o l'aggiunta di vincoli).
        + Non sono disponibili i vari servizi aggiuntivi offerti da un DBMS.
- **Peculiarità di un DBMS**
    * Un DBMS è un sistema software che gestisce grandi quantità di dati persistenti e condivisi, e che offre supporto per almeno un modello dei dati in grado di fornire agli utenti un'astrazione di alto livello attraverso cui definire strutture di dati complesse e interagire con il DB.
    * Attua indipendenza tra programmi e dati, e tra programmi e operazioni.
    * Garantisce efficienza nella gestione di grandi quantità di dati e persegue obiettivi di efficacia, nel senso di supporto soddisfacente alla produttività degli utenti.
    * La persistenza e la condivisione richiedono che un DBMS fornisca meccanismi per garantire l'affidabilità dei dati (fault tolerance), per il controllo degli accessi e per il controllo della concorrenza.
    * Diverse altre funzionalità sono messe a disposizione per semplificare la descrizione delle informazioni, lo sviluppo delle applicazioni, l'amministrazione di un DB...
- **Chiarezza su nomi e sigle**
    * **Database Engine/Storage Engine**: componente software che un DBMS usa per gestire le operazioni su un DB (Create, Read, Update, Delete..). Spesso il termine è usato come sinonimo di Database Server o anche di DBMS.
    * **Database Instance**: indica i processi e le strutture dati di un database engine in esecuzione.
    * **DBS** (Data Base System) = DBMS + n * DB
    * **Sistema Informatico** = n * DBS
    * **RDBMS** (Relational Data Base Management System) = DBMS che offre il modello relazionale come modello logico dei dati (la maggior parte dei RDBMS usa il linguaggio SQL, Structured Query Language).
- **Ottimizzatore query**
    * L’ottimizzatore (optimizer) è un modulo software che si fa carico di determinare il miglior piano di accesso (queryplan) per eseguire una query. 
    * Un metodo di accesso è un modo per accedere ai dati che utilizza una specifica organizzazione "fisica", ad esempio un indice B+tree.
- **NoSQL** = Non utilizzo il modello relazionale.
- **Caratteristiche di un SI**
    * Visione globale del sistema e del suo ruolo nell’ente
    * Quadro concettuale di riferimento
    * Adeguate metodologie di progetto e di controllo di qualità
    * Appropriate metodologie per il monitoraggio in esercizio
    * Esigenze di interoperabilità e scalabilità
    * Flessibilità e possibilità "illimitate" di accesso ai dati
    * Architettura a elevate prestazioni
    * Ridondanza, Sicurezza, Protezione
    * Disponibilità continua dei dati
    * Interfaccia utente adattativa
- **Ciclo di vita di un SI** (Database Development Life Cycle)
    * **Analisi (Progettazione concettuale, passi successivi di raffinamento)** (una delle fasi principali per lo sviluppo di un SI)
        1. **Strategic study** (definizione strategica)
            + Si assumono decisioni sulle aree aziendali che devono essere oggetto di automazione.
        2. **Information system planning** (pianificazione)
            + Si definiscono gli obiettivi e si evidenziano i fabbisogni; viene condotto uno studio di fattibilità per individuare possibili strategie d'attuazione e avere una prima idea dei costi, dei benefici e dei tempi.
        3. **Business analysis** (analisi dell’organizzazione)
            + Si formalizzano i requisiti e si producono macro-specifiche per la fase di progettazione.
    * **Progettazione e testing (Progettazione logica)**
        1. **System design** (progettazione del sistema)
            + Si interpretano i requisiti in una soluzione architetturale di massima. Sono prodotte specifiche formali indipendenti dagli  strumenti che saranno usati per la costruzione del sistema.
        2. **Construction design** (progettazione esecutiva)
            + Le specifiche del passo precedente sono rese vincolanti per lo staff addetto alla realizzazione, descrivendo la struttura dei componenti dell'architettura hardware, softwaree di rete.
        3. **Construction and workbench test (Progettazione fisica)** (realizzazione e collaudo in fabbrica) 
        4. **Installation** (installazione)
        5. **Test of installed system** (collaudo del sistema installato)
    * **Esecuzione e morte**
        1. **Operation** (esercizio)
        2. **Evolution** (evoluzione)
        3. **Phase out** (messa fuori servizio)
        4. **Post mortem**
- **Fasi di progettazione di un SI nel dettaglio**
    * **Progettazione concettuale** (fornire specifiche indipendenti dagli strumenti di realizzazione)
        + Vengono impiegati
            - **Schemi Entity/Relationship** per modellare gli aspetti statici.
            - **Data Flow Diagram** per modellare gli aspetti funzionali.
            - **State Diagram** per modellare gli aspetti dinamici.

            ![alt text](./res/progettazione_concettuale.png "ER Scheme, Data Flow Diagram, State Diagram.")
    * **Progettazione logica** (relazionale)
        + Obiettivo: descrivere la base dati secondo il modello logico (relazionale) adottatodal DBMS (RDBMS).
        ![alt text](./res/progettazione_logica.png "Progettazione logica.")
    * **Progettazione fisica** 
        + Obiettivo: scelta delle strutture di memorizzazione e degli indici, considerando anche il carico di lavoro atteso in termini di query. Con riferimento all'esempio una soluzione potrebbe prevedere la creazione di:
            ```
            - indice su LINEE_AEREE.CodLinea (chiaveprimaria) clustered
            - indice su DESTINAZIONI.CodDest (chiaveprimaria) clustered
            - indice su DESTINAZIONI.NomeDest (chiavesecondaria) unclustered 
            - indice su VOLI.NumVolo (chiaveprimaria) unclustered
            - indice su VOLI.CodDest (chiavesecondaria) clustered
            - indice su VOLI.CodLinea (chiavesecondaria) unclustered
            ```
    * **Progettazione dell'applicazione**
        + Obiettivo: realizzazione moduli software per le funzioni e per l'interfaccia utente.
        ![alt text](./res/progettazione_applicazione.png "Progettazione applicazione.")

- **Durante lo sviluppo di un SI è fondamentale**
    * **Modellazione di aspetti ontologici** nella fase di analisi; in sostanza l'interazione fra utenti e analisti deve essere svolta in modo strutturato e coerente, cercando di superare incomprensioni derivanti dai differenti patrimoni conoscitivi e ambiguità proprie del linguaggio naturale, servendosi anche di verifiche appropriate in itinere.
        + **Statici**
            - **Conoscenza concreta** (oggetti e fatti specifici, le loro caratteristiche e le loro interrelazioni)
            - **Conoscenza astratta** (fatti generali che descrivono la conoscenza concreta e regolan il modo in cui essa può evolvere)
        + **Funzionali**
            - **Conoscenza procedurale** (modalità per modificare la conoscenza concreta e ricavare altri fatti)
        + **Dinamici**
            - **Dinamica** (modalità per l'evoluzione nel tempo della conoscenza)
            - **Comunicazione** (modalità per accedere alla conoscenza)
- **Data Warehouse**
    * **A cosa serve?**
        + Input
            - Dati estratti dall'ambiente di produzione
            - Dati provenienti da SI esterni all'organizzazione
        + Output
            - Visione unificata e integrata dei dati in input
    * Esempio
        + Mobile Database System (MDS) = Spazio d'informazione interconnesso + wireless
- **Big Data**
    * Raccolta di dataset così grande e complessa da richiedere strumenti differenti da quelli tradizionali, in tutte le fasi del processo: dall'acquisizione, alla condivisione, all'analisi e alla visualizzazione.
    * Mole dati: Zettabyte (miliardi di Terabye)
    * **Fonti**
        + Social Media
        + Social Network
        + Motori di ricerca
        + Sensori ambientali
        + E-commerce
        + ...
    * **Applicazioni**
        + Analisi di mercato
        + Predizione
        + Studi di settore
        + Previsioni climatiche
        + Analisi scientifica/medica
        + ...
    * **Caratteristiche**
        + **Volume** = Dimensione del dataset (Ogni 60 secondi nel mondo ci sono 1,820 Terabyte di dati creati!)
        + **Velocità** = Velocità di generazione dati
        + **Varietà** = Varie tipologie di dati
        + **Variabilità** = Possibilità di inconsistenza
        + **Complessità** = Maggiore è la dimensione del dataset, maggiore è la complessità dei dati da gestire; il compito più difficile è collegare le informazioni e derivare nuova conoscenza
    * **Data Mining, Information Extraction**
        + Da grandi quantità di dati si possono derivare nuove informazioni e creare nuova conoscenza.
            - Es: Analisi dei social media
                * Social Net Analysis
                * Sentiment Analysis
                * Profiling di utenti
    * **Paradigma**
        + Prima: Pochi generano dati, tutti gli altri ne fanno uso
        + Dopo: Tutti generano dati, tutti ne fanno uso


- **Internet of things** (IoT)
    * "A dynamic global network infrastructure with self-configuring capabilities based on standard and interoperable communication protocols where physical and virtual "things" have identities, physical attributes, and virtual personalities and use intelligent interfaces, and are seamlessly integrated into the information network."