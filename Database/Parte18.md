### Parte 18 - Join methods

- Nell'implementazione più semplice il join di due relazioni R e S, rispettivamente con $`NR_{R}`$ record in $`NP_{R}`$ pagine e $`NR_{S}`$ record in $`NP_{S}`$ pagine, prevede il confronto di ogni record di R con ogni record di S, con complessit à O($`NR_{R} \times NR_{S}`$).
- **Two-way**: coinvolge due relazioni.
- **Multi-way**: coinvolge più di due operazioni.
    * Tra n relazioni può essere risolto tramite l'esecuzione di n-1 two-way join, indipendenti l'uno dall'altro.
- **Join tree**
    * E' un albero binario i cui nodi interni sono operatori di join e le cui foglie sono relazioni.
    * **Tipi**
        + **Left-deep**

            ![alt text](./res/es-left-deep.png "Esempio left deep tree.")

        + **Rigth-deep**

            ![alt text](./res/es-right-deep.png "Esempio right deep tree.")

        + **Zig-zag** (include sia left che right join)

            ![alt text](./res/es-zig-zag.png "Esempio zig-zag tree.")

        + **Bushy** (coincide con i possibili join tree)

            ![alt text](./res/es-bushy.png "Esempio bushy tree.")
- **Pipeline**
    * In molti casi non è necessario che l'esecuzione di un join termini prima dell'inizio del successivo.

# A rega non ho più tempo

- [Link all'ultimo pacco di slide](./res/lez18.pdf)