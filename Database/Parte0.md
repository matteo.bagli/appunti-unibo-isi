### Parte 0 - Syllabus

- Glossario
    * Definizioni
        + **SI** (Sistema Informativo) = l'insieme di persone, risorse, strumenti e infrastrutture che un'organizzazione impiega in modo coordinato con l'obiettivo di acquisire, selezionare e ridistribuire le informazioni utili a individuare strategie di gestione efficienti ed efficaci.
        + **DBMS** (Data Base Management System) = sistema software in grado di gestire efficientemente le informazioni necessarie a un SI, rappresentandone i dati in forma integrata, secondo un modello logico, e  garantendone la persistenza, la condivisione, l'affidabilità e la privatezza.
        + **RDBMS** (Relational Data Base Management System) = un DBMS che adotta il modello relazionale per la rappresentazione dei dati a livello logico.
        + **DB** (Data Base) = in termini generali rappresenta una collezione di dati d'interesse per una o più applicazioni; nel contesto del corso è intesa come una collezione di dati gestita tramite un DBMS. I dati sono strutturati e collegati tra loro, a livello logico, nel rispetto del modello di rappresentazione (es. relazionale) adottato dal DBMS e, a livello fisico, risiedono su dispositivi di memoria organizzati in particolari strutture. Gli utenti si interfacciano con la base di dati attraverso un Query Language (es. SQL).
        + **Modello E/R** (Entity Relationship) = è un modello concettuale dei dati utile per descrivere ad alto livello la realtà osservata, indipendentemente da come i dati saranno logicamente e  fisicamente rappresentati.
    * Utenti
        + **Data Base Administrator** (DBA) = si occupa di installare, configurare e gestire il DMBS; crea gli oggetti logici necessari (es. tabelle, viste, indici...) per le applicazioni database; crea gli utenti e concede loro i dovuti privilegi; garantisce la sicurezza e l'integrità dei DB; effettua controllo e monitoraggio degli accessi ai DB; monitora e ottimizza le performance dei DB e delle applicazioni che li utilizzano; pianifica strategie di backup e recovery.
        + **Data Base Designer** (DBD) = cura la formulazione di un modello dettagliato del DB da implementare; il modello esplicita tutte le scelte progettuali a livello concettuale, logico e fisico e può essere usato per l'implementazione del database e per lo sviluppo di applicazioni che devono interagire con esso.
        + **Software Engineer** (SWE) = analisti di sistema e programmatori di applicazioni; i primi determinano le esigenze degli utenti finali e dettano specifiche per le transazioni che saranno realizzate a cura dei programmatori tramite linguaggi e strumenti opportuni.
        + **End User** (EU) = sono persone che interagiscono, a vari livelli, con una o più basi di dati per lo svolgimento delle proprie attività lavorative o per esigenze di altra natura.
            - **Naïve End User** (Parametric End User) = accede al DB tramite query preconfezionate all'interno di un'applicazione; non ha di norma nessuna conoscenza né del DBMS né della struttura della base dati (es. prenotazione di un viaggio, prelievo di denaro con bancomat...).
            - **Sophisticated End User** = hanno un certo grado di conoscenza della struttura del DB e delle potenzialità del DBMS; sono in grado di interagire direttamente con la base dati attraverso l'uso di un linguaggio d'interrogazione o, indirettamente, attraverso l'uso d'interfacce e/o di strumenti avanzati di reportistica e di analisi dei dati sempre nel rispetto dei privilegi d'accesso al DB che sono stati loro concessi.
        + **DBMS designer** = progettista dell'architettura software di un database engine.
        + **DBMS developer** = sviluppa moduli e interfacce di un DBMS.
        + **DB Design Tool developer** = sviluppa strumenti d'ausilio alla progettazione di DB e di applicazioni.
        + **Data Base Consultant** = svolge consulenza finalizzata all’impiego delle tecnologie delle basi di dati per migliorare i processi del sistema informativo.
        + **Data Custodian** = responsabile della conservazione dei dati, del loro trasporto, del loro trattamento nel rispetto delle regole d'integrità, privatezza e sicurezza.
        + **Data Steward** = responsabile della qualità dei dati nel rispetto delle regole di governo dell'organizzazione.
        + **Data Scientist** = ha competenze per analizzare, elaborare e interpretare big data.
