### Parte 14 - Indici

 - **Indice**: mappa che memorizza entrate del tipo: [valore chiave di ricerca, riferimento/i al/ai record].
    * **Famiglie**
        + **Ordered**: i valori di chiave sono mantenuti ordinati; possono essere a uno o più livelli.

            ![alt text](./res/tip-ind.png "Tabella dei tipi di indici ordinati.")

            - **NB**: sparse & unclustered sono quasi sempre incompatibili.

            ![alt text](./res/p-c-s-s-i.png "Esempio indice.")

            ![alt text](./res/s-u-d-s-i.png "Esempio indice.")

            - **Record pointer e block pointer**

                ![alt text](./res/rec-po-blo-po.png "Esempio record pointer e block pointer.")
            
            - **Organizzazione**
                * **Primaria**: se contiene i record.
                * **Secondaria**: se è un ulteriore accesso all'organizzazione primaria.
            - **Indici mono-livello**

                ![alt text](./res/es-i-m-l.png "Esempio di indici mono-livello.")

                * **Accesso**
                    1. Accesso all'indice
                    2. Ricerca della coppia (k(i), p(i))
                    3. Conversione di p(i) in indirizzo assoluto
                    4. Accesso al blocco dati relativo
                * **Ricerca binaria**
                    + Le coppie (k(i),p(i)) possono essere mantenute ordinate in base ai valori k(i).
                    + Il numero di accessi che si risparmiano è una costante che non dipende dalla dimensione del file dati, ma solo dalla lunghezza dei record e della chiave.
                    + **Costi d'accesso**
                        - Il file dati è memorizzato in NP blocchi di capacità C record e l'indice è memorizzato in un file di IP blocchi di capacità IC (> C).
                            * Vale la relazione: C × NP = IC × IP in quanto C × NP è il numero di record (che coincide con il numero di coppie (k(i),p(i)) nell'indice).
                        - Analisi costi
                            * Costo reperimento di un record con ricerca binaria sull'indice: log2(IP) (accessi a blocchi indice) + 1 (accesso a blocco dati)
                            * Risparmio rispetto ricerca dicotomica su file ordinato: log2(IC/C) - 1.
            - **Indici multi-livello**
                * **Requisiti**
                    + **Bilanciamento**: l’indice deve essere bilanciato ma considerando i blocchi anziché i singoli nodi, in quanto è il numero di blocchi a  cui bisogna accedere che determina il costo di I/O di una ricerca.
                    + **Occupazione minima**: limite inferiore all'utilizzo dei blocchi, onde evitare eccessivo spreco di memoria.
                    + **Efficienza di aggiornamento**: le operazioni di aggiornamento devono avere un costo limitato.
        + **Hash**: i valori di chiave con i relativi riferimenti ai record [valore chiave di ricerca, riferimento/i al/ai record] sono memorizzati in bucket i cui indirizzi sono generati da una funzione hash.