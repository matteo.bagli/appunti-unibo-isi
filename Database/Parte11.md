### Parte 11 - Livello fisico

- **Unità di misura e abbreviazioni**

    ![alt text](./res/misura.png "Tabella unità di misura.")

    ![alt text](./res/abbre.png "Tabella abbreviazioni.")

- **Gerarchia di memorie**
    * **Internal**: Processor registers and cache
    * **Main**: RAM
    * **On-line mass storage**: Secondary storage
    * **On-site storage on removable storage**: Near-line storage
    * **Off-line bulk storage**: Tertiary and Off-Line storage
- **Prestazioni memoria**
    * Dato un indirizzo di memoria, le prestazioni si misurano in termini di tempo di accesso, determinato dalla somma della latenza (tempo necessario per accedere al primo byte) e del tempo di trasferimento (tempo necessario per muovere i dati).
    * **Tempo di accesso** = **Latenza** + (**Dimensione dati** / **Velocità trasferimento**)
- **Disco**
    * Un DBMS gestisce i dati on-line facendo riscorso principalmente a dischi magnetici e solid-state drive.
    * I dati per essere elaborati devono essere trasferiti in memoria centrale:
        + il trasferimento avviene in termini di data block (pagine).
        + pagine piccole comportano un maggior numero di operazioni di I/O,  pagine grandi tendono ad aumentare la frammentazione interna (pagine parzialmente riempite) e richiedono più spazio in memoria per essere caricate.
    * Le operazioni di I/O costituiscono il collo di bottiglia del sistema, quindi si rende necessario ottimizzare l'implementazione fisica del DB.
    * **Astrazione**
        + **Livello applicazione**: record logici.
        + **Livello di sistema di archiviazione**: si lavora su blocchi di byte (es: 4096), la cui dimensione può dipendere dalle caratteristiche del sistema operativo (filesystem), o può essere stabilita dall'utente.
        + **Livello dispositivo**: si lavora su blocchi di byte, la cui dimensione è fissa, nel qual caso si parla più propriamente di settori (es: dischi) oppure variabile (macchine specifiche).
    * **Input/output**: astrazione costruita sopra una raw disk device interface oppure sopra OS file system. Una pagina può essere vista dunque come un "virtual block" corrispondente all'unità minima di dati che un DBMS può leggere o scrivere. Le richieste di I/O da parte di un DBMS riguardano multipli di pagina, mentre a livello di OS avvengono normalmente a multipli di OS block.
        + **Accesso**: a seconda del tipo di dispositivo sono permesse una o più modalità di accesso ai dati.
            - **Sequenziale**: i record sono localizzabili solamente in sequenza (dal primo all'ultimo) all'interno del file.
            - **Diretto**: i record sono localizzabili in base alla loro posizione (numero d'ordine) relativa all'interno del file.
            - **Associativo**: i record sono localizzabili in base al valore di un campo chiave all'interno del file.
        + Il S.O. vede un disco virtuale con blocchi numerati consecutivamente che vengono mappati dal controller del disco sui vari settori.
        + **Prestazioni**
            - **Latenza** = **Command Overhead Time** + **Seek Time** + **Settle Time** + **Rotational Latency**
                * **Command Overhead Time**: impartire comandi al drive (trascurabile).
                * **Seek Time**: spostare le testine sul cilindro desiderato.
                    + Tipi
                        - Brevi (meno di 200-400 cilindri): prevale la fase di accelerazione, il cui tempo si può assumere proporzionale alla radice del numero di cilindri attraversati.
                        - Lunghi: prevale la seconda fase a velocità costante, e il tempo si può assumere proporzionale al numero di cilindri attraversati più una costante.
                    + Il seek time per scritture è superiore (di circa 1 ms) rispetto al seek time per letture.
                    + **Average seek time**
                        - **Stima**: Average seek time = 1/3 * cilindri
                        - **Seek distance** = |X - Y| dove X e Y sono due cilindri. 
                            * **Stima**: Average seek distance = n/3   
                    + **Formule**
                        - t(d) = (a x sqrt(d-1)) + (b x (d-1)) + c
                            * E(t(d)) != t(E(d))
                            * 0 < d < n_cilindri
                            * n_cilindri > 200
                            * Primo termine: accelerazione/decelerazione
                            * Secondo termine: velocità costante
                            * Terzo termine: minimo tempo di seek
                        - a = (-10 x t(min) + 15 x t(aver) - 5 x t(max)) / (3 x sqrt(n_cilindri) 
                        - b = (7 x t(min) - 15 x t(aver) + 8 x t(max)) / (3 x n_cilindri)
                        - c = t(min)
                * **Settle Time**:  stabilizzare le testine (trascurabile).
                * **Rotational Latency**: settore interessato passi sotto la testina.
                    + **Average** = (60/Spindle Speed) * 0.5 * 1000

                    | Spindle Speed (RPM) | Worst-Case Rotational Latency(Full Rotation) (ms) | Average Rotational Latency (Half Rotation) (ms) | 
                    | ---------- |:-----------:| --------:|
                    | 3600     | 16.7         | 8.3    |
                    | 4200  | 14.2         | 7.1    |
                    | 4500     | 13.3         | 6.7     |
                    | 4900    | 12.2         | 6.1     |
                    | 5200     | 11.5         | 5.8    |
                    | 5400  | 11.1         | 5.6    |
                    | 7200     | 8.3         | 4.2     |
                    | 10000    | 6.0         | 3.0     |
                    | 12000     | 5.0         | 2.5    |
                    | 15000  | 4.0         | 2.0    |
            - **Transfer rate**
                * **Internal Media Transfer Rate** (media rate): rappresenta la velocità massima alla quale il drive può leggere o scrivere bit, espressa in Mb/s. 
                    + ((bytes/sector) x (sectors/track))/rotation_time
                * **Sustained Transfer Rate**: include overhead per head switch time e cylinder switch time, misurata in MB/s.
                    + **Cylinder skew**: offset di 3 settori tra tracce consecutive, per compensare cylinder switch time.
                    + **Head skew**: offset di un settore fra i piatti adiacenti, per compensare head switch time.
        + **Vari funzionamenti del disco**
            - **Lettura**
                * **Read-ahead**: il controllore "anticipa" le richieste di lettura, caricando nella cache il contenuto di una o più tracce.
                * **NCQ - Native Command Queuing**: gestione di più richieste in contemporanea da parte del controllore, che può quindi decidere autonomamente in quale ordine sia meglio servirle.
            - **Scrittura**
                * **Write-through**
                    1. Scrittura dati su cache
                    2. Scrittura dati su disco
                    3. Notifica del completamento dell'operazione
                * **Write-back**
                    1. Scrittura dati su cache
                    2. Notifica del completamento dell'operazione
                    3. Scrittura dati su disco in modo asincrono rispetto alle operazioni del software
                * **Read after write**: il controller, dopo aver scritto un blocco, aspetta una rotazione completa e rilegge il blocco. Nel caso di discordanza ripete la scrittura, eventualmente su un altro settore.
        + **I/O controller - DMA**
            - Gestisce uno o più dispositivi e fornisce l'interfaccia con il bus di sistema.
            - Con la tecnica detta di DMA (Direct Memory Access), il controllore accede direttamente alla memoria e gestisce autonomamente il trasferimento dei dati dal dispositivo verso la memoria centrale (lettura) o dalla memoria verso il dispositivo (scrittura).
        + **RAID** (Redundant Array of Independent Disks): tecnica di raggruppamento di diversi dischi rigidi come se fosse un unico volume che sfrutta, con modalità differenti, i principi di ridondanza dei dati e di parallelismo degli accessi per garantire, rispetto a un disco singolo, incrementi di prestazioni, aumenti nella capacità di memorizzazione disponibile e miglioramenti nella tolleranza ai guasti.
- **Il DB fisico**
    * **Definizione**
        + Insieme di file, ognuno dei quali viene visto come una collezione di pagine di dimensione fissa (es: 4 KB).
            - Ogni pagina memorizza più record (corrispondenti alle tuple logiche).
                * Un record consiste di più campi che rappresentano gli attributi.
    * **Modello di memorizzazione**
        + **DB2**
            - Spazio fisico organizzato in tablespace, ognuno dei quali è una collezione di container.
                * **Tipi tablespace**
                    + **SMS** (System Managed Space): piccole dimensioni
                        - La gestione dello spazio è demandata al sistema operativo
                        - Un container corrisponde a una directory del filesystem
                        - I file sono estesi (dal filesystem) una pagina alla volta
                    + **DMS** (Database Managed Space): grandi dimensioni
                        - La gestione dei parametri dello storage è a carico dell'utente
                        - Un container è o un file di dimensione prefissata o un dispositivo (HD)
                        - Il tablespace può essere esteso solo aggiungendo container
                    + **AS** (Automatic Storage): grandi dimensioni
                        - La gestione è completamente a carico del DBMS.
                * **Atributi tablespace**: alla creazione di un tablespace è possibile specificare una serie di parametri
                    + EXTENTSIZE: numero di blocchi dell'extent
                    + BUFFERPOOL: nome del pool di buffer associato al tablespace
                    + PREFETCHSIZE: numero di pagine da trasferire in memoria prima che vengano effettivamente richieste
                    + OVERHEAD (ottimizzatore): stima del tempo medio di latenza per un'operazione di I/O
                    + TRANSFERRATE (ottimizzatore): stima del tempo medio per il trasferimento di una pagina
            - Solitamente diversi container sono diversi dischi.
            - Ogni container è diviso in extent, unità minima di allocazione su disco costituita da insiemi contigui di pagine.
        + **Oracle**
            - Uno o più tablespace composti da più datafile, strutture fisiche conformi al sistema operativo.
    * **Rappresentazione dei valori**
        + Stringhe a lunghezza fissa: CHAR(n)
            - Si allocano n byte, eventualmente usando un carattere speciale per valori lunghi meno di n.
                * Esempio: se A è CHAR(5), ‘cat’ è memorizzato come cat⊥⊥.
        + Stringhe a lunghezza variabile: VARCHAR(n) 
            - Si allocano m+p byte, con m (≤n) byte usati per gli m caratteri effettivamente presenti e p byte per memorizzare il valore di m.
                * Esempio: se A è VARCHAR(10), ‘cat’ viene memorizzato in 4 byte come 3cat.
        + DATE e TIME sono rappresentati esternamente con stringhe di lunghezza fissa (es. DATE: 10 caratteri YYYY-MM-DD; TIME: 8 caratteri HH:MM:SS) e internamente come sequenze di packed decimal digit (es. DATE: 4 byte, TIME: 3 byte).
        + Tipi enumerati: si usa una codifica intera
            - Esempio: week = {SUN, MON, TUE, ..., SAT} richiede un byte per valore: SUN: 00000001, MON: 00000010, TUE: 00000011, ...
    * **Record**
        + **Header**
            - Lunghezza del record
            - Riferimenti necessari per reperire campi a lunghezza variabile
            - Identificatore della relazione cui il record appartiene
            - Identificatore univoco del record nel DB
            - Timestamp che indica quando il record è stato inserito o modificato l'ultima volta
        + **Tipi**
            - **A lunghezza fissa**
                ```
                CREATE TABLE MOVIESTAR (
                    name        CHAR(30) PRIMARY KEY,
                    address     CHAR(255),
                    gender      CHAR(1),
                    birthdate   DATE )
                ```

                ![alt text](./res/rec-lf.png "Esempio di record a lunghezza fissa.")

            - **A lunghezza variabile**
                * Memorizzo prima tutti i campi a lunghezza fissa; per ogni campo a lunghezza variabile ho un prefix pointer che riporta l'indirizzo del rpimo byte del campo.
                ```
                CREATE TABLE MOVIESTAR (
                    name        VARCHAR(30) PRIMARY KEY,
                    address     VARCHAR(255),
                    gender      CHAR(1),
                    birthdate   DATE )
                ```
                * La lunghezza dei dati è pari a 22 byte, ma nel suo complesso il record occupa 34 byte.

                ![alt text](./res/rec-var.png "Esempio di record a lunghezza variabile.")

            - **Con attributi multipli**
                * Memorizzo in un blocco separato gli elementi a lunghezza variabile.

                ![alt text](./res/rec-mul.png "Esempio di record a lunghezza variabile con attributi multipli.")

        + **Organizzazione dei record nelle pagine**
            
            ![alt text](./res/rec-pag.png "Esempio di record in pagina.")

            - La Directory contiene un puntatore per ogni record nella pagina.
            - L'identificatore di un record, detto RID (Record Identifier) o TID (Tuple Identifier) nel DB è formato da una coppia:
                * PID: identificatore della pagina
                * Slot: posizione all'interno della directory.
            - È possibile sia individuare velocemente un record, sia permettere la sua riallocazione nella pagina senza modificare il RID.

            - Esempio:
                * Record = 290 bute
                * Pagina = 4 KB = 4096 byte
                * Header Pagina = 24 byte
                * Spazio utilizzabile pagina: 4096 - 24 = 4072 byte
                * Record per pagina: intinf(4072/290) = 14
                * 12 bytes sempre inutilizzati
                * Se la relazione X contiene 10000 tuple serviranno almeno 715 pagine per memorizzarla: intsup(10000/14) = 715
                * Se la lettura di una pagina da disco richiede in media 10ms, la lettura di tutte le tuple durerà 7.15 secondi, nel caso peggiore.
            - NB: letture e scritture di dati avvengono a indirizzi multipli di 4 (processori 32 bit) o di 8 (processori 64 bit). Il requisito di allineamento comporta spreco di spazi nei record. Gli esempi precedenti dovrebbero essere rivisti alla luce di queste considerazioni.
        + **Overflow**
            - Se un update fa aumentare la dimensione di un record e non c'è più spazio nella stessa pagina, il record viene spostato in  un'altra pagina.
            - Il RID del record tuttavia non cambia, ma s'introduce un livello di indirezione.
            - Periodicamente è necessario riorganizzare il file, altrimenti degrado delle prestazioni.
    * **Lettura e scrittura di pagine**
        + La lettura di una tupla richiede che la pagina corrispondente sia prima trasferita in memoria, in un'area gestita dal DBMS detta buffer pool.
        + Ogni buffer nel pool può ospitare una copia di una pagina su disco.
        + La gestione del buffer pool è demandata a un modulo del DBMS, detto Buffer Manager (BM) che si occupa di lettura e scrittura.
            - **Buffer manager**
                * **Funzionamento**
                    + Se la pagina è già in un buffer, si restituisce al programma chiamante l'indirizzo del buffer.
                    + Altrimenti:
                        - BM seleziona un buffer per la pagina richiesta. Se tale buffer è già occupato, la pagina che lo occupava viene riscritta su disco se è stata modificata e se nessuno la sta usando.
                        - BM può leggere la pagina e copiarla nel buffer prescelto, rimpiazzando così quella precedente.
                * **Moduli**
                    + **getAndPinPage**: richiede la pagina al BM e vi pone un pin (spillo), a indicarne l'uso
                    + **unPinPage**: rilascia la pagina e elimina un pin
                    + **setDirty**: indica che la pagina è stata modificata, ovvero è dirty
                    + **flushPage**: forza la scrittura della pagina su disco, rendendola così clean