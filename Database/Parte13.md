### Parte 13 - External sorting

- **Internal sort**: insertion sort, quick sort, heap sort, merge sort...
    * Per grandi dataset algoritmi troppo lenti.
- **External sort**: ordinamento di file.
    * **Z-way sort merge**
        + Fasi
            - **Internal sort**
                * File suddiviso in SR sequenze ordinate (run).
                * Area di buffering e uno qualsiasi degli algoritmi di internal sort per ordinare le singole sequenze.
                * Le run generate (chunk) sono memorizzate su uno o più drive.
            - **Merge**
                * Medesima area di buffering le run vengono fuse Z alla volta.
                * Più passi di fusione.
                * A ogni passo diminuisce il numero delle run da fondere e aumenta la loro lunghezza.
                * Si ottiene un singolo file ordinato.
        + **2-way sort merge**
            - Fasi
                * **Internal sort**: si ordina il file in input una pagina alla volta, producendo NP run ordinate singolarmente e di dimensione pari a 1 pagina.
                * **Merge**: si fondono le run 2 alla volta.
            - Analisi
                * Costo sort: NP letture e NP scritture.
                * Costo ogni merge: NP letture e NP scritture.
                * All'ultima fusione ottengo 1 run di NP pagine.
                * Passi di fusione: PF = intsup(log2(NP)).
                * Passi totali (sort + merge): intsup(log2(NP)) + 1
                * **Costo I/O** totale peggiore: NP × intsup(log2(NP)) + 1 letture, NP × intsup(log2(NP)) + 1 scritture.
            - **Miglioramenti**
                * Semplicemente si utilizza lo Z-way:
                    + Nel passo di internal sort si utilizzano tutte le B pagine buffer poducendo SR = intsup(NP/B) run iniziali ordinate.
                    + Nei passi di merge si fondono Z run alla volta utilizzando Z = B - 1 buffer per input e 1 buffer per output.
                    + **Costo I/O**: 2 × NP × intsup(logZ(NP))

        + Esempi

            ![alt text](./res/z-w.png "Esempio Z-way sort-merge.")

            - NP = 8192, B = 11, Z = 10 -> PF = 3

            | N. vie di merge | N. run | Lung. max. | N. run con lung. max.       | Lung. ultima run | N. passo |
            | ---------- |:-----------:| --------:| -------------:| -------------:| -------------:|
            |      | 745         | 11    | 744 | 8 | 0 - sort
            | 10  | 75         | 110    | 74 | 52 | 1 - merge
            | 10     | 8         | 1100     | 7| 492 | 2 - merge
            | 8    | 1         | 8192     | 1          | | 3 - merge
        
            | N. read | N. write | Tot I/O |        |
            | ---------- |:-----------:| --------:| -------------:|
            | 8192     | 8192         | 16384    | sort |
            | 24576  | 24576         | 49152    | merge |
            |      | totali         |      | |
            | 32768    | 32768         | 65536     | sort + merge |
            |      | .         |      | |
            | T_read (min)    | T_write (min)    | Tot minuti     |  |
            |  5.46    |    5.46      |   10.92   | |
        + **Minimo numero di passi**
            - Quanto può essere grande un file per essere ordinato in solo 2 passi (sort + merge) avendo B pagine buffer a disposizione?
                * Non possono esservi più di B - 1 run ognuna lunga non più di B pagine, dunque: NP ≤ B × (B - 1).
            - In generale con sort-merge multiway, un file può essere ordinato in (PF + 1) passi (sort + merge) se: NP ≤ B × (B - 1)^PF.
        + **Merge di un file orientato ai record**
            - Presupponiamo che sia sufficiente un solo passo per fondere gli SR file. Ciò implica che devono essere disponibili SR buffer per l'input, 1 buffer per l'output e 1 buffer per RC record presenti correntemente nella memoria del programma.
            - L’algoritmo è valido anche in presenza di repliche dello stesso valore della combinazione di attributi di ordinamento.
            - **Algoritmo**
                1. Inizializzazione: si legge il primo record da ognuno degli SR file e si inserisce nell'insieme dei record correnti RC.
                    - repeat
                    2. Selezione: si scrive in output il record con il più piccolo valore della combinazione di attributi di ordinamento tra quelli in RC e si elimina da RC.
                    3. Rimpiazzamento: si legge un record, se esiste, dal file da cui è stato scelto il record al passo di selezione e si inserisce in RC.
                    - until RC = ∅.
        + **Merge di un file orientato ai blocchi** (Z-way Merge)
            - Z + 1 frame
            - B blocchi (multipli di Z + 1)
            - Ogni frame: FS ≥ 1 blocchi
            - Fattore blocking: FS = B/(Z + 1)
            - Buffering frame: FB = B/FS

            ![alt text](./res/z-mer.png "Esempio Z-way merge.")
            
            - Run = collezione di record ordinati secondo un criterio.
            - Z-way Merge fonde SR run singolarmente ordinate avvalendosi di file ausiliari.
            - Sia NP il numero complessivo dei blocchi contenuti nelle SR run, al termine dell'esecuzione si produce un'unica run ordinata con NP blocchi.
            - La fusione avviene in PF passi; a ogni passo si fondono Z run alla volta.
            - A ogni passo di fusione Z = FB - 1 frame sono usati per l'input e 1 frame per l'output.

            ![alt text](./res/es-z-me.png "Esempio Z-way merge.")