### Parte 4 - Progettazione concettuale schema E/R

- **Raccolta dei requisiti**
    * **Fonti**
        + **Utenti**
            - **Mezzo**
                * Interviste
            - **Accorgimenti**
                * Utenti diversi possono fornire informazioni diverse con riferimento allo stesso tema.
                * Utenti a livello più alto hanno spesso una visione più ampia ma meno dettagliata.
        + **Documentazione** 
            - **Mezzo**
                * Normative (leggi, regolamenti di settore)
                * Regolamenti interni, procedure aziendali
                * Realizzazioni preesistenti
        + **Modulistica**
    * **Glossario**
        + Scopi
            - Fornire una breve descrizione di ogni concetto rilevante riorganizzando le frasi per concetti.
            - Individuare omonimi e sinonimi, e unificare i termini
                * Omonimi: lo stesso termine viene usato per descrivere concetti differenti (es: libro e copia di libro, posto di lavoro e geografico).
                * Sinonimi: termini diversi vengono usati per descrivere lo stesso concetto (esempio: docente e professore).
            - Mettere in relazione i concetti simili rendendo esplicito il riferimento fra termini.
        + Esempio:

        ![alt text](./res/es-gloss.png "Esempio glossario termini.") 
    * **Tabella delle operazioni**
        + Operazioni da effettuare sui dati e frequenza con cui devono essere eseguite.
        + Esempio:

        ![alt text](./res/es-tab-op.png "Esempio tabella delle operazioni.")
    * **Riorganizzare le frasi raggruppandole in base al concetto a cui si riferiscono**
- **Dai concetti allo schema E/R**
    * Un concetto non è di per sé un'entità, un'associazione, un attributo, o altro; dipende dal contesto.
    * **Classificazione dei concetti**
        + Unconcetto sarà rappresentato come:
            - **Entità** se ha proprietà significative e descrive oggetti con esistenza autonoma
            - **Attributo** se è semplice (o composto) e non ha proprietà
            - **Associazione** se correla due o più concetti
            - **Generalizzazione/specializzazione** se generalizza altri concetti/se è una specializzazione di un altro concetto
- **Strategie di progettazione**
    * **Top-down**
        + Si parte da uno schema iniziale molto astratto ma completo, che viene successivamente raffinato fino ad arrivare allo schema finale.
        + **Pro**: non è inizialmente necessario specificare i dettagli
        + **Contro**: richiede sin dall'inizio una visione globale del problema, non sempre ottenibile in casi complessi

        ![alt text](./res/top-down.png "Schema top down.")
    * **Bottom-up**
        + Si suddividono le specifiche in modo da sviluppare semplici schemi parziali ma dettagliati, che poi vengono integrati tra loro.
        + **Pro**: permette una ripartizione delle attività
        + **Contro**: richiede una fase d'integrazione

        ![alt text](./res/bottom-up.png "Schema bottom up.")
        + **Inside-out** 
            - Lo schema si sviluppa "a macchia d'olio", partendo dai concetti più importanti, aggiungendo quelli a essi correlati, e così via.
            - È un caso particolare della strategia bottom-up, in cui il raffinamento inizia dai concetti principali per poi estendersi a quelli più lontani attraverso una "navigazione" delle specifiche.
            - **Pro**: non richiede passi di integrazione
            - **Contro**: richiede a ogni passo di esaminare tutte le specifiche per trovare i concetti non ancora rappresentati

            ![alt text](./res/inside-out.png "Schema inside out.")
    * **Ibrido**
        1. Si individuano i concetti principali e si realizza uno schema scheletro, che rappresenta solamente i concetti più importanti

        ![alt text](./res/es-schema-scheletro-ibrido.png "Esempio schema scheletro ibrido.")
        
        2. Sulla base dello schema scheletro si può decomporre

        ![alt text](./res/es-decomposizione-ibrida.png "Esempio decomposizione ibrida.")

        3. Successivamente si raffina, si espande, si integra

        ![alt text](./res/es-raffinamento-ibrido.png "Esempio raffinamento ibrido.")
        ![alt text](./res/es-integrazione-ibrida.png "Esempio di integrazione ibrida.")
- **Modello relazionale**
    * **Inventore**: E. F. Codd
    * **Data**: 1970
    * E' un modello logico nel senso che risponde al requisito di indipendenza dalla particolare rappresentazione dei dati adottata a livello fisico; inoltre i legami fra i dati non sono stabiliti con puntatori ma per mezzo dei valori dei domini.
    * **Relazioni**
        + **Definizione**
            - Si indichi con dom(A) il dominio dell'attributo A e si consideri un insieme di attributi X = {A1, A2, ...,An}.
            - Una tupla t su X è una funzione che associa a ogni Ai ∈ X un valore di dom(Ai).
            - Uno schema di relazione su X è definito da un nome (della relazione) R e dall'insieme di attributi X, e si indica con R(X).
            - Uno stato (o estensione) di relazione su X è un insieme r di tuple su X, che è anche denominato semplicemente "relazione".

            ![alt text](./res/rel-def.png "Definizione di relazione.")
        
        + **Notazione**
            - **Relazione singola**
                * R(A,B,C) = Relazione fra attributi A, B e C.
                * Se t è una tupla su X e A ∈ X, allora t[A] o t.A è il valore di t su A.
                    + Es (riferito a immagine precedente): t[TeamOspite] = t.TeamOspite= 'Fiat Torino' 
                * La stessa notazione si usa per insiemi di attributi, e denota una tupla.
                    + Es (riferito a immagine precedente): t[TeamOspite,PuntiOspite] è una tupla su {TeamOspite,PuntiOspite}
                * Livello intensionale: R(X)
                    + Es: PARTITE(TeamCasa,TeamOspite,PuntiCasa,PuntiOspite)
                * Livello estensionale: r
                    + Es: r.TeamOspite= 'Fiat Torino' (estensione, non istanza)
            - **Insieme di relazioni** (Data Base)
                * Livello intensionale: insieme di schemi di relazioni con nomi distinti
                    + R = {R1(X1), R2(X2), ..., Rm(Xm)}
                * Livello estensionale: insieme di stati di relazioni
                    + r = {r1, r2, ..., rm}
                * Esempio:
                    + AZIENDA = {IMPIEGATI(Matricola,Cognome,Nome,Livello,Stipendio), FILIALI(CodiceFiliale,Nome,Indirizzo,Direttore), FORNITORI(RagioneSociale,Indirizzo,PartitaIVA)}

                    ![alt text](./res/stato-db-rel.png "Stato di un Data Base relazionale.")
        + **Matematica**
            - Si considerino due insiemi A e B, non vuoti e non necessariamente distinti; ogni sottoinsieme non vuoto del prodotto cartesiano A × B è detto relazione da A a B. Se B = A allora un sottoinsieme non vuoto del prodotto cartesiano A^(2) è detto anche relazione in A o su A. 
            - Data una relazione r ⊆ A × B, si dice che l'elemento a ∈ A è in relazione con l'elemento b ∈ B se la coppia(a,b) ∈ r.
            - Una relazione (matematica) su D1, D2, ..., Dn è un qualunque sottoinsieme del prodotto cartesiano D1 × D2 × ... × Dn
                * D1, D2, ..., Dn sono i domini della relazione
                * Il valore di n è detto grado della relazione
                * Il numero di n-ple di una relazione è la sua cardinalità
            - Una relazione è un insieme di n-ple:
                * Distinte fra loro
                * Non ordinate
                * Con domini ordinati
                    + (D1×D2) ≠ (D2×D1)
                    + {(a,1),(c,1),(c,2)} ≠ {(1,a),(1,c),(2,c)}
                * Con domini che possono essere usati in più posizioni
                    + {(2,a,1),(1,c,1),(1,c,2)}  ⊆ D2 × D1 × D2
                * Contemplante anche 
                    + Il caso in cui n = 1:
                        - D1 = {Carlo,Mario,Giacomo,Marco,Giorgio}
                        - r = {(Carlo),(Giorgio)} è dunque una relazione
                    + Numeri infiniti di n-ple (se definibili attraverso algoritmo finito)
                    + Domini infiniti
        + **Rappresentazione**
            - A ogni occorrenza di dominio si associa un nome univoco nella relazione, detto attributo, il cui compito è specificare il ruolo che quel dominio svolge nella relazione ("cosa significa").
            - **Tipi**
                * Insiemistica
                * Tabellare
                    + Attributi: intestazioni delle colonne
                    + Rappresenta una relazione se:
                        - I valori di ciascuna colonna sono tra loro omogenei (definiti sullo stesso dominio)
                        - Le righe sono tra loro diverse
                        - Le intestazioni delle colonne sono diverse tra loro
                    + Se rappresenta una relazione:
                        - L'ordinamento delle righe è irrilevante
                        - L'ordinamento delle colonne è irrilevante

                ![alt text](./res/rap-tab.png "Rappresentazione tabellare.")

                * Multi-dimensionale
                    + Attributi: assi

                ![alt text](./res/rap-multi.png "Rappresentazione multidimensionale.")

    * **Informazione incompleta**
        + Le informazioni che si vogliono rappresentare mediante relazioni non sempre corrispondono pienamente allo schema prescelto; in particolare, per alcune tuple e per alcuni attributi potrebbe non essere possibile specificare, per diversi motivi, un valore del dominio.
        + **Soluzione**: si adotta il concetto di valore nullo (NULL), che denota assenza di un valore nel dominio (e non è un valore del dominio); pertanto t[A] ∈ dom(A)∪{NULL} ricordando però che NULL ≠ NULL ma, ai fini della verifica di assenza di tuple duplicate, NULL = NULL.
        + **Restrizioni**
            
            ![alt text](./res/restr-null.png "Restrizioni valori nulli.")

    * **Vincoli di integrità** (intra-relazionale, interessano una relazione alla volta)
        + E' una proprietà che deve essere soddisfatta da ogni possibile stato osservabile di una relazione; ogni vincolo può quindi essere descritto da una funzione booleana che associa a ogni stato il valore VERO o FALSO.
        + **Tupla**
            - Esprimono condizioni su ciascuna tupla, indipendentemente dalle altre.

            ![alt text](./res/v-tup.png "Esempio vincoli di tupla.")

            - **Dominio**
                * Si riferisce ai valori ammissibili per un singolo attributo.

                ![alt text](./res/v-dom.png "Esempio vincoli di dominio.")

        + **Chiave**
            - Vietano la presenza di tuple distinte che hanno lo stesso valore su uno o più attributi.

            ![alt text](./res/v-key.png "Esempio vincoli di chiave.")

    + **Chiavi**
        - **Superchiave**: in ogni stato ammissibile r di R(X) non esistono due tuple distinte t1 e t2 tali che t1[K] = t2[K].
        - **Chiave**: non esiste K' ⊂ K con K' superchiave.
        - Esempio: 
            * Nella relazione STUDENTI (immagine sopra)
                + {Matricola} e {CodiceFiscale} sono due chiavi
                + {Matricola,Cognome} e {CodiceFiscale,Nome} sono solo superchiavi
                + {Cognome,Nome,DataNascita} non è superchiave
        - **Utilità**: l'esistenza delle chiavi garantisce l'accessibilità a ciascun dato del DB, in quanto ogni singolo valore è univocamente individuato da:
            * Nome della relazione = individua una relazione del DB 
            * Valore della chiave = individua una tupla della relazione 
            * Nome dell'attributo = individua il valore desiderato della tupla
        - **Chiave primaria**: chiave su cui non si ammettono valori nulli.
            * Si sottolineano gli attributi che ne fanno parte.
            * Nei casi in cui per nessuna chiave si possa garantire la disponibilità di valori, è necessario introdurre un nuovo attributo (un "codice") che svolga le funzioni di chiave primaria.
    + **Vincoli di integrità referenziale** (inter-relazionali)
        - Si considerino due schemi R1(X1) e R2(X2) di un DBR, e sia Y un insieme di attributi in X2. 
        - Un vincolo di integrità referenziale su Y impone che in ogni stato r = {r1, r2, ...} del DB l'insieme dei valori di Y in r2 sia un sottoinsieme dell'insieme dei valori della chiave primaria di R1(X1) presenti nello stato r1.
        - L'insieme Y viene detto una foreign key (o "chiave importata").

        ![alt text](./res/es-for-key.png "Esempio di foreign key.")

        - E' possibile importare una chiave in una relazione e cambiare il nome della così generata foreign key.

        ![alt text](./res/import-key.png "Esempio importazione chiave as foreign key con nome modificato.")

        - Foreign key e primary key possono far parte della stessa relazione, ovviamente con Y ≠ K; in questo caso la foreign key può amettere valori nulli, questa verrà quindi flaggata con '*'.
        - **Notazione**
            * Modalità 1
                + AGENZIE(Agenzia, Luogo)
                + IMPIEGATI(CodImpiegato, Cognome, Nome, CodAgenzia)
                + FK: CodAgenzia REFERENCES AGENZIE(Agenzia) oppure FK: CodAgenzia REFERENCES AGENZIE.
            * Modalità 2
                + AGENZIE(Agenzia, Luogo)
                + IMPIEGATI(CodImpiegato, Cognome, Nome, CodAgenzia:AGENZIE)
            * Se gli attributi che compongono una foreign key hanno nomi uguali a quelli nella primary key da cui sono importati e non vi sono ambiguità, si omette l'esplicita indicazione dello schema da cui sono derivati: 
                + AGENZIE(Agenzia, Luogo)
                + IMPIEGATI(CodImpiegato, Cognome, Nome, Agenzia)
            * Per denotare che un attributo A ammette solo valori non ripetuti, si usa scrivere: Unique(A).
                + AGENZIE(CodAgenzia, Nome, Sede, Direttore:IMPIEGATI)
                + Unique(Direttore)
                + IMPIEGATI(Codice, Nome, Cognome, Agenzia*:AGENZIE, DataInizio*)