### Part 5

- Reordering instructions
    - It is possible when they do not write on the same variables and they can only read the same variables
    - Data dependence: two memory accesses are involved in a data dependence if they may refer to the same memory location and one of the references is a write
        - True dependence: Write X – Read X (Read After Write)
        - Output dependence: Write X – Write X (Write After Write)
        - Anti dependence: Read X – Write X (Write After Read)
    - Control dependence: an instruction S2 has a control dependence on S1 if the outcome of S1 determines whether S2 is executed or not
    - Theorem of dependence: any reordering transformation that preserves every dependence in a program preserves the meaning of that program
        - Application
            1. Find data dependences in loop
            2. No dependences crossing iteration boundary → parallelization of loop’s iterations is safe

    - Dependencies drawing

        ![alt text](./res/dep-draw-1.png "Dependencies drawing 1")

        ![alt text](./res/dep-draw-2.png "Dependencies drawing 2")

        ![alt text](./res/dep-draw-3.png "Dependencies drawing 3")

        ![alt text](./res/dep-draw-4.png "Dependencies drawing 4")

        ![alt text](./res/dep-draw-5.png "Dependencies drawing 5")

    - Removing dependences
        - Loop aligning

            ![alt text](./res/loop-aligning.png "Loop aligning example")

        - Reordering and aligning

            ![alt text](./res/reor-1.png "Reordering operations")

            ![alt text](./res/reor-2.png "Aligning operations")

        - Loop interchange

            ![alt text](./res/loop-inter.png "Loop interchange example")

        - Example: Arnold's Cat Map (bijective)

            ![alt text](./res/arnold-cat.png "Arnold's cat map")

            - NxN bitmap
            - (x, y) -> (x', y')
                - x' = (2x + y) mod N
                - y' = (x + y) mod N
            - Which loop(s) can be parallelized?

                ![alt text](./res/arnold-script.png "Arnold's cat map script parallelization")

        - Example: Difficult dependences

            ![alt text](./res/difficul-dep.png "Example of difficult dependences")

            - How to solve: Wavefront sweep

                ![alt text](./res/wavefront-sweep.png "Wavefront sweep")

                ![alt text](./res/wave-code.png "Wavefront sweep implementation")