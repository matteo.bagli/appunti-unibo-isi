### Part 3

- How fast can we run?
    - 12 tasks, each one requiring 1s
        - Serial time: 12s
        - Parallel time: SerialTime / NProcessors (if tasks are indipendent)
    - If the processors can not execute the tasks with the same speed
        - Load imbalance
            - Processor n will finish the execution before processor r, so idle, not good!
    - If tasks are dependent
        - Processor n will finish the execution before processor r, so idle, not good!

            ![alt text](./res/dependent-task.png "Task dependence")

        - Calculating execution time is harder
        - We need to optimize the critical path
- Scalability
    - How much faster can a given problem be solved with p workers instead of one?
- Speedup
    - Variables
        - $`p`$ = Number of processors / cores
        - $`T_{serial}`$ = Execution time of the serial program
        - $`T_{parallel}(p)`$ = Execution time of the parallel program with pprocessors / cores
    - Speedup $`S(p) = \frac{T_{serial}}{T_{parallel}(p)} = \frac{T_{parallel}(1)}{T_{parallel}(p)}`$
        - In the ideal case, the parallel program requires $`\frac{1}{p}`$ the time of the sequential program
        - $`S(p) = p`$ is the ideal case of linear speedup, realistically $`S(p) \le p`$
        - NB: We'll always use $`S(p) = \frac{T_{parallel}(1)}{T_{parallel}(p)}`$ so $`T_{serial} = T_{parallel}(1)`$
        - Exam: Is it possible to observe $`S(p) > p`$ (superlinear speedup)?
            - If all the work is divided in $`n`$ processors and also the INPUT, this could be entirely cached, so here we have a superlinear speedup
            - If i have an architecture that has specialized hardware i could have superlinear speedup 
- Non-parallelizable portions
    - Suppose that a fraction $`α`$ of the total execution time of the serial program can not be parallelized and he remaining fraction $`(1 - α)`$ can be fully parallelized
        - $`T_{parallel}(p) = α \times T_{serial} + \frac{(1 - α) \times T_{serial}}{p}`$
    - Maximum speedup?
        - Amdahl's Law: $`\frac{1}{α + \frac{1 - α}{p}}`$
            - Asymptotic speedup: $`\frac{1}{α}`$ when p grows to infinity

                ![alt text](./res/speedup-and-alpha.png "Speedup and alpha")
- Scaling
    - Types
        - Strong: how the solution time varies with the number of processors for a fixed total problem size
            - Efficiency: $`E(p) = \frac{S(p)}{p} = \frac{T_{parallel}(1)}{p \times T_{parallel}(p)}`$, $`0 < E(p) < 1`$

                ![alt text](./res/sseff-amdahl.png "Strong scaling efficiency and Amdahl's law")

        - Weak: how the solution time varies with the number of processors for a fixed problem size per processor
            - Efficiency: $`W(p) = \frac{T_{1}}{T_{p}}`$
                - $`T_{1}`$ = time required to complete 1 work unit with 1 processor
                - $`T_{p}`$ = time required to complete p work units with $`p`$ processors
    - Example
        - [omp-matmul.c](./code/omp-matmul.c)
            - [demo-strong-scaling.sh](./code/demo-strong-scaling.sh) computes the times required for strong scaling ([demo-strong-scaling.ods](./code/demo-strong-scaling.ods))
            - [demo-weak-scaling.sh](./code/demo-weak-scaling.sh) computes the times required for weak scaling ([demo-weak-scaling.ods](./code/demo-weak-scaling.ods))
        - NB
            - For a given $`n`$, the amount of “work” required to compute the product of two $`n \times n`$ matrices is $`~ n^{3}`$
            - To double the work, we must use matrices of size $`\sqrt[3]{2n} \times \sqrt[3]{2n}`$
            - In general, with $`p`$ processes we use matrices of size $`\sqrt[3]{pn} \times \sqrt[3]{pn}`$
- Time measurements
    - During the course the measurements will be done 5-10 times for each programme, and then math average of times
    - How to?

        ![alt text](./res/mea-time.png "Measuring time")

- Rules for graphing data
    - Understanding the data should require the least possible effort from the reader
    - Provide enough information to make the graph self-contained
    - The origin of plots should be (0, 0)
    - The effect should be on the y axis, the cause on the x axis
    - Do not overlap related data
    - Do not connect points when intermediate values can not exist
    - All figures must be referenced and described in the main text of your document
