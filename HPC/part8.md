### Part 8

- CUDA (Compute Unified Device Architecture)
    - Data parallel programming model: same program on N data
    - Basic concepts
        - Parts
            - Host: CPU
                - Executes the host portion of the program (I/O, user interaction..)
            - Device: GPU
                - Executes the device portion of the program (computation..)
        - Interaction
            1. Host program copies data into the global memory of the GPU
            2. Host program calls device functions to initiate the processing on the GPU
            3. Host program read the result stored in device memory
        - GPU structure
            - SIMD processors (SM) with one control unit, each one is composed by:
                - Functional units (cores) that can execute the same instruction on different data
        - Logical structure
            - GRID: piece of work that can be executed by the GPU
            - BLOCK: independent subpiece of work that can be executed in any order by a SIMD processor (max number of threads in a block = 1024)
            - THREAD: minimal unit of work; all the threads execute the same KERNEL function and are grouped in WARPs of 32
                - Individual threads of a warp start together at the same program address
                - Each thread has its own program counter and register state 
                - Each thread knows
                    - Its index: `threadIdx.x`
                    - The index of its block: `blockIdx.x` 
                    - The number of threads per block: `blockDim.x`
                - Each thread is free to branch and execute independently but in this case each branch will be executed serially

                    ![alt text](./res/thfunc.png "How warps work")

            - WARP: minimum granularity of efficient SIMD execution
                - The hardware is most efficiently utilized when all threads in a warp execute instructions from the same program address
                - If threads in a warp diverge, then some execution pipelines go unused
                - If threads in a warp access aligned contiguous blocks of DRAM the accesses are coalesced into a single high-bandwidth access
            
            ![alt text](./res/thblgr.png "Logical organization of GPU")

        - Automatic scalability: there's no need to take care of the number of SM because the block scheduler does everything for us

            ![alt text](./res/autosc.png "Auto scaling of the GPU scheduler")
            
    - Code
        - `nvcc`: CUDA compiler
        - "Constructs"
            - `__global__void mykernel(void) {  }`
                - Runs on the device
                - Is called from host code
                - Must return void
            - `mykernel<<<B,T>>>( );` (call from host code to device code)
                - It means that the function is executed by T threads for each B block
                - It's asynchronous, when this function is ran the host code continues its execution
            - `__shared__void mykernel(void) {  }`
                - Function that uses shared memory (see next..) 
            - Memory: host and device memory are separate entities
                ```cpp
                __global__ void add(int *a, int *b, int *c) {
                    *c = *a + *b;
                }
                ```
                - As we can see from the above piece of code there is a particular use of pointers, this because `add()` runs on the device, so `a`, `b` and `c` must point to device memory
                    - Device pointers point to GPU memory
                        - May be passed to/from host code
                        - May NOT be dereferenced in host code
                    - Host pointers point to CPU memory
                        - May be passed to/from device code
                        - May NOT be dereferenced in device code
                - APIs or handling device memory
                    - `cudaMalloc()`
                    - `cudaFree()`
                    - `cudaMemcpy()`
            - Synchronization between host and device
                - As already sai kernel launches are asynchronous
                - To read the result of a CUDA computation the host must be synchronized
                    - `cudaMemcpy()`: blocks the CPU until the copy is complete, copy begins when all preceding CUDA calls have completed
                    - `cudaMemcpyAsync()`: same as `cudaMemcpy()` but asynchronous
                    - `cudaDeviceSynchronize()`: blocks the CPU until all preceding CUDA calls have completed
            - Timing: calculate execution time in CUDA
                ```c
                #include "hpc.h"
                ...
                double tstart, tend;
                
                tstart = hpc_gettime();
                mykernel<<<X, Y>>>( );   /* kernel invocation         */
                cudaDeviceSynchronize(); /* wait for kernel to finish */
                tend = hpc_gettime();
                printf("Elapsed time %f\n", tend - tstart);
                ```
            - Defining device specific functions (`__device__`)
                - Can be called just by the device
                - Functions are copied where they are called, there isn't a real context switch
                ```c
                __device__ float cuda_fmaxf(float a, float b) {
                    return (a>b ? a : b);
                }
                
                __global__void my_kernel( float *v, int n ) {
                    int i = threadIdx.x + blockIdx.x * blockDim.x;
                    if (i<n) {
                        v[i] = cuda_fmaxf(1.0, v[i]); 
                    }
                }
                ```
            - Defining host specific functions (`__host__`)
                - Can be called from host code only 
                - Optional
            - Defining host and device functions 
                - The compiler produces two versions of the function: one for the GPU and one for the CPU
                ```c
                __host__ __device__ float my_fmaxf(float a, float b) {
                    return (a>b ? a : b);
                }
                ```
            - Defining device specific variables (`__device__`)
                - Can be applied to global variables only
                - `cudaMemcpyToSymbol()` / `cudaMemcpyFromSymbol()` must be used to copy data from/to `__device__` variables   
                ```c
                #define BLKDIM 1024
                
                __device__ float d_buf[BLKDIM];
                
                int main( void ) {
                    float buf[BLKDIM];
                    ...
                    cudaMemcpyToSymbol(d_buf, buf, BLKDIM*sizeof(float));
                    ...
                    cudaMemcpyFromSymbol(buf, d_buf, BLKDIM*sizeof(float));
                }
                ```   
            - Defining device specific constants (`__constant__`)
                - The host must use `cudaMemcpyToSymbol()` to copy data from host memory to constant memory
                - The device can not write to constant memory
            - Errors (use `hpc.h`)
                - `cudaSafeCall(Exp)` execute Exp and checks for return status
                - `cudaCheckError()` checks error code from last CUDA operation
                ```c
                cudaSafeCall( cudaMemcpy(d_a, h_a, size, cudaMemcpyHostToDevice) );
                my_kernel<<< 1, 1 >>>(d_a); cudaCheckError();
                cudaSafeCall( cudaMemcpy(h_a, d_a, size, cudaMemcpyDeviceToHost) );
                ```
            - Device management
                - `CUDA_VISIBLE_DEVICES=0 ./cuda-stencil1d`: select the first device
                - `CUDA_VISIBLE_DEVICES=1 ./cuda-stencil1d`: select the second device
                - `deviceQuery`: retrieve info about devices installed on the machine
            - Memory access patterns
                - Each memory access moves 32 or 128 consecutive bytes
                - The GPU can pack together (coalesce) memory accesses when consecutive threads access consecutive memory addresses (more or less is the same principles of CPU cache lines)
                - Examples
                    - 32 aligned 4-byte words = 1 cache line

                        ![alt text](./res/calin.png "Cache line utilization")

                    - 32 misaligned 4-byte words = 2 cache line

                        ![alt text](./res/calin1.png "Cache line utilization")

                    - ... (in general, is better to align memory accesses)
            - Performance evaluation
                - Throughput: number of processed data items/seconds as a function of the input size
                - Speedup vs CPU implementation
            - Examples
                - [Two vectors addition (more blocks)](./code/cuda-vecadd1.cu)

                    ![alt text](./res/cudavecadd.png "Scheme of example of CUDA programming")
                
                - [Two vectors addition (more threads)](./code/cuda-vecadd2.cu)
                    - NB: pay attenction to not exceed the maximum number of 1024 threads!

                    ![alt text](./res/cudavecadd1.png "Scheme of example of CUDA programming")

                - [Two vectors addition (threads + blocks + arbitrary vector size)](./code/cuda-vecadd3.cu)
                    - Handle threads and blocks indexes at the same time: `index = threadIdx.x + blockIdx.x * blockDim.x;`

                        ![alt text](./res/thblock.png "How to map blocks and threads together")

                    - Handle arbitrary vetor size
                        - Avoid accessing beyond the end of the array
                            ```cpp
                            __global__ void add(int *a, int *b, int *c, int n) {
                                int index = threadIdx.x + blockIdx.x * blockDim.x;
                                if (index < n) {
                                    c[index] = a[index] + b[index]; 
                                }
                            }
                            ```
                        - Update kernel launch
                            - `add<<<(N + BLKDIM-1)/BLKDIM, BLKDIM>>>(d_a, d_b, d_c, N);`

                    ![alt text](./res/cudavecadd2.png "Scheme of example of CUDA programming")

                - [1D stencil](./code/cuda-stencil1d-shared.cu): each output element is the sum of input elements within a given radius, in this case 3 (first and last radius elements of the output array are not computed)

                    ![alt text](./res/1dsten.png "Example of CUDA stencil programming")

                    - Problem

                        - Each thread processes one output element

                            ![alt text](./res/multireadsten.png "Stencil read problem")

                        - Input elements are read several times; with radius R each input element is read (2R+1) times: NOT COOL, SLOW
                    - Solution: use shared memory

                        ![alt text](./res/memorycuda.png "CUDA memory organization")

                        - Global memory: everyone has access to it
                        - Constant memory: readonly, everyone can read
                        - Texture memory: graphic reserved
                        - Shared memory: shared; it could be not enough, so use prof libs to handle errors

                    - How to
                        - Simplifying assumptions
                            - The array length is a multiple of the thread block size
                            - The input (and output) array already includes an halo of 2*RADIUS elements
                            - The halo is ignored for the output array
                        - Let's do this
                            - Read (blockDim.x + 2 * radius) input elements from global memory to shared memory
                                - Each thread block keeps a local cache of blockDim.x + 2 * RADIUS elements

                                    ![alt text](./res/shmem1.png "Stencil e shared memory passo 1")

                                - Each thread copies one element from the global array to the local cache

                                    ![alt text](./res/shmem2.png "Stencil e shared memory passo 2")

                                - The first RADIUS threads also take care of filling the halo

                                    ![alt text](./res/shmem3.png "Stencil e shared memory passo 3")

                                - NB: now all the threads MUST wait for eachother (`__syncthreads()`, synchronizes all threads within a block), else there would be data-races!
                            - Compute blockDim.x output elements
                            - Write blockDim.x output elements to global memory
                            - Each block needs a halo of radius elements at each boundary
                - [Matrix-Matrix multiplication](./code/cuda-matmul.cu)
                    - How to?
                        - Theorically
                            - Decompose the result matrix r into square blocks
                            - Assign each block to a thread block
                            - All threads of the thread block compute one element of the result matrix

                            ![alt text](./res/matmat.png "Matrix x Matrix multiplication")
                        
                        - Practically
                            - Setting up the thread blocks
                                - The dim3 data type can be used to define a one-, two-, or three-dimensional “size” for a thread or grid block
                                    - `dim3 blk(3)`: defines a variable “dim” representing a 3x1x1 block
                                    - `dim3 blk(3, 4)`: defines a variable “dim” representing a 3x4x1 block
                                    - `dim3 blk(3, 4, 7)`: defines a variable “dim” representing a 3x4x7 block
                                    - Example
                                        - Launch 3 blocks, 16 threads per block: `mykernel<<<3, 16>>>( );`
                                        - Same but with `dim3`
                                            ```c
                                            dim3 grid(3);
                                            dim3 block(16);
                                            mykernel<<<grid, block>>>( );
                                            ```
                                        - Launch (16 * 4) blocks, (8 * 8 * 8) threads per block
                                            ```c
                                            dim3 grid(16, 4);    /* 2D */
                                            dim3 block(8, 8, 8); /* 3D */
                                            mykernel<<<grid, block>>>( );
                                            ```
                            - Each thread computes a single element r[i][j] of the result matrix r
                                ```c
                                const int i = blockIdx.y * blockDim.y + threadIdx.y;
                                const int j = blockIdx.x * blockDim.x + threadIdx.x;
                                ```

                                ![alt text](./res/matmat1.png "How threads calculate the cell id they have to calculate")
                    
                    - Reducing memory pressure (reduce number of read operations)
                        - Therically
                            - Divide the (BLKDIM * n) stripes of p and q into square blocks of (BLKDIM * BLKDIM) elements each
                            
                                ![alt text](./res/redmem.png "Reducing the memory pressure")

                        - Practically
                            - For each m
                                - Copy blocks from p and q into shared memory local_p and local_q

                                    ![alt text](./res/redmem1.png "Reducing the memory pressure")

                                - Compute the matrix product local_p * local_q in parallel

                                    ![alt text](./res/redmem2.png "Reducing the memory pressure")

                                - m ← m + BLKDIM

                                    ![alt text](./res/redmem3.png "Reducing the memory pressure")

                                - And then reiterate for every m in this line (wait for all threads in the block to complete before starting the next iteration with `__syncthreads()`)
                - [Array reduction](./code/cuda-reduction3.cu): given a nonempty array of floats or ints, compute the sum of all elements of the array
                    - How to?
                        - We'll use [Atomic functions](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#atomic-functions)
                - [CUDA image rotation](./code/cuda-image-rotate.cu)
                    - Theorically
                    
                        ![alt text](./res/imrot.png "CUDA image rotation")

                        ![alt text](./res/imrot1.png "CUDA image rotation")

                        ![alt text](./res/imrot2.png "CUDA image rotation")

                    - Practically
                        - Slow
                            ```c
                            typedef struct {
                                float x;
                                float y;
                                float z;
                            } point3d;
                            
                            __global__ void compute( point3d *points, int n ) {
                                int i = threadIdx.x;
                                float x = points[i].x;
                                float y = points[i].y;
                                float z = points[i].z;
                                /* use x, y, z ... */
                            }
                            ```

                            ![alt text](./res/imgrotar1.png "CUDA image rotation")

                            ![alt text](./res/imgrotar2.png "CUDA image rotation")

                            ![alt text](./res/imgrotar3.png "CUDA image rotation")

                        - Fast
                            ```c
                            typedef struct {
                                float *x;
                                float *y;
                                float *z;
                            } points3d;
                            
                            __global__ void compute( points3d *points, int n ) {
                                int i = threadIdx.x;
                                float x = points->x[i];
                                float y = points->y[i];
                                float z = points->z[i];
                                /* use x, y, z ... */
                            }
                            ```

                            ![alt text](./res/imgrotarb1.png "CUDA image rotation")

                            ![alt text](./res/imgrotarb2.png "CUDA image rotation")

                            ![alt text](./res/imgrotarb3.png "CUDA image rotation")

                        - In general we must try to align memory addresses and maximize the throughput