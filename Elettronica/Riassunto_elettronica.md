# Elettronica

- ### [Dropbox](https://www.dropbox.com/sh/bmr8exrbnkap1vh/AABFNgonNP4bAfAZ9R9Ulx9ta?dl=0)

- ### Ambiente programmazione visuale: LabView (3 mesi di prova studente: `M84X42504`)

- ### Per informazioni su componenti elettronici cerca i loro datasheet

- ### Progetto: https://gitlab.com/myasnik/progetto-di-elettronica-morse-interpreter

### Parte 1 - Intro

- Formule
    - Energia fra due cariche: $`F = k \frac{q \times Q}{R^{2}}`$
    - Energia fra n cariche: $`F = k \sum{\frac{q_{i} \times Q_{i}}{R^{2}}}`$
    - Energia gravitazionale: $`F = G \frac{m \times M}{R^{2}}`$
    - Energia potenziale: $`U = mgh`$
    - Energia elastica: $`F = -k\Delta{x}`$
    - $`e = 1.6 \times 10^{-14}`$
    - Campo elettrico: $`F = k\frac{Q}{R^{2}}`$
    - Differenza di potenziale: $`\Delta{V} = \frac{\Delta{U}}{q_{0}}`$
    - Differenza di potenziale integrata: $`\Delta{V} = k_{0}\frac{Q}{R} + c_{0}`$ con $`k_{0} = \frac{1}{4\pi}\epsilon_{0}`$
- Importante
    - Non prendere mai per oro colato quello che uno strumento digitale misura
    - Leggere sempre quello che c'è scritto sul pannello degli strumenti
    - Le batterie hanno una resistenza interna quindi se vengono sovralimentate tendono a scaldarsi (potrebbero esplodere)
- Conduttori: permettono lo spostamento delle cariche
    - Dentro campo elettrico = 0
- Isolanti: non permettono lo spostamento delle cariche
- Semiconduttori: a metà

### Parte 2 - Resistenze e batterie

- Formule
    - Differenza di potenziale in termine di R ed I (Legge di Ohm): $`V = RI`$
    - Differenza di potenziale in termine di R, I e R strumento: $`V = (R_{v} + R_{s})I`$
    - Differenza di potenziale strumento: $`V_{s} = R_{s}I`$
    - Potenza: $`P = VI`$
- Resistenze

    ![alt text](./res/resis.jpeg)

    - Serie
        - $`R_{t} = \sum{R_{i}}`$
        - Stessa corrente
        - $`V_{t} = R_{t}I`$

        ![alt text](./res/resisser.jpg)

    - Parallelo
        - $`R_{t} = \frac{1}{\sum{\frac{1}{R_{i}}}}`$
            - Conduttanza: $`\frac{1}{R}`$
        - Stessa differenza di potenziale
        - $`V_{t} = R_{t}I`$

        ![alt text](./res/respar.png)

    - Qualsiasi elemento ha una sua resistenza interna (bassa)
- Batterie
    - Serie
        - $`V_{t} = \sum{V_{i}}`$

        ![alt text](./res/batser.jpg)

    - Parallelo
        - Aumento la capacità totale
        - Stessa differenza di potenziale

        ![alt text](./res/batpar.jpg)

- Usare multimetro
    - Misurare corrente fra batteria e resistenza
        - Batteria in serie a multimetro in serie a resistenza
    - Misurare voltaggio fra batteria e resistenza
        - Multimetro in parallelo a batteria in parallelo a resistenza
    - Importante: ogni volta che aggiungo un elemento al circuito questo muta in quanto viene aumentata la resistenza
    
    ![alt text](./res/multuse.png)

- Componente = Resistore, Grandezza = Resistenza
- Trimmer = resistenza regolabile (potenziometro)
- Nodo = giunzione fra 3 o più componenti

    ![alt text](./res/nodo.png)

- Maglia = qualsiasi percorso chiuso in un circuito (se si dirama sono considerate più maglie)

    ![alt text](./res/maglia.png)

- Leggi di Kirchoff
    - Leggi
        - Legge dei nodi: la somma delle correnti entranti e uscenti da un nodo è uguale a zero (in condizioni statiche, non appena il circuito viene acceso ma un po dopo)

            ![alt text](./res/kirnod.png)

        - Legge delle maglie: la somma delle differenze di potenziale in una maglia è uguale a zero

            ![alt text](./res/kirmag.png)

- Ponte di Wheatstone: quando ho due rami simmetrici in mezzo la corrente non passa (perchè la ddv è equivalente, quindi la corrente non ha proprio senso che passi)

    ![alt text](./res/wheatstone.png)

- Se attraverso una resistenza passa più energia di quanta ne sopporta questa si scalda e il valore della sua resistenza varia
    - Potenza: $`W=VI`$
    - Esempio di circuito di test e relativo [progetto di LabView](https://www.dropbox.com/sh/bmr8exrbnkap1vh/AAA8AJVBKn9uKfqI5Y3ASgK1a/Progetti%20Labview?dl=0&subfolder_nav_tracking=1) 
    
        ![alt text](./res/reshot.png)

### Parte 3 - Capacitori

![alt text](./res/capacitor.png)

- Formule
    - $`C = \frac{Q}{V}`$
    - $`C = \frac{\epsilon S}{d}`$
    - $`E = QV`$
- Tipi
    - Capacitori elettrolitici

        ![alt text](./res/capele.png)

        - Polarizzati (se lo colleghi al contrario esplode)
- Collegamento
    - Parallelo: $`C_{t} = \sum{C_{i}}`$

        ![alt text](./res/conpar.png)
    
    - Serie: $`\frac{1}{C_{t}} = \sum{\frac{1}{C_{i}}}`$

        ![alt text](./res/conser.png)

- Esempio di circuito R + C

    ![alt text](./res/escircuit.png)

- Carica e scarica
    - Carica: $`q(t) = Q(1-e^{\frac{-t}{RC}})`$
    - Scarica: $`q(t) = Q e^{\frac{-t}{RC}}`$

### Parte 4 - Diodi

- Diodo: elemento che conduce solo in un verso
- Tipi di materiali
    - Conduttori
    - Isolanti
    - Semiconduttori
- Modello atomico
    - Quando un elettrone passa da un "guscio" dell'atomo ad un altro vengono emesse delle radiazioni
    - Gli elettroni fanno parte della famiglia delle particelle "fermioni"
        - Ogni elettrone ha uno spin 
        - Quando due atomi sono vicini gli elettroni dello stesso livello sono "gelosi", non vogliono stare allo stesso livello degli elettroni degli altri atomi; per questo si creano tanti "sottolivelli" per ogni livello dell'atomo
    - Elettroni di valenza: elettroni che gli atomi si scambiano quando sono legati
    - Elettroni liberi: elettroni che possono circolare liberamente in tutto il cristallo
    - Banda di conduzione: quando ho un cristallo a cui manca un elettrone in un livello viene importato in questo livello un elettrone della banda di conduzione
- Composizione diodo
    - Due semiconduttori che si bilanciano scambiandosi elettroni tramite la zona di svuotamento
    - Polarizzando un semiconduttore positivamente e uno negativamente la corrente scorrerà solo dal semiconduttore positivo a quello negativo

        ![alt text](./res/zonsvuo.png)

    - Avendo un diodo con la barra a "destra" per esempio la corrente scorrerà "da sinistra a destra"

        ![alt text](./res/diodp.png)

- Quando uso un segnale in corrente invece che uno in tensione?
    - Quando devo trasmettere un segnale tramite un canale lungo (tipo 50 metri); la tensione si degrada per la resistenza del cavo, mentre la corrente rimane costante (segnale 4-20mA, light it up stoner)
- Porte logiche
    - OR

        ![alt text](./res/or.png)

    - AND
        - Esempio su [Tinkercad](https://www.tinkercad.com/things/0FvrbmCgMGD-dazzling-waasa-gaaris/editel?sharecode=hom_6A85WOgszGq9xXx02WmJNEKJZfASZ2Bc2TWZZH4=)

        ![alt text](./res/and.png)

### Parte 5 - Transistors

- Tipi
    - BJT (Bipolar junction transistor)
        - Importante: usare sempre un resistore di base, altrimenti il transistor si brucia (come nel caso dei led)
        - Controllato: in tensione
        - Sottotipi
            - NPN (Not Pointing In)
            - PNP: è un NPN che lavora in modo esattamente opposto

            ![alt text](./res/intro.png)

        - Modello di scorrimento

            ![alt text](./res/flowtran.png)

        - Modalità di lavoro
            - On

                ![alt text](./res/water-on.png)

                - Saturation: contro circuito, passaggio libero fra collettore ed emettitore
            - Off

                ![alt text](./res/water-off.png)

                - Cut-off: circuito aperto, non passa corrente fra collettore e emettitore
            - Flow control

                ![alt text](./res/water-control.png)

                - Active: la corrente fra il collettore e l'emettitore è proporzionale a quella che scorre nella base
                - Reverse-Active: come la modalità  attiva ma l'elettricità scorre al contrario

            ![alt text](./res/mode-quadrants.png)

        - Esempi applicazione
            - NOT

                ![alt text](./res/not.png)

            - AND

                ![alt text](./res/anddis.png)

            - OR

                ![alt text](./res/ordis.png)

- Come funziona
    - L'emettitore, carico positivamente, fa fluire la carica verso il collettore (negativo) per diffusione mediante la base; questo è possibile solo perchè la base è molto sottile
    - Facendo scorrere una piccola corrente nella base si ottiene una corrente maggiore nel collettore
    - Resistenze
        - Input: ~25 Ohm
        - Output: Vari MegaOhms
- Formule
    - $`\alpha = \frac{i_{c}}{i_{e}}`$ (con una corrente di emettitore, quanto riesco a controllare di quella di collettore)
    - $`h_{fe} = \beta = \frac{i_{c}}{i_{b}}`$ (con una corrente di base, quanto riesco a controllare di quella di collettore) 
        - $`h_{fe}`$ = A/C
        - $`h_{FE}`$ = D/C
    - $`i_{e} = i_{c} + i_{b}`$
- Amplificatori
    - Tipi

        ![alt text](./res/ampli.png)

### Parte 6 - Analisi degli errori

- In qualsiasi misura ci saranno degli errori
- Tipi
    - Incertezze in lettura delle scale (strumento e/o metodo di misura)
        - Incertezze dovute all'ambiente esterno 
    - Incertezze in misure ripetibili (casuali e sistematiche)

        ![alt text](./res/cassis.png)

        - Discrepanza: differenza fra due valori misurati della stessa grandezza
- Rappresentare ed usare le incertezze
    - Stima migliore ± Incertezza
        - Regola per valutare le incertezze: le incertezze sperimentali dovrebbero di regola essere arrotondate ad una cifra significativa
        - Esempio
            - BENE: $`g = 9.82 ± 0.02 m/s^{2}`$
            - MALE: $`g = 9.82 ± 0.02385 m/s^{2}`$
        - Eccezione: Se la prima cifra significativa è 1 allora può essere meglio tenere due cifre significative
- Incertezze relative
    - Precisione dello strumento (incertezza) / |Misura eseguita| = Precisione percentuale (incertezza frazionaria)
    - Numero medio di eventi nel tempo T = $`v ± \sqrt{v}`$
- Propagazione delle incertezze 
    - Quando una misura finale è data da tante misure intermedie bisogna riuscire a combinare le varie incertezze ottenute
    - Incertezza in una differenza (non sempre vero): se le grandezze x e y con le incertezze ix e iy, e se i valori di x e y sono utilizzati per calcolare la differenza q = x - y, allora l'incertezza in q è la somma delle incertezze in x e in y 
    - Incertezza di un prodotto (non sempre vero): se due quantità x e y sono state misurate con piccole incertezze relative e se i valori misurati x e y sono usati per calcolare il prodotto q = xy, allora l'incertezza relativa su q è la somma delle incertezze relative su x e y 
    - Per differenze e prodotti valgono le regole prima definite che  si ripercuotono anche in somme e quozienti
    - Prodotto di una grandezza misurata per un numero esatto: se la grandezza x è misurata con incertezza ix ed è utilizzata per calcolare il prodotto q = Bx dove B non ha incertezza, allora l'incertezza in q è proprio |B| volte quella in x
    - Incertezze indipendenti in somma: $`\delta q = \sqrt{(\delta x^{2}) + (\delta y^{2})}`$
    - Incertezza in una funzione di una variabile: se x è misurato con incertezza $`\delta x`$ ed è utilizzato per calcolare la funzione $`q(x)`$, allora l'incertezza $`\delta q`$ è $`\delta q = |\frac{dq}{dx}|\delta x`$
    - Incertezza di una potenza: se x è misurato con incertezza $`\delta x`$ ed è utilizzato per calcolare la potenza $`q = x^{n}`$ (dove n è un numero noto fissato), allora l'incertezza relativa in q è |n| volte quella in x
    - Incertezza in una funzione con parecchie variabili: supponiamo che x...z siano misurate con incertezze $`\delta x ... \delta z`$, e i valori misurati utilizzati per calcolare la funzione $`q(x...z)`$. Se le incertezze x....z sono indipendenti e casuali allora l'incertezza in q è $`\delta q = \sqrt{(\frac{\Delta q}{\Delta x}\delta x)^{2} + ... + (\frac{\Delta q}{\Delta z}\delta z)^{2}}`$. In ogni caso essa non è mai superiore alla somma ordinaria.
- Scarto quadratico medio (deviazione standard): distanza fra ogni misura e valor medio in media
    - $`\delta = \sqrt{\frac{1}{N}\sum^{N}_{i=1} (x_{i}-x_{medio})^{2}}`$
    - Non ha senso calcolare la deviazione standard per una sola misura
    - 70% circa di probabilità di cadere in un intervallo $`\delta`$ rispetto alla media
    - Deviazione standard della media: $`\frac{\delta}{\sqrt{N}}`$
- Media pesata
    - $`\frac{\sum w_{i}x_{i}}{\sum w_{i}}`$
    - Peso = $`1/\sigma`$ (più l'incertezza è bassa più la misura pesa)
- Metodo dei minimi quadrati (minimizzo scarto quadratico medio come distanza dalla retta)
    - Formule che spero non useremo mai
        - $`A = \frac{\sum x^{2} \sum y - \sum x \sum xy}{\Delta}`$
        - $`B = \frac{N \sum xy - \sum x \sum y}{\Delta}`$
        - $`\Delta = N \sum x^{2} - (\sum x)^{2}`$
        - $`\sigma _{y} = \sqrt{\frac{1}{N-2} \sum ^{N}_{i=1} (y_{i} - A - Bx_{i})^{2}}`$
        - $`\sigma _{A} = \sigma_{y} \sqrt{\frac{\sum x^{2}}{\Delta}}`$
        - $`\sigma _{B} = \sigma_{y} \sqrt{\frac{N}{\Delta}}`$

### Parte 7 - Amplificatori operazionali

- Amplificatore operazionale ideale
    - Guadagno: la principale funzione di un amplificatore è amplificare quindi maggiore è il guadagno meglio è. Mettiamo che il guadagno sia infinito
    - Impedenza dell'input: assunta infinita; così la sorgente in input non viene influenzata da quella dell'amplificatore
    - Impedenza di output: assunta uguale a zero
    - Tempo di risposta: assunto uguale a zero
    - Offset: l'output sarà zero se un segnale uguale a zero appare fra l'input invertente e quello non invertente
    - Importante
        - Flow = 0 in input
        - Feedback: prendo il segnale in uscita e lo rimando in entrata (utile, può essere usato per controllare l'input)
            - Quando un feedback negativo viene applicato all'amplificatore operazionale la differenza di potenziale tende a zero

        ![alt text](./res/idealopamp.png)

- Esempio

    ![alt text](./res/esamp.png)

- Voltage follower (buffer non invertente)

    ![alt text](./res/volfol.png)

- Amplificatore non invertente

    ![alt text](./res/noninvamp.png)

    ![alt text](./res/ampliid.png)

- Amplificatore invertente

    ![alt text](./res/invamp.png)

### Parte 8 - NTC (resistenza variabile in base alla temperatura)

- Ripasso potenziale e resistenze

    ![alt text](./res/potresrip.png)

- Esempio di utilizzo e calcoli su NTC

    ![alt text](./res/ntc.png)

- Importante: il passaggio di corrente stesso fa scaldare la resistenza, come risolviamo il problema?
    - Non utilizzando una corrente costante quindi semplicemente "accendiamo e spegniamo" il sensore; in questo modo il riscaldamento sarà minore e quindi la misura sarà più accurata

## Riassunto degli argomenti da Owen Bishop

### Celle e batterie

- Celle: più compatta fonte di energia prodotta da una reazione chimica
    - I reagenti chimici quando la cella è costruita sono pronti per reagire isieme
    - Quando le reazioni finiscono la cella è scarica
    - Esistono anche celle ricaricabili, la reazione chimica è riportata allo stato iniziale
- Batterie: un numero di celle connesse insieme per aumentare il voltaggio

### Corrente, voltaggio e potenza

- Corrente: "elettroni" che passano nel circuito
    - Unità: ampere (A) o milliampere (mA)
    - Scorre dal polo positivo al polo negativo
- Voltaggio: forza elettrica che pilota la corrente nel circuito
    - Unità: volt (V)
- Potenza: rate con il quale un device elettrico converte energia da una forma ad un'altra
    - Unità: watt (W)
- $`Potenza = Corrente \times Voltaggio`$

### Resistenza

- Forza che si oppone al passaggio della corrente
- Unità: ohm ($`\Omega`$)
- Legge di Ohm: $`Voltaggio = Corrente \times Resistenza`$

### Resistori

- Resistori fissi: classiche resistenze, definite da un color code
- Resistori variabili: potenziometri, in base a dove il piezo fa contatto la resistenza del componente varia
- Non sovraccaricare i resistori in quanto si rovinerebbero e il circuito non lavorerebbe come ci si aspetta
- I resistori non sono precisissimi, hanno una soglia di errore (-5%, +5%)
- Collegamenti fra resistori
    - Serie: il valore della resistenza è la somma delle singole
        - Corrente è costante
        - La corrente totale che arriva in un punto di congiunzione equivale a quella che ne esce
        - Seguendo lo scorrimento della corrente il voltaggio droppa ad ogni resistenza incontrata, il drop complessivo deve essere uguale al voltaggio totale
    - Parallelo: il valore della resistenza è l'opposto della somma degli opposti delle resistenze
        - Voltaggio costante

### Capacitori

- Due piatti di metallo separati da uno strato isolante; quando il suddetto capacitore viene connesso alla sorgente elettrica acquisisce il voltaggio di essa istantaneamente e rimane carico in quanto le due laste di metallo sono isolate fra loro
- Serve a contenere energia
- Sono polarizzati (esiste polo positivo e negativo)
- Tolleranza: -20%, +20%
- Unità
    - Capacità: farad (F)
    - Carica: coulomb (C)
- $`Capacità = \frac{Carica}{Voltaggio}`$
- Collegamenti fra capacitori
    - Serie: il valore della capacità è l'opposto della somma degli opposti delle capacità
    - Parallelo: il valore della capacità è la somma delle singole
- Carica/scarica
    - Carica (non arriva mai al 100%)

        ![alt text](./res/charge.png)

    - Scarica (non arriva mai allo 0%)

        ![alt text](./res/discharge.png)

### Induttori

- Quando una corrente scorre in un filo un campo magnetico è generato
- Quando una corrente scorre in una coil il campo magnetico generato è simile a quello di una bussola

    ![alt text](./res/induc.png)

- Induzione: quando un campo magnetico si muove in una coil in questa viene indotta una corrente elettrica, per muovere il campo magnetico serve una certa forza che viene tradotta in voltaggio (l'elemento da cui scaturisce il campo magnetico si oppone al movimento)
- Auto-induzione: quando la corrente che scorre in una coil cambia, il campo magnetico cambia. Questo cambiamento di campo funge da magnete che si muove vicino alla coil, induce un'altra corrente nella coil. La direzione della corrente si oppone al cambio di corrente stesso

### Diodi

- Fatti di silicio (drogato), un semiconduttore
- Hanno un anodo e un catodo (segnato con banda nera)
- Un diodo conduce solamente dall'anodo al catodo
- Modi di funzionamento
    - Forward biased: conduce, ha un voltage drop di circa 0.7 V (quindi non conduce se il voltaggio è inferiore a 0.7 V)
    - Reverse biased: non conduce
- Light emitting diodes: LED
    - Si illuminano con una corrente che va da 5 a 20 mA

### Transistor

- Tipi
    - NPN

        ![alt text](./res/npn.png)

    - PNP: uguale ma con emettitore al posto di collettore (in pratica il funzionamento, vedi sotto, è invertito)
- La parte flat è quella con i due terminali
- Viene utilizzato come uno switch: se nella base passa corrente allora la corrente del collettore passa all'emettitore (sommata a quella della base che però è trascurabile), altrimenti non passa niente

### Termistori

- Utilizzati per registrare i cambiamenti di temperatura
- Composti da un semiconduttore
- Sono molto piccoli quindi rispondono in fretta ai cambiamenti di temperatura
- Sono composti da due terminali in cui scorre corrente
- La resistenza dei termistori diminuisce se la temperatura aumenta (ntc, negative temperature coefficient), al contrario sono meno usati e sono chiamati ptc

### Ponte di Wheatstone

- $`\frac{R1}{R2} = \frac{R_{gauge}}{R_{dummy}}`$

    ![alt text](./res/whe.png)

### Amplificatori

- Voltage gain: $`\frac{V_{out}}{V_{in}}`$
- Amplificatori operazionali
    - Info di base
        - Resistenza di input alta
        - Poca corrente necessaria per amplificare
        - Resistenza di output bassa
        - Tanta corrente in output senza drop significativi nel voltaggio di output
        - Gain molto alto
    - Struttura

        ![alt text](./res/opa.png)

        - Devono essere alimentati (V+ e V-) ma non serve un punto a 0V
        - L'output oscilla verso V+ quando il voltaggio a + è maggiore rispetto -
        - L'output oscilla verso V- quando il voltaggio a + è minore rispetto -
    - Amplificatore operazionale invertente

        ![alt text](./res/invop.png)

        - Parte dell'output è fatto tornare all'inverting input, questo riduce il voltage gain a: $`G_{V} = \frac{R2}{R1}`$
        - Il segnale di input viene invertito oltre che amplificato
