# RETI - Pratica
### Packet Analysis

- Tools
    * Tcpdump
        + CLI
        + Multipli parametri
            - Più utili
                * -n → Non risolvere il DNS
                * -v → Verbose
                * -c <n> → Cattura n pacchetti
                * -i <int> → Cattura su interfaccia int 
    * Wireshark
        + GUI
        + Filtri
    * ifconfig/ipconfig → Network configuration
    * ping → Connettifity check
    * traceroute -n
    * route -n → Routing tables

- FTP 
    * Una singola connessione può aprire varie connessioni di trasporto simultaneamente
    * Tipi connessioni → (Su due porte diverse contemporaneamente)
        + Command → Client si connette al server su porta 21
        + Data
            - Active → Server apre data connection (ma se client dietro nat la connessione è bloccata)
            - Passive → Client apre data connection (su porta definita da server)

- Telnet
    * Port 25
    * Bi-directional communication
    * Data transfer
    * remote connection

- DNS
    * Symbolic names
        + Non scelti arbitrariamente
        + La composizione del nome rispecchia una gestione gerarchica del dominio
        + I domain possono essere splittati in subdomain
        + Esempio
            ```
                deisnet.deis.unibo.it

                deisnet → Specifico nome dell'host (arbitrario)
                deis → Dominio di terzo livello
                unibo → Dominio di secondo livello
                it → Dominio di primo livello
            ```
        + Registro.it → Registro di tutti i domini .it
            - Associa un gruppo di numeri (IPs) a un nome (memorizzata in Dbna, database of assigned names)
            - Le regole sono definite dall'ICANN, che ha delegato la gestione dei .it a registro.it
        + Whois → Verifica se e a chi un dominio è intestato
            - Registro.it non registra i subdomain
    * Funzionamento DNS → In Riassunto teoria
        + Most common queries
            - A (default) → Hostname to IP
            - ANY → Ritorna tutti i field DNS associati ad un indirizzo 
            - PTR → IP to Hostname
            - MX → Hostname to mailserver
        + Example → nslookup -querytype=ANY www.cisco.com oppure dig

### Socket Programming

- TCP
    * Buffers
        + Sending buffer → Beffera pacchetti inviati finche un ACK non viene ricevuto
        + Receiving buffer → Buffera pacchetti ricevuti finche non riordinati e passati a app
    * Socket libraries
        + <sys/socket.h>
            - int socket(int domain, int type, int protocol);
                * RET → OK = Socket file descriptor, Err = -1
            - int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
                * RET → OK = 0, Err = -1
                * addr = Indirizzo che sarà associato alla socket (memoria)
            - int listen(int sockfd, int backlog);
                * RET → OK = 0, Err = -1
                * backlog = Massimo numero di connessioni che possono essere accodate
            - int accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
                * RET → OK = Non negative int (la socket), Err = -1
                * addr = Fillato con il socket address del peer
            - int connect(int sockfd, const struct sockaddr *addr, socklen_t, addrlen);
                * RET → OK = 0, Err = -1
                * SOCK_STREAM → connect() effettua una connessione (three-way handshake)
                * SOCK_DGRAM → connect() manda/riceve pacchetti solo all'IP definito (no three-way handshake)
            - ssize_t recv(int sockfd, void *buffer, size_t length, int flags);
                * RET → OK = n. bytes ricevuti, Err = -1, 0 = socket peer shutdown "educato"
                * buffer = Input buffer (length = its size)
            - ssize_t send(int sockfd, const void *buffer, size_t length, int flags);
                * RET → OK = n. bytes sendati, Err = -1
                * buffer = Output buffer (length = its size)
        + <unistd.h>
            - int close(int sockfd);
                * RET → OK = 0, Err = -1
    * Interazione client/server
        + Sequenziale → Il server accetta una connessione, finisce il task e poi ne accetta un'altra
        ```
        SCHEMA:
                SERVER                                  CLIENT
            Creo socket per                          Creo socket
               richieste
               socket()                                socket()
               bind()
               listen()
         ----------
       |          |                                      |
       |          v                                      v
       |  
       |    Aspetto incoming      <----------->      Send conn
       |        request                                 req
       |        accept()                             connect()
       | 
       |          |                                      |
       |          v                                      v
       |
       |    Lettura segmento      <-----------   Scrittura segmento
       |        recv()                                 send()
       | 
       |          |                                      |       
       |          v                                      v
       |
       |    Scrittura seg         ----------->    Lettura segmento
       |        send()                                 recv()
       |
       |          |                                      |
       |          v                                      v
       |
       |        close()           <---------->         close()
       |           
       |          |
         ----------
        
        SORGENTE SERVER IN C:

        #include <\stdio.h>
        #include <\string.h>
        #include <\stdlib.h>
        #include <\errno.h>
        #include <\sys/types.h>
        #include <\sys/socket.h>
        #include <\netinet/in.h>
        #include "myfunction.h”
        
        #define MAX_BUF_SIZE 1024 
        #define SERVER_PORT 9876 // Server port
        #define BACK_LOG 2 // Maximum queued requests

        int main(int argc, char *argv[]){
            struct sockaddr_in server_addr; // struct containing server address information
            struct sockaddr_in client_addr; // struct containing client address information
            int sfd; // Server socket filed descriptor
            int newsfd; // Client communication socket - Accept result
            int br; // Bind result
            int lr; // Listen result
            int i;
            int stop = 0;
            ssize_t byteRecv; // Number of bytes received
            ssize_t byteSent; // Number of bytes to be sent

            socklen_t cli_size;
            char receivedData [MAX_BUF_SIZE]; // Data to be received
            char sendData [MAX_BUF_SIZE]; // Data to be sent

            sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (sfd < 0){
                perror("socket"); // Print error message
                exit(EXIT_FAILURE);
            }

            // Initialize server address information
            server_addr.sin_family = AF_INET;
            server_addr.sin_port = htons(SERVER_PORT); // Convert to network byte 
            server_addr.sin_addr.s_addr = INADDR_ANY; // Bind to any address
            br = bind(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
            if (br < 0){
                perror(”bind"); // Print error message
                exit(EXIT_FAILURE);
            }
            cli_size = sizeof(client_addr);

            // Listen for incoming requests
            lr = listen(sfd, BACK_LOG);
            if (lr < 0){
                perror(”listen"); // Print error message
                exit(EXIT_FAILURE);
            }

            for(;;){
                // Wait for incoming requests
                newsfd = accept(sfd, (struct sockaddr *) &client_addr, &cli_size);
                if (newsfd < 0){
                    perror(”accept"); // Print error message
                    exit(EXIT_FAILURE);
                }
                while(1){
                    byteRecv = recv(newsfd, receivedData, sizeof(receivedData), 0);
                    if (byteRecv < 0){
                        perror("recv");
                        exit(EXIT_FAILURE);
                    }

                    printf("Received data: ");
                    printData(receivedData, byteRecv);
                    if(strncmp(receivedData, "exit", byteRecv) == 0){
                        printf("Command to stop server received\n");
                        close(newsfd);
                        break;
                    }
                    convertToUpperCase(receivedData, byteRecv);
                    printf("Response to be sent back to client: ");
                    printData(receivedData, byteRecv);

                    byteSent = send(newsfd, receivedData, byteRecv, 0);
                    if(byteSent != byteRecv){
                        perror("send");
                        exit(EXIT_FAILURE);
                    }
                }
            }  // End of for(;;)
            close(sfd);
            return 0;
        }

        SORGENTE CLIENT IN C:

        #include <\stdio.h>
        #include <\string.h>
        #include <\stdlib.h>
        #include <\errno.h>
        #include <\unistd.h>
        #include <\sys/types.h>
        #include <\sys/socket.h>
        #include <\arpa/inet.h>
        #include <\netinet/in.h>
        #include "myfunction.h”

        #define MAX_BUF_SIZE 1024 
        #define SERVER_PORT 9876 // Server port

        int main(int argc, char *argv[]){
            struct sockaddr_in server_addr; // struct containing server address information
            struct sockaddr_in client_addr; // struct containing client address information
            int sfd; // Server socket filed descriptor
            int br; // Bind result
            int cr; // Connect result
            int stop = 0;

            ssize_t byteRecv; // Number of bytes received
            ssize_t byteSent; // Number of bytes to be sent
            size_t msgLen; 
            socklen_t serv_size;
            char receivedData [MAX_BUF_SIZE]; // Data to be received
            char sendData [MAX_BUF_SIZE]; // Data to be sent 
            sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (sfd < 0){
                perror("socket"); // Print error message
                exit(EXIT_FAILURE);
            }

            server_addr.sin_family = AF_INET;
            server_addr.sin_port = htons(SERVER_PORT);
            server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
            serv_size = sizeof(server_addr);

            cr = connect(sfd, (struct sockaddr *) &server_addr, sizeof(server_addr));

            if (cr< 0){
                perror(”connect"); // Print error message
                exit(EXIT_FAILURE);
            }

            while(!stop){
                printf("Insert message:\n");
                scanf("%s", sendData);
                printf("String going to be sent to server: %s\n", sendData);

                if(strcmp(sendData, "exit") == 0){
                    stop = 1;
                }

                msgLen = countStrLen(sendData);
                byteSent = sendto(sfd, sendData, msgLen, 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
                printf("Bytes sent to server: %zd\n", byteSent);
                if(!stop){
                    byteRecv = recv(sfd, receivedData, MAX_BUF_SIZE, 0);
                    printf("Received from server: ");
                    printData(receivedData, byteRecv);
                }
            } // End of while
            close(sfd);
            return 0;
        }

        MYFUNCTION.H:

        #include <\ctype.h>
        
        size_t countStrLen(char *str){
            size_t c = 0;
            
            while(*str != ‘\0’){
                c += 1;
                str++;
            }
            return c;
        }
        
        void printData(char *str, size_t numBytes){
            for(int i = 0; i < numBytes; i++){
                printf("%c", str[i]);
            }
            printf("\n");
        }

        void convertToUpperCase(char *str, size_t numBytes){
            for(int i = 0; i < numBytes; i++){
                str[i] = toupper(str[i]);
            }
        }
        ```

        + Concorrente
            - Il server accetta una connessione e si forka
            - Una fork gestisce la connessione appena accettata e poi termina
            - L'altra torna al welcome per accettare altre connessioni
        ```
        SCHEMA:
                SERVER                                  CLIENT
            Creo socket per                          Creo socket
               richieste
            sock = socket()                            socket()
               bind()
               listen()
         ----------
       |          |                                      |
       |          v                                      v
       |  
       |    Aspetto incoming      <----------->      Send conn
       |        request                                 req
       |     newSock = accept()                       connect()
       | 
       |          |                                      |
       |          v                                      |
       |                                                 |       
       |        fork()     --> PID == 0                  |
       |        PID != 0          |                      |
       |                          |                      |
       |          |               |                      |
       |          v               v                      |
       |                                                 |
       |     close(newSock)   close(sock)                |
       |        send()                                   |
       |                                                 |
       |          |               |                      |
         ---------                 v                      v

                            Segment read/write      Segment red/write   
                               recv(newSock)   <--      send()
                               send(newSock)   -->      recv()        

                                   |                      |       
                                   v                      v

                                close()                 close()

        PSEUDOCODICE:

        …
        int sock, newSock;
        sock = socket(…);
        bind(sock, …);
        listen(sock, 5);
        for(;;){
            newSock = accept(sock, …);
            if (fork() == 0) {  // clone process
                close(sock);
                doTask(sock);
                exit(0);
            } else {
                close(newSock);  // father process
            }
        }   
               ```
    * Tipi di socket
        + Association server socket (welcome socket)
            - Associata a una porta
            - Bind, listen, accept
        + Communication server socket
            - Associata ad una connessione già esistente
            - Read, write
        + Association client socket and communication
            - Associata ad una porta se porta disconnessa o a una connessione se porta connessa
            - Bind, connect (se porta non connessa), read e write (se porta connessa)
    * Ci sarebbe anche pag 31..36 ma molto improbabile che le chieda

