s=input("1, 2 o 3?");

switch s
    case 1
        x = linspace(-3,3,100);
        figure
        y=@(x) x.^3-3.*x;
        plot(x,y(x))
    case 2
        x = linspace(0,2.*pi,100);
        figure
        y=@(x) 3.*x.*cos(2.*x);
        plot(x,y(x))
    case 3
        x = linspace(-8.*pi,8.*pi,100);
        figure
        y=@(x) sin(x)./x;
        plot(x,y(x))
end