%1
%Somma prima n numeri naturali dispari con for

n=input("Inserisci n: ");
somma = 0;
z = 1;
if (n < 1)
    exit();
end
for i=1:n
    somma=somma+z;
    z = z + 2;
end
somma

%Somma prima n numeri naturali dispari senza for

n=input("Inserisci n: ");
if (n < 1)
    exit();
end
clear sum
sum(1:2:2*n)