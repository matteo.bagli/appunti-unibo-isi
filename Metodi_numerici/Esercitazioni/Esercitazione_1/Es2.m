k=[1/3 1/2 1 2];
x = linspace(-pi,pi,100);

figure
for i=1:4
    y = sin(x.*k(i));
    subplot(2,2,i)
    plot(x,y)
end