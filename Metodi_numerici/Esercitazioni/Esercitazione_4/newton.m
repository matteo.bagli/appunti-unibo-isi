function [x, xk, it] = newton(fname,fpname,x0,tolx,tolf,maxit)

fx0=fname(x0);
dfx0=fpname(x0);

if abs(dfx0) > eps
    d=fx0/dfx0;
    x1=x0-d;
    fx1=fname(x1);
    it=1;
    xk(it)=x1;
else
    fprintf('Derivata nulla in x0 - EXIT \n');
    return
end

while it < maxit && abs(fx1)>=tolf && abs(d)>=tolx*abs(x1)
    x0=x1;
    fx0=fname(x0);
    dfx0=fpname(x0);
    
    if abs(dfx0) > eps
        d=fx0/dfx0;
        x1=x0-d;
        fx1=fname(x1);
        it=it+1;
        xk(it)=x1;
    else
        fprintf('Derivata nulla in x0 - EXIT \n');
        return
    end
end

x=xk(it);