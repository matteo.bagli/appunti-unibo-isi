function [x1, xk, it] = corde(fname,fpname,x0,tolx,tolf,maxit) %E la x???

%%QUALCOSA NON VAAA
m=fpname(x0);
fx0=fname(x0);
d=fx0/m;
x1=x0-d;    % Intersezione della retta passante per (x0,f(x0)) e tangente alla curva con asse x (y = 0)
fx1=fname(x1);
it = 1;
xk(it)=x1;
    while it <= maxit && abs(fx1) >= tolf && abs(d) >= tolx*abs(x1)
        x0=x1;
        fx0=fname(x0);
        d=fx0/m;
        x1 = x0-d;
        fx1=fname(x1);
        it = it +1;
        xk(it,:)=x1;
    end
if it==maxit 
    fprintf('raggiunto massimo numero di iterazioni \n');
end