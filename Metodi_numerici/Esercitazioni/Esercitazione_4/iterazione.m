function [x1,xk,it]=iterazione(g,x0,tolx,maxit)

x1=g(x0);
d=x1-x0;
it=1;
xk(it)=x1;

while it<maxit && abs(d)>=tolx*abs(x1)
    x0=x1;
    x1=g(x0);
    d=x1-x0;
    it=it+1;
    xk(it)=x1;
end

if it==maxit
    fprintf('Raggiunto massimo numero iterazioni');
end