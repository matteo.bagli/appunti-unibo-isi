function [x, xk, it] = secanti(fname,xm1,x0,tolx,tolf,maxit)

fxm1=fname(xm1);
fx0=fname(x0);
d=fx0*(x0-xm1)/(fx0-fxm1);
x1=x0-d;
fx1=fname(x1);
it=1;
xk(it)=x1;

while it < maxit && abs(fx1)>=tolf && abs(d)>=tolx*abs(x1)
    xm1=x0;
    x0=x1;
    fxm1=fname(xm1);
    fx0=fname(x0);
    d=fx0*(x0-xm1)/(fx0-fxm1);
    x1=x0-d;
    fx1=fname(x1);
    it=it+1;
    xk(it)=x1;
end

x=xk(it);