% Mi sa es 6

clc
clear all
close all

tolx=1.e-7;
itmax=1000;
x0=1.5;

syms x
gx=sqrt(10/(x+4));

g=matlabFunction(gx);
dg=matlabFunction(diff(gx,x,1));

[x1,xk,it]=iterazione(g,x0,tolx,itmax)

costante_asintotica=abs(dg(x1))