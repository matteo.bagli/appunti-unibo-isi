%Ultima funz

%NON VAAA

clc
clear all
close all
n=input('Grado del polinomio');
a=-5;
b=5;
x = linspace(a,b,n+1);
y = 1./1+(x.^2);

%Punti valutazione interpolante di newton
xv=linspace(min(x),max(x),200);
yv=1./1+(x.^2);
%Costruiamo i coefficienti del polinomio interpolatore di newton
c=interpN(x,y);
%Valutazione del polinomio interpolatore di Newtin in xv
yNewton=HornerN(c,x,xv);


figure
plot(xv,yNewton,'b-',x,y,'r*',xv,yv,'g--');
legend('Interpolante di newton','Dati','Funzione esatta');