clc
clear all
close all

x = [-55 -45 -35 -25 -15 -5 5 15 25 35 45 55 65];
y = [3.7 3.7 3.52 3.27 3.2 3.15 3.15 3.25 3.47 3.52 3.65 3.67 3.52];

%Punti valutazione interpolante di newton
xv=linspace(min(x),max(x),200);

%Costruiamo i coefficienti del polinomio interpolatore di newton
c=interpN(x,y);
%Valutazione del polinomio interpolatore di Newtin in xv
yNewton=HornerN(c,x,xv);

%Valutiamo il polinomio interookatore di newtn nella latitudine L1=-42
L1=-42;
yNew_L1=HornerN(c,x,L1);

%Valutiamo il polinomio interookatore di newtn nella latitudine L2=42
L2=42;
yNew_L2=HornerN(c,x,L2);

figure
plot(xv,yNewton,'b-',x,y,'r*',L1,yNew_L1,'go',L2,yNew_L2,'mo');
legend('Interpolante di newton','Dati','Stima in L1','Stima in L2');