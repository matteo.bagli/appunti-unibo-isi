function pval = HornerN(c,x,z)
% Calcola i valori del polinomio di Newton nei punti del vettore z
n = length(c);
pval = c(n)*ones(size(z));
for k=n-1:-1:1
    pval = (z-x(k)).*pval + c(k);
end