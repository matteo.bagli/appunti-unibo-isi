clear all
close all
clc

x=0:pi/2:2*pi;
y=sin(x);

xv=linspace(0,2*pi,200);
yv=sin(xv);
yL_v=interpL(x,y,xv);

plot(xv,yL_v,'b--',x,y,'*',xv,yv,'c-');
legend('Interpolante di Lagrange','Punti di interpolazione','Funzione seno nei punti di valutazione');
