clc
clear all
close all

xnodi=0:1/4:1;

n=length(xnodi);

xv=linspace(xnodi(1), xnodi(n),200);

for k=1:n
    p=plagr(xnodi,k);
    L=polyval(p,xv);
    figure(k)
    hold on
    plot(xnodi,zeros(1,n),'ro');
    plot(xnodi(k),1,'c*');
    plot(xv,L,'b-');
end


