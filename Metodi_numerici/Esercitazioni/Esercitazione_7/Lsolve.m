function [x] = Lsolve(A, b) %%Dpvrebbe esserci la flag a uno se un elem della diag è a 0

n = size(A);
n = n(1);

sum = 0;
i = n;
j = i + 1;
while i > 0
    while j <= n
        sum = sum + A(i, j) * x(j);
        j = j + 1;
    end
    x(i) = (b(i) - sum) / A(i, i);
    sum = 0;
    i = i - 1;
    j = i + 1;
end