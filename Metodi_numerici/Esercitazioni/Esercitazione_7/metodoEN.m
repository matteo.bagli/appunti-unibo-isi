function [a]=metodoEN(x,y,n)
% n grado del polinomio approssimante e a vettore dei suoi coefficienti
% Costruzione della matrice rettangolare di Vandermonde
l = length(x);
H_completa = vander(x);
H = H_completa(:,l-n:l);
% Risolve il sistema delle equazioni normali H� ∗ Ha = H�y
% con fattorizzazione di Cholesky
A = H' * H;
b = H' * y;
[R, p] = chol(A);
if p > 0
disp("A non definita positiva")
disp("Calcolo soluzione sistema con \")
a = A \ b;
else
b1 = Lsolve(R', b);
a = Usolve(R, b1);
end