clear all
clc
close all

% Penso di non aver capito una sega

x1 = [-3.5, -3, -2, -1.5, -0.5, 0.5, 1.7, 2.5, 3]';
y1 = [-3.9, -4.8, -3.3, -2.5, 0.3, 1.8, 4, 6.9, 7.1]';
x2 = [-3.14, -2.4, -1.57, -0.7, -0.3, 0, 0.4, 0.7, 1.57]';
y2 = [0.02, -1, -0.9, -0.72, -0.2, -0.04, 0.65, 0.67, 1.1]';
x3 = linspace(0, 3, 12)';
y3 = exp(x3).* cos(4 * x3) + randn(12, 1);
x4 = [1.001, 1.0012, 1.0013, 1.0014, 1.0015, 1.0016]';
y4 = [-1.2, -0.95, -0.9, -1.15, -1.1,-1]';

a1=metodoEN(x1,y1,1)
b1=metodoQR(x1,y1,1)

a2=metodoEN(x2,y2,1)
b2=metodoQR(x2,y2,1)

a3=metodoEN(x3,y3,1)
b3=metodoQR(x3,y3,1)

a4=metodoEN(x4,y4,1)
b4=metodoQR(x4,y4,1)