function [x] = general_solve(A, b) %Es2

[L,U,P,flag]=LU_parziale(A);

if flag == 0
    y = L\(P*b);
    x = U\y;
else
    x = -1;
end