clc
clear all
close all

A=[1 2 3; 0 0 1; 1 3 5];
b=[6;1;9];

A1=[1 1 0 3; 2 1 -1 1; -1 2 3 -1; 3 -1 -1 2];
b1=[5;3;3;3];

[L,U,P,flag]=LU_nopivot(A);

if flag == 0
    LU_solve(L,U,P,b)
end

[L,U,P,flag]=LU_parziale(A);

if flag == 0
    LU_solve(L,U,P,b)
end

[L,U,P,flag]=LU_nopivot(A1);

if flag == 0
    LU_solve(L,U,P,b1)
end

[L,U,P,flag]=LU_parziale(A1);

if flag == 0
    LU_solve(L,U,P,b1)
end