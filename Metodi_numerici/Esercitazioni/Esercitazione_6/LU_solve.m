function [x] = LU_solve(L, U, P, b)

y = resolve_forward(L,P * b);
x = resolve_backward(U, y);