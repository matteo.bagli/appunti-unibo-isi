function [IN,N]=simptoll(fun,a,b,tol)

Nmax=2048;
err=1;

N=1;
IN=SimpComp(fun,a,b,N);

while N<=Nmax && err>tol
    N=2*N;
    I2N=SimpComp(fun,a,b,N);
    err=abs(IN-I2N)/15;
    IN=I2N;
end

if N>Nmax
    fprintf('Raggiunto nmax di intervalli con simptoll\n')
    N=[]; IN=[];
else
    fprintf('Q=%g, N=%g \n',IN,N)
end
