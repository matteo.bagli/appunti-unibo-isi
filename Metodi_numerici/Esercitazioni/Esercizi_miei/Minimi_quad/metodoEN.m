function [a]=metodoEN(x,y,n)

% Metodo delle equazioni normali per risolvere il problema di approssimazione ai minimi quadrati
% INPUT
% x vettore colonna con le ascisse dei punti
% y vettore colonna con le ordinate dei punti 
% n grado del polinomio approssimante
% OUTPUT
% a vettore colonna contenente i coefficienti incogniti

%  H(:,n+1)=ones(length(x),1);
%  for j=n:-1:1
%     H(:,j)=x.*H(:,j+1);
%  end
 l=length(x);
 H_completa=vander(x);
 H=H_completa(:,l-n:l);


% Risolvi il sistema delle equazioni normali H'*H a = H'y
A=H'*H;
b=H'*y;

%fattorizzazione di Choleski
[R,p]=chol(A);

if p>0
   disp('A is not positive definite')
   disp('Calcolo soluzione sistema con \')
   a=A\b;
else
   b1=Lsolve(R',b);
   a=Usolve(R,b1);
end

