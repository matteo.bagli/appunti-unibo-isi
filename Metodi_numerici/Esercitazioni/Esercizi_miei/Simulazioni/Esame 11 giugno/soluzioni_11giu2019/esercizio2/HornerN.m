  function pval = HornerN(c,x,z)
%Valuta il polinomio interpolatore di Newton nel vettore z dei punti di
%valutazione 

  n = length(c); 
  pval = c(n)*ones(size(z));
  for k=n-1:-1:1
     pval = (z-x(k)).*pval + c(k);
  end
