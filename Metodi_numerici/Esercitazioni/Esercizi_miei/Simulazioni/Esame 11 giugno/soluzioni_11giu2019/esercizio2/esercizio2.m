clc
clear all
close all

%Punto 1)
nodi=linspace(0,5,5);
fnodi_exact=besselj(0,nodi)
 
for i=1:5
    f=@(t) 1/pi*cos(nodi(i)*sin(t)); 
    [I,N]=traptoll(f,0,pi,1.e-2);
    N
    fnodi_trap(i)=I;
end
err_rel=abs(fnodi_exact-fnodi_trap)./abs(fnodi_exact)

%Punto 2)
c=InterpN(nodi,fnodi_trap);
xx=linspace(nodi(1),nodi(5),100);
yy=HornerN(c,nodi,xx);

figure(1)
hold on
plot(nodi,fnodi_trap,'bo',xx,yy,'r-')
plot(xx,besselj(0,xx),'b--')

%Punto 3) 
xx=linspace(0,5,100);
cont=0; 
for n=10:5:25
    cont=cont+1;
%    nodi=linspace(0,5,n);
     for i=0:n-1
         nodi(i+1)=5*i/(n-1);
     end

     %La costante di Lebesgue si ottiene calcolando il massimo della somma
     %dei valori assoluti di tutti gli n polinomi di base di Lagrange
     %relativi a ciascun nodo e valutati nei punti di valutazione xx. In Lc
     %accumulo tale somma
    Lc=zeros(1,100);
    for l=1:n        
        pc=plagr(nodi,l);
        Lc=Lc+abs(polyval(pc,xx));
    end
     leb(cont)=max(Lc);
    %----------------------------
end

 figure(2)
 semilogy([10:5:25],leb,'go-')
leb
  
 
