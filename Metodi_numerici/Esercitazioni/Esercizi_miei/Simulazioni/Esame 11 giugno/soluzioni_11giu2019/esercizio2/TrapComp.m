%formula dei trapezi composita su n sottointervalli 
%definiti da nodi equispaziati

function I=TrapComp(fname,a,b,n)
h=(b-a)/n;
nodi=[a:h:b];
%i nodi sono n+1
f=feval(fname,nodi);

I=(f(1)+2*sum(f(2:n))+f(n+1))*h/2;
