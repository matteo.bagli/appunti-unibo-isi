 function [x,flag]=Usolve(U,b)
 % Risoluzione con procedura backward di Ux=b con U triangolare superiore 
 
 % Test dimensione
 [n,m]=size(U); 
 flag=0;
 if n ~= m, disp('errore: matrice non quadrata'), x=[]; flag=1; return, end
 % Test singolarita'
 if all(diag(U)) ~= 1, disp('el. diag. nullo'), x=[]; flag=1; return, end
 % Preallocazione vettore soluzione
 x=zeros(n,1);
 % Risoluzione backward
 for i=n:-1:1
     s=U(i,i+1:n)*x(i+1:n); % scalare=vettore riga * vettore colonna
     x(i)=(b(i)-s)/U(i,i);
 end
 
