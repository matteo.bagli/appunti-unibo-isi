clc
clear all

syms k 

%parte 1)  %A singolare per k=sqrt(7)/2
A=[-3/2 4 -k; 1 -3/2 0; 0 -k 1];
det(A)

%parte 2)
k=2;
A1=[-3/2 4 -k; 1 -3/2 0; 0 -k 1];
b=[1; 0; 0];
  
[L,U,P,flag]=LU_parziale(A1);
P
[y,flag]=Lsolve(L,P*b);
[x,flag]=Usolve(U,y);
x
%verifica
A1*x-b
 