clc
clear all
close all

syms k
%parte 3)
%A � resa simmetrica scambiando prima e seconda riga
Ak=[1 -3/2 0; -3/2 4 -k; 0 -k 1];
det(Ak)
%Il minore principale di ordine 1 ha determinate positivo, il minore principale di ordine 2 ha
%determinate positivo, verifico per quali valori di k, reale positivo, risulta positibo il
%determinante. Essendo il deteminante uguale a 7/4-k^2, e dpvendo essere k
%positvo, il range di k per cui A risulta definita positiva � 0<k<sqrt(7)/2

%Verifico che per questo range di k, la matrice simmetrica A ha valori
%singolari reali positivi

for k=0.01:0.1:sqrt(7)/2-0.01
A=[1 -3/2 0; -3/2 4 -k; 0 -k 1];
    sign(eig(A));
end

%parte 4)
%Permuto il termine noto allo stesso modo con cui ho permutato le righe
%della matrice
b=[0; 1; 0];

kvec=linspace(0.01,sqrt(7)/2-0.01,20);

for i=1:20
    k=kvec(i);
    A1=[1 -3/2 0; -3/2 4 -k; 0 -k 1];
    [R,p]=chol(A1); 
    y=Lsolve(R',b);
    x=Usolve(R,y); 
    
    norma2(i)=norm(x,2);
    normainf(i)=norm(x,inf);
end

plot([1:20],norma2,'b.-',[1:20],normainf,'r.-')
legend('norma2','normainf')
