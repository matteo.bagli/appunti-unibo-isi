clc
clear all
close all

Itrap=TrapComp(@es1_punto1,-2,2,1)

Itrapcomp=TrapComp(@es1_punto1,-2,2,150)

%matlab integration

%la funzione quad di matlab fa un'approssimazione numerica l'integrale della funzione facendo uso delle formule adattive di Simpson 
%con una tolleranza di 01^-6

Imatlab=quad(@es1_punto1,-2,2)