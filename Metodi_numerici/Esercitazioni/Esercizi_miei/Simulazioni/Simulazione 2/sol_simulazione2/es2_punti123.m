clc
clear all

x=[0.0, 0.25, 0.6, 2.0, 3.5, 5.0, 6.5, 8.0];
y=[0.001 0.016, 0.029, 0.037, 0.033, 0.030, 0.030, 0.029];

%punti di valutazione
xv=linspace(min(x),max(x),100);

%................................
%polinomio di approssimazione di grado 3
%a1=polyfit(x,y,3);
a1=metodoQR(x',y',3);

yv1=polyval(a1,xv);
%................................
%polinomio di approssimazione di grado 4
%a2=polyfit(x,y,4);
a2=metodoQR(x',y',4);

yv2=polyval(a2,xv);
%................................

%Determino i coefficienti a,b,c risolvendo il sistema sovradeterminato 
%costruito imponendo il passaggio della curva nella base esponenziale per i punti (x,y)

M=[ones(size(x')), exp(-x'), x'.*exp(-x')];
% coeff=M\y';   %coeff � il vettore [a b c]'
[Q,R]=qr(M);
yq=Q'*y';
coeff=Usolve(R(1:3,:),yq);

yv3=coeff(1)+coeff(2)*exp(-xv)+coeff(3)*xv.*exp(-xv);
%................................


%somme degli errori quadratici commessi nei nodi
E1=sum((polyval(a1,x)-y).^2)
E2=sum((polyval(a2,x)-y).^2)
E3=sum( (coeff(1)+coeff(2)*exp(-x)+coeff(3)*x.*exp(-x)-y).^2 )

%La curva nella base esponenziale fornisce l'approssimazione migliore delle tre, infatti: E3<E2<E1.
%.................................

figure(1);
hold on
plot(x,y,'k*');
plot(xv,yv1,'b-',xv,yv2,'r-',xv,yv3,'g-')
legend('punti','grado 3','grado 4','esponenziale')
