clc
clear all
close all

% osservando il grafico di f(t) si vede uno zero vicino a -0.5
% che puo' essere determinato dal metodo di bisezione scegliendo per esempio [a,b]=[-1,0]
% in quanto risulta verificata l'ipotesi f(a)*f(b)<0

% c'e' anche uno zero vicino a 1 in cui pero' la curva appare
% tangente e quindi non posso usare il metodo di bisezione per determinarlo

tol = 10^(-6);
a = -1;
b = 0;
[x,it,mezzo]=bisez(@es1_punto1,a,b,tol);
