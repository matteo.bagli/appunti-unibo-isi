function f=es1_punto1(t)

f = zeros(size(t));

% scriviamo la function in modo che possa ricevere in ingresso un array

for i=1:length(t)
    f(i) = det([1.25 t(i) t(i);t(i) 1 t(i);t(i) t(i) 1]);
end