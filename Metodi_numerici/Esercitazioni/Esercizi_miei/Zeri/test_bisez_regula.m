clc
clear all
format long e

f=@(x) log(x)-sin(x);
a=1;
b=2*pi;

% f=@(x) x-cos(x).^2-sin(x);
% a=0;
% b=pi;

figure(1)
hold on
fplot(f,[a b])
plot([a b],[f(a) f(b)],'k--')
plot([a b],[0 0],'r:')

tol=1e-8;
nmax=500;
[alfa_b,alfak_b,iter_b]=bisez(f,a,b,tol); 
iter_b
alfa_b
disp('...........................')
[alfa_rf, alfak_rf, iter_rf] = regula_falsi(f,a,b,tol,nmax);
iter_rf
alfa_rf

for k=1:iter_rf
    plot(alfak_rf(k),0,'k*') 
    text(alfak_rf(k),-0.02,num2str(k))
end