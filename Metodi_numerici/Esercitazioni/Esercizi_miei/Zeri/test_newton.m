clc
close all
clear all
format long e

func=@(x) -x^2+2*x+5;
func1=@(x) -2*x+2;
a = -2;
b = 3;
nmax = 100;

[x,xk,it]=newton_mio(func,func1,20,eps*2,eps*2,nmax)