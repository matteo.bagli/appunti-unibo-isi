function [x, xk, it] = regula_falsi_mio(fname,a,b,tol,nmax)

fa=feval(fname,a);
fb=feval(fname,b);
if sign(fa)*sign(fb)>=0
    error('intervallo non corretto');
else
    it=0;
    fxk=feval(fname,a);
    while it<nmax && abs(b-a)>=tol+eps*max([abs(a) abs(b)]) && abs(fxk)>=tol 
        it = it + 1;
        xk(it)=a-fa*(b-a)/(fb-fa);
        fxk = feval(fname,xk(it));
        if fxk == 0
            break;
        elseif sign(fxk)*sign(fa) > 0
            a = xk(it);
            fa = fxk;
        elseif sign(fxk)*sign(fb) > 0
            b = xk(it);
            fb = fxk;
        end
    end
    if it == nmax
        x = [];
        disp("Too much iterations");
    else
        x = xk(it);
    end
end