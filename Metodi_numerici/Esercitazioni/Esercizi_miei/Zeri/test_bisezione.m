clc
close all
clear all
format long e

func=@(x) -x^2+2*x+5;
a = -2;
b = 3;
nmax = 1000;

[x,xk,it]=bisez(func,a,b,eps)
