clc
close all
clear all
format long e

func=@(x) -x^2+2*x+5;
func1=@(x) -2*x+2;
a = -2;
b = 3;
nmax = 1000;

[x,xk,it]=corde_mio(func,func1,20,eps*2,eps*2,nmax)