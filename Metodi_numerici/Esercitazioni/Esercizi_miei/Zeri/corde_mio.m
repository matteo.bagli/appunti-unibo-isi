function [x1,xk,it]=corde_mio(func,func1,x0,tolx,tolf,nmax)

m=func1(x0);
fx0=func(x0);
d=fx0/m;
x1=x0-d;
fx1=func(x1);
it=1;
xk(it)=x1;

while it<nmax && abs(fx1)>=tolf && abs(d)>=tolx*abs(x1)
    x0=x1;
    fx0=func(x0);
    d=fx0/m;
    x1=x0-d;
    fx1=func(x1);
    xk(it,:)=x1;
    it=it+1;
end

if it==nmax
    disp("MAXIT");
end