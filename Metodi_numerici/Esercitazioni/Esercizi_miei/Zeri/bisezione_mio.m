function [x, vx, it]=bisezione_mio(func, a, b, tol)

fa = feval(func,a);
fb = feval(func,b);

if sign(fa) * sign(fb) >= 0
    disp("ERRORE");
else
    maxit=ceil(log((b-a)/tol)/log(2));
    vx = zeros(1, maxit);
    it = 0;
    while it <= maxit && abs(b-a) >= tol + eps*max([abs(a),abs(b)])
        it = it + 1;
        vx(it) = a + (b-a)*0.5;
        fvx = feval(func,vx(it));
        if fvx == 0
            break;
        elseif sign(fvx) * sign(fa) > 0
            a = vx(it);
            fa = fvx;
        elseif sign(fvx) * sign(fb) > 0
            b = vx(it);
            fb = fvx;
        end
        x = vx(it);
    end
end