function [L,U,P,flag]=LU_parziale_mio(A)

flag=0;
[n,m]=size(A);

if n~=m
    disp("No quadra");
    L=[]; U=[]; P=[]; flag=1;
    return;
end

P=eye(n);
for k=1:n-1
    [pivot,ir_pivot]=max(abs(A(k:n,k)));
    ir_pivot=ir_pivot+(k-1);
    if pivot==0
        disp("Pivot nullo");
        L=[]; U=[]; P=[]; flag=1;
        return;
    end
    if ir_pivot~=k
        temp=A(k,:); 
        A(k,:)=A(ir_pivot,:);
        A(ir_pivot,:)=temp;
        temp=P(k,:); 
        P(k,:)=P(ir_pivot,:);
        P(ir_pivot,:)=temp;
    end
    A(k+1:n,k)=A(k+1:n,k)/A(k,k);
    A(k+1:n,k+1:n)=A(k+1:n,k+1:n)-A(k+1:n,k)*A(k,k+1:n);
end

L=tril(A,-1)+eye(n);
U=triu(A);