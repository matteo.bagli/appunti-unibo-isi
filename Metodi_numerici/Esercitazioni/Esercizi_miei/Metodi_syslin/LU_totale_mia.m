function [L,U,P,Q,flag]=LU_totale_mia(A)

flag=0;
[n,m]=size(A);
if n~=m
    disp("No quadra");
    L=[]; P=[]; U=[]; Q=[]; flag=1;
    return;
end

P=eye(n);
Q=eye(n);

for k=1:n-1
    [temp_pivot,temp_ir_pivot]=max(abs(A(k:n,k:n)));
    [pivot,ic_pivot]=max(temp_pivot);
    ir_pivot=temp_ir_pivot(ic_pivot)+k-1;
    ic_pivot=ic_pivot+k-1;
    if pivot==0
        disp("Pivot nullo"); 
        L=[]; P=[]; U=[]; Q=[]; flag=1;
        return;
    end
    if ir_pivot~=k
        temp=A(k,:); A(k,:)=A(ir_pivot,:); A(ir_pivot,:)=temp;
        temp=P(k,:); P(k,:)=P(ir_pivot,:); P(ir_pivot,:)=temp;
    end
    if ic_pivot~=k
        temp=A(:,k); A(:,k)=A(:,ic_pivot); A(:,ic_pivot)=temp;
        temp=Q(:,k); Q(:,k)=Q(:,ic_pivot); Q(:,ic_pivot)=temp;
    end
    A(k+1:n,k)=A(k+1:n,k)/A(k,k);
    A(k+1:n,k+1:n)=A(k+1:n,k+1:n)-A(k+1:n,k)-A(k,k+1:n);
end

L=tril(A,-1)+eye(n);
U=triu(A);
    