function [L,U,P,flag]=LU_nopivot_mia(A)

flag=0;
[n,m]=size(A);
if n~=m
    disp("No quadrata");
    L=[]; U=[]; P=[];
    flag=1;
    return
end

P=eye(n);
for k=1:n-1
    if A(k,k)==0
        disp("Elemento nullo in diag");
        L=[]; U=[]; P=[];
        flag=1;
        return;
    end
    A(k+1:n,k)=A(k+1:n,k)/A(k,k);
    A(k+1:n,k+1:n)=A(k+1:n,k+1:n)-A(k+1:n,k)*A(k,k+1:n);
end

L=tril(A,-1)+eye(n);
U=triu(A);
    