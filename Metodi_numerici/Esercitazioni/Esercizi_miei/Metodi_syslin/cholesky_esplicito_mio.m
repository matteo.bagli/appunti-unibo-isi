function [L]=cholesky_esplicito_mio(A)

[n,m]=size(A);
if n~=m
    disp("No quadra");
    return;
end
L=zeros(n);
L(1,1)=sqrt(A(1,1));

for i=2:n
    for j=1:i-1
        s=0;
        for k=1:i-1
            s=s+L(i,k)*L(j,k);
        end
        L(i,j)=(A(i,j)-s)/L(j,j);
    end
    L(i,i)=sqrt(A(i,i)-sum(L(i,1:i-1).^2));
end


