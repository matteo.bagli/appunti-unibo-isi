function [R,flag]=cholesky(A)
  % Metodo di fattorizzazione di Cholesky A=R'R con R triangolare superiore
  
  % Test dimensione
  [n,m]=size(A);
  flag=0;
  if n ~= m, disp('errore: matrice non quadrata'), R=[]; flag=1; return, end
  % Preallocazione e copia A in R  per evitare copia array in chiamata function
  R=zeros(size(A)); 
  R=A;
  
  for k=1:n
      Rkk=R(k,k); 
      if Rkk <= 0, disp('Matrice non definita positiva'), flag=1; return, end 
      Rkk=sqrt(R(k,k)); % Determina rho
      R(k,k)=Rkk;
      R(k,k+1:n)=R(k,k+1:n)./Rkk; % Determina sottovettore riga r^T
      R(k+1:n,k+1:n)=R(k+1:n,k+1:n)-R(k,k+1:n)'*R(k,k+1:n); % Aggiorna la sottomatrice
  end    
  R=triu(R); 
