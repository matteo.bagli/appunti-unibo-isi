function [L,U,P,flag]=LU_parziale(A)
% Fattorizzazione PA=LU con pivot parziale
% In output:
%  L matrice triangolare inferiore
%  U matrice triangolare superiore
%  P matrice di permutazione sulle righe
%    tali che  LU=PA
  
  % Test dimensione
  [n,m]=size(A);
  flag=0;
  if n ~= m, disp('errore: matrice non quadrata'),  L=[]; U=[]; P=[]; flag=1; return, end
  % Copia A in U  
 
  P=eye(n);
  % Fattorizzazione
  for k=1:n-1
      %Scelta pivot parziale + scambi su U e P
      [pivot,ir_pivot]=max(abs(A(k:n,k))); 
      ir_pivot=ir_pivot+(k-1); 
      if pivot == 0, disp('pivot nullo'),  L=[]; flag=1; return, end
      if ir_pivot  ~= k
          
          %SCambio sulle righe di A
         temp=A(k,:); A(k,:)=A(ir_pivot,:); A(ir_pivot,:)=temp;
         %Scambio su P per memorizzare lo scambio
         temp=P(k,:); P(k,:)=P(ir_pivot,:); P(ir_pivot,:)=temp;
      end
      %Eliminazione gaussiana
      A(k+1:n,k)=A(k+1:n,k)/A(k,k);                         % Memorizza i moltiplicatori	  
      A(k+1:n,k+1:n)=A(k+1:n,k+1:n)-A(k+1:n,k)*A(k,k+1:n);  % Eliminazione gaussiana sulla matrice
  end
  
  L=tril(A,-1)+eye(n); % Estrae i moltiplicatori 
  U=triu(A);           % Estrae la parte triangolare superiore+diagonale
  