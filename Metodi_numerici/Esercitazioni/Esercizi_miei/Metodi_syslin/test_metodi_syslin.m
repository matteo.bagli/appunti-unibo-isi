% test dei metodi  

clc
clear all
nmin=5;
nmax=200;
for n=nmin:nmax
   A=gallery('orthog',n,1); 
   xesatta=(1:n)'; 
   b=A*xesatta;
   norm_xesatta=norm(xesatta);
   %Gauss
   %...................................
   tic
   [Lnopivot,Unopivot,Pnopivot,flag]=LU_nopivot(A);
   [xnopivot,flag]=LUsolve(Lnopivot,Unopivot,Pnopivot,b);
   toc_nop(n-nmin+1)=toc;
   errore_relativo_nopivot(n-nmin+1)=norm(xesatta-xnopivot)/norm_xesatta;
   %Gauss_parziale  
   %...................................
   tic
   [Lparziale,Uparziale,Pparziale,flag]=LU_parziale(A);
   [xparziale,flag]=LUsolve(Lparziale,Uparziale,Pparziale,b);
   toc_par(n-nmin+1)=toc;
   errore_relativo_parziale(n-nmin+1)=norm(xesatta-xparziale)/norm_xesatta;
   %Gauss_totale  
   %...................................
   tic
   [Ltotale,Utotale,Ptotale,Qtotale,flag]=LU_totale(A);
   [xtotale,flag]=LUtot_solve(Ltotale,Utotale,Ptotale,Qtotale,b);
   toc_tot(n-nmin+1)=toc;
   errore_relativo_totale(n-nmin+1)=norm(xesatta-xtotale)/norm_xesatta;
   %Matlab
   %................................... 
   tic
   xmatlab=A\b;
   toc_mat(n-nmin+1)=toc;
   errore_relativo_matlab(n-nmin+1)=norm(xesatta-xmatlab)/norm_xesatta;  
end
xn=(nmin:nmax)';
figure(1)
semilogy(xn,errore_relativo_nopivot,xn,errore_relativo_parziale,xn,errore_relativo_totale,xn,errore_relativo_matlab)
title('Metodo di Eliminazione di Gauss')
legend('nopivot','parziale','totale','matlab')
xlabel('Dimensione n')
ylabel('Log errore relativo')
figure(2)
curva=xn.^3;
semilogy(xn,toc_nop,xn,toc_par,xn,toc_tot,xn,toc_mat,xn,curva)
title('Tempi di calcolo')
legend('nopivot','parziale','totale','matlab','n^3')
xlabel('Dimensione n')
ylabel('Log tempo di calcolo')
