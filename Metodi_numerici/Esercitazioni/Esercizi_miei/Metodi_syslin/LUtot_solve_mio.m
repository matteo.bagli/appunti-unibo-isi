function [x,flag]=LUtot_solve_mio(L,U,P,Q,b)

[z,flag]=Lsolve(L,P*b);
if flag==0
    [y,flag]=Usolve(U,z);
    x=Q*y;
end