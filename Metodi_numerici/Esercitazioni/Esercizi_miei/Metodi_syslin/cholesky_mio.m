function [R,flag]=cholesky_mio(A)

[n,m]=size(A);
if n~=m
    disp("Non quadra");
    flag=1; R=[];
    return;
end

R=A;

for k=1:n
    if R(k,k)<=0
        disp("Matrice non def pos");
        flag=1;
        return;
    end
    R(k,k)=sqrt(R(k,k));
    R(k,k+1:n)=R(k,k+1:n)./R(k,k);
    R(k+1:n,k+1:n)=R(k+1:n,k+1:n)-R(k,k+1:n)'*R(k,k+1:n);
end

R=triu(R);