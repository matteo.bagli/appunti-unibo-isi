function [L,U,P,Q,flag]=LU_totale(A)
% Fattorizzazione PAQ=LU con pivot totale
% In output:
%  L matrice triangolare inferiore
%  U matrice triangolare superiore
%  P matrice di permutazione sulle righe
%  Q matrice di permutazione sulle colonne
%    tali che  LU=PAQ

% Test dimensione
[n,m]=size(A);
flag=0;
if n ~= m, disp('errore: matrice non quadrata'),  L=[]; U=[]; P=[]; Q=[]; flag=1; return, end

P=eye(n);
Q=eye(n);
for k=1:n-1
    %Scelta pivot totale + scambi su U, P e Q 
    [temp_pivot,temp_ir_pivot]=max(abs(A(k:n,k:n)));
    [pivot,ic_pivot]=max(temp_pivot);
    ir_pivot=temp_ir_pivot(ic_pivot)+k-1;
    ic_pivot=ic_pivot+k-1;
    if pivot == 0, disp('pivot nullo'),  L=[]; flag=1; return, end
    if ir_pivot ~= k
        temp=A(k,:); A(k,:)=A(ir_pivot,:); A(ir_pivot,:)=temp;
        temp=P(k,:); P(k,:)=P(ir_pivot,:); P(ir_pivot,:)=temp; 
    end
    if ic_pivot ~= k
        temp=A(:,k); A(:,k)=A(:,ic_pivot); A(:,ic_pivot)=temp;
        temp=Q(:,k); Q(:,k)=Q(:,ic_pivot); Q(:,ic_pivot)=temp;
    end
    A(k+1:n,k) = A(k+1:n,k)/A(k,k);
    A(k+1:n,k+1:n) =A(k+1:n,k+1:n) - A(k+1:n,k)* A(k,k+1:n);
end

L=tril(A,-1)+eye(n); % Estrae i moltiplicatori
U=triu(A);           % Estrae la parte triangolare superiore+diagonale