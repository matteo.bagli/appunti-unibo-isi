function [x,flag]=LUsolve_mio(L,U,P,b)

[y,flag]=Lsolve(L,P*b);
if flag==0
    [x,flag]=Usolve(U,y);
end
