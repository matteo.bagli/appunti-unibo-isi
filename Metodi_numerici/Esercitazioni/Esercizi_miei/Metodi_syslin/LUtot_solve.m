function [x,flag]=LUtot_solve(L,U,P,Q,b)
 % Risoluzione a partire da PAQ =LU assegnata
 Pb=P*b;
 [z,flag]=Lsolve(L,Pb);
 if flag == 0
    [y,flag]=Usolve(U,z);
    x=Q*y;
 else
    return
 end
