function [x] = resolve_forward(A, b) %%Dpvrebbe esserci la flag a uno se un elem della diag è a 0

n = size(A);
n = n(1);

sum = 0;
i = 1;
j = 1;
while i <= n
    while j <= i - 1
        sum = sum + A(i, j) * x(j);
        j = j + 1;
    end
    x(i) = (b(i) - sum) / A(i, i);
    sum = 0;
    i = i + 1;
    j = 1;
end