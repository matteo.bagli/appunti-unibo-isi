% Calcolo exp(x) con serie di Taylor troncata in accordo a test
% dell'incremento con x campionato in [a,b]
%--------------------------------------------------------------------------
clc
clear all
format long e

% Input
a=-10;               %input('Estremo sinistro intervallo di campionamento=');
b=10;                %input('Estremo destro intervallo di campionamento=');
ncampio=1000;        %input('Numero di campionamenti=');

xc=linspace(a,b,ncampio);  %campionamenti
exp_es=exp(xc); %esponenziale esatta
%--------------------------------------------------------------------------
%Pre-allocazione dei vettori
exp_app=zeros(1,ncampio); 
nt=zeros(1,ncampio);     %indice n della serie

for i=1:ncampio
    [exp_app(i),nt(i)]=esp_taylor_1(xc(i));
end

err_rel=abs(exp_app-exp_es)./abs(exp_es); 
%--------------------------------------------------------------------------
figure(1)
plot(xc,exp_app,'b-',xc,exp_es,'r--')
xlabel('x')
ylabel('exp')
title('Approssimazione esponenziale con serie di Taylor troncata')
legend('exp_{app}','exp_{es}')
%
figure(2)
plot(xc,err_rel)
xlabel('x')
ylabel('err_{rel}')
title('Errore relativo - scala cartesiana')
%
figure(3)
semilogy(xc,err_rel)
xlabel('x')
ylabel('err_{rel}')
title('Errore relativo - scala semilogaritmica')
%
figure(4)
plot(xc,nt)
xlabel('x')
ylabel('indice n')
title('Indice n')

 
%--------------------------------------------------------------------------
%come migliorare andamento errore relativo
%--------------------------------------------------------------------------
for i=1:ncampio 
    if xc(i)>=0
        [exp_app(i),nt(i)]=esp_taylor_1(xc(i));
    else
        [exp_app(i),nt(i)]=esp_taylor_2(xc(i));
    end
end 
err_rel=abs(exp_app-exp_es)./abs(exp_es);  
%--------------------------------------------------------------------------
%
figure(5)
semilogy(xc,err_rel)
xlabel('x')
ylabel('err_{rel}')
title('Errore relativo - scala semilogaritmica')
