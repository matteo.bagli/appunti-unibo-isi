close all
clear all
clc

format long

% F(10,5,L,U) e considero a e b che hanno una rappresentazione esatta in F
% e che verificano che 1/3<=a^2/b^2<=3

a=0.1e1;
b=0.14125e1;

vex=a^2-b^2

% Algoritmo 1
apb=vpa(a+b,5)  %fl(a+b) con mantissa 5 cifre
amb=vpa(a-b,5)  %fl(a-b) con ////
ris1=vpa(2.4125*(-0.4125),5) %-0.99516

err1=abs(-0.99516-vex)/abs(vex)

% Algoritmo 2
a2=vpa(a^2,5)   %fl(a^2) 1
b2=vpa(b^2,5)   %fl(b^2) 1.9952
ris2=vpa(1.0-1.9952,5)  %fl(fl(a^2)-fl(b^2)) = -0.9952

err2=abs(-0.9952-vex)/abs(vex)
