clear all
close all
clc

format long e

A=hilb(4)
b=ones(4, 1)

K=cond(A)

x=A\b

deltaB=0.01*[1; -1; 1; -1;];

% Soluzione sistema perturbato
xp=A\(b+deltaB)

% Errore relativo sui dati
err_dati=norm(deltaB, inf)/norm(b, inf)

err_dati_perc=err_dati*100

% Errore sulla soluzione del sistema
err_sol=norm(xp-x, inf)/norm(x, inf)
err_sol_perc=err_sol*100

% Grafico delle rette rappresentate dalle due equazioni
f1=@(x) (15-5*x)/10;
f2=@(x) (1-2*x);

f1_p=@(x) (15-5*x)/10.1;

xv=linspace(-1,1,100);