clear all
close all
clc

format long e

A=[6 63 662.2; 63 662.2 6967.8; 662.2 6967.8 73393.5664];
b=[1.1 ; 2.33; 1.7];

K=cond(A)

x=A\b

deltaA=0.01*[1 0 0; 0 0 0; 0 0 0];

% Soluzione sistema perturbato
xp=(A+deltaA)\b

% Errore relativo sui dati
err_dati=norm(deltaA, inf)/norm(A, inf)

err_dati_perc=err_dati*100

% Errore sulla soluzione del sistema
err_sol=norm(xp-x, inf)/norm(x, inf)
err_sol_perc=err_sol*100