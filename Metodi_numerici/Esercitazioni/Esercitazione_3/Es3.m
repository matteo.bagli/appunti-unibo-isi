clear all
close all
clc

format long e

A=[3 5; 3.01 5.01];
b=[10 ; 1];

K=cond(A)

x=A\b

deltaA=[0 0; 0.01 0];

% Soluzione sistema perturbato
xp=(A+deltaA)\b

% Errore relativo sui dati
err_dati=norm(deltaA, inf)/norm(A, inf)

err_dati_perc=err_dati*100

% Errore sulla soluzione del sistema
err_sol=norm(xp-x, inf)/norm(x, inf)
err_sol_perc=err_sol*100

% Grafico delle rette rappresentate dalle due equazioni
f1=@(x) (10-3*x)/5;
f2=@(x) (1-3.01*x)/5.01;

f2_p=@(x) (1-3.02*x)/5.01;

figure
xv=linspace(-2255.005,-2254.995,100);

figure(1)
plot(xv,f1(xv),'b-',xv,f2(xv),'r--')

figure(2)
plot(xv,f1(xv),'b-',xv,f2_p(xv),'g--')