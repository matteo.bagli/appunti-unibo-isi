clc
clear all
close all
% Approssimazione derivata con rapporto incrementale
%--------------------------------------------------------------------------
h(1)=1;
for i=2:500
    h(i)=h(i-1)*0.9;
end
%oppure 
% k=[0:-1:-20]';
% h=10.^k;
%--------------------------------------------------------------------------
des=cos(1);            % derivata esatta
x=1;
rai=(sin(x+h)-sin(x))./h; %rapporto incrementale
%......................
err_rel=abs(rai-des)./abs(des); % errore relativo
loglog(h,err_rel,h,h)
xlabel('h')
ylabel('err_{rel}')
title('Approssimazione derivata con rapporto incrementale')
legend('Err_{rel} rapporto incrementale','h')