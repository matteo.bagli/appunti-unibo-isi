clear all
close all
clc

format long e

a = 0.96e-01;
b = 0.99e-1;

s1 = vpa(a + b,2)
c1 = vpa(0.2*0.5,2)

s2 = vpa(b-a,2)
aus = vpa(3.0e-3*0.5,2)
c2 = vpa(a+1.5e-3,2)