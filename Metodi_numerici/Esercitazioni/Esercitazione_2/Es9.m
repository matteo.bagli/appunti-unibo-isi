clear all
close all
clc

k = 1:1:8;
a = 1;
c = 1;
b = 10.^k;

x1 = (-b + sqrt((b.^2) - 4*a*c))./(2*a)
x2 = (-b - sqrt((b.^2) - 4*a*c))./(2*a)

x1new = c ./ (a*x2);

x1vera = -10.^(-k);

err1 = abs(x1 -x1vera)./abs(x1vera);

err1new = abs(x1new-x1vera)./abs(x1vera);

figure 
semilogy(k,err1,'r--',k,err1new,'b--')
legend('Errore formula 1', 'Errore formula x1 new')

