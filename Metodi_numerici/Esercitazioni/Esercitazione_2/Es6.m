clc
clear all
format long e

x=0.1e-15;

s=1;
for i=1:9
    s=s+x;
end

disp('somma di 1+0.1e-15+0.1e-15...+0.2e-15')
s

s1=x;
for i=1:8
    s1=s1+x;
end
s1=s1+1;

disp('somma di 0.1e-15+0.1e-15...+0.2e-15+1')
s1

disp('-----------------------------------------')

sum([1 ones(1,9)*x])

sum([ones(1,9)*x 1])

