a=[1.2 5.4 6 1.5 9];
b=[5.2 pi 1.2 1.5 2];

% Punto a
sqrt(a)
% Punto b
exp(1/2 * a)
% Punto c
c = a + b;
% Punto d
d = a .* b;
% Punto e
t=0:0.5:30
% Punto f
z=linspace(1, 2, 100)
% Punto g
q=[linspace(10, 20, 6); linspace(20, 10, 6)]
% Punto h
j=sum(d)
% oppure j=a*b'
% Punto i
A=[a; b]
% Punto j
cosangolo=j/(norm(a) .* norm(b));
acos(cosangolo)
% Punto k
y=A * b'
% Punto l
diag(a)