function [a,p]=quadGeom(s)
a=area(s);
p=perimeter(s);


function a=area(s)
a=s^2;

function p=perimeter(s)
p=4*s;