function f=funzione_sinusoidale(x)
f=sin(x).*cos(x);
