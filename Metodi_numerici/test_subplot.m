x=linspace(-pi,pi,100)
fp=@funzione_sinusoidale;

figure
subplot(1,2,1)
plot(x,fp(x))
axis tight
axis equal


f=@(x) x.^3-x.^2;

subplot(1,2,2)
plot(x,f(x))
title("funz cubica")
xlabel("x")
ylabel("y")
axis tight
axis equal