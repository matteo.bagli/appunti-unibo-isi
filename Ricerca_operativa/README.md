# Ricerca Operativa

### Come passare l'esame

#### (con Vigo)

In questa folder sono presenti tutte le slide del corso divise per modulo con anche gli esercizi svolti e non.

Consiglio di studiare principalmente gli esercizi in quanto la teoria non sarà presente all'esame.

- Tipologie esercizi d'esame
    - Modulo 1
        - L’algoritmo del simplesso (e il metodo delle 2 fasi per alcuni casi particolari insieme alla regola di bland)
        - Branch and bound
        - Saper disegnare i problemi e le zone ammissibili
        - Esercizi derivati dai precedenti
        - Applicazione di entrambi gli algoritmi
    - Modulo 2
        - Kruskal
        - Prim/Dijkstra
        - Dijkstra tabella dei cammini minimi
        - Floyd Warshall con matrice 2 e 3
        - Flusso massimo sul grafo con procedure di etichettamento
        - NO FLUSSO MINIMO
